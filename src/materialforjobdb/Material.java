package materialforjobdb;

public class Material implements Comparable<Material> {
	private String materialNumber;
	private String barcode;
	private String name;
	private double price;
	private Colli colli;
	private String description;

	public enum Colli {
		STK, KASSE, RULLE, METER;
	}

	public Material() {
	}

	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Material(String materialNumber, String barcode, String name,
			double price, Colli colli, String description) {
		super();
		this.materialNumber = materialNumber;
		this.barcode = barcode;
		this.name = name;
		this.price = price;
		this.setColli(colli.STK);
		this.description = description;
	}

	private void setColli(Colli stk) {		
	}

	@Override
	public int compareTo(Material another) {
		return this.getName().compareTo(another.getName());
	}

	// ---------- get ----------

	public Colli getColli() {
		return colli;
	}

	public String getMaterialNumber() {
		return materialNumber;
	}

	public String getBarcode() {
		return barcode;
	}

	public String getName() {
		return name;
	}

	public double getPrice() {
		return price;
	}

	public String getDescription() {
		return description;
	}
}
