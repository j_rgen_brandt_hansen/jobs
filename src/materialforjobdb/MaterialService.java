package materialforjobdb;

import java.util.Collections;
import java.util.Vector;

import android.content.Context;
import android.database.Cursor;

public class MaterialService {

        public static Vector<Material> getMaterialList(Context context) {
                Vector<Material> materialList = new Vector<Material>();

                Material material;

                DatabaseAdapter databaseAdapter = new DatabaseAdapter(context);
                databaseAdapter.open();

                // Get all sampleData from db
                Cursor cursor = databaseAdapter.getAllBooks();
                cursor.moveToFirst();
               
                if (cursor.getCount() != 0) {
                        materialList = new Vector<Material>();
                        do {
                                material = new Material();
                               
                                material.setName(cursor.getString(cursor.getColumnIndex("FirstName")));
                                material.setBarcode(cursor.getString(cursor
                                                .getColumnIndex("Company")));
                                material.setDescription(cursor.getString(cursor
                                                .getColumnIndex("Address")));
                                material.setMaterialNumber(cursor.getString(cursor.getColumnIndex("RecNo")));
                              //  material.setPrice(cursor.getDouble(cursor.getColumnIndex("Email")));
                                materialList.add(material);
                        } while (cursor.moveToNext());
                }

                // deactivate and closing cursor
                cursor.deactivate();
                cursor.close();

                // Closing db connection
                databaseAdapter.close();

                Collections.sort(materialList);
                return materialList;
        }

}

//import java.util.Collections;
//import java.util.Vector;
//
//import android.content.Context;
//import android.database.Cursor;
//
//public class MaterialService {
//
//        public static Vector<Material> getUserList(Context context) {
//                Vector<Material> bookList = new Vector<Material>();
//
//                Material material;
//
//                DatabaseAdapter databaseAdapter = new DatabaseAdapter(context);
//                databaseAdapter.open();
//
//                // Get all sampleData from db
//                Cursor cursor = databaseAdapter.getAllBooks();
//                cursor.moveToFirst();
//               
//                if (cursor.getCount() != 0) {
//                        bookList = new Vector<Book>();
//                        do {
//                                book = new Book();
//                               
//                                book.setAuthor(cursor.getString(cursor.getColumnIndex("Author")));
//                                book.setCountry(cursor.getString(cursor
//                                                .getColumnIndex("Country")));
//                                book.setLanguage(cursor.getString(cursor
//                                                .getColumnIndex("Language")));
//                                book.setTitle(cursor.getString(cursor.getColumnIndex("Title")));
//                                book.setYear(cursor.getString(cursor.getColumnIndex("Year")));
//                                bookList.add(book);
//                        } while (cursor.moveToNext());
//                }
//
//                // deactivate and closing cursor
//                cursor.deactivate();
//                cursor.close();
//
//                // Closing db connection
//                databaseAdapter.close();
//
//                Collections.sort(bookList);
//                return bookList;
//        }
//
//}

