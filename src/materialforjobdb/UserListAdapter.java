package materialforjobdb;

import java.util.Vector;

import com.skyhost.jobs.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("ResourceAsColor")
public class UserListAdapter extends BaseAdapter {

	private static final String TAG = UserListAdapter.class.getName();
	private Activity activity;
	private Vector<Material> items;

	public UserListAdapter(Activity activity, Vector<Material> items) {
		Log.i(TAG, TAG);
		this.activity = activity;
		this.items = items;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
			convertView = inflater.inflate(
					R.layout.rowlayout_findmaterialforjob, null);
			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(R.id.nameTV);
			holder.headingLL = (LinearLayout) convertView
					.findViewById(R.id.headingLL);
			holder.headingTV = (TextView) convertView
					.findViewById(R.id.headingTV);
			holder.nameLL = (LinearLayout) convertView
					.findViewById(R.id.nameLL);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if (position < items.size()) {
			final Material material = items.get(position);
			if (material != null && (material.getName().length() == 1)) {
				holder.nameLL.setVisibility(View.GONE);
				holder.headingLL.setVisibility(View.VISIBLE);
				holder.headingTV.setText(material.getName());
				holder.headingLL
						.setBackgroundColor(android.R.color.transparent);
			} else {
				holder.nameLL.setVisibility(View.VISIBLE);
				holder.headingLL.setVisibility(View.GONE);
				holder.name.setText(material.getName());
				View ll = (LinearLayout) holder.name.getParent();
				ll.setFocusable(true);
				ll.setSelected(true);
				ll.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Toast.makeText(activity.getApplicationContext(),
								"Clicked on " + material.getName(),
								Toast.LENGTH_SHORT).show();
						Intent intent = new Intent(activity,
								SelectedActivity.class);
						intent.putExtra("SELECTED_ROW", material.getName());
						activity.startActivity(intent);
					}
				});
			}
		}
		return convertView;
	}

	private static class ViewHolder {
		TextView name, headingTV;
		LinearLayout nameLL, headingLL;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
}
