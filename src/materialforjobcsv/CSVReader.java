package materialforjobcsv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import materialforjobcsv.Material.Colli;

import android.net.Uri;
import android.os.Environment;

public class CSVReader {

	BufferedReader br = null;
	String line = "";
	String csvSplitBy = ",";
	static Vector<Material> materials = new Vector<Material>();

	public CSVReader() {
		run();
	}

	public void run() {
		for (int i = 0; i < 1; i++) {
			Material m1 = new Material("101", "3351687", "Muffe", 12.00,
					Colli.KASSE, "Louis Poulsen muffer");
			Material m2 = new Material("102", "3351679", "Samlemuffe", 16.77,
					Colli.KASSE, "Solar");
			Material m3 = new Material("103", "3351680", "B�jning", 92.63,
					Colli.KASSE, "Solar b�jning");
			Material m4 = new Material("201", "3351681", "Ledning", 67.81,
					Colli.RULLE, "S�lges kun i l�bende meter");
			Material m5 = new Material("202", "3351682", "Kronmuffe", 78.07,
					Colli.KASSE, "12 pr kasse");
			Material m6 = new Material("301", "3351683", "GaffaTape", 16.79,
					Colli.KASSE, "L�rlingetape");
			Material m7 = new Material("401", "3351684", "Forskruning", 16.80,
					Colli.STK, "Passer kun til Lemvigh Muller");
			Material m8 = new Material("501", "3351685", "Fordeler", 84.27,
					Colli.STK, "Kinafordeler");

			materials.add(m1);
			materials.add(m2);
			materials.add(m3);
			materials.add(m4);
			materials.add(m5);
			materials.add(m6);
			materials.add(m7);
			materials.add(m8);
		}
		// File mFile = Environment.getExternalStorageDirectory();
		// File file = new File(mFile, "testdata.csv");
		// Uri mOutputFileUri = Uri.fromFile(file);
		//
		// String csvFile = mOutputFileUri.toString();
		//
		// try {
		// br = new BufferedReader(new FileReader(csvFile));
		// while ((line = br.readLine()) != null) {
		//
		// // use comma as separator
		// String[] material = material = line.split(csvSplitBy);
		//
		// materials.add(material);
		// }
		//
		// } catch (FileNotFoundException e) {
		// e.printStackTrace();
		// } catch (IOException e) {
		// e.printStackTrace();
		// } finally {
		// if (br != null) {
		// try {
		// br.close();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// }
		// }
	}

	public static Vector getMaterials() {
		return new Vector(materials);
	}
}

//
// /**
//
// Copyright 2005 Bytecode Pty Ltd.
//
//
//
// Licensed under the Apache License, Version 2.0 (the "License");
//
// you may not use this file except in compliance with the License.
//
// You may obtain a copy of the License at
//
//
//
// http://www.apache.org/licenses/LICENSE-2.0
//
//
//
// Unless required by applicable law or agreed to in writing, software
//
// distributed under the License is distributed on an "AS IS" BASIS,
//
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//
// See the License for the specific language governing permissions and
//
// limitations under the License.
//
// */
//
//
//
// /*
//
// * The code copied from http://opencsv.sourceforge.net/
//
// *
//
// * While incorporating into secrets, the following changes were made:
//
// *
//
// * - Added support of generics
//
// * - removed the following methods to keep the bytecode smaller:
//
// * readAll(), some constructors
//
// */
// import java.io.BufferedReader;
// import java.io.IOException;
// import java.io.Reader;
// import java.util.ArrayList;
// import java.util.List;
//
// /**
// * A very simple CSV reader released under a commercial-friendly license.
// *
// * @author Glen Smith
// *
// */
// public class CSVReader {
//
// private BufferedReader br;
// private boolean hasNext = true;
// private char separator;
// private char quotechar;
// private int skipLines;
// private boolean linesSkiped;
//
// /** The default separator to use if none is supplied to the constructor. */
//
// public static final char DEFAULT_SEPARATOR = ',';
//
// /**
// * The default quote character to use if none is supplied to the
// * constructor.
// */
// public static final char DEFAULT_QUOTE_CHARACTER = '"';
//
// /**
// * The default line to start reading.
// */
// public static final int DEFAULT_SKIP_LINES = 0;
//
// /**
// * Constructs CSVReader using a comma for the separator.
// *
// * @param reader
// * the reader to an underlying CSV source.
// */
// public CSVReader(Reader reader) {
// this(reader, DEFAULT_SEPARATOR, DEFAULT_QUOTE_CHARACTER,
// DEFAULT_SKIP_LINES);
// }
//
// /**
// * Constructs CSVReader with supplied separator and quote char.
// *
// * @param reader
// * the reader to an underlying CSV source.
// * @param separator
// * the delimiter to use for separating entries
// * @param quotechar
// * the character to use for quoted elements
// * @param line
// * the line number to skip for start reading
// */
// public CSVReader(Reader reader, char separator, char quotechar, int line) {
// this.br = new BufferedReader(reader);
// this.separator = separator;
// this.quotechar = quotechar;
// this.skipLines = line;
// }
//
// /**
// * Reads the next line from the buffer and converts to a string array.
// *
// * @return a string array with each comma-separated element as a separate
// * entry.
// *
// * @throws IOException
// * if bad things happen during the read
// */
// public String[] readNext() throws IOException {
// String nextLine = getNextLine();
// return hasNext ? parseLine(nextLine) : null;
// }
//
// /**
// * Reads the next line from the file.
// *
// * @return the next line from the file without trailing newline
// * @throws IOException
// * if bad things happen during the read
// */
// private String getNextLine() throws IOException {
// if (!this.linesSkiped) {
// for (int i = 0; i < skipLines; i++) {
// br.readLine();
// }
// this.linesSkiped = true;
// }
//
// String nextLine = br.readLine();
//
// if (nextLine == null) {
// hasNext = false;
// }
// return hasNext ? nextLine : null;
// }
//
// /**
// * Parses an incoming String and returns an array of elements.
// *
// * @param nextLine
// * the string to parse
// * @return the comma-tokenized list of elements, or null if nextLine is null
// * @throws IOException if bad things happen during the read
// */
// private String[] parseLine(String nextLine) throws IOException {
//
// if (nextLine == null) {
// return null;
// }
//
// List<String> tokensOnThisLine = new ArrayList<String>();
// StringBuffer sb = new StringBuffer();
// boolean inQuotes = false;
// do {
// if (inQuotes) {
// // continuing a quoted section, reappend newline
// sb.append("\n");
// nextLine = getNextLine();
// if (nextLine == null)
// break;
// }
//
// for (int i = 0; i < nextLine.length(); i++) {
// char c = nextLine.charAt(i);
//
// if (c == quotechar) {
// // this gets complex... the quote may end a quoted block, or escape another
// quote.
// // do a 1-char lookahead:
// if( inQuotes // we are in quotes, therefore there can be escaped quotes in
// here.
// && nextLine.length() > (i+1) // there is indeed another character to check.
// && nextLine.charAt(i+1) == quotechar ){ // ..and that char. is a quote also.
// // we have two quote chars in a row == one quote char, so consume them both
// and
// // put one on the token. we do *not* exit the quoted text.
// sb.append(nextLine.charAt(i+1));
// i++;
// }else{
// inQuotes = !inQuotes;
// // the tricky case of an embedded quote in the middle: a,bc"d"ef,g
// if(i>2 //not on the begining of the line
// && nextLine.charAt(i-1) != this.separator //not at the begining of an escape
// sequence
// && nextLine.length()>(i+1) &&
// nextLine.charAt(i+1) != this.separator //not at the end of an escape sequence
// ){
// sb.append(c);
// }
// }
// } else if (c == separator && !inQuotes) {
// tokensOnThisLine.add(sb.toString());
// sb = new StringBuffer(); // start work on next token
// } else {
// sb.append(c);
// }
// }
// } while (inQuotes);
// tokensOnThisLine.add(sb.toString());
// return (String[]) tokensOnThisLine.toArray(new String[0]);
// }
//
// /**
// * Closes the underlying reader.
// *
// * @throws IOException if the close fails
// */
// public void close() throws IOException{
// br.close();
// }
// }