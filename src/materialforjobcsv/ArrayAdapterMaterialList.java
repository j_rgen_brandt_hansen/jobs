package materialforjobcsv;

import java.util.Vector;

import com.skyhost.jobs.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("ResourceAsColor")
public class ArrayAdapterMaterialList extends BaseAdapter {

	private static final String TAG = ArrayAdapterMaterialList.class.getName();
	private Activity activity;
	private Vector<Material> materials;

	public ArrayAdapterMaterialList(Activity activity, Vector<Material> materials) {
		Log.i(TAG, TAG);
		this.activity = activity;
		this.materials = materials;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
			convertView = inflater.inflate(
					R.layout.rowlayout_findmaterialforjob, null);
			holder = new ViewHolder();
//			holder.name = (TextView) convertView.findViewById(R.id.name);
//			holder.materialNumber = (LinearLayout) convertView
//					.findViewById(R.id.materialnumber);
//			holder.colli = (TextView) convertView
//					.findViewById(R.id.colli);
//			holder.barcode = (LinearLayout) convertView
//					.findViewById(R.id. barcode);
//			holder.price = (LinearLayout) convertView
//					.findViewById(R.id.price);
//			holder.description = (LinearLayout) convertView
//					.findViewById(R.id.description);
//			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
				holder.name.setText(materials.get(position).getName());
				holder.materialNumber.setText(materials.get(position).getMaterialNumber());
				holder.barcode.setText(materials.get(position).getBarcode());
				holder.colli.setText(materials.get(position).getColli().toString());
				holder.description.setText(materials.get(position).getDescription());
				holder.price.setText(String.valueOf( materials.get(position).getPrice()));
		
		

		return convertView;
	}

	class ViewHolder {
		TextView materialNumber, barcode, name, price, description, colli;
		}

	@Override
	public int getCount() {
		return 0;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	
}
