package com.skyhost.jobs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.DatePicker.OnDateChangedListener;

public class DateTimePicker4Job extends Activity implements OnClickListener,
		OnDateChangedListener {

	private android.os.PowerManager powerManager;
	private PowerManager.WakeLock wakeLock;
	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.datetimepicker);

		new PauseDialogLoader().execute();

		// Initializing
		powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		Button buttonOk = (Button) findViewById(R.id.datetimepicker_ok_button);
		buttonOk.setOnClickListener(this);

		Button buttonReset = (Button) findViewById(R.id.datetimepicker_reset_button);
		buttonReset.setOnClickListener(this);

		TimePicker timePicker = (TimePicker) findViewById(R.id.timepicker);
		timePicker.setIs24HourView(true);
	}

	@Override
	public void onPause() {
		super.onPause();

		if (progressDialog != null) {
			progressDialog.cancel();
		}

		setPowerSaveState(true);
	}

	@Override
	public void onResume() {
		super.onResume();

		setPowerSaveState(false);
		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}
	
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(DateTimePicker4Job.this, Job.class);
		startActivity(intent);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.datetimepicker_ok_button) {
			DatePicker datePicker = (DatePicker) findViewById(R.id.datepicker);
			TimePicker timePicker = (TimePicker) findViewById(R.id.timepicker);

			// Make sure hour, minute, day and month always consists of 2 digits
			// Hour
			int tempHour = timePicker.getCurrentHour();
			String selectedHour = "";

			if (tempHour <= 9) {
				selectedHour = "0" + tempHour;
			} else {
				selectedHour = "" + tempHour;
			}

			// Minute
			int tempMinute = timePicker.getCurrentMinute();
			String selectedMinute = "";

			if (tempMinute <= 9) {
				selectedMinute = "0" + tempMinute;
			} else {
				selectedMinute = "" + tempMinute;
			}

			// Day
			int tempDay = datePicker.getDayOfMonth();
			String selectedDay = "";

			if (tempDay <= 9) {
				selectedDay = "0" + tempDay;
			} else {
				selectedDay = "" + tempDay;
			}

			// Month
			int tempMonth = datePicker.getMonth();

			// Month calculation is 0 index based
			tempMonth += 1;

			String selectedMonth = "";

			if (tempMonth <= 9) {
				selectedMonth = "0" + tempMonth;
			} else {
				selectedMonth = "" + tempMonth;
			}

			String selectedDate = datePicker.getYear() + selectedMonth
					+ selectedDay + selectedHour + selectedMinute;
			saveSetting("selectedDate", selectedDate);
			saveSetting("reloadJobs", true);
			new AsyncSetJobTime().execute();

			if (loadSettingBoolean("begin") == true) {
				Intent intent = new Intent(DateTimePicker4Job.this, Job.class);
				startActivity(intent);
			} else {
				Intent intent = new Intent(DateTimePicker4Job.this, Jobs.class);
				startActivity(intent);
			}
		} else if (v.getId() == R.id.datetimepicker_reset_button) {
			Intent intent = new Intent(DateTimePicker4Job.this, DateTimePicker4Job.class);
			startActivity(intent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.notes, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			Intent jobs = new Intent(DateTimePicker4Job.this, Jobs.class);
			startActivity(jobs);
			return true;

		case R.id.activity_job: // Job
			Intent job = new Intent(DateTimePicker4Job.this, Job.class);
			startActivity(job);
			return true;

		case R.id.activity_hellogooglemaps: // Map
			saveSetting("sCommand", "allJobs");
			// saveSetting("allJobs", jsonArray.toString());
			Intent map = new Intent(DateTimePicker4Job.this,
					HelloGoogleMaps.class);
			startActivity(map);
			return true;

		case R.id.activity_newnote: // Add Note
			Intent newNote = new Intent(DateTimePicker4Job.this, NewNote.class);
			startActivity(newNote);
			return true;
		}
		return true;
	}

	public void setPowerSaveState(boolean enabled) {

		if (enabled == false) {
			wakeLock = powerManager.newWakeLock(
					PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "My Tag");
			wakeLock.acquire();
		} else {
			if (wakeLock != null) {
				wakeLock.release();
			}
		}
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private Boolean loadSettingBoolean(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		Boolean content = preferences.getBoolean(tag, false);
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private void saveSetting(String tag, Boolean content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(tag, content);
		editor.commit();
	}

	// ---------- Asynkrone Hj�lpemetoder ----------

	private class AsyncSetJobTime extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			String jobId = "";
			String begin = "";
			String end = "";

			if (!loadSetting("selectedJobId").equals("")) {
				jobId = loadSetting("selectedJobId");
			}

			if (loadSettingBoolean("begin") == true) {
				// Begin time is changed
				if (!loadSetting("selectedDate").equals("")) {
					begin = loadSetting("selectedDate");

					int selectedJobIndex = -1;
					if (!(loadSetting("selectedJobIndex").equals(""))) {
						selectedJobIndex = Integer
								.parseInt(loadSetting("selectedJobIndex"));
					} else {
						showToast("Der skete en fejl datoen blev ikke �ndret");

					}

					String tempEnd = WebApi.getWebApi().getJobs_endTime()[selectedJobIndex];

					end = tempEnd.replaceAll("[-: ]+", "");
				} else {
					showToast("Der skete en fejl datoen blev ikke �ndret");
					return null;
				}
			} else {
				// End time is changed redirect to Jobs
				if (!loadSetting("selectedDate").equals("")) {

					int selectedJobIndex = -1;
					if (!(loadSetting("selectedJobIndex").equals(""))) {
						selectedJobIndex = Integer
								.parseInt(loadSetting("selectedJobIndex"));
						String tempBegin = WebApi.getWebApi()
								.getJobs_beginTime()[selectedJobIndex];

						begin = tempBegin.replaceAll("[-: ]+", "");

						end = loadSetting("selectedDate");
					} else {
						showToast("Der skete en fejl datoen blev ikke �ndret");
					}
				} else {
					showToast("Der skete en fejl datoen blev ikke �ndret");

				}
			}

			WebApi.getWebApi().setJobTime(preferences, jobId, begin, end);
			saveSetting("begin", false);

			// Error sending
			if (WebApi.getWebApi().isNewData_Jobs_SetJobTime() == false) {
				showToast("Fejlkode: "
						+ WebApi.getWebApi().getResponse_ErrorCode(), WebApi
						.getWebApi().getResponse_ErrorText()
						+ " Pr�v igen senere!");

				Intent status = new Intent(DateTimePicker4Job.this, Job.class);
				startActivity(status);
			}
			// Success sending
			else if (WebApi.getWebApi().isNewData_Jobs_SetJobTime() == true) {
				String errorHeader = WebApi.getWebApi()
						.getResponse_ErrorHeader();
				// String test =
				// WebApi.getWebApi().getResponse_Status().toString();
				// Success answer
				if (errorHeader.equals("OK")) {
					showToast("Din registrering er gemt");
					saveSetting("reloadJobs", true);
					// Intent intent = new Intent(DateTimePicker4Job.this,
					// Job.class);
					// startActivity(intent);
				}
				// Error in answer
				else if (WebApi.getWebApi().getResponse_Status()
						.equals("Error")) {
					if (!WebApi.getWebApi().getResponse_ErrorText().equals("")) {
						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");

						Intent intent = new Intent(DateTimePicker4Job.this,
								Job.class);
						startActivity(intent);
					} else {
						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");

						Intent intent = new Intent(DateTimePicker4Job.this,
								Job.class);
						startActivity(intent);
					}
				}

			} else {
				Intent intent = new Intent(DateTimePicker4Job.this, Job.class);
				startActivity(intent);
			}

			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);

			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

		protected void showToast(final String text) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), text,
							Toast.LENGTH_LONG).show();
				}
			});
		}

		protected void showToast(final String header, final String text) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							header + " " + text, Toast.LENGTH_LONG).show();
				}
			});
		}
	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(DateTimePicker4Job.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	@Override
	public void onDateChanged(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
	}
}