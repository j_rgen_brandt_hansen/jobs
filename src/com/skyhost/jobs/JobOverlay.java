package com.skyhost.jobs;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class JobOverlay extends OverlayItem{
	private String index;
	
	public JobOverlay(GeoPoint point, String title, String snippet, String index) {
		super(point, title, snippet);
		this.index = index;
	}

	public String getIndex() {
		return index;
	}
}
