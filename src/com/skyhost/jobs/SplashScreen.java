package com.skyhost.jobs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

public class SplashScreen extends Activity {

	private Handler mHandler;
	private static String response_ErrorText;

	private Runnable runUpdateTimeTask = new Runnable() {
		public void run() {

			new AsyncLoader().execute();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen);

		// Initialize
		response_ErrorText = "";
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mHandler != null) {
			mHandler.removeCallbacks(runUpdateTimeTask);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (loadSetting("Username").equals("")
				|| loadSetting("Password").equals("")) {
			Intent intent = new Intent(SplashScreen.this, Settings.class);
			SplashScreen.this.startActivity(intent);
			SplashScreen.this.finish();
		}

		mHandler = new Handler();
		mHandler.post(runUpdateTimeTask);
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private class AsyncLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			WebApi.getWebApi().Update_Jobs(preferences);
			try {
				WebApi.getWebApi().storeImages();
			} catch (Exception ex) {
			}

			boolean b = WebApi.getWebApi().isNewData_Jobs();
			if (WebApi.getWebApi().isNewData_Jobs()) {
				if (loadSetting("Username").equals("")
						|| loadSetting("Password").equals("")) {
					response_ErrorText = WebApi.getWebApi()
							.getResponse_ErrorText();
					Intent intent = new Intent(SplashScreen.this,
							Settings.class);
					SplashScreen.this.startActivity(intent);
					SplashScreen.this.finish();
				}

				Message message = new Message();
				Intent mainIntent = new Intent(SplashScreen.this, Jobs.class);
				SplashScreen.this.startActivity(mainIntent);
				SplashScreen.this.finish();
			} else {
				String errorCode = WebApi.getWebApi().getResponse_ErrorCode();
				if (errorCode == null || errorCode.equals("")) {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {

							// Muligvis Timeout
							Toast.makeText(SplashScreen.this,
									"Ingen forbindelse, pr�ver igen.",
									Toast.LENGTH_LONG).show();
						}
					});

					mHandler.postDelayed(runUpdateTimeTask, 3000);
				} else if (errorCode.equals("3")) {
					response_ErrorText = WebApi.getWebApi()
							.getResponse_ErrorText();
					Intent intent = new Intent(SplashScreen.this,
							Settings.class);
					SplashScreen.this.startActivity(intent);
					SplashScreen.this.finish();
				} else {
					mHandler.postDelayed(runUpdateTimeTask, 5000);
				}
			}
			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	public static String getResponse_ErrorText() {
		return response_ErrorText;
	}

	public static void setResponse_ErrorText(String response_ErrorText) {
		SplashScreen.response_ErrorText = response_ErrorText;
	}
}