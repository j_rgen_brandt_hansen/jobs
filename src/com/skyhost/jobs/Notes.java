package com.skyhost.jobs;

import java.text.DecimalFormat;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;

public class Notes extends ListActivity implements OnClickListener {

	private Handler handler;

	private ProgressDialog progressDialog;

	private String[] jobs_Notes_Names;
	private String[] jobs_Notes_Texts;
	private String[] jobs_Notes_Timestamps;

	private Runnable runUpdateTask = new Runnable() {
		public void run() {

			new AsyncLoader().execute();
		}
	};

	// private Runnable runUpdateJobsTask = new Runnable() {
	// public void run() {
	//
	// new AsyncUpdateJobs().execute();
	// }
	// };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notes);

		new PauseDialogLoader().execute();

		// Initializing
		ListView listView = getListView();
		WebApi.getWebApi().setNewData_Jobs(false);
		handler = new Handler();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (handler != null) {
			handler.removeCallbacks(runUpdateTask);
		}

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		if (handler != null) {
			handler.post(runUpdateTask);
		}
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(Notes.this, Job.class);
		Notes.this.startActivity(intent);
	}

	private void updateScreen() {

		int selectedJobIndex = -1;

		if (!(loadSetting("selectedJobIndex").equals(""))) {
			selectedJobIndex = Integer
					.parseInt(loadSetting("selectedJobIndex"));
		}

		final ArrayAdapterNotes adapter = new ArrayAdapterNotes(this, WebApi
				.getWebApi().getJobs_notes_names()[selectedJobIndex], WebApi
				.getWebApi().getJobs_notes_texts()[selectedJobIndex], WebApi
				.getWebApi().getJobs_notes_timestamps()[selectedJobIndex]);

		try {
			setListAdapter(adapter);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		if (progressDialog != null) {
			progressDialog.hide();
		}
	}

	@Override
	public void onClick(View v) {
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.notes, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			Intent jobs = new Intent(Notes.this, Jobs.class);
			startActivity(jobs);
			return true;

		case R.id.activity_job: // Job
			Intent job = new Intent(Notes.this, Job.class);
			startActivity(job);
			return true;

		case R.id.activity_hellogooglemaps: // Map
			saveSetting("sCommand", "allJobs");
			// saveSetting("allJobs", jsonArray.toString());
			Intent map = new Intent(Notes.this, HelloGoogleMaps.class);
			startActivity(map);
			return true;

		case R.id.activity_newnote: // Add Note
			Intent newNote = new Intent(Notes.this, NewNote.class);
			startActivity(newNote);
			return true;

		case R.id.activity_notes: // Notes
			Intent notes = new Intent(Notes.this, Notes.class);
			startActivity(notes);
			return true;

		case R.id.activity_status: // Status
			Intent status = new Intent(Notes.this, StatusSkyhost.class);
			startActivity(status);
			return true;
			//
		case R.id.activity_settings: // Settings
			Intent settings = new Intent(Notes.this, Settings.class);
			startActivity(settings);
			return true;
		}
		return true;
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	// ---------- Asynkrone Hjælpemetoder ----------

	private class AsyncLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			WebApi.getWebApi().Update_Jobs(preferences);

			if (WebApi.getWebApi().isNewData_Jobs() == true) {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						updateScreen();
					}
				});
				handler.postDelayed(runUpdateTask, 30000);
			} else {
				handler.postDelayed(runUpdateTask, 5000);
			}
			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);

			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	// private class AsyncUpdateJobs extends AsyncTask<Void, Void, Void> {
	// protected void onPreExecute() {
	// super.onPreExecute();
	// }
	//
	// protected Void doInBackground(Void... params) {
	// // Initialize
	// SharedPreferences preferences = getSharedPreferences("PREFERENCES",
	// Activity.MODE_PRIVATE);
	//
	// // First set boolean false - true will be from most recent update
	// WebApi.getWebApi().setNewData_Jobs(false);
	//
	// WebApi.getWebApi().Update_Jobs(preferences);
	//
	// boolean isNewData_Jobs = WebApi.getWebApi().isNewData_Jobs();
	//
	// if (isNewData_Jobs == true) {
	// runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// updateScreen();
	// }
	// });
	// updateJobsHandler.postDelayed(runUpdateJobsTask, 30000);
	// } else {
	// updateJobsHandler.postDelayed(runUpdateJobsTask, 5000);
	// }
	// return null;
	// }
	//
	// protected void onPostExecute(Void number) {
	// super.onPostExecute(number);
	//
	// }
	// }

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(Notes.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}
}