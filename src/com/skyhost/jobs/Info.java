package com.skyhost.jobs;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class Info extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.info);

		String version = System.getProperty("os.version"); // OS version
		String model = android.os.Build.MODEL;
		String manufacturer = android.os.Build.MANUFACTURER;
		PackageInfo packageInfo;
		String sAppVersion = "";

		try {
			packageInfo = getPackageManager().getPackageInfo(getPackageName(),
					0);
			sAppVersion = packageInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		if (version != null && model != null && manufacturer != null) {
			TextView textViewInfo = (TextView) findViewById(R.id.textview_info_version);
			textViewInfo.setText(Html.fromHtml("Producent: "
					+ manufacturer.substring(0, 1).toUpperCase()
					+ manufacturer.substring(1) + "<br>" + "Model: " + model
					+ "<br>" + "Version: " + version + "<br>" + "AppVersion: "
					+ sAppVersion));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent intent = null;

		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			intent = new Intent(Info.this, Jobs.class);
			break;
		}
		startActivity(intent);
		return true;
	}
}