package com.skyhost.jobs;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

public class FindMaterialForJob extends ListActivity implements
		OnClickListener, OnItemSelectedListener {

	private static String JARGON;
	private ArrayAdapter statusAdapter;
	private Context context;
	private int spinnerPosition;
	private int statusSpinnerPosition;
	private ListView listView;
//	private android.os.PowerManager powerManager;
//	private PowerManager.WakeLock wakeLock;
	private ProgressDialog pd;
	private SharedPreferences sharedPreferences;
	private Spinner statusSpinner;
	private String selectedJobId;
	private String selectedJobIndex;
	private String spinnerString;
	private String[] status_Spinner_Array;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.findmaterialforjob);

//		powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);

		new PauseDialogLoader().execute();

		context = getApplicationContext();

		// Get the intent, verify the action and get the query
		Intent intent = getIntent();
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);
			doMySearch(query);
		}

		// listView = (ListView) findViewById(R.id.list);
		listView = getListView();

		String[] values = new String[] { "Hammer", "S�m", "Forskalling 22mm",
				"Sm�lfespark" };

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,

		android.R.layout.simple_list_item_1, R.id.textview_search_dialog,
				values);

		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				int itemPosition = position;
				String itemValue = (String) listView
						.getItemAtPosition(position);
			}
		});

		// statusSpinner.setOnItemSelectedListener(new OnItemSelectedListener()
		// {
		// @Override
		// public void onItemSelected(AdapterView<?> parentView,
		// View selectedItemView, int position, long id) {
		// if (position == 0) {
		// spinnerString = "assigned";
		// } else if (position == 1) {
		// spinnerString = "started";
		// } else if (position == 2) {
		// spinnerString = "waiting";
		// } else if (position == 3) {
		// spinnerString = "finished";
		// }
		// statusSpinnerPosition = position;
		// }
		//
		// @Override
		// public void onNothingSelected(AdapterView<?> arg0) {
		// }
		// });
		selectedJobIndex = null;
		if (!(loadSettingSP("selectedJobIndex").equals(""))) {
			selectedJobIndex = loadSettingSP("selectedJobIndex");
		}
		// if (!WebApi.getWebApi().getJobs_status().equals("")) {
		// spinnerString = WebApi.getWebApi().getJobs_status()[Integer
		// .parseInt(selectedJobIndex)];
		// if (spinnerString.equals("assigned")) {
		// spinnerPosition = 0;
		// } else if (spinnerString.equals("started")) {
		// spinnerPosition = 1;
		// } else if (spinnerString.equals("waiting")) {
		// spinnerPosition = 2;
		// } else if (spinnerString.equals("finished")) {
		// spinnerPosition = 3;
		// }
		// }

		// ArrayAdapter positionAdapter = (ArrayAdapter) statusSpinner
		// .getAdapter(); // cast to an ArrayAdapter
		// statusSpinner.setSelection(spinnerPosition);
		pd.dismiss();
	}

	@Override
	public void onPause() {
		super.onPause();

//		setPowerSaveState(true);
	}

	@Override
	public void onResume() {
		super.onResume();

//		setPowerSaveState(false);
	}

	private void doMySearch(String query) {

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.addmaterialtojob, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			Intent jobs = new Intent(FindMaterialForJob.this, Jobs.class);
			startActivity(jobs);
			return true;
		}
		return true;
	}

	@Override
	public boolean onSearchRequested() {
		Bundle appData = new Bundle();
		appData.putBoolean(FindMaterialForJob.JARGON, true);
		startSearch(null, false, appData, false);
		return super.onSearchRequested();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		handleIntent(intent);
	}

	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);
			// doMySearch(query);
		}
	}

	// private void setListAdapter() {
	// this.setListAdapter(new ArrayAdapterSearchDialog(this, WebApi.getWebApi()
	// .getJobs_title(), WebApi.getWebApi().getJobs_priority(), WebApi
	// .getWebApi().getJobs_linked(), WebApi.getWebApi()
	// .getJobs_timeRegMinutes(), WebApi.getWebApi()
	// .getJobs_beginTime(),
	// WebApi.getWebApi().getJobs_files_titles(), WebApi.getWebApi()
	// .getJobs_drawables(), WebApi.getWebApi()
	// .getJobs_endTime(), WebApi.getWebApi()
	// .getJobs_duration(), WebApi.getWebApi()
	// .getJobs_address_street(), WebApi.getWebApi()
	// .getJobs_address_zip()));
	// }

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.add_material_spinner_button:
			String item = statusSpinner.getSelectedItem().toString();
			statusSpinner.setSelection(statusSpinnerPosition);
			SharedPreferences preferences = getSharedPreferences(
					Jobs.PREFERENCES, Activity.MODE_PRIVATE);
			selectedJobId = null;
			if (!(loadSettingSP("selectedJobId").equals(""))) {
				selectedJobId = loadSettingSP("selectedJobId");
				WebApi.getWebApi().changeStatus(preferences, selectedJobId,
						spinnerString);
			}
			if (WebApi.getWebApi().isNewData_Status() == false) {
				Toast.makeText(
						FindMaterialForJob.this,
						"Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode()
								+ " : "
								+ WebApi.getWebApi().getResponse_ErrorText()
								+ " Pr�v igen senere!", 3000).show();
				Intent status = new Intent(FindMaterialForJob.this,
						Jobs.class);
				startActivity(status);
			} else if (WebApi.getWebApi().isNewData_Response() == false) {
				if (WebApi.getWebApi().getResponse_Status() != null) {
					if (!WebApi.getWebApi().getResponse_ErrorText().equals("")) {
						Toast.makeText(
								FindMaterialForJob.this,
								"Fejlkode: "
										+ WebApi.getWebApi()
												.getResponse_ErrorCode()
										+ " : "
										+ WebApi.getWebApi()
												.getResponse_ErrorText()
										+ " Pr�v igen senere!", 3000).show();
						Intent intent = new Intent(FindMaterialForJob.this,
								Jobs.class);
						startActivity(intent);
					} else {
						Toast.makeText(
								FindMaterialForJob.this,
								"Fejlkode: "
										+ WebApi.getWebApi()
												.getResponse_ErrorCode()
										+ " : "
										+ WebApi.getWebApi()
												.getResponse_ErrorText()
										+ " Pr�v igen senere!", 3000).show();
						Intent intent = new Intent(FindMaterialForJob.this,
								Jobs.class);
						startActivity(intent);
					}
				}
			} else if (WebApi.getWebApi().isNewData_Response() == true) {
				String tmp = WebApi.getWebApi().getResponse_Status();
				if (tmp.equals("OK")) {
					Toast.makeText(FindMaterialForJob.this,
							"Status er �ndret", 3000).show();
					Intent status = new Intent(FindMaterialForJob.this,
							SplashScreen.class);
					startActivity(status);
				}
			} else {
				Intent intent = new Intent(FindMaterialForJob.this,
						Jobs.class);
				startActivity(intent);
			}
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

//	public void setPowerSaveState(boolean enabled) {
//
//		if (enabled == false) {
//			wakeLock = powerManager.newWakeLock(
//					PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "My Tag");
//			wakeLock.acquire();
//		} else {
//			if (wakeLock != null) {
//				wakeLock.release();
//			}
//		}
//	}

	private String loadSettingSP(String tag) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		String content = sharedPreferences.getString(tag, "");
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences(Jobs.PREFERENCES,
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			pd = new ProgressDialog(FindMaterialForJob.this,
					R.style.SkyhostProgressDialog);
			pd.setTitle("Vent venligst...");
			pd.setMessage("Data hentes...");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

			// Set Runnable to remove splash screen just in case
			final Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					pd.dismiss();
				}
			}, 5000);
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			// pd.dismiss();
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);

			pd.dismiss();
		}

	}
}
