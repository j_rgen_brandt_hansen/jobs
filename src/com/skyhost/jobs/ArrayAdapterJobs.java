package com.skyhost.jobs;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import android.R.color;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ArrayAdapterJobs extends ArrayAdapter<String> {

	private Map<Integer, String> selected;

	private Activity context;
	private String[] jobs_title; // ikke null
	private String[] jobs_priority;
	private String[] jobs_linked;// ikke null
	private String[] jobs_timeRegMinutes;
	private String[][] jobs_files;
	//private String[] jobs_icon;
	private Drawable[] jobs_drawables;
	private String[] jobs_endTime;
	private String[] jobs_duration;

	private String[][] jobs_address_street;
	public HashMap<Integer, String> mapAfter;

	int previousPosition = -1;

	private Date previousDate = new Date(2000, 1, 1);

	public ArrayAdapterJobs(Activity context, String[] jobs_title,
			String[] jobs_priority, String[] jobs_linked,
			String[] jobs_timeRegMinutes, String[] jobs_createdAt,
			String[][] jobs_files, Drawable[] jobs_drawables,
			String[] jobs_endTime, String[] jobs_duration,
			String[][] jobs_address_street, String[][] jobs_address_zip) {
		super(context, R.layout.rowlayout_jobs, jobs_title);
		this.context = context;
		this.jobs_title = jobs_title;
		this.jobs_priority = jobs_priority;
		this.jobs_linked = jobs_linked;
		this.jobs_timeRegMinutes = jobs_timeRegMinutes;
		this.jobs_files = jobs_files;
		//this.jobs_icon = jobs_icon;
		this.jobs_endTime = jobs_endTime;
		this.jobs_duration = jobs_duration;
		this.jobs_address_street = jobs_address_street;
		this.jobs_drawables = jobs_drawables;

//		selected = new HashMap<Integer, String>();
//		for (int i = 0; i < jobs_priority.length; i++) {
//			selected.put(i, jobs_priority[i]);
//		}
	}

	static class ViewHolder {
		public LinearLayout bar;
		public TextView title;
		public TextView address;
		public TextView zip;
		public ImageView icon_status;
		public ImageView priority;
		public ImageView timer;
		public TextView endTime;
		public TextView duration;
		public TextView timeRegMinutes;
		public TextView files;
		public LinearLayout backgroundLayout;
	}

	@Override
	public int getCount() {
		return jobs_title.length;
	}

	@Override
	public String getItem(int position) {
		return jobs_title[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = null;
		ViewHolder holder = null;

		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.rowlayout_jobs, null);
			holder = new ViewHolder();
			holder.bar = (LinearLayout) convertView
					.findViewById(R.id.job_header_bar);
			holder.title = (TextView) convertView
					.findViewById(R.id.jobs_description);
			holder.address = (TextView) convertView
					.findViewById(R.id.jobs_address);
			holder.priority = (ImageView) convertView
					.findViewById(R.id.imageview_priority);
			holder.icon_status = (ImageView) convertView
					.findViewById(R.id.icon_status);
			holder.timer = (ImageView) convertView
					.findViewById(R.id.Imageview_timer);
			holder.endTime = (TextView) convertView
					.findViewById(R.id.job_endtime);
			holder.duration = (TextView) convertView
					.findViewById(R.id.job_time_allocated);
			holder.timeRegMinutes = (TextView) convertView
					.findViewById(R.id.job_time_used);
			holder.files = (TextView) convertView.findViewById(R.id.job_status);
			holder.backgroundLayout = (LinearLayout) convertView
					.findViewById(R.id.job_list_background);

			convertView.setTag(holder);
		} else {
			view = convertView;
			holder = (ViewHolder) view.getTag();
		}
		if (jobs_title[position].startsWith("Internt")) {
			holder.bar.setVisibility(View.GONE);
		} else {
			// Initialiser now og endTime
			Calendar calendar = Calendar.getInstance();
			Date now = new Date();
			calendar.setTime(now);
			String input_date = jobs_endTime[position];
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Date endTime = null;
			try {
				endTime = format1.parse(input_date);
			} catch (java.text.ParseException e) {
				e.printStackTrace();
			}
			if(endTime != null){
			DateFormat format2 = new SimpleDateFormat("EEEE dd. MMM yyyy",
					new Locale("da", "DK"));

			String weekDay = format2.format(endTime);
			weekDay = weekDay.substring(0, 1).toUpperCase()
					+ weekDay.substring(1);

			// Farvekode p� overskrift: r�d overskredet, gr�n ikke overskredet
			if (now.after(endTime)) {
				holder.bar.setBackgroundResource(R.color.button_red);
			} else {
				holder.bar.setBackgroundResource(R.color.green);
			}

			// Dato p� overskrift
			if (!jobs_title[position].startsWith("Internt")) {
				holder.endTime.setText(weekDay);
			}
			if (previousPosition == -1 || previousPosition <= position) {
				// Dato bar samler flere emner under same dato
				if (endTime.equals(previousDate)) {
					holder.bar.setVisibility(View.GONE);
				} else {
					holder.bar.setVisibility(View.VISIBLE);
					previousDate = endTime;
				}
			}
			// Dato bar laver een overskrift per emne
			else if (position == 0) {
				holder.bar.setVisibility(View.VISIBLE);
				previousDate = endTime;
			} else {
				// Sp�rg p� emnet f�r PreviousDate - er de ens eller ikke
				// String[] temp = WebApi.getWebApi().getJobs_endTime();
				// String pos = jobs_endTime[position - 1];
				// String pos1 = jobs_endTime[position];
				if (jobs_endTime[position - 1] != null) {
					if (jobs_endTime[position - 1].substring(0, 10).equals(
							jobs_endTime[position].substring(0, 10))
							|| jobs_title[position - 1].startsWith("Internt")) {
						holder.bar.setVisibility(View.GONE);
					} else {
						holder.bar.setVisibility(View.VISIBLE);
					}
				}
			}}
		}
		holder.title.setText(jobs_title[position]);
		// autoScaleTextViewTextToHeight(holder.title);
		if (jobs_address_street[position] != null) {
			holder.address.setText(jobs_address_street[position][0]);
		} else {
			holder.address.setText("");
		}

		if (jobs_drawables != null && jobs_drawables.length != 0 && jobs_drawables.length > position) {
			holder.icon_status.setImageDrawable(jobs_drawables[position]);
			if (jobs_priority[position] != null) {
				String priority = jobs_priority[position];
				if (priority.equals("low")) {
					holder.priority.setImageResource(R.drawable.low_32);
					holder.priority.setVisibility(View.VISIBLE);
				} else if (priority.equals("high")) {
					holder.priority.setImageResource(R.drawable.high_32);
					holder.priority.setVisibility(View.VISIBLE);
				} else if (priority.equals("urgent")) {
					holder.priority.setImageResource(R.drawable.urgent_32);
					holder.priority.setVisibility(View.VISIBLE);
				} else {
					holder.priority.setVisibility(View.INVISIBLE);
				}
			}
		}
		if (jobs_timeRegMinutes[position] != null
				&& jobs_timeRegMinutes[position] != "") {
			DecimalFormat df = new DecimalFormat("0.0");
			holder.timeRegMinutes.setText(df.format(
					Float.parseFloat(jobs_timeRegMinutes[position]) / 60)
					.toString()
					+ " " + "t Brugt");
		} else {
			holder.timeRegMinutes.setText("      ");
		}

		if (jobs_duration[position] != null && jobs_duration[position] != "") {
			DecimalFormat df = new DecimalFormat("0.0");
			holder.duration.setText(df.format(
					Float.parseFloat(jobs_duration[position]) / 60).toString()
					+ " " + "t Afsat");
		} else {
			holder.duration.setText("      ");
		}
		if (jobs_files[position] != null && jobs_files[position][0] != null) {
			int count = jobs_files[position].length;
			holder.files.setText("Filer " + count);
		} else {
			holder.files.setText("Filer 0");
			if (position == 0) {
				holder.files.setText("");
			}
		}

		if (jobs_linked[position].equals("1")) {
			holder.backgroundLayout
					.setBackgroundResource(R.drawable.background_timer);
			
			holder.timer.setVisibility(View.VISIBLE);
		} else {
			holder.backgroundLayout.setBackgroundResource(color.transparent);
			holder.timer.setVisibility(View.GONE);
		}
		if (jobs_title.length == 0) {
			holder.title.setText("Der er ingen registrerede jobs");
		}
		previousPosition = position;
		return convertView;
	}
}
