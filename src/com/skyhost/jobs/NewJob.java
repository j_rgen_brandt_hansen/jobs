package com.skyhost.jobs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("ResourceAsColor")
public class NewJob extends Activity implements OnClickListener {

	private Bitmap bitmap;
	private Handler handler;
	private ProgressDialog progressDialog;
	private long timestamp_PushImgButton;

	// Camera
	private static final int CAMERA_IMAGE_CAPTURE = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newjob);

		new PauseDialogLoader().execute();

		// Initialize

		// Ordernumber
		EditText orderNumberEditText = (EditText) findViewById(R.id.newjob_ordernumber_edittext);

		if (!(loadSetting("selectedOrderNumber").equals(""))) {
			orderNumberEditText.setText(loadSetting("selectedOrderNumber"));
		}

		// Name
		final EditText nameEditText = (EditText) findViewById(R.id.newjob_name_edittext);

		if (!(loadSetting("selectedName").equals(""))) {
			nameEditText.setText(loadSetting("selectedName"));
		}

		// Note
		final EditText commentEditText = (EditText) findViewById(R.id.newjob_comment_edittext);

		if (!(loadSetting("selectedNote").equals(""))) {
			commentEditText.setText(loadSetting("selectedNote"));
		}

		// TextView begin time
		TextView beginTimeTextView = (TextView) findViewById(R.id.newjob_begin_textview_text);
		beginTimeTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				saveSetting("begin", true);
				Intent intent = new Intent(NewJob.this, JobSetTime.class);
				startActivity(intent);
			}
		});

		String selectedNewJobBeginDate = "";
		if (!loadSetting("selectedNewJobBeginDate").equals("")) {

			selectedNewJobBeginDate = loadSetting("selectedNewJobBeginDate");

			if (selectedNewJobBeginDate.length() >= 12) {
				String beginDate = selectedNewJobBeginDate.substring(0, 4)
						+ "-" + selectedNewJobBeginDate.substring(4, 6) + "-"
						+ selectedNewJobBeginDate.substring(6, 8) + " "
						+ selectedNewJobBeginDate.substring(8, 10) + ":"
						+ selectedNewJobBeginDate.substring(10, 12);
				beginTimeTextView.setText(beginDate);
			}
		}

		// TextView end time
		TextView endTimeTextView = (TextView) findViewById(R.id.newjob_end_textview_text);
		endTimeTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				saveSetting("begin", false);
				Intent intent = new Intent(NewJob.this, JobSetTime.class);
				startActivity(intent);
			}
		});

		String selectedNewJobEndDate = "";

		if (!loadSetting("selectedNewJobEndDate").equals("")) {

			selectedNewJobEndDate = loadSetting("selectedNewJobEndDate");

			if (selectedNewJobEndDate.length() >= 12) {
				String endDate = selectedNewJobEndDate.substring(0, 4) + "-"
						+ selectedNewJobEndDate.substring(4, 6) + "-"
						+ selectedNewJobEndDate.substring(6, 8) + " "
						+ selectedNewJobEndDate.substring(8, 10) + ":"
						+ selectedNewJobEndDate.substring(10, 12);
				endTimeTextView.setText(endDate);
			}
		}

		// Duration
		Button durationButton = (Button) findViewById(R.id.newjob_duration_button);
		durationButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent jobSetDuraion = new Intent(NewJob.this,
						JobSetDuration.class);
				startActivity(jobSetDuraion);
			}
		});

		String selectedDuration = "";

		if (!loadSetting("selectedDuration").equals("")) {

			selectedDuration = loadSetting("selectedDuration");

			if (selectedDuration.length() >= 4) {
				String duration = selectedDuration.substring(0, 2) + ":"
						+ selectedDuration.substring(2, 4);
				durationButton.setText(duration);
			}
		}

		// Address
		final TextView addressTextView = (TextView) findViewById(R.id.newjob_address_edittext);
		String selectedAddress = "";

		if (!loadSetting("temporaryAddressJob").equals("")) {
			selectedAddress = loadSetting("temporaryAddressJob");
			addressTextView.setText(selectedAddress);
		}

		// Validate Button
		Button validateMapButton = (Button) findViewById(R.id.newjob_validate_map_button);
		validateMapButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				saveSetting("suggestedNewJobAddress", addressTextView.getText()
						.toString());
				saveSetting("sCommand", "" + "newJobAddress");

				Intent helloGoogleMaps = new Intent(NewJob.this,
						HelloGoogleMaps.class);
				startActivity(helloGoogleMaps);
			}
		});

		// Radiogroup
		RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.radio0:
					saveSetting("iSelectedPriority", "0");
					saveSetting("selectedPriority", "low");
					break;

				case R.id.radio1:
					saveSetting("iSelectedPriority", "1");
					saveSetting("selectedPriority", "high");
					break;

				case R.id.radio2:
					saveSetting("iSelectedPriority", "2");
					saveSetting("selectedPriority", "urgent");
					break;
				}
			}
		});

		RadioButton radioButtonLow = (RadioButton) findViewById(R.id.radio0);
		RadioButton radioButtonHigh = (RadioButton) findViewById(R.id.radio1);
		RadioButton radioButtonUrgent = (RadioButton) findViewById(R.id.radio2);

		int iSelectedPriority = 0;

		if (!loadSetting("iSelectedPriority").equals("")) {
			iSelectedPriority = Integer
					.parseInt(loadSetting("iSelectedPriority"));
			switch (iSelectedPriority) {
			case 0:
				radioButtonLow.setChecked(true);
				saveSetting("iSelectedPriority", "0");
				break;

			case 1:
				radioButtonHigh.setChecked(true);
				saveSetting("iSelectedPriority", "1");
				break;

			case 2:
				radioButtonUrgent.setChecked(true);
				saveSetting("iSelectedPriority", "2");
				break;
			}
		}

		// Select Users / Resources
		Button ressourcesButton = (Button) findViewById(R.id.newjob_ressources_button);
		ressourcesButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				saveSetting("isNewJob", true);
				Intent intent = new Intent(NewJob.this, JobSetUsers.class);
				startActivity(intent);
			}
		});

		// put selectedRessources in textview
		String selectedRessourcesNames = "";

		if (!loadSetting("selectedRessourcesNames").equals("")) {
			selectedRessourcesNames = loadSetting("selectedRessourcesNames");
			TextView ressourcesTextView = (TextView) findViewById(R.id.newjob_ressources_textview_list);

			String[] parts = selectedRessourcesNames.split(",");

			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i <= parts.length - 1; i++) {
				stringBuilder.append(Html.fromHtml(parts[i] + "<br>"));
			}

			String result = stringBuilder.toString();

			ressourcesTextView.setText(result);
			// Html.fromHtml("Valgte Ressourcer: "
			// + parts.substring(0, 1).toUpperCase()
			// + manufacturer.substring(1) + "<br>" + "Model: " + model
			// + "<br>" + "Version: " + version + "<br>" + "AppVersion: "
			// + sAppVersion)
		}

		// Existing file jpg,jpeg,png,bmp,pdf,doc,docx,xls,xlsx
		Button buttonChooseExistingImage = (Button) findViewById(R.id.newjob_existing_image_button);
		buttonChooseExistingImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent pickPhoto = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(pickPhoto, 1);
			}
		});

		// Existing Image
		Button buttonSelectRessourceImage = (Button) findViewById(R.id.newjob_existing_image_button);
		buttonChooseExistingImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent pickPhoto = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(pickPhoto, 1);
			}
		});

		// New Image
		Button buttonStartCamera = (Button) findViewById(R.id.newjob_camera_button);
		buttonStartCamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// Catch double clickers
				long timeSinceLastPush = System.currentTimeMillis()
						- timestamp_PushImgButton;
				if (timeSinceLastPush < 1500) {
					return;
				}

				timestamp_PushImgButton = System.currentTimeMillis();

				// Create folder if not exists
				File folder = new File(Environment
						.getExternalStorageDirectory() + "/Skyhost");
				if (!folder.exists()) {
					folder.mkdir();
				}

				// OPEN CAMERA
				Intent cameraIntent = new Intent(
						android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				final Calendar c = Calendar.getInstance();

				String new_Date = c.get(Calendar.DAY_OF_MONTH) + "-"
						+ ((c.get(Calendar.MONTH)) + 1) + "-"
						+ c.get(Calendar.YEAR) + "_" + c.get(Calendar.HOUR)
						+ "-" + c.get(Calendar.MINUTE) + "-"
						+ c.get(Calendar.SECOND);
				// saveSetting("pathImages", pathImages);
				String selectedImage = String.format(
						Environment.getExternalStorageDirectory()
								+ "/Skyhost/%s.jpg", "SkyhostPoi(" + new_Date
								+ ")");

				saveSetting("selectedImage", selectedImage);

				File photo = null;
				try {
					photo = new File(selectedImage);

					photo.getParentFile().createNewFile();
				} catch (IOException ex) {
					ex.printStackTrace();
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(photo));
				startActivityForResult(cameraIntent, CAMERA_IMAGE_CAPTURE);
			}
		});

		Button buttonCreateAttachment = (Button) findViewById(R.id.newjob_create_button);
		buttonCreateAttachment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// Catch double clickers
				long timeSinceLastPush = System.currentTimeMillis()
						- timestamp_PushImgButton;
				if (timeSinceLastPush < 1500) {
					return;
				}

				timestamp_PushImgButton = System.currentTimeMillis();

				Intent intent = new Intent(NewJob.this, Job.class);
				startActivity(intent);
			}
		});

		// NewJobCreateButton
		Button createNewJobButton = (Button) findViewById(R.id.newjob_create_button);
		createNewJobButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// If more than 2 letters create Job
				String jobName = nameEditText.getText().toString();

				if (jobName.length() >= 2) {
					saveSetting("newJobName", nameEditText.getText().toString());
					new AsyncCreateNewJob().execute();

				}
				// Else less than 2 letters -> showtoast
				else {
					showToast("Jobnavn skal indeholde mindst 2 bogstaver");
				}
			}
		});
		handler = new Handler();

		// Keyboard
		InputMethodManager imm = (InputMethodManager) this
				.getSystemService(Service.INPUT_METHOD_SERVICE);
		imm.showSoftInput(orderNumberEditText, InputMethodManager.SHOW_IMPLICIT);
	}

	@Override
	public void onPause() {
		super.onPause();

		// Ordernumber
		EditText orderNumberEdittext = (EditText) findViewById(R.id.newjob_ordernumber_edittext);

		// Name
		final EditText nameEditText = (EditText) findViewById(R.id.newjob_name_edittext);

		// Note
		final EditText commentEditText = (EditText) findViewById(R.id.newjob_comment_edittext);

		saveSetting("selectedOrderNumber", orderNumberEdittext.getText()
				.toString());
		saveSetting("selectedName", nameEditText.getText().toString());
		saveSetting("selectedNote", commentEditText.getText().toString());
	}

	@Override
	public void onResume() {
		super.onResume();

		saveSetting("isNewJob", true);

		if (progressDialog != null) {
			progressDialog.dismiss();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	protected void onActivityResult(int requestCode, int resultCode,
			Intent imageReturnedIntent) {
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

		ImageView imageViewUserImage = (ImageView) findViewById(R.id.newjob_image_imageview);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		if (requestCode == CAMERA_IMAGE_CAPTURE && resultCode == RESULT_OK) {

			String selectedImage = loadSetting("selectedImage");
			if (!selectedImage.equals("")) {

				try {

					File photo = new File(selectedImage);
					Bitmap temporaryThumbnail = (Bitmap) decodeFile(photo);

					ExifInterface exif = new ExifInterface(photo.getPath());
					int orientation = exif.getAttributeInt(
							ExifInterface.TAG_ORIENTATION,
							ExifInterface.ORIENTATION_NORMAL);

					// Hardcoded value
					int angle = 90;

					if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
						angle = 90;
					} else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
						angle = 180;
					} else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
						angle = 270;
					}

					Matrix mat = new Matrix();
					mat.postRotate(angle);

					bitmap = Bitmap.createBitmap(temporaryThumbnail, 0, 0,
							temporaryThumbnail.getWidth(),
							temporaryThumbnail.getHeight(), mat, true);

					bitmap.compress(CompressFormat.JPEG, 85, bos);

					save(bos);

					Drawable d = new BitmapDrawable(getResources(), bitmap);
					scaleImage(imageViewUserImage, d);

				} catch (IOException e) {
					e.printStackTrace();
				} catch (OutOfMemoryError oom) {
					oom.printStackTrace();
				}
			}
		}

		if (requestCode == 1 && resultCode == RESULT_OK) {
			Uri selectedImage = imageReturnedIntent.getData();

			try {
				bitmap = decodeUri(selectedImage);
				imageViewUserImage.setImageBitmap(bitmap);
				bitmap.compress(CompressFormat.JPEG, 85, bos);

				save(bos);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void save(ByteArrayOutputStream os) {
		FileOutputStream fos;
		String pathImage = loadSetting("selectedImage");

		try {
			if (!pathImage.equals("")) {
				fos = new FileOutputStream(pathImage);
				os.writeTo(fos);
				os.flush();
				fos.flush();
				os.close();
				fos.close();
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(
				getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 800;
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		if (width_tmp > REQUIRED_SIZE) {
		} else if (height_tmp > REQUIRED_SIZE) {
		}

		int scale = 1;
		while (true) {
			if (width_tmp < REQUIRED_SIZE || height_tmp < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
			// scale = calculateInSampleSize(o, 800, 800);
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(
				getContentResolver().openInputStream(selectedImage), null, o2);
	}

	public static Bitmap decodeSampledBitmapFromResource(Resources res,
			int resId, int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	private void scaleImage(ImageView imageView, Drawable drawable) {
		// Get the ImageView and its bitmap
		ImageView view = imageView;
		Drawable drawing = drawable;
		if (drawing == null) {
			return; // Checking for null & return, as suggested in comments
		}
		Bitmap bitmap = ((BitmapDrawable) drawing).getBitmap();

		// Get current dimensions AND the desired bounding box
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		int bounding = dpToPx(650);

		// Determine how much to scale: the dimension requiring less scaling is
		// closer to the its side. This way the image always stays inside your
		// bounding box AND either x/y axis touches it.
		float xScale = ((float) bounding) / width;
		float yScale = ((float) bounding) / height;
		float scale = (xScale <= yScale) ? xScale : yScale;

		// Create a matrix for the scaling and add the scaling data
		Matrix matrix = new Matrix();
		matrix.postScale(scale, scale);

		// Create a new bitmap and convert it to a format understood by the
		// ImageView
		Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height,
				matrix, true);
		width = scaledBitmap.getWidth(); // re-use
		height = scaledBitmap.getHeight(); // re-use
		BitmapDrawable result = new BitmapDrawable(scaledBitmap);

		// Apply the scaled bitmap
		view.setImageDrawable(result);

		// Now change ImageView's dimensions to match the scaled image
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view
				.getLayoutParams();
		params.width = android.support.v4.view.ViewPager.LayoutParams.FILL_PARENT;
		params.height = android.support.v4.view.ViewPager.LayoutParams.MATCH_PARENT;
		// params.width = width;
		// params.height = height;
		view.setLayoutParams(params);

	}

	private int dpToPx(int dp) {
		float density = getApplicationContext().getResources()
				.getDisplayMetrics().density;
		return Math.round((float) dp * density);
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	public static String getCurrentTimeStamp() {
		SimpleDateFormat sdfDate = new SimpleDateFormat(
				"EEEE dd. MMM yyyy HH:mm", new Locale("da", "DK"));
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

	public void setPowerSaveState(boolean enabled) {
		if (enabled == false)
			android.provider.Settings.System.putInt(getContentResolver(),
					android.provider.Settings.System.SCREEN_OFF_TIMEOUT, -1);
		else
			android.provider.Settings.System.putInt(getContentResolver(),
					android.provider.Settings.System.SCREEN_OFF_TIMEOUT, 60000);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent menuIntent = null;

		switch (item.getItemId()) {
		case R.id.activity_jobs:
			menuIntent = new Intent(NewJob.this, Jobs.class);
			break;
		}
		startActivity(menuIntent);
		return true;
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private boolean loadSettingBoolean(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		boolean content = preferences.getBoolean(tag, false);
		return content;
	}

	private int loadSettingInt(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		int content = preferences.getInt(tag, 0);
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private void saveSetting(String tag, Boolean content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(tag, content);
		editor.commit();
	}

	public void showToast(final String toast) {
		runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(NewJob.this, toast, Toast.LENGTH_SHORT).show();
			}
		});
	}

	// ---------- Hj�lpeklasser ----------
	// private class AsyncLoaderServiceReport extends AsyncTask<Void, Void,
	// Void> {
	//
	// protected void onPreExecute() {
	// super.onPreExecute();
	// }
	//
	// protected Void doInBackground(Void... arg0) {
	// SharedPreferences preferences = getSharedPreferences("PREFERENCES",
	// Activity.MODE_PRIVATE);
	//
	// String comment = "";
	// if (!loadSetting("comment").equals("")) {
	// comment = loadSetting("comment");
	// }
	//
	// String selectedRatingId = "";
	// if (!loadSetting("selectedRatingId").equals("")) {
	// selectedRatingId = loadSetting("selectedRatingId");
	// }
	//
	// try {
	//
	// String selectedToolId = "";
	// if (!(loadSetting("selectedToolId").equals(""))) {
	// selectedToolId = loadSetting("selectedToolId");
	// }
	//
	// // WebApi.getWebApi().serviceReport(preferences, selectedToolId,
	// // selectedRatingId, comment, bitmap);
	//
	// String responseStatus = WebApi.getWebApi().getResponse_Status();
	//
	// if (responseStatus.equals("ERROR")) {
	// String errorHeader = WebApi.getWebApi()
	// .getResponse_ErrorHeader();
	// String errorText = WebApi.getWebApi()
	// .getResponse_ErrorText();
	// showToast("Fejl: " + errorHeader.substring(1) + " "
	// + errorText.substring(1));
	// showToast("Fejl: " + errorHeader.substring(1) + " "
	// + errorText.substring(1));
	// }
	//
	// if (responseStatus.equals("OK")) {
	// showToast(responseStatus);
	// Intent scanner = new Intent(NewJob.this, Job.class);
	// startActivity(scanner);
	// }
	// } catch (Exception ex) {
	// }
	// return null;
	// }
	//
	// protected void onPostExecute(Void bool) {
	// super.onPostExecute(bool);
	//
	// if (progressDialog != null) {
	// progressDialog.dismiss();
	// }
	// }
	//
	// protected void onProgressUpdate(Void... values) {
	// super.onProgressUpdate(values);
	// }
	//
	// protected void showToast(final String sHeader) {
	// runOnUiThread(new Runnable() {
	// @Override
	// public void run() {
	// Toast.makeText(getApplicationContext(), sHeader,
	// Toast.LENGTH_LONG).show();
	// }
	// });
	// }
	// }

	private Bitmap decodeFile(File file) {
		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(file), null, o);

			// The new size we want to scale to
			final int REQUIRED_SIZE = 640;

			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			while (true) {
				if (o.outWidth < REQUIRED_SIZE || o.outHeight < REQUIRED_SIZE) {
					break;
				}
				o.outWidth /= 2;
				o.outHeight /= 2;
				scale *= 2;
			}
			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			o2.inJustDecodeBounds = false;
			o2.inPreferredConfig = Config.RGB_565;
			o2.inDither = true;

			Bitmap bitmap = BitmapFactory.decodeStream(
					new FileInputStream(file), null, o2);

			return bitmap;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (OutOfMemoryError oom) {
			oom.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private class AsyncCreateNewJob extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			// Ordernumber
			EditText orderNumberEdittext = (EditText) findViewById(R.id.newjob_ordernumber_edittext);
			String orderNumber = "";
			if (!orderNumberEdittext.getText().equals("")) {
				String temp = orderNumberEdittext.getText().toString();
				try {
					orderNumber = URLEncoder.encode(temp, "utf-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}

			// Name
			EditText nameEditText = (EditText) findViewById(R.id.newjob_name_edittext);
			String newJobName = "";
			if (!nameEditText.getText().equals("")) {
				String temp = nameEditText.getText().toString();
				try {
					newJobName = URLEncoder.encode(temp, "utf-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}

			// Comment
			EditText commentEdittext = (EditText) findViewById(R.id.newjob_comment_edittext);
			String comment = "";
			if (!commentEdittext.getText().equals("")) {
				String temp = commentEdittext.getText().toString();
				try {
					orderNumber = URLEncoder.encode(temp, "utf-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}

			// BeginDate
			String selectedNewJobBeginDate = "";
			if (!loadSetting("selectedNewJobBeginDate").equals("")) {
				selectedNewJobBeginDate = loadSetting("selectedNewJobBeginDate");
			}

			// EndDate
			String selectedNewJobEndDate = "";
			if (!loadSetting("selectedNewJobEndDate").equals("")) {
				selectedNewJobEndDate = loadSetting("selectedNewJobEndDate");
			}

			// Duration
			String selectedDuration = "";
			if (!loadSetting("selectedDuration").equals("")) {
				selectedDuration = loadSetting("selectedDuration");
			}

			// Address
			String selectedAddress = "";
			if (!loadSetting("temporaryAddressJob").equals("")) {
				String temp = loadSetting("temporaryAddressJob");
				try {
					selectedAddress = URLEncoder.encode(temp, "utf-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}

			// Latitude
			String selectedLatitude = "";
			if (!loadSetting("temporaryLatitudeJob").equals("")) {
				selectedLatitude = loadSetting("temporaryLatitudeJob");
			}

			// Longitude
			String selectedLongitude = "";
			if (!loadSetting("temporaryLongitudeJob").equals("")) {
				selectedLongitude = loadSetting("temporaryLongitudeJob");
			}

			// Priority
			String selectedPriority = "";
			if (!loadSetting("selectedPriority").equals("")) {
				selectedPriority = loadSetting("selectedPriority");
			}

			// Ressources
			String selectedUsers = "";
			if (!loadSetting("selectedRessources").equals("")) {
				selectedUsers = loadSetting("selectedRessources");
			}

			// Ressources
			String selectedImage = "";
			if (!loadSetting("selectedImage").equals("")) {
				selectedImage = loadSetting("selectedImage");
			}

			WebApi.getWebApi().createNewJob(preferences, newJobName,
					orderNumber, comment, selectedNewJobBeginDate,
					selectedNewJobEndDate, selectedDuration, selectedAddress,
					selectedLatitude, selectedLongitude, selectedPriority,
					selectedUsers, selectedImage);

			// Response error and success handling -> redirection
			// Error
			if (WebApi.getWebApi().isNewData_Jobs_CreateNewJob() == false) {
				if (!WebApi.getWebApi().getResponse_ErrorText().equals("")
						|| !WebApi.getWebApi().getResponse_ErrorCode()
								.equals("")) {
					showToast("Jobbet blev ikke oprettet! " + "Fejlkode: "
							+ WebApi.getWebApi().getResponse_ErrorCode(),
							WebApi.getWebApi().getResponse_ErrorText()
									+ " Pr�v igen senere!");
				} else {
					showToast("Jobbet blev ikke oprettet - Pr�v igen senere!");
				}
			}

			// Success
			else if (WebApi.getWebApi().isNewData_Jobs_CreateNewJob() == true) {
				String text = WebApi.getWebApi().getResponse_ErrorText();
				String code = WebApi.getWebApi().getResponse_ErrorCode();
				if (text.equals("Job created") && code.equals("0")) {
					showToast("Jobbet er oprettet!");

					saveSetting("selectedOrderNumber", "");
					saveSetting("selectedName", "");
					saveSetting("selectedNote", "");

					saveSetting("selectedNewJobBeginDate", "");
					saveSetting("selectedNewJobEndDate", "");
					saveSetting("selectedDuration", "");
					saveSetting("selectedAddress", "");
					saveSetting("selectedLatitude", "");
					saveSetting("selectedLongitude", "");
					saveSetting("temporaryLatitudeJob", "");
					saveSetting("temporaryLongitudeJob", "");
					saveSetting("selectedPriority", "");
					saveSetting("selectedRessources", "");
					saveSetting("selectedImage", "");

					Intent jobs = new Intent(NewJob.this, Jobs.class);
					startActivity(jobs);

				} else {
					showToast("Jobbet blev ikke oprettet! " + "Fejlkode: "
							+ WebApi.getWebApi().getResponse_ErrorCode(),
							WebApi.getWebApi().getResponse_ErrorText()
									+ " Pr�v igen senere!");
				}
			} else {
				showToast("Jobbet blev ikke oprettet - Pr�v igen senere!");
			}

			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);

			saveSetting("isNewJob", false);
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

		protected void showToast(final String sHeader, final String sContext) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sContext, Toast.LENGTH_LONG)
							.show();
				}
			});
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}
	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(NewJob.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(true);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}
	}

	@Override
	public void onClick(View v) {
	}

}
