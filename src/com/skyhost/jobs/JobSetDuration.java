package com.skyhost.jobs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class JobSetDuration extends Activity implements OnClickListener {

	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.jobsetduration);

		new PauseDialogLoader().execute();

		// Initializing
		// Hours
		String[] hours = new String[25];
		for (int i = 0; i < hours.length; i++) {
			hours[i] = i + "";
		}

		Spinner spinnerHours = (Spinner) findViewById(R.id.spinner_hours);
		@SuppressWarnings("unchecked")
		ArrayAdapter adapterHours = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, hours);
		adapterHours
				.setDropDownViewResource(R.layout.rowlayout_timerselecttime_spinnerhours);
		spinnerHours.setAdapter(adapterHours);
		spinnerHours.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				String selectedHour = "0";
				selectedHour = String.valueOf(position);
				saveSetting("selectedHour", selectedHour);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		// Minutes
		String[] minutes = new String[4];
		minutes[0] = "00";
		minutes[1] = "15";
		minutes[2] = "30";
		minutes[3] = "45";

		Spinner spinnerMinutes = (Spinner) findViewById(R.id.spinner_minutes);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		ArrayAdapter adapterMinutes = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, minutes);
		adapterMinutes
				.setDropDownViewResource(R.layout.rowlayout_timerselecttime_spinnerminutes);
		spinnerMinutes.setAdapter(adapterMinutes);
		spinnerMinutes.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				@SuppressWarnings("unused")
				String selectedMinute = "00";
				if (position == 0) {
					saveSetting("selectedMinute", "00");
				} else if (position == 1) {
					saveSetting("selectedMinute", "15");
				} else if (position == 2) {
					saveSetting("selectedMinute", "30");
				} else if (position == 3) {
					saveSetting("selectedMinute", "45");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		Button saveButton = (Button) findViewById(R.id.jobsetduration_button_save);
		saveButton.setOnClickListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		if (progressDialog != null) {
			progressDialog.hide();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			Intent jobs = new Intent(JobSetDuration.this, Jobs.class);
			startActivity(jobs);
			return true;

		case R.id.activity_timer: // Job
			Intent timer = new Intent(JobSetDuration.this, Timer.class);
			startActivity(timer);
			return true;
		}
		return true;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private Boolean loadSettingBoolean(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		Boolean content = preferences.getBoolean(tag, false);
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private void saveSetting(String tag, Boolean content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(tag, content);
		editor.commit();
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.jobsetduration_button_save) {

			String selectedMinute = "";
			if (!loadSetting("selectedMinute").equals("")) {
				selectedMinute = loadSetting("selectedMinute");
			}

			String selectedHour = "";
			if (!loadSetting("selectedHour").equals("")) {
				selectedHour = loadSetting("selectedHour");
			}

			// If hour and minute nothing is not selected
			if (selectedMinute.equals("00") && selectedHour.equals("0")) {
				Toast.makeText(getApplicationContext(),
						"Du skal v�lge tid: Timer eller minutter",
						Toast.LENGTH_LONG).show();
			} else {
				new AsyncSetJobDuration().execute();
			}
		}
	}

	private class AsyncSetJobDuration extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			String selectedMinute = "";
			if (!loadSetting("selectedMinute").equals("")) {
				selectedMinute = loadSetting("selectedMinute");
			}

			String selectedHour = "";
			if (!loadSetting("selectedHour").equals("")) {
				selectedHour = loadSetting("selectedHour");
			}

			int hours = Integer.parseInt(selectedHour);
			int minutesFromHours = hours * 60;
			int minutes = Integer.parseInt(selectedMinute);
			int total = minutesFromHours + minutes;
			String totalMinutes = String.valueOf(total);

			String selectedJobId = "";
			if (!(loadSetting("selectedJobId").equals(""))) {
				selectedJobId = loadSetting("selectedJobId");
			}

			// If fields are not null or empty
			if (!selectedJobId.equals("")) {

				SharedPreferences preferences = getSharedPreferences(
						"PREFERENCES", Activity.MODE_PRIVATE);

				WebApi.getWebApi().setJobDuration(preferences, selectedJobId,
						totalMinutes);
			}

			// Error sending
			if (WebApi.getWebApi().isNewData_Jobs_SetJobDuration() == false) {
				showToast("Fejlkode: "
						+ WebApi.getWebApi().getResponse_ErrorCode(), WebApi
						.getWebApi().getResponse_ErrorText()
						+ " Pr�v igen senere!");

				Intent status = new Intent(JobSetDuration.this, Job.class);
				startActivity(status);
			}
			// Success sending
			else if (WebApi.getWebApi().isNewData_Jobs_SetJobDuration() == true) {
				String errorHeader = WebApi.getWebApi()
						.getResponse_ErrorHeader();

				// Success answer
				if (errorHeader.equals("OK")) {
					showToast("Din registrering er gemt");
					saveSetting("reloadJobs", true);
					saveSetting("reloadJob", true);
					saveSetting("editState", false);
					Intent intent = new Intent(JobSetDuration.this, Jobs.class);
					startActivity(intent);
				}

				// Error in answer
				else if (WebApi.getWebApi().getResponse_Status()
						.equals("Error")) {
					if (!WebApi.getWebApi().getResponse_ErrorText().equals("")) {
						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");

						Intent intent = new Intent(JobSetDuration.this,
								Job.class);
						startActivity(intent);
					} else {
						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");

						Intent intent = new Intent(JobSetDuration.this,
								Job.class);
						startActivity(intent);
					}
				}
			} else {
				Intent intent = new Intent(JobSetDuration.this, Job.class);
				startActivity(intent);
			}

			return null;
		}

		protected void showToast(final String sHeader, final String sComment) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sComment, Toast.LENGTH_LONG)
							.show();
				}
			});
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(JobSetDuration.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}
}