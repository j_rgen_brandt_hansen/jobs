package com.skyhost.jobs;

public class Material implements Comparable<Material> {

	private String matId;
	private String name;
	private String description;
	private String reference;
	private String colli;
	private String groupNo;
	private String groupName;
	private String supplierItemNo;
	private String eanNo;
	private String elNo;
	private String eanBarcode;
	private String internalBarcode;
	private String internalItemNo;
	private String internalItemShortNo;
	private int amount;

	// Colli prep.
	public enum Colli {
		STK("Stk"), KASSE("Kasse"), RULLE("Rulle"), METER("Meter");
		private String friendlyName;

		private Colli(String friendlyName) {
			this.friendlyName = friendlyName;
		}

		@Override
		public String toString() {
			return friendlyName;
		}
	}

	public Material() {
		super();
		this.matId = matId;
		this.name = name;
		this.description = description;
		this.reference = reference;
		this.colli = colli;
		this.groupNo = groupNo;
		this.groupName = groupName;
		this.supplierItemNo = supplierItemNo;
		this.eanNo = eanNo;
		this.elNo = elNo;
		this.eanBarcode = eanBarcode;
		this.internalBarcode = internalBarcode;
		this.internalItemNo = internalItemNo;
		this.internalItemShortNo = internalItemShortNo;
		this.amount = amount;
	}

	@Override
	public int compareTo(Material another) {
		return this.getName().compareTo(another.getName());
	}

	// ---------- get ----------

	public String getColliString() {
		return colli.toString();
	}

	public String getColli() {
		return colli;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getMatId() {
		return matId;
	}

	public String getReference() {
		return reference;
	}

	public String getGroupNo() {
		return groupNo;
	}

	public String getGroupName() {
		return groupName;
	}

	public String getSupplierItemNo() {
		return supplierItemNo;
	}

	public String getEanNo() {
		return eanNo;
	}

	public String getElNo() {
		return elNo;
	}

	public String getEanBarcode() {
		return eanBarcode;
	}

	public String getInternalBarcode() {
		return internalBarcode;
	}

	public String getInternalItemNo() {
		return internalItemNo;
	}

	public String getInternalItemShortNo() {
		return internalItemShortNo;
	}

	public int getAmount() {
		return amount;
	}

	// ---------- set ----------

	public void setColli(String stk) {
		colli = stk;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setMatId(String matId) {
		this.matId = matId;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public void setGroupNo(String groupNo) {
		this.groupNo = groupNo;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void setSupplierItemNo(String supplierItemNo) {
		this.supplierItemNo = supplierItemNo;
	}

	public void setEanNo(String eanNo) {
		this.eanNo = eanNo;
	}

	public void setElNo(String elNo) {
		this.elNo = elNo;
	}

	public void setEanBarcode(String eanBarcode) {
		this.eanBarcode = eanBarcode;
	}

	public void setInternalBarcode(String internalBarcode) {
		this.internalBarcode = internalBarcode;
	}

	public void setInternalItemNo(String internalItemNo) {
		this.internalItemNo = internalItemNo;
	}

	public void setInternalItemShortNo(String internalItemShortNo) {
		this.internalItemShortNo = internalItemShortNo;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

}
