package com.skyhost.jobs;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import org.json.JSONArray;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Jobs extends ListActivity implements OnClickListener {

	private Handler handler;
	private Handler handlerSaveSettingsAllJobs;
	private ProgressDialog progressDialog;
	public static int iSelectedJobIndex;

	private Runnable runSaveSettingsAllJobs = new Runnable() {
		@Override
		public void run() {
			new AsyncSaveSettingsAllJobs().execute();
		}
	};

	private Runnable runUpdateTask = new Runnable() {
		public void run() {
			reloadApi();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.jobs);

		new ShowAsyncPauseDialog().execute();

		// Initialize
		setTitle("Jobs");

		ListView listView = getListView();

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> l, View v, int position,
					long arg3) {

				Intent intent = new Intent(Jobs.this, Job.class);

				saveSetting("selectedJobIndex", "" + position);

				// TODO rethink non global/Bad solution
				iSelectedJobIndex = position;
				saveSetting("selectedJobId", (String) WebApi.getWebApi()
						.getJobs_jobId().get(position));

				saveSetting("sCommand", "oneJob");
				startActivity(intent);
			}
		});
		handler = new Handler();
		handlerSaveSettingsAllJobs = new Handler();
	}

	@Override
	public void onPause() {
		super.onPause();

		if (handler != null) {
			handler.removeCallbacks(runUpdateTask);
		}
		if (handlerSaveSettingsAllJobs != null) {
			handlerSaveSettingsAllJobs.removeCallbacks(runSaveSettingsAllJobs);
		}

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		if (handler != null) {
			handler.post(runUpdateTask);
		}
	}

	@Override
	public void onBackPressed() {
	}

	private void reloadApi() {
		boolean reloadJobs = false;

		// Reloads Api
		reloadJobs = loadSettingBoolean("reloadJobs");

		// Reset boolean
		if (reloadJobs == true) {
			new AsyncLoaderJobs().execute();
			saveSetting("reloadJobs", false);
		} else {
			Date now = new Date();
			Date previous = WebApi.getWebApi().getTimestampJobs();
			long lResult = now.getTime() - previous.getTime();
			// If more than 30 seconds ago - update again
			if (lResult <= 0 || lResult > 30000) {
				new AsyncLoaderJobs().execute();
			} else {
				boolean isNewDataJobs = WebApi.getWebApi().isNewData_Jobs();
				if (isNewDataJobs == true) {
					updateScreen();
					if (loadSettingBoolean("AutoUpdate") == true) {
						handler.postDelayed(runUpdateTask, 30000);
					}
				} else {
					handler.postDelayed(runUpdateTask, 3000);
				}
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.activity_timer: // Timer
			Intent timer = new Intent(Jobs.this, Timer.class);
			startActivity(timer);
			return true;

		case R.id.activity_hellogooglemaps: // Map
			saveSetting("sCommand", "allJobsNotOneJob");
			Intent map = new Intent(Jobs.this, HelloGoogleMaps.class);
			startActivity(map);
			return true;

		case R.id.activity_settings: // Settings
			Intent settings = new Intent(Jobs.this, Settings.class);
			startActivity(settings);
			return true;

		case R.id.activity_create_new_job: // Opret nyt job
			AlertDialog.Builder alertDialog = null;
			alertDialog = new AlertDialog.Builder(this);
			AlertDialog alert = alertDialog.create();
			final Button buttonPositive = alert
					.getButton(AlertDialog.BUTTON_POSITIVE);
			LayoutInflater li = LayoutInflater.from(this);
			final View promptsView = li.inflate(R.layout.createnewjobdialog,
					null, false);
			alertDialog.setView(promptsView);
			alertDialog.setTitle("Opret nyt Job");
			// alertDialog.setMessage("Indtast Job");
			alertDialog.setIcon(R.drawable.icon_add_32);
			final EditText userInput = (EditText) promptsView
					.findViewById(R.id.editTextDialogUserInput);
			alertDialog.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

							// If more than 2 letters create Job
							String temp = userInput.getText().toString();
							if (temp.length() >= 2) {
								saveSetting("newJobName", userInput.getText()
										.toString());
								new AsyncCreateNewJob().execute();
								Intent jobs = new Intent(Jobs.this, Jobs.class);
								startActivity(jobs);
							}
							// Else less than 2 letters -> showtoast
							else {
								showToast("Jobnavn skal indeholde mindst 2 bogstaver");
							}
						}
					});

			alertDialog.setNegativeButton("Fortryd",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Intent jobs = new Intent(Jobs.this, Jobs.class);
							startActivity(jobs);
						}
					});

			if (buttonPositive != null) {
				buttonPositive.setEnabled(false);
			}

			alertDialog.show();

			return true;
		case R.id.activity_info: // Info
			Intent info = new Intent(Jobs.this, Info.class);
			startActivity(info);
			return true;
		}
		return true;
	}

	private class AsyncCreateNewJob extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			// New Job Name
			String newJobName = "";
			if (!loadSetting("newJobName").equals("")) {
				String temp = loadSetting("newJobName");
				try {
					newJobName = URLEncoder.encode(temp, "utf-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}

			WebApi.getWebApi()
					.createNewJobSimpleMethod(preferences, newJobName);

			// Response error and success handling -> redirection
			// Error
			if (WebApi.getWebApi().isNewData_Jobs_CreateNewJob() == false
					|| WebApi.getWebApi().isNewData_Response() == false) {
				if (!WebApi.getWebApi().getResponse_ErrorText().equals("")
						|| !WebApi.getWebApi().getResponse_ErrorCode()
								.equals("")) {
					showToast("Jobbet blev ikke oprettet! " + "Fejlkode: "
							+ WebApi.getWebApi().getResponse_ErrorCode(),
							WebApi.getWebApi().getResponse_ErrorText()
									+ " Pr�v igen senere!");
				} else {
					showToast("Jobbet blev ikke oprettet - Pr�v igen senere!");
				}
			}

			// Success
			else if (WebApi.getWebApi().isNewData_Response() == true) {
				String text = WebApi.getWebApi().getResponse_ErrorText();
				String code = WebApi.getWebApi().getResponse_ErrorCode();
				if (text.equals("Job created") && code.equals("0")) {
					showToast("Jobbet er oprettet!");
				} else {
					showToast("Jobbet blev ikke oprettet! " + "Fejlkode: "
							+ WebApi.getWebApi().getResponse_ErrorCode(),
							WebApi.getWebApi().getResponse_ErrorText()
									+ " Pr�v igen senere!");
				}
			} else {
				showToast("Jobbet blev ikke oprettet - Pr�v igen senere!");
			}

			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

		protected void showToast(final String sHeader, final String sContext) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sContext, Toast.LENGTH_LONG)
							.show();
				}
			});
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}
	}

	private void updateScreen() {
		if (WebApi.getWebApi().getJobs_title().length == 0) {
			TextView title = (TextView) findViewById(R.id.jobs_textview_nojobs);
			title.setVisibility(View.VISIBLE);
			title.setText("Der er ingen registrerede jobs");
		} else {

			final ArrayAdapterJobs adapter = new ArrayAdapterJobs(this, WebApi
					.getWebApi().getJobs_title(), WebApi.getWebApi()
					.getJobs_priority(), WebApi.getWebApi().getJobs_linked(),
					WebApi.getWebApi().getJobs_timeRegMinutes(), WebApi
							.getWebApi().getJobs_beginTime(), WebApi
							.getWebApi().getJobs_files_titles(), WebApi
							.getWebApi().getJobs_drawables(), WebApi
							.getWebApi().getJobs_endTime(), WebApi.getWebApi()
							.getJobs_duration(), WebApi.getWebApi()
							.getJobs_address_street(), WebApi.getWebApi()
							.getJobs_address_zip());
			runOnUiThread(new Runnable() {

				@Override
				public void run() {

					try {
						setListAdapter(adapter);

					} catch (Exception ex) {
						ex.printStackTrace();
					}

					if (progressDialog != null) {
						progressDialog.hide();
					}
				}
			});
		}
		if (handlerSaveSettingsAllJobs != null) {
			handlerSaveSettingsAllJobs.post(runSaveSettingsAllJobs);
		}
	}

	public int getSelectedJobIndex() {
		int iSelectedJobIndex = 0;
		if (!loadSetting("selectedJobIndex").equals("")) {
			iSelectedJobIndex = Integer
					.parseInt(loadSetting("selectedJobIndex"));
		}
		return iSelectedJobIndex;

	}

	private void saveSettingAllJobs() {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < WebApi.getWebApi().getJobs_title().length; i++) {
			jsonArray.put(WebApi.getWebApi().getJobs_title()[i]);
		}
		saveSetting("allJobs", jsonArray.toString());
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private Boolean loadSettingBoolean(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		Boolean content = preferences.getBoolean(tag, false);
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private void saveSetting(String tag, boolean content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(tag, content);
		editor.commit();
	}

	@Override
	public void onClick(View arg0) {
	}

	public void showToast(final String toast) {
		runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(Jobs.this, toast, Toast.LENGTH_SHORT).show();
			}
		});
	}

	private class AsyncLoaderJobs extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			WebApi.getWebApi().Update_Jobs(preferences);
			try {
				WebApi.getWebApi().storeImages();
			} catch (Exception ex) {
			}

			boolean isNewDataJobs = WebApi.getWebApi().isNewData_Jobs();
			if (isNewDataJobs == true) {
				updateScreen();
				if (loadSettingBoolean("AutoUpdate") == true) {
					handler.postDelayed(runUpdateTask, 30000);
				}
			} else {
				handler.postDelayed(runUpdateTask, 3000);
			}

			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);

			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class AsyncSaveSettingsAllJobs extends
			AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {
			saveSettingAllJobs();
			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class DialogLoaderCreateNewJob extends
			AsyncTask<Void, Integer, Void> {
		AlertDialog.Builder alertDialog;

		protected void onPreExecute() {
			super.onPreExecute();

			LayoutInflater li = LayoutInflater.from(getApplicationContext());
			View promptsView = li.inflate(R.layout.createnewjobdialog, null);

			alertDialog = new AlertDialog.Builder(getApplicationContext());
			alertDialog.setTitle("Opret nyt Job");
			alertDialog.setMessage("Indtast Job");
			alertDialog.setIcon(R.drawable.icon_add_32);
			alertDialog.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Intent jobs = new Intent(Jobs.this, Jobs.class);
							startActivity(jobs);
						}
					});
			alertDialog.setNegativeButton("Fortryd",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Intent jobs = new Intent(Jobs.this, Jobs.class);
							startActivity(jobs);
						}
					});
			alertDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (alertDialog != null) {
				((DialogInterface) alertDialog).dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class ShowAsyncPauseDialog extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(Jobs.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}
}