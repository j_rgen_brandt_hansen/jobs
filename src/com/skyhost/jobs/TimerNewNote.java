package com.skyhost.jobs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class TimerNewNote extends Activity implements OnClickListener {

	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timernewnote);

		new PauseDialogLoader().execute();

		// Initialize
		String displayTime = null;
		if (!(loadSetting("displayTime").equals(""))) {
			displayTime = loadSetting("displayTime");
		}

		setTitle(displayTime);
		final EditText addNote = (EditText) findViewById(R.id.addnote);
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(addNote, InputMethodManager.SHOW_IMPLICIT);
		Button addNewNoteButton = (Button) findViewById(R.id.add_note_button);
		addNewNoteButton.setOnClickListener(this);

		progressDialog.dismiss();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.timernewnote, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			Intent jobs = new Intent(TimerNewNote.this, TimerNewJob.class);
			startActivity(jobs);
			return true;

		case R.id.activity_timernewjob: // Job
			Intent job = new Intent(TimerNewNote.this, TimerNewJob.class);
			startActivity(job);
			return true;
		}
		return true;
	}

	@Override
	public void onPause() {
		super.onPause();

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.add_note_button) {

			new AsyncSaveTimerNewNote().execute();
		}
	}

	private class AsyncSaveTimerNewNote extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);
			String selectedRecId = null;
			if (!(loadSetting("selectedRecId").equals(""))) {
				selectedRecId = loadSetting("selectedRecId");

				final EditText addNote = (EditText) findViewById(R.id.addnote);

				WebApi.getWebApi().saveTimerNewNote(preferences, selectedRecId,
						addNote.getText().toString());
			}
			if (WebApi.getWebApi().isNewData_TimerNewNote() == false) {

				showToast("Din note blev ikke gemt! " + "Fejlkode: "
						+ WebApi.getWebApi().getResponse_ErrorCode(), WebApi
						.getWebApi().getResponse_ErrorText()
						+ " Pr�v igen senere!");

			} else if (WebApi.getWebApi().isNewData_Response() == false) {
				if (WebApi.getWebApi().getResponse_Status() != null) {
					if (!WebApi.getWebApi().getResponse_ErrorText().equals("")) {

						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");

						Intent intent = new Intent(TimerNewNote.this,
								TimerNewJob.class);
						startActivity(intent);
					}
					// Duplicate for if else check before remove
					else {

						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");

						Intent intent = new Intent(TimerNewNote.this,
								TimerNewJob.class);
						startActivity(intent);
					}
				}
			} else if (WebApi.getWebApi().isNewData_Response() == true) {
				if (WebApi.getWebApi().getResponse_Status().equals("OK")) {

					showToast("Din note er gemt", "");

					Intent intent = new Intent(TimerNewNote.this,
							TimerNewJob.class);
					startActivity(intent);
				}
			} else {
				Intent intent = new Intent(TimerNewNote.this, TimerNewJob.class);
				startActivity(intent);
			}
			Intent intent = new Intent(TimerNewNote.this, TimerNewJob.class);
			startActivity(intent);

			return null;
		}

		protected void showToast(final String sHeader, final String sComment) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sComment, Toast.LENGTH_LONG)
							.show();
				}
			});
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);

			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(TimerNewNote.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);

			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);

		}

	}
}