package com.skyhost.jobs;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterTimerNewJob extends ArrayAdapter<String> {

	private final Activity context;
	private int week;
	private int day;
	private static String[][][] headers;
	private static String[][][] subTitles;
	private String[][][] recIds;
	
	public ArrayAdapterTimerNewJob(Activity context, String[][][] headers,
			String[][][] subTitles, String[][][] recIds, int week, int day) {
		super(context, R.layout.rowlayout_timernewjob, headers[week][day]);

		this.context = context;
		this.headers = headers;
		this.subTitles = subTitles;
		this.recIds = recIds;
		this.week = week;
		this.day = day;
	}

	static class ViewHolder {

		public TextView textView_Header;
		public TextView textView_SubTitle;
		public TextView textView_RecId;
	}

	@Override
	public int getCount() {
		return headers[week][day].length;
	}

	@Override
	public String getItem(int position) {
		return headers[week][day][position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;

		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.rowlayout_timernewjob, null);
			holder = new ViewHolder();
			holder.textView_Header = (TextView) convertView
					.findViewById(R.id.timernewjob_title);
			holder.textView_SubTitle = (TextView) convertView
					.findViewById(R.id.timernewjob_normalhours);
			holder.textView_RecId = (TextView) convertView
					.findViewById(R.id.textViewRecId);
			convertView.setTag(holder);
		} else {
			view = convertView;
			holder = (ViewHolder) view.getTag();
		}
		if(headers[week][day][position] != null){
			holder.textView_Header.setText(headers[week][day][position]);
		}
		if(subTitles[week][day][position] != null){
			holder.textView_SubTitle.setText(subTitles[week][day][position]);	
		}
		if(recIds[week][day][position] != null){
			holder.textView_RecId.setText(recIds[week][day][position]);		
		}
		return convertView;
	}
}