package com.skyhost.jobs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class StatusSkyhost extends Activity implements OnClickListener,
		OnItemSelectedListener {

	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.status);

		// Pause
		new ShowAsyncPauseDialog().execute();

		// Initialize
		String[] status_Spinner_Array = new String[] { "Tildelt", "Startet",
				"Afventer", "Afsluttet" };

		Spinner statusSpinner = (Spinner) findViewById(R.id.status_spinner);
		ArrayAdapter statusAdapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, status_Spinner_Array);
		statusAdapter
				.setDropDownViewResource(R.layout.rowlayout_timerselecttime_spinnerhours);
		statusSpinner.setAdapter(statusAdapter);
		// status_spinner.setOnItemSelectedListener(this);
		Button change_status_button = (Button) findViewById(R.id.change_status_spinner_button);
		change_status_button.setOnClickListener(this);

		String selectedJobIndex = "";
		if (!(loadSetting("selectedJobIndex").equals(""))) {
			selectedJobIndex = loadSetting("selectedJobIndex");
		}

		statusSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {

				if (position == 0) {
					saveSetting("spinnerString", "assigned");
				} else if (position == 1) {
					saveSetting("spinnerString", "started");
				} else if (position == 2) {
					saveSetting("spinnerString", "waiting");
				} else if (position == 3) {
					saveSetting("spinnerString", "finished");
				}
				saveSetting("sStatusSpinnerPosition", String.valueOf(position));
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		int spinnerPosition = 0;

		if (!WebApi.getWebApi().getJobs_status().equals("")) {
			String spinnerString = WebApi.getWebApi().getJobs_status()[Integer
					.parseInt(selectedJobIndex)];
			saveSetting("originalSpinnerStatus", spinnerString);

			if (spinnerString.equals("assigned")) {
				spinnerPosition = 0;
			} else if (spinnerString.equals("started")) {
				spinnerPosition = 1;
			} else if (spinnerString.equals("waiting")) {
				spinnerPosition = 2;
			} else if (spinnerString.equals("finished")) {
				spinnerPosition = 3;
			}
		}

		ArrayAdapter positionAdapter = (ArrayAdapter) statusSpinner
				.getAdapter(); // cast to an ArrayAdapter
		statusSpinner.setSelection(spinnerPosition);
		progressDialog.dismiss();
	}

	@Override
	public void onPause() {
		super.onPause();

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.status, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			Intent jobs = new Intent(StatusSkyhost.this, Jobs.class);
			startActivity(jobs);
			return true;

		case R.id.activity_job: // Job
			Intent job = new Intent(StatusSkyhost.this, Job.class);
			startActivity(job);
			return true;

		case R.id.activity_hellogooglemaps: // Map
			saveSetting("sCommand", "allJobs");
			// saveSetting("allJobs", jsonArray.toString());
			Intent map = new Intent(StatusSkyhost.this, HelloGoogleMaps.class);
			startActivity(map);
			return true;

		case R.id.activity_newnote: // Add Note
			Intent newNote = new Intent(StatusSkyhost.this, NewNote.class);
			startActivity(newNote);
			return true;

		case R.id.activity_notes: // Notes
			Intent notes = new Intent(StatusSkyhost.this, Notes.class);
			startActivity(notes);
			return true;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.change_status_spinner_button) {

			new AsyncChangeStatus().execute();
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private class AsyncChangeStatus extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {
			// Initialize
			Intent navigateToJobs = new Intent(StatusSkyhost.this, Jobs.class);
			Spinner statusSpinner = (Spinner) findViewById(R.id.status_spinner);
			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			String spinnerString = "";
			if (!loadSetting("spinnerString").equals("")) {
				spinnerString = loadSetting("spinnerString");
			}

			String originalSpinnerStatus = "";
			if (!loadSetting("originalSpinnerStatus").equals("")) {
				originalSpinnerStatus = loadSetting("originalSpinnerStatus");
			}

			// Compare original status to new status, if same return to Jobs
			if (spinnerString.equals(originalSpinnerStatus)) {
				showToast("Intet �ndret");
				startActivity(navigateToJobs);
				return null;
			}

			String selectedJobId = "";
			if (!(loadSetting("selectedJobId").equals(""))) {
				selectedJobId = loadSetting("selectedJobId");
				WebApi.getWebApi().changeStatus(preferences, selectedJobId,
						spinnerString);
			}

			// Response - Error and Success handling
			Boolean bResponse = WebApi.getWebApi().isNewData_Response();
			if (WebApi.getWebApi().isNewData_Status() == false
					|| bResponse == false) {
				if (!WebApi.getWebApi().getResponse_ErrorText().equals("")
						|| !WebApi.getWebApi().getResponse_ErrorCode()
								.equals("")) {
					showToast("Fejlkode: "
							+ WebApi.getWebApi().getResponse_ErrorCode(),
							WebApi.getWebApi().getResponse_ErrorText()
									+ " Pr�v igen senere!");
				} else {
					showToast("Der skete en fejl -  Pr�v igen senere!");
				}
				startActivity(navigateToJobs);

			} else if (bResponse == true) {
				String sResponse = WebApi.getWebApi().getResponse_Status();
				if (sResponse.equals("OK")) {

					showToast("Status er �ndret");

					Intent navigateToSplashScreen = new Intent(
							StatusSkyhost.this, SplashScreen.class);
					startActivity(navigateToSplashScreen);
				}
			} else {
				showToast("Intet �ndret");
				startActivity(navigateToJobs);
			}

			return null;
		}

		protected void showToast(final String sHeader, final String SComment) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + SComment, Toast.LENGTH_LONG)
							.show();
				}
			});
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);

			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class ShowAsyncPauseDialog extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(StatusSkyhost.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}
}