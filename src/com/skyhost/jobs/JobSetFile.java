package com.skyhost.jobs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

@SuppressLint("ResourceAsColor")
public class JobSetFile extends Activity implements OnClickListener {

	private Bitmap bitmap;
	private Handler handler;
	private ProgressDialog progressDialog;
	private long timestamp_PushImgButton;
	private final int CAMERA_IMAGE_CAPTURE = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.jobsetfile);

		new PauseDialogLoader().execute();

		Button buttonChooseExistingImage = (Button) findViewById(R.id.jobsetfile_existing_resource_button);
		buttonChooseExistingImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent pickPhoto = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(pickPhoto, 1);
			}
		});

		Button buttonStartCamera = (Button) findViewById(R.id.jobsetfile_camera_button);
		buttonStartCamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// Create folder if not exists
				File folder = new File(Environment
						.getExternalStorageDirectory() + "/Skyhost");
				if (!folder.exists()) {
					folder.mkdir();
				}

				final Calendar c = Calendar.getInstance();

				String new_Date = c.get(Calendar.DAY_OF_MONTH) + "-"
						+ ((c.get(Calendar.MONTH)) + 1) + "-"
						+ c.get(Calendar.YEAR) + "_" + c.get(Calendar.HOUR)
						+ "-" + c.get(Calendar.MINUTE) + "-"
						+ c.get(Calendar.SECOND);
				// saveSetting("pathImages", pathImages);
				String pathImages = String.format(
						Environment.getExternalStorageDirectory()
								+ "/Skyhost/%s.jpg", "SkyhostPoi(" + new_Date
								+ ")");

				saveSetting("pathImages", pathImages);

				File photo = null;
				try {
					photo = new File(pathImages);

					photo.getParentFile().createNewFile();
				} catch (IOException ex) {
					ex.printStackTrace();
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				Intent cameraIntent = new Intent(
						MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(cameraIntent, 0);// zero can be replaced
														// with any action code
			}
		});

		Button buttonCreateAttachment = (Button) findViewById(R.id.jobsetfile_create_button);
		buttonCreateAttachment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// Catch double clickers
				long timeSinceLastPush = System.currentTimeMillis()
						- timestamp_PushImgButton;

				if (timeSinceLastPush < 1500) {
					return;
				}

				timestamp_PushImgButton = System.currentTimeMillis();
				new AsyncSetJobOrdernumberAndImageAttached().execute();
			}
		});
		handler = new Handler();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();

		if (progressDialog != null) {
			progressDialog.dismiss();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	protected void onActivityResult(int requestCode, int resultCode,
			Intent imageReturnedIntent) {
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

		ImageView imageViewUserImage = (ImageView) findViewById(R.id.imageView_tool_image);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		if (requestCode == CAMERA_IMAGE_CAPTURE && resultCode == RESULT_OK) {

			Uri selectedImage = imageReturnedIntent.getData();
			try {
				getContentResolver().openInputStream(selectedImage);

				try {
					Bitmap temporaryBitmap = decodeUri(selectedImage);

					ExifInterface exif = new ExifInterface(
							selectedImage.getPath());
					int orientation = exif.getAttributeInt(
							ExifInterface.TAG_ORIENTATION,
							ExifInterface.ORIENTATION_NORMAL);

					// Hardcoded value
					int angle = 90;

					if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
						angle = 90;
					} else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
						angle = 180;
					} else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
						angle = 270;
					}

					Matrix mat = new Matrix();
					mat.postRotate(angle);
					int w = temporaryBitmap.getWidth();
					int h = temporaryBitmap.getHeight();
					bitmap = Bitmap.createBitmap(temporaryBitmap, 0, 0,
							temporaryBitmap.getWidth(),
							temporaryBitmap.getHeight(), mat, true);

					Drawable d = new BitmapDrawable(getResources(), bitmap);
					scaleImage(imageViewUserImage, d);

					bitmap.compress(CompressFormat.JPEG, 85, bos);
					save(bos);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (OutOfMemoryError oom) {
					oom.printStackTrace();

				}

			} catch (FileNotFoundException e) {
				Toast.makeText(getApplicationContext(),
						"FilenotfoundException", 5000).show();
				e.printStackTrace();
			} catch (IOException e) {
				Toast.makeText(getApplicationContext(), "IOException", 5000)
						.show();
				e.printStackTrace();
			}
		}

		if (requestCode == 1 && resultCode == RESULT_OK) {
			Uri selectedImage = imageReturnedIntent.getData();

			try {
				bitmap = decodeUri(selectedImage);
				imageViewUserImage.setImageBitmap(bitmap);

				bitmap.compress(CompressFormat.JPEG, 85, bos);
				save(bos);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void save(ByteArrayOutputStream os) {
		FileOutputStream fos;
		String pathImages = loadSetting("pathImages");

		try {
			if (!pathImages.equals("")) {
				fos = new FileOutputStream(pathImages);
				os.writeTo(fos);
				os.flush();
				fos.flush();
				os.close();
				fos.close();
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(
				getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 800;
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		if (width_tmp > REQUIRED_SIZE) {
		} else if (height_tmp > REQUIRED_SIZE) {
		}

		int scale = 1;
		while (true) {
			if (width_tmp < REQUIRED_SIZE || height_tmp < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(
				getContentResolver().openInputStream(selectedImage), null, o2);
	}

	public static Bitmap decodeSampledBitmapFromResource(Resources res,
			int resId, int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee a final image with both dimensions larger than or equal
			// to the requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	private void scaleImage(ImageView imageView, Drawable drawable) {
		// Get the ImageView and its bitmap
		ImageView view = imageView;
		Drawable drawing = drawable;
		if (drawing == null) {
			return; // Checking for null & return, as suggested in comments
		}
		Bitmap bitmap = ((BitmapDrawable) drawing).getBitmap();

		// Get current dimensions AND the desired bounding box
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		int bounding = dpToPx(650);

		// Determine how much to scale: the dimension requiring less scaling is
		// closer to the its side. This way the image always stays inside your
		// bounding box AND either x/y axis touches it.
		float xScale = ((float) bounding) / width;
		float yScale = ((float) bounding) / height;
		float scale = (xScale <= yScale) ? xScale : yScale;

		// Create a matrix for the scaling and add the scaling data
		Matrix matrix = new Matrix();
		matrix.postScale(scale, scale);

		// Create a new bitmap and convert it to a format understood by the
		// ImageView
		Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height,
				matrix, true);
		width = scaledBitmap.getWidth(); // re-use
		height = scaledBitmap.getHeight(); // re-use
		BitmapDrawable result = new BitmapDrawable(scaledBitmap);

		// Apply the scaled bitmap
		view.setImageDrawable(result);

		// Now change ImageView's dimensions to match the scaled image
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view
				.getLayoutParams();
		params.width = android.support.v4.view.ViewPager.LayoutParams.FILL_PARENT;
		params.height = android.support.v4.view.ViewPager.LayoutParams.MATCH_PARENT;
		// params.width = width;
		// params.height = height;
		view.setLayoutParams(params);

	}

	private int dpToPx(int dp) {
		float density = getApplicationContext().getResources()
				.getDisplayMetrics().density;
		return Math.round((float) dp * density);
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	public static String getCurrentTimeStamp() {
		SimpleDateFormat sdfDate = new SimpleDateFormat(
				"EEEE dd. MMM yyyy HH:mm", new Locale("da", "DK"));
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

	public void setPowerSaveState(boolean enabled) {
		if (enabled == false)
			android.provider.Settings.System.putInt(getContentResolver(),
					android.provider.Settings.System.SCREEN_OFF_TIMEOUT, -1);
		else
			android.provider.Settings.System.putInt(getContentResolver(),
					android.provider.Settings.System.SCREEN_OFF_TIMEOUT, 60000);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent menuIntent = null;

		switch (item.getItemId()) {
		case R.id.activity_jobs:
			menuIntent = new Intent(JobSetFile.this, Jobs.class);
			break;
		}
		startActivity(menuIntent);
		return true;
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private int loadSettingInt(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		int content = preferences.getInt(tag, 0);
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private void saveSetting(String tag, Boolean content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(tag, content);
		editor.commit();
	}

	public void showToast(final String toast) {
		runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(JobSetFile.this, toast, Toast.LENGTH_SHORT)
						.show();
			}
		});
	}

	// ---------- Hj�lpeklasser ----------

	// TODO Find correct way to attach image
	private class AsyncSetJobOrdernumberAndImageAttached extends
			AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			int selectedJobIndex = -1;
			if (!(loadSetting("selectedJobIndex").equals(""))) {
				selectedJobIndex = Integer
						.parseInt(loadSetting("selectedJobIndex"));
			}

			String selectedBeginDate = "";
			if (!(WebApi.getWebApi().getJobs_beginTime()[selectedJobIndex])
					.equals("")) {
				String begin = WebApi.getWebApi().getJobs_beginTime()[selectedJobIndex];
				selectedBeginDate = begin.replaceAll("[-: ]+", "");
			}

			String selectedEndDate = "";
			if (!(WebApi.getWebApi().getJobs_endTime()[selectedJobIndex])
					.equals("")) {
				String end = WebApi.getWebApi().getJobs_endTime()[selectedJobIndex];
				selectedEndDate = end.replaceAll("[-: ]+", "");
			}

			String selectedJobId = "";
			if (!(loadSetting("selectedJobId").equals(""))) {
				selectedJobId = loadSetting("selectedJobId");
			}

			String imagePath = "";
			if (!(loadSetting("pathImages").equals(""))) {
				imagePath = loadSetting("pathImages");
			}

			// If fields are not null or empty
			if (!selectedJobId.equals("")) {

				SharedPreferences preferences = getSharedPreferences(
						"PREFERENCES", Activity.MODE_PRIVATE);

				WebApi.getWebApi().setJobTimeAndImage(preferences,
						selectedJobId, selectedBeginDate, selectedEndDate,
						imagePath);
			}

			// Error sending
			if (WebApi.getWebApi().isNewData_Jobs_SetJobTime() == false) {
				showToast("Fejlkode: "
						+ WebApi.getWebApi().getResponse_ErrorCode(), WebApi
						.getWebApi().getResponse_ErrorText()
						+ " Pr�v igen senere!");

				Intent status = new Intent(JobSetFile.this, Job.class);
				startActivity(status);
			}
			// Success sending
			else if (WebApi.getWebApi().isNewData_Jobs_SetJobTime() == true) {
				String errorHeader = WebApi.getWebApi()
						.getResponse_ErrorHeader();

				// Success answer
				if (errorHeader.equals("OK")) {
					showToast("Din registrering er gemt");
					saveSetting("reloadJob", true);
					saveSetting("editState", false);
					Intent intent = new Intent(JobSetFile.this, Job.class);
					startActivity(intent);
				}
				// Error in answer
				else if (WebApi.getWebApi().getResponse_Status()
						.equals("Error")) {
					if (!WebApi.getWebApi().getResponse_ErrorText().equals("")) {
						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");

						Intent intent = new Intent(JobSetFile.this, Job.class);
						startActivity(intent);
					} else {
						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");

						Intent intent = new Intent(JobSetFile.this, Job.class);
						startActivity(intent);
					}
				}

			} else {
				Intent intent = new Intent(JobSetFile.this, Job.class);
				startActivity(intent);
			}
			return null;
		}

		protected void showToast(final String sHeader, final String sComment) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sComment, Toast.LENGTH_LONG)
							.show();
				}
			});
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}
	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(JobSetFile.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(true);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}
	}

	@Override
	public void onClick(View v) {
	}
}