package com.skyhost.jobs;

import java.util.Map;

import android.os.Bundle;
import android.provider.Settings;
import android.app.Activity;
import android.content.Intent;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Routes extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.routes);
		((TextView) ((FrameLayout) ((LinearLayout) ((ViewGroup) getWindow()
				.getDecorView()).getChildAt(0)).getChildAt(0)).getChildAt(0))
				.setGravity(Gravity.CENTER);
		setTitle("Ruter");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.routes, menu);
		return true;
	}

//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		switch (item.getItemId()) {
//		case R.id.activity_jobs: // Jobs
//			Intent jobs = new Intent(Jobs.this, Jobs.class);
//			startActivity(jobs);
//			return true;
//
//		case R.id.activity_job: // Job
//			Intent job = new Intent(Jobs.this, Job.class);
//			startActivity(job);
//			return true;
//
//		case R.id.activity_hellogooglemaps: // Map
//			saveSetting("sCommand", "allJobs");
//			saveSetting("allJobs", jsonArray.toString());
//			Intent map = new Intent(Jobs.this, HelloGoogleMaps.class);
//			startActivity(map);
//			return true;
//
//		case R.id.activity_newnote: // Add Note
//			Intent newNote = new Intent(Jobs.this, NewNote.class);
//			startActivity(newNote);
//			return true;
//
//		case R.id.activity_notes: // Notes
//			Intent notes = new Intent(Jobs.this, Notes.class);
//			startActivity(notes);
//			return true;
//
//		case R.id.activity_routes: // Routes
//			Intent i = new Intent(Jobs.this, Routes.class);
//			startActivity(i);
//			return true;
//
//		case R.id.activity_status: // Status
//			Intent status = new Intent(Jobs.this, Status.class);
//			startActivity(status);
//			return true;
//			//
//		case R.id.activity_settings: // Settings
//			Intent settings = new Intent(Jobs.this, Settings.class);
//			startActivity(settings);
//		}
//		return true;
//	}

}
