package com.skyhost.jobs;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class Status extends Activity implements OnClickListener, OnItemSelectedListener{
	private Spinner statusSpinner;
	private int statusSpinnerPosition;
	private ArrayAdapter statusAdapter;
	private String selectedJobId;
	private String selectedJobIndex;
	private SharedPreferences sharedPreferences;
	private String spinnerString;
	private int spinnerPosition;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.status);
		
		// Centrer titel
		ViewGroup decorView= (ViewGroup) this.getWindow().getDecorView(); 
		LinearLayout root= (LinearLayout) decorView.getChildAt(0); 
		FrameLayout titleContainer= (FrameLayout) root.getChildAt(0); 
		TextView title= (TextView) titleContainer.getChildAt(0); 
		title.setGravity(Gravity.CENTER);
		setTitle("Status");
		
		String status_Spinner_Array[] = new String[]{"Tildelt", "Startet", "Afventer", "Afsluttet"};
		
		statusSpinner = (Spinner) findViewById(R.id.status_spinner);
		statusAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, status_Spinner_Array);
		statusSpinner.setAdapter(statusAdapter);
		//status_spinner.setOnItemSelectedListener(this);
		Button change_status_button = (Button)findViewById(R.id.change_status_spinner_button);
		change_status_button.setOnClickListener(this);
	
		statusSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		    	if(position == 0){
					spinnerString = "assigned";
				}
				else if(position == 1){
					spinnerString = "started";
				}
				else if(position == 2){
					spinnerString = "waiting";
				}
				else if(position == 3) {
					spinnerString = "finished";
				}
		        SharedPreferences preferences = getSharedPreferences(Jobs.PREFERENCES,
						Activity.MODE_PRIVATE);
				selectedJobId = null;
				if (!(loadSettingSP("selectedJobId").equals(""))) {
					selectedJobId = loadSettingSP("selectedJobId");
					WebApi.getWebApi().changeStatus(preferences, selectedJobId, spinnerString );
				}
				 statusSpinnerPosition = position;
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) {
		        // your code here
		    }

		});
		selectedJobIndex = null;
		if (!(loadSettingSP("selectedJobIndex").equals(""))) {
			selectedJobIndex = loadSettingSP("selectedJobIndex");
		}
		if(!WebApi.getWebApi().getJobs_status().equals("")){
			spinnerString = WebApi.getWebApi().getJobs_status()[Integer.parseInt(selectedJobIndex)];
			if(spinnerString.equals("assigned")){
				spinnerPosition = 0;
			}
			else if(spinnerString.equals("started")){
				spinnerPosition = 1;
			}
			else if(spinnerString.equals("waiting")){
				spinnerPosition = 2;
			}
			else if (spinnerString.equals("finished")){
				spinnerPosition = 3;
			}
		}
		
		ArrayAdapter positionAdapter = (ArrayAdapter) statusSpinner.getAdapter(); //cast to an ArrayAdapter

		//int spinnerPosition = statusAdapter.getPosition(spinnerString);

		//set the default according to value
		statusSpinner.setSelection(spinnerPosition);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.status, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			Intent jobs = new Intent(Status.this, Jobs.class);
			startActivity(jobs);
			return true;

		case R.id.activity_job: // Job
			Intent job = new Intent(Status.this, Job.class);
			startActivity(job);
			return true;

		case R.id.activity_hellogooglemaps: // Map
			saveSetting("sCommand", "allJobs");
			//saveSetting("allJobs", jsonArray.toString());
			Intent map = new Intent(Status.this, HelloGoogleMaps.class);
			startActivity(map);
			return true;

		case R.id.activity_newnote: // Add Note
			Intent newNote = new Intent(Status.this, NewNote.class);
			startActivity(newNote);
			return true;

		case R.id.activity_notes: // Notes
			Intent notes = new Intent(Status.this, Notes.class);
			startActivity(notes);
			return true;

		case R.id.activity_routes: // Routes
			Intent i = new Intent(Status.this, Routes.class);
			startActivity(i);
			return true;

		case R.id.activity_status: // Status
			Intent status = new Intent(Status.this, Status.class);
			startActivity(status);
			return true;
			//
		case R.id.activity_settings: // Settings
			Intent settings = new Intent(Status.this, Settings.class);
			startActivity(settings);
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.change_status_spinner_button:
			String item = statusSpinner.getSelectedItem().toString();
			statusSpinner.setSelection(statusSpinnerPosition);
			Intent status = new Intent(Status.this, Job.class);
			startActivity(status);
			break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}

	private String loadSettingSP(String tag) {
		sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		String content = sharedPreferences.getString(tag, "");
		return content;
	}
	
	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences(Jobs.PREFERENCES,
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}
}
