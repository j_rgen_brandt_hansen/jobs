package com.skyhost.jobs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TimerNewJob extends ListActivity implements OnClickListener,
		OnCreateContextMenuListener {

	private Handler handler;
	private ProgressDialog progressDialog;

	private static final int DIALOG_ALERT = 10;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timernewjob);

		new PauseDialogLoader().execute();

		// Initializing
		TextView textView_OrderNumber = (TextView) findViewById(R.id.textViewOrderNumbers);
		textView_OrderNumber.setText("Ordrenumre:");
		textView_OrderNumber.setPadding(5, 20, 5, 0);
		textView_OrderNumber.setTextSize(20);
		textView_OrderNumber.setTypeface(null, Typeface.BOLD);
		textView_OrderNumber.setTextColor(getResources()
				.getColor(R.color.black));
		Button newTimerSelectJob = (Button) findViewById(R.id.button_addnewjob);
		newTimerSelectJob.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.button_addnewjob:
					Intent intent = new Intent(TimerNewJob.this,
							TimerSelectJob.class);
					startActivity(intent);
				}
			}
		});

		final ListView listView = getListView();
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int position, long arg3) {
				registerForContextMenu(listView);
				view.showContextMenu();
			}
		});
		handler = new Handler();
	}

	private void weekAndDayNotNull() {
		// String[][] temp = WebApi.getWebApi().getTimer_RegSums();
		if (WebApi.getWebApi().getTimer_RegSums() != null) {

			int day = -1;
			if (!(loadSetting("day").equals(""))) {
				day = Integer.parseInt(loadSetting("day"));
			}

			int week = -1;
			if (!(loadSetting("week").equals(""))) {
				week = Integer.parseInt(loadSetting("week"));
			}

			TextView textView_Sum = (TextView) findViewById(R.id.textViewSum);
			textView_Sum.setTextColor(getResources().getColor(R.color.black));

			if (WebApi.getWebApi().getTimer_RegSums()[week][day] == null) {
				textView_Sum.setText("Sum: 0");
				setDateAsTitle();
			} else {
				textView_Sum.setText("Sum: "
						+ WebApi.getWebApi().getTimer_RegSums()[week][day]);
				setDateAsTitle();
				final ListView listView = getListView();
				listView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View view,
							int position, long arg3) {
						registerForContextMenu(listView);
						view.showContextMenu();
					}
				});
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		if (handler != null) {
			handler.removeCallbacks(runUpdateTask);
		}

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		int week = -1;
		if (!(loadSetting("week").equals(""))) {
			week = Integer.parseInt(loadSetting("week"));
		}

		int day = -1;
		if (!(loadSetting("day").equals(""))) {
			day = Integer.parseInt(loadSetting("day"));
		}

		if (week != -1 || day != -1) {
			weekAndDayNotNull();
		}
		setDateAsTitle();
		if (handler != null) {
			handler.postDelayed(runUpdateTask, 100);
		}

	}

	private Runnable runUpdateTask = new Runnable() {
		public void run() {

			if (WebApi.getWebApi().isNewData_Timer() == true) {
				updateScreen();

				SharedPreferences preferences = getSharedPreferences(
						"PREFERENCES", Activity.MODE_PRIVATE);

				if (preferences.getBoolean("AutoUpdate", false) == true) {
					handler.postDelayed(runUpdateTask, 30000);
				}
			} else {
				handler.postDelayed(runUpdateTask, 3000);
			}
		}
	};

	private void setDateAsTitle() {
		// Initialiser now og endTime
		Calendar calendar = Calendar.getInstance();
		Date now = new Date();
		calendar.setTime(now);

		int day = -1;
		if (!(loadSetting("day").equals(""))) {
			day = Integer.parseInt(loadSetting("day"));
		}

		int week = -1;
		if (!(loadSetting("week").equals(""))) {
			week = Integer.parseInt(loadSetting("week"));
		}

		String input_date = WebApi.getWebApi().getTimer_Days()[week][day];
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
		inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date parsed = null;
		try {
			parsed = inputFormat.parse(input_date);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		DateFormat format2 = new SimpleDateFormat("EEEE dd. MMM yyyy",
				new Locale("da", "DK"));

		String displayTime = format2.format(parsed);
		displayTime = displayTime.substring(0, 1).toUpperCase()
				+ displayTime.substring(1);
		saveSetting("displayTime", displayTime);
		String outputText = outputFormat.format(parsed);

		String selectedDate = outputText.substring(0, 4);
		selectedDate += outputText.substring(5, 7);
		selectedDate += outputText.substring(8, 10);

		saveSetting("selectedDate", selectedDate);

		// Dato p� overskrift
		setTitle(displayTime);
	}

	private void updateScreen() {

		int day = -1;
		if (!(loadSetting("day").equals(""))) {
			day = Integer.parseInt(loadSetting("day"));
		}

		int week = -1;
		if (!(loadSetting("week").equals(""))) {
			week = Integer.parseInt(loadSetting("week"));
		}

		this.setListAdapter(new ArrayAdapterTimerNewJob(this, WebApi
				.getWebApi().getTimer_JobName(), WebApi.getWebApi()
				.getTimer_SubTitles(), WebApi.getWebApi().getTimer_RecIds(),
				week, day));

		if (progressDialog != null) {
			progressDialog.hide();
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		long selelectedRow = info.id;
		Integer position = (int) (long) selelectedRow;

		int day = -1;
		if (!(loadSetting("day").equals(""))) {
			day = Integer.parseInt(loadSetting("day"));
		}

		int week = -1;
		if (!(loadSetting("week").equals(""))) {
			week = Integer.parseInt(loadSetting("week"));
		}

		// String recId =
		// WebApi.getWebApi().getTimer_RecIds()[week][day][position];
		saveSetting("selectedRecId",
				WebApi.getWebApi().getTimer_RecIds()[week][day][position]);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.timernewjob_optional_menu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.activity_delete: // Jobs
			showDialog(DIALOG_ALERT);
			return true;

		case R.id.activity_addnote: // Jobs

			Intent newNote = new Intent(TimerNewJob.this, TimerNewNote.class);
			startActivity(newNote);
			return true;

		case R.id.settings_linearlayout_phone_producer: // Add Material on Job

			Intent navigateToAddMaterialsOnJob = new Intent(TimerNewJob.this,
					MaterialsQuickScrollListView.class);
			startActivity(navigateToAddMaterialsOnJob);
			return true;

		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_ALERT:
			// create out AlterDialog
			Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Jobbet slettes!");
			builder.setCancelable(true);

			builder.setPositiveButton("Slet",
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {

							new AsyncLoaderDeleteJob().execute();
						}
					});
			builder.setNegativeButton("Nej, annuller",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
		return super.onCreateDialog(id);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.timernewjob, menu);
		return true;
	}

	public boolean optionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.timernewjob_optional_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_timer: // Jobs
			Intent timer = new Intent(TimerNewJob.this, Timer.class);
			startActivity(timer);
			return true;
		}
		return true;
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	@Override
	public void onClick(View v) {
	}

	private class AsyncLoaderDeleteJob extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(TimerNewJob.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();

			// Set Runnable to remove splash screen just in case
			final Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					progressDialog.dismiss();
				}
			}, 5000);
		}

		protected Void doInBackground(Void... arg0) {

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			String recId = "";
			if (!loadSetting("selectedRecId").equals("")) {
				recId = loadSetting("selectedRecId");
			}

			WebApi.getWebApi().deleteTimerNewNote(preferences, recId);
			WebApi.getWebApi().update_Timer(preferences);

			if (WebApi.getWebApi().getResponse_Command().equals("time=delete")
					&& WebApi.getWebApi().getResponse_Status().equals("OK")) {

				showToast("Jobbet er slettet");

				weekAndDayNotNull();
				handler.postDelayed(runUpdateTask, 3000);
			} else if (WebApi.getWebApi().getResponse_Command()
					.equals("time=delete")
					&& WebApi.getWebApi().getResponse_Status().equals("ERROR")) {
				showToast("Der skete en fejl - pr�v igen !!");

			}
			return null;
		}

		protected void showToast(final String sComment) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sComment,
							Toast.LENGTH_LONG).show();
				}

			});
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);

			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(TimerNewJob.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);

			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}
}