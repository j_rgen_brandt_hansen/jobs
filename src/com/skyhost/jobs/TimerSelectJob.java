package com.skyhost.jobs;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

public class TimerSelectJob extends ListActivity implements
		OnCreateContextMenuListener {

	private Handler handler = new Handler();
	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timerselectjob);

		new PauseDialogLoader().execute();

		// Initializing
		int week = -1;
		if (!(loadSetting("week").equals(""))) {
			week = Integer.parseInt(loadSetting("week"));
		}

		int day = -1;
		if (!(loadSetting("day").equals(""))) {
			day = Integer.parseInt(loadSetting("day"));
		}

		// Initialiser now og endTime
		Calendar calendar = Calendar.getInstance();
		Date now = new Date();
		calendar.setTime(now);
		String input_date = WebApi.getWebApi().getTimer_Days()[week][day];
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");

		Date endTime = null;

		try {
			endTime = format1.parse(input_date);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}

		DateFormat format2 = new SimpleDateFormat("EEEE dd. MMM yyyy",
				new Locale("da", "DK"));

		String displayTime = format2.format(endTime);
		displayTime = displayTime.substring(0, 1).toUpperCase()
				+ displayTime.substring(1);
		saveSetting("displayTime", displayTime);

		// Dato p� overskrift
		setTitle(displayTime + " - V�lg Job");
		ListView listView = getListView();

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int position, long arg3) {

				String id = WebApi.getWebApi().getTimer_JobIdsOnDay()[position];
				String title = WebApi.getWebApi().getTimer_JobsOnDay()[position];
				saveSetting("intern", "false");
				saveSetting("selectedJobIdOnDay", id);
				saveSetting("selectedJobOnDay", title);
				// mHandler.post(pauseDialog);
				Intent progressBar = new Intent(TimerSelectJob.this,
						TimerSelectRoute.class);
				startActivity(progressBar);
			}
		});

		String input_date1 = WebApi.getWebApi().getTimer_Days()[week][day];
		// "Z" appears not to be supported for some reason.
		DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
		inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		// SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date parsed = null;
		try {
			parsed = inputFormat.parse(input_date1);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		handler = new Handler();
	}

	private Runnable runUpdateTask = new Runnable() {
		public void run() {

			new AsyncUpdateTimer().execute();
		}
	};

	@Override
	public void onPause() {
		super.onPause();
		if (handler != null) {
			handler.removeCallbacks(runUpdateTask);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (handler != null) {
			handler.postDelayed(runUpdateTask, 100);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		long selelectedRow = info.id;
		Integer position = (int) (long) selelectedRow;

		int week = -1;
		if (!(loadSetting("week").equals(""))) {
			week = Integer.parseInt(loadSetting("week"));
		}

		int day = -1;
		if (!(loadSetting("day").equals(""))) {
			day = Integer.parseInt(loadSetting("day"));
		}

		// String recId =
		// WebApi.getWebApi().getTimer_RecIds()[week][day][position];
		saveSetting("selectedRecId",
				WebApi.getWebApi().getTimer_RecIds()[week][day][position]);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.timernewjob_optional_menu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.activity_delete: // Jobs

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			String recId = "";
			if (!loadSetting("selectedRecId").equals("")) {
				recId = loadSetting("selectedRecId");
			}

			WebApi.getWebApi().deleteTimerNewNote(preferences, recId);
			WebApi.getWebApi().update_Timer(preferences);

			int week = -1;
			if (!(loadSetting("week").equals(""))) {
				week = Integer.parseInt(loadSetting("week"));
			}

			int day = -1;
			if (!(loadSetting("day").equals(""))) {
				day = Integer.parseInt(loadSetting("day"));
			}

			if (WebApi.getWebApi().getTimer_RecIds()[week][day] == null) {
				Intent backToTimerNewJob = new Intent(TimerSelectJob.this,
						TimerNewJob.class);
				startActivity(backToTimerNewJob);
			} else {
				ViewGroup vg = (ViewGroup) findViewById(R.id.linearlayout_timernewjob);
				vg.removeAllViews();
				vg.refreshDrawableState();
			}
			return true;

		case R.id.activity_addnote: // Jobs
			Intent newNote = new Intent(TimerSelectJob.this, TimerNewNote.class);
			startActivity(newNote);
			return true;

		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.timernewjob, menu);
		return true;
	}

	public boolean optionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.timernewjob_optional_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_timer: // Jobs
			Intent timer = new Intent(TimerSelectJob.this, Timer.class);
			startActivity(timer);
			return true;
		}
		return true;
	}

	private void updateScreen() {

		final ArrayAdapterTimerSelectJob adapter = new ArrayAdapterTimerSelectJob(
				this, WebApi.getWebApi().getTimer_JobsOnDay(), WebApi
						.getWebApi().getTimer_OrdernumbersOnDay(), WebApi
						.getWebApi().getTimer_JobIdsOnDay());
		runOnUiThread(new Runnable() {

			@Override
			public void run() {

				try {
					setListAdapter(adapter);

					if (progressDialog != null) {
						progressDialog.hide();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});

	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private class AsyncUpdateTimer extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);
			try {

				String selectedDate = "";
				if (!(loadSetting("selectedDate").equals(""))) {
					selectedDate = loadSetting("selectedDate");
				}

				WebApi.getWebApi().update_Timer_JobsOnDay(preferences,
						selectedDate);

			} catch (Exception ex) {
			}

			if (WebApi.getWebApi().isNewData_Timer_SelectJobsOnDateUrl() == true) {

				// Hvis der ikke er jobs, g� til k�rsler
				if (WebApi.getWebApi().getTimer_JobIdsOnDay().length == 0) {

					Intent goToTimerSelectRoute = new Intent(
							TimerSelectJob.this, TimerSelectRoute.class);
					startActivity(goToTimerSelectRoute);
				} else {

					// Hvis der er jobs
					updateScreen();
					if (preferences.getBoolean("AutoUpdate", false) == true) {
						handler.postDelayed(runUpdateTask, 30000);
					}
					return null;
				}
				// check
				// updateScreen();
				// if (preferences.getBoolean("AutoUpdate", false) == true) {
				// handler.postDelayed(runUpdateTask, 30000);
				// }
			} else {
				handler.postDelayed(runUpdateTask, 3000);
			}

			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);

			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(TimerSelectJob.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();

			// // Set Runnable to remove splash screen just in case
			// final Handler handler = new Handler();
			// handler.postDelayed(new Runnable() {
			// @Override
			// public void run() {
			// progressDialog.dismiss();
			// }
			// }, 5000);
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);

			if (progressDialog != null) {
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

	}
}