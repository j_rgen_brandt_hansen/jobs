package com.skyhost.jobs;

import java.util.HashMap;
import java.util.Map;
import android.R.color;
import android.app.Activity;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class ArrayAdapterTimerSelectRoute extends ArrayAdapter<String> {
	private final Activity context;
	private Map positions;
	private String[] stringList;
	private String[] headers;
	private String[] subtitles;
	
	public static final String PREFERENCES = "PREFERENCES";
	public static HashMap<Integer, String> mapAfter;
	public ArrayAdapterTimerSelectRoute(Activity context, String[] ids,
			String[] headers, String[] subtitles) {
		super(context, R.layout.rowlayout_timerselectroute, headers);
	
		this.context = context;
		this.headers = headers;
		this.subtitles = subtitles;

		this.stringList = headers;
		positions = new HashMap();
		int length = headers.length;
		for (int i = 0; i < headers.length; i++) {
			positions.put(i, false);
		}
	}

	static class ViewHolder {
		public LinearLayout row;
		public TextView textView_Id;
		public TextView textView_Header;
		public TextView textView_Subtitle;
		public CheckedTextView check;
	}

	@Override
	public int getCount() {
		return stringList.length;
	}

	@Override
	public String getItem(int position) {
		return stringList[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;

		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.rowlayout_timerselectroute, null);
			holder = new ViewHolder();
			holder.row = (LinearLayout) convertView
					.findViewById(R.id.linearlayout_timernewjob);
			holder.textView_Id = (TextView) convertView
					.findViewById(R.id.timerselecttrip_id);
			holder.textView_Header = (TextView) convertView
					.findViewById(R.id.timerselecttrip_header);
			holder.textView_Subtitle = (TextView) convertView
					.findViewById(R.id.timerselecttrip_subtitle);
			holder.check = (CheckedTextView) convertView
					.findViewById(R.id.textView_check);
			convertView.setTag(holder);
		} else {
			view = convertView;
			holder = (ViewHolder) view.getTag();
		}
		holder.textView_Header.setText(headers[position]);

		// set selected item
		LinearLayout ActiveItem = (LinearLayout) (LinearLayout) convertView
				.findViewById(R.id.linearlayout_timernewjob);
		
		SparseBooleanArray sparseBooleanArray = TimerSelectRoute.sparseBooleanArray;

		if(position <= sparseBooleanArray.size()-1) {
			if (sparseBooleanArray.valueAt(position) == true) {
				int pos = sparseBooleanArray.keyAt(position);

				positions.put(pos, true);
			} else {
				int pos = sparseBooleanArray.keyAt(position);

				positions.put(pos, false);
			}
		}

		if (positions.get(position).equals(true)) {

			ActiveItem.setBackgroundResource(R.color.green_dark_skyhost);
			((CheckedTextView) holder.check).setChecked(true);
			// for focus on it
			int top = (ActiveItem == null) ? 0 : ActiveItem.getTop();
			((ListView) parent).setSelectionFromTop(position, top);
		} else {
			ActiveItem.setBackgroundResource(color.transparent);
			((CheckedTextView) holder.check).setChecked(false);
		}

		holder.textView_Subtitle.setText(subtitles[position]);
		return convertView;
	}
}