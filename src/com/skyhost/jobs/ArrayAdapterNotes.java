package com.skyhost.jobs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ArrayAdapterNotes extends ArrayAdapter<String> {

	private static Activity context;
	private static String[] jobs_notes_names;
	private static String[] jobs_notes_texts;
	private static String[] jobs_notes_timestamps;

	private Date previousDate = new Date(2000, 1, 1);

	static int selectedNoteIndex;

	public ArrayAdapterNotes(Activity context2, String[] jobs_Notes_Names,
			String[] jobs_Notes_Texts, String[] jobs_Notes_Timestamps) {
		super(context2, R.layout.rowlayout_notes, jobs_Notes_Names);

		this.context = context2;
		this.jobs_notes_names = jobs_Notes_Names;
		this.jobs_notes_texts = jobs_Notes_Texts;
		this.jobs_notes_timestamps = jobs_Notes_Timestamps;
	}

	static class ViewHolder {
		public LinearLayout header;
		public TextView names;
		public TextView texts;
		public TextView timestamps;
		public TextView time;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.rowlayout_notes, null, true);
			holder = new ViewHolder();
			holder.header = (LinearLayout) rowView
					.findViewById(R.id.notes_header_timestamp);
			holder.names = (TextView) rowView.findViewById(R.id.notes_name);
			holder.texts = (TextView) rowView.findViewById(R.id.notes_text);
			holder.timestamps = (TextView) rowView
					.findViewById(R.id.notes_timestamp);
			holder.time = (TextView) rowView.findViewById(R.id.notes_time);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		Calendar calendar = Calendar.getInstance();
		Date now = new Date();
		calendar.setTime(now);
		String input_date = jobs_notes_timestamps[position];
		SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date endTime = null;
		try {
			endTime = format1.parse(input_date);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}

		DateFormat format2 = new SimpleDateFormat("EEEE dd. MMM yyyy",
				new Locale("da", "DK"));
		String weekDay = null;
		DateFormat timeFormat = new SimpleDateFormat("HH:mm");
		String time = null;

		weekDay = format2.format(endTime);
		weekDay = weekDay.substring(0, 1).toUpperCase() + weekDay.substring(1);

		time = timeFormat.format(endTime);

		holder.timestamps.setText(weekDay);

		holder.time.setText(time);
		holder.names.setText(jobs_notes_names[position]);
		holder.texts.setText(jobs_notes_texts[position]);

		// Dato bar laver een overskrift per emne
		if (position == 0) {
			holder.header.setVisibility(View.VISIBLE);
			previousDate = endTime;
		} else {
			// Sp�rg p� emnet f�r PreviousDate - er de ens eller ikke
			String[] temp = WebApi.getWebApi().getJobs_endTime();
			String pos = jobs_notes_timestamps[position - 1];
			String pos1 = jobs_notes_timestamps[position];
			if (jobs_notes_timestamps[position - 1] != null) {
				if (jobs_notes_timestamps[position - 1].substring(0, 10)
						.equals(jobs_notes_timestamps[position]
								.substring(0, 10))) {
					holder.header.setVisibility(View.GONE);
				} else {
					holder.header.setVisibility(View.VISIBLE);
				}
			}
		}
		return rowView;
	}
}