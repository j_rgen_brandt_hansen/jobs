package com.skyhost.jobs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class JobSetOrderNumber extends Activity implements OnClickListener {

	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.jobsetordernumber);

		new PauseDialogLoader().execute();

		// Initializing
		EditText editText = (EditText) findViewById(R.id.jobsetordernumber_edittext);
		editText.setOnClickListener(this);
		editText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					saveAndGoBack();
					return true;
				}
				return false;
			}
		});

		// Keyboard
		InputMethodManager imm = (InputMethodManager) this
				.getSystemService(Service.INPUT_METHOD_SERVICE);
		imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);

		Button buttonSave = (Button) findViewById(R.id.jobsetordernumber_button_save);
		buttonSave.setOnClickListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		if (progressDialog != null) {
			progressDialog.hide();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			Intent jobs = new Intent(JobSetOrderNumber.this, Jobs.class);
			startActivity(jobs);
			return true;

		case R.id.activity_timer: // Job
			Intent timer = new Intent(JobSetOrderNumber.this, Timer.class);
			startActivity(timer);
			return true;
		}
		return true;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private void saveSetting(String tag, boolean content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(tag, content);
		editor.commit();
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.jobsetordernumber_button_save) {

			saveAndGoBack();
		}
	}

	private void saveAndGoBack() {
		EditText editText = (EditText) findViewById(R.id.jobsetordernumber_edittext);
		String tmp = editText.getText().toString();

		if (tmp.equals("")) {
			Intent intent = new Intent(JobSetOrderNumber.this, Job.class);
			startActivity(intent);
		} else {

			new AsyncSetJobOrderNumber().execute();

			// Set here if background thread updates too slow
			saveSetting("reloadJobs", true);
			saveSetting("setJobOrderNumber", true);

			// Go to Jobs
			// If go to Job: field will occationally not update
			Intent intent = new Intent(JobSetOrderNumber.this, Jobs.class);
			startActivity(intent);
		}
	}

	private class AsyncSetJobOrderNumber extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			EditText editText = (EditText) findViewById(R.id.jobsetordernumber_edittext);
			String temp = editText.getText().toString();

			String orderNumber = "";
			// TODO
			// orderNumber = URLEncoder.encode(temp, "UTF-8");

			final String ALLOWED_URI_CHARS = ".,";
			orderNumber = Uri.encode(temp, ALLOWED_URI_CHARS);

			String selectedJobId = "";
			if (!(loadSetting("selectedJobId").equals(""))) {
				selectedJobId = loadSetting("selectedJobId");
			}

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			WebApi.getWebApi().setJobOrderNumber(preferences, selectedJobId,
					orderNumber);

			// Error sending
			if (WebApi.getWebApi().isNewData_Jobs_SetJobOrderNumber() == false) {
				showToast("Fejlkode: "
						+ WebApi.getWebApi().getResponse_ErrorCode(), WebApi
						.getWebApi().getResponse_ErrorText()
						+ " Pr�v igen senere!");
				saveSetting("reloadJobs", false);
				saveSetting("setJobOrderNumber", false);
			}

			// Success sending
			else if (WebApi.getWebApi().isNewData_Jobs_SetJobOrderNumber() == true) {
				String errorHeader = WebApi.getWebApi()
						.getResponse_ErrorHeader();

				// Success answer
				if (errorHeader.equals("OK")) {
					showToast("Din registrering er gemt");
					saveSetting("reloadJobs", true);
					saveSetting("setJobOrderNumber", true);
				}

				// Error in answer
				else if (WebApi.getWebApi().getResponse_Status()
						.equals("Error")) {
					if (!WebApi.getWebApi().getResponse_ErrorText().equals("")) {
						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");
					} else {
						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");
					}
				}
			}

			return null;
		}

		protected void showToast(final String sHeader, final String sComment) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sComment, Toast.LENGTH_LONG)
							.show();
				}
			});
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(JobSetOrderNumber.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}
}