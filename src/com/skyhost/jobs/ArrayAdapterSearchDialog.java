package com.skyhost.jobs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;
import org.json.JSONArray;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ArrayAdapterSearchDialog extends ArrayAdapter<String> {
	
	private static Context context = null;
	private static String[] jobs_notes_names;
	private String[] jobs_notes_texts;
	private String[] jobs_notes_timestamps;

	public ArrayAdapterSearchDialog(Activity context, String[] search_String) {
		super(context, R.layout.rowlayout_search_dialog, jobs_notes_names);
		this.context = context;
	}

	// static to save the reference to the outer class and to avoid access to
	// any members of the containing class
	static class ViewHolder {
		public LinearLayout header;
		public TextView names;
		public TextView texts;
		public TextView timestamps;
		public TextView time;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		// ViewHolder will buffer the access to the individual fields of the row
		// layout
		ViewHolder holder;
		// Recycle existing view if passed as parameter
		// This will save memory and time on Android
		// This only works if the base layout for all classes are the same
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			rowView = inflater.inflate(R.layout.rowlayout_notes, null, true);
			holder = new ViewHolder();
			holder.names = (TextView) rowView.findViewById(R.id.notes_name);
			holder.texts = (TextView) rowView.findViewById(R.id.notes_text);
			holder.timestamps = (TextView) rowView
					.findViewById(R.id.notes_timestamp);
			holder.time = (TextView) rowView.findViewById(R.id.notes_time);
			// holder.header = (LinearLayout) holder.findViewById(R.id.n)
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		holder.names.setText(jobs_notes_names[position]);
		holder.texts.setText(jobs_notes_texts[position]);
		
		return rowView;
	}

	private static String loadSetting(String tag) {
		SharedPreferences preferences = context.getSharedPreferences(
				"PREFERENCES", Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}
}
