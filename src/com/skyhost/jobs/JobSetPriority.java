package com.skyhost.jobs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class JobSetPriority extends Activity implements OnClickListener {

	private ProgressDialog progressDialog;

	private String selectedPriority;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.jobsetpriority);

		new PauseDialogLoader().execute();

		// Initializing
		// Priority
		selectedPriority = "low";
		final String[] priority = new String[3];
		priority[0] = "Lav";
		priority[1] = "H�j";
		priority[2] = "Haster";

		Spinner spinnerPriority = (Spinner) findViewById(R.id.jobsetpriority_spinner);
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, priority);
		adapter.setDropDownViewResource(R.layout.rowlayout_timerselecttime_spinnerminutes);
		spinnerPriority.setAdapter(adapter);
		spinnerPriority.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				if (position == 0) {
					selectedPriority = "low";
				} else if (position == 1) {
					selectedPriority = "high";
				} else if (position == 2) {
					selectedPriority = "urgent";
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		int selectedJobIndex = 0;

		if (!(loadSetting("selectedJobIndex").equals(""))) {
			selectedJobIndex = Integer
					.parseInt(loadSetting("selectedJobIndex"));
		}

		String initialPriority = WebApi.getWebApi().getJobs_priority()[selectedJobIndex];
		int position = 0;

		if (initialPriority.equals("low")) {
			position = 0;
		} else if (initialPriority.equals("high")) {
			position = 1;
		} else if (initialPriority.equals("urgent")) {
			position = 2;
		}

		spinnerPriority.setSelection(position);

		Button saveButton = (Button) findViewById(R.id.button_save);
		saveButton.setOnClickListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		if (progressDialog != null) {
			progressDialog.hide();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			Intent jobs = new Intent(JobSetPriority.this, Jobs.class);
			startActivity(jobs);
			return true;

		case R.id.activity_timer: // Job
			Intent timer = new Intent(JobSetPriority.this, Timer.class);
			startActivity(timer);
			return true;
		}
		return true;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private void saveSetting(String tag, boolean content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(tag, content);
		editor.commit();
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.button_save) {
			new AsyncSetPriority().execute();
		}
	}

	private class AsyncSetPriority extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			String selectedJobId = "";
			if (!(loadSetting("selectedJobId").equals(""))) {
				selectedJobId = loadSetting("selectedJobId");
			}

			// If fields are not null or empty
			if (!selectedJobId.equals("")) {
				SharedPreferences preferences = getSharedPreferences(
						"PREFERENCES", Activity.MODE_PRIVATE);

				WebApi.getWebApi().setJobPriority(preferences, selectedJobId,
						selectedPriority);
			}

			// Error sending
			if (WebApi.getWebApi().isNewData_Jobs_SetJobPriority() == false) {
				showToast("Fejlkode: "
						+ WebApi.getWebApi().getResponse_ErrorCode(), WebApi
						.getWebApi().getResponse_ErrorText()
						+ " Pr�v igen senere!");

				Intent status = new Intent(JobSetPriority.this, Job.class);
				startActivity(status);
			}

			// Success sending
			else if (WebApi.getWebApi().isNewData_Jobs_SetJobPriority() == true) {
				String errorHeader = WebApi.getWebApi()
						.getResponse_ErrorHeader();

				// Success answer
				if (errorHeader.equals("OK")) {
					showToast("Din registrering er gemt");
					saveSetting("reloadJobs", true);
					saveSetting("jobSetPriority", true);
					Intent intent = new Intent(JobSetPriority.this, Job.class);
					startActivity(intent);
				}

				// Error in answer
				else if (WebApi.getWebApi().getResponse_Status()
						.equals("Error")) {
					if (!WebApi.getWebApi().getResponse_ErrorText().equals("")) {
						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");

						Intent intent = new Intent(JobSetPriority.this,
								Job.class);
						startActivity(intent);
					} else {
						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");

						Intent intent = new Intent(JobSetPriority.this,
								Job.class);
						startActivity(intent);
					}
				}
			} else {
				Intent intent = new Intent(JobSetPriority.this, Job.class);
				startActivity(intent);
			}

			return null;
		}

		protected void showToast(final String sHeader, final String sComment) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sComment, Toast.LENGTH_LONG)
							.show();
				}
			});
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(JobSetPriority.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}
}