package com.skyhost.jobs;

import java.lang.reflect.Type;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class AddMaterialToJob extends Activity implements OnClickListener,
		OnItemSelectedListener {

	private android.os.PowerManager powerManager;
	private PowerManager.WakeLock wakeLock;
	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addmaterialtojob);

		new ShowAsyncPauseDialog().execute();

		// --- Initializing ---
		powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		final TextView materialId = (TextView) findViewById(R.id.addmaterialtojob_matid_text);
		final TextView materialName = (TextView) findViewById(R.id.addmaterialtojob_name_text);
		final TextView materialDescription = (TextView) findViewById(R.id.addmaterialtojob_description_text);
		final TextView materialReference = (TextView) findViewById(R.id.addmaterialtojob_reference_text);
		final TextView materialColli = (TextView) findViewById(R.id.addmaterialtojob_colli_text);
		final TextView materialGroupNo = (TextView) findViewById(R.id.addmaterialtojob_groupno_text);
		final TextView materialGroupName = (TextView) findViewById(R.id.addmaterialtojob_groupname_text);
		final TextView materialSupplierItemNo = (TextView) findViewById(R.id.addmaterialtojob_supplieritemno_text);
		final TextView materialEanNo = (TextView) findViewById(R.id.addmaterialtojob_eanno_text);
		final TextView materialElNo = (TextView) findViewById(R.id.addmaterialtojob_elno_text);
		final TextView materialEanBarcode = (TextView) findViewById(R.id.addmaterialtojob_eanbarcode_text);
		final TextView materialInternalBarcode = (TextView) findViewById(R.id.addmaterialtojob_internalbarcode_text);
		final TextView materialInternalItemNo = (TextView) findViewById(R.id.addmaterialtojob_internalitemno_text);
		final TextView materialInternalItemShortNo = (TextView) findViewById(R.id.addmaterialtojob_internalitemshortno_text);
		final EditText materialAmount = (EditText) findViewById(R.id.addmaterialtojob_amount_text);
		// add material
		final Button add_material_button = (Button) findViewById(R.id.addmaterialtojob_add_or_edit_material_button);
		materialAmount
				.setOnEditorActionListener(new EditText.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_DONE) {
							addMaterial(materialAmount, add_material_button);
							return true;
						}
						return false;
					}
				});

		// Load selected Material
		// Material Id
		String sSelectedMaterialId = "";
		if (!loadSetting("selectedMaterialId").equals("")) {
			sSelectedMaterialId = loadSetting("selectedMaterialId");
		}

		// Load saved materials
		String[][] materials = null;
		try {
			if (!loadSetting("materials").equals("")) {
				Type type = new TypeToken<String[][]>() {
				}.getType();

				String str = loadSetting("materials");
				materials = new Gson().fromJson(str, type);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		add_material_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				addMaterial(materialAmount, add_material_button);
			}

		});

		int index = linearSearchArray(materials[0], sSelectedMaterialId);

		// Display Material on Screen
		if (index != -1) {

			if (materials != null) {

				if (materials[0][index].equals("")) {
					LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addmaterialtojob_linearlayout_matid);
					linearLayout.setVisibility(View.GONE);
				} else {
					materialId.setText(materials[0][index]);
				}

				if (materials[1][index].equals("")) {
					LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addmaterialtojob_linearlayout_name);
					linearLayout.setVisibility(View.GONE);
				} else {
					materialName.setText(materials[1][index]);
				}

				if (materials[2][index].equals("")) {
					LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addmaterialtojob_linearlayout_description);
					linearLayout.setVisibility(View.GONE);
				} else {
					materialDescription.setText(materials[2][index]);
				}

				if (materials[3][index].equals("")) {
					LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addmaterialtojob_linearlayout_reference);
					linearLayout.setVisibility(View.GONE);
				} else {
					materialReference.setText(materials[3][index]);
				}

				if (materials[4][index].equals("")) {
					LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addmaterialtojob_linearlayout_colli);
					linearLayout.setVisibility(View.GONE);
				} else {
					materialColli.setText(materials[4][index]);
				}

				if (materials[5][index].equals("")) {
					LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addmaterialtojob_linearlayout_groupno);
					linearLayout.setVisibility(View.GONE);
				} else {
					materialGroupNo.setText(materials[5][index]);
				}

				if (materials[6][index].equals("")) {
					LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addmaterialtojob_linearlayout_groupname);
					linearLayout.setVisibility(View.GONE);
				} else {
					materialGroupName.setText(materials[6][index]);
				}

				if (materials[7][index].equals("")) {
					LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addmaterialtojob_linearlayout_supplieritemno);
					linearLayout.setVisibility(View.GONE);
				} else {
					materialSupplierItemNo.setText(materials[7][index]);
				}

				if (materials[8][index].equals("")) {
					LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addmaterialtojob_linearlayout_eanno);
					linearLayout.setVisibility(View.GONE);
				} else {
					materialEanNo.setText(materials[8][index]);
				}

				if (materials[9][index].equals("")) {
					LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addmaterialtojob_linearlayout_elno);
					linearLayout.setVisibility(View.GONE);
				} else {
					materialElNo.setText(materials[9][index]);
				}

				if (materials[10][index].equals("")) {
					LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addmaterialtojob_linearlayout_eanbarcode);
					linearLayout.setVisibility(View.GONE);
				} else {
					materialEanBarcode.setText(materials[10][index]);
				}

				if (materials[11][index].equals("")) {
					LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addmaterialtojob_linearlayout_internalbarcode);
					linearLayout.setVisibility(View.GONE);
				} else {
					materialInternalBarcode.setText(materials[11][index]);
				}

				if (materials[12][index].equals("")) {
					LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addmaterialtojob_linearlayout_internalitemno);
					linearLayout.setVisibility(View.GONE);
				} else {
					materialInternalItemNo.setText(materials[12][index]);
				}

				if (materials[13][index].equals("")) {
					LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addmaterialtojob_linearlayout_internalitemshortno);
					linearLayout.setVisibility(View.GONE);
				} else {
					materialInternalItemShortNo.setText(materials[13][index]);
				}
			}
		}
		progressDialog.dismiss();
	}

	@Override
	public void onPause() {
		super.onPause();

		if (progressDialog != null) {
			progressDialog.cancel();
		}

		setPowerSaveState(true);
	}

	@Override
	public void onResume() {
		super.onResume();
		final Button add_material_button = (Button) findViewById(R.id.addmaterialtojob_add_or_edit_material_button);
		add_material_button.setEnabled(true);

		setPowerSaveState(false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.addmaterialtojob, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			Intent navigateToJobs = new Intent(AddMaterialToJob.this,
					Jobs.class);
			startActivity(navigateToJobs);
			return true;
		case R.id.activity_materialsonjob: // MaterialsOnJob
			Intent navigateToMaterialsOnJob = new Intent(AddMaterialToJob.this,
					MaterialsOnJob.class);
			startActivity(navigateToMaterialsOnJob);
			return true;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
	}

	/**
	 * @param materialAmount
	 * @param add_material_button
	 */
	private void addMaterial(final EditText materialAmount,
			final Button add_material_button) {
		// If Amount = 0
		String tmp = materialAmount.getText().toString();
		if (tmp.equals("0") || tmp.equals("")) {
			Toast.makeText(getApplicationContext(),
					"Der skal indtastes et antal", Toast.LENGTH_LONG).show();
		} else if (tmp.length() > 10) {
			Toast.makeText(getApplicationContext(),
					"Maksimal indtastning 10 tal...", Toast.LENGTH_LONG).show();
		} else {
			add_material_button.setEnabled(false);

			new AsyncAddMaterialToJob().execute();
		}
	}

	protected void showToast(final String sHeader, final String sContent) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(getApplicationContext(),
						sHeader + ": " + sContent, Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

	public void setPowerSaveState(boolean enabled) {

		if (enabled == false) {
			wakeLock = powerManager.newWakeLock(
					PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "My Tag");
			wakeLock.acquire();
		} else {
			if (wakeLock != null) {
				wakeLock.release();
			}
		}
	}

	public static int linearSearchArray(String[] list, String target) {
		boolean found = false;
		int i = 0;
		while (!found && i < list.length) {
			if (list[i].equals(target))
				found = true;
			else
				i++;
		}
		if (found)
			return i;
		else
			return -1;
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	private class AsyncAddMaterialToJob extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {
			// Initialize
			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);
			Intent navigateToMaterialsOnJob = new Intent(AddMaterialToJob.this,
					MaterialsOnJob.class);
			String jobId = "";
			String materialId = "";
			String amount = "";

			if (!loadSetting("selectedJobId").equals("")) {
				jobId = loadSetting("selectedJobId");
			}

			if (!loadSetting("selectedMaterialId").equals("")) {
				materialId = loadSetting("selectedMaterialId");
			}

			EditText materialAmount = (EditText) findViewById(R.id.addmaterialtojob_amount_text);
			amount = materialAmount.getText().toString();

			WebApi.getWebApi().addMaterialToJob(preferences, jobId, materialId,
					amount);

			// Response error and success handling -> redirection
			// Error
			if (WebApi.getWebApi().isNewData_Jobs_AddMaterialToJob() == false
					|| WebApi.getWebApi().isNewData_Response() == false) {
				if (!WebApi.getWebApi().getResponse_ErrorText().equals("")
						|| !WebApi.getWebApi().getResponse_ErrorCode()
								.equals("")) {
					showToast("Materialerne blev ikke registreret! "
							+ "Fejlkode: "
							+ WebApi.getWebApi().getResponse_ErrorCode(),
							WebApi.getWebApi().getResponse_ErrorText()
									+ " Pr�v igen senere!");
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							final Button add_material_button = (Button) findViewById(R.id.addmaterialtojob_add_or_edit_material_button);
							add_material_button.setEnabled(true);
						}
					});

				} else {
					showToast("Materialerne blev ikke registreret - Pr�v igen senere!");

					startActivity(navigateToMaterialsOnJob);
				}
			}

			// Success
			else if (WebApi.getWebApi().isNewData_Response() == true) {
				String text = WebApi.getWebApi().getResponse_ErrorText();
				String code = WebApi.getWebApi().getResponse_ErrorCode();
				if (text.equals("Created") && code.equals("0")) {
					showToast("Materialet er registreret!");

					// Get and save number of materials
					int tmp = WebApi.getWebApi().getJobs_MaterialsOnJob_MatID().length;
					tmp++;

					String sSelectedJobId = "";
					if (!loadSetting("selectedJobId").equals("")) {
						sSelectedJobId = loadSetting("selectedJobId");
					}

					saveSetting("sNumberOfMaterialsOnJob" + sSelectedJobId,
							String.valueOf(tmp));

					startActivity(navigateToMaterialsOnJob);
				} else {
					showToast("Materialerne blev ikke registreret! "
							+ "Fejlkode: "
							+ WebApi.getWebApi().getResponse_ErrorCode(),
							WebApi.getWebApi().getResponse_ErrorText()
									+ " Pr�v igen senere!");
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							final Button add_material_button = (Button) findViewById(R.id.addmaterialtojob_add_or_edit_material_button);
							add_material_button.setEnabled(true);
						}
					});

				}
			} else {
				showToast("Materialerne blev ikke registreret - Pr�v igen senere!");
				startActivity(navigateToMaterialsOnJob);
			}
			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

		protected void showToast(final String sHeader, final String sContext) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sContext, Toast.LENGTH_LONG)
							.show();
				}
			});
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}
	}

	private class ShowAsyncPauseDialog extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(AddMaterialToJob.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);

			progressDialog.dismiss();
		}
	}
}