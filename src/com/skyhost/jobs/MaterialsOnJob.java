package com.skyhost.jobs;

import java.util.ArrayList;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MaterialsOnJob extends ListActivity implements OnClickListener {
	private Handler handler;

	ArrayList<Material> materialsOnJobList;

	private ProgressDialog progressDialog;

	private static final int DIALOG_ALERT = 10;

	private Runnable runUpdateTask = new Runnable() {
		public void run() {

			new AsyncGetMaterials().execute();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.materialsonjob);

		new ShowPauseDialog().execute();

		// Initialize
		final ListView listView = getListView();
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> l, View view, int position,
					long arg3) {
				registerForContextMenu(listView);
				view.showContextMenu();
				
				// Slet
				TextView id = (TextView) view.findViewById(R.id.materialsonjob_matid);
				String selectedMaterialOnJobIndexId = id.getText().toString();
				saveSetting("selectedMaterialOnJobIndexId",
						selectedMaterialOnJobIndexId);
			}
		});

		Button addMaterial = (Button) findViewById(R.id.materialsonjob_addmaterial_button);
		addMaterial.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent navigateToMaterialsQuickScrollListView = new Intent(
						MaterialsOnJob.this, MaterialsQuickScrollListView.class);
				startActivity(navigateToMaterialsQuickScrollListView);
			}
		});

		handler = new Handler();
	}

	@Override
	public void onPause() {
		super.onPause();

		if (handler != null) {
			handler.removeCallbacks(runUpdateTask);
		}
		
		if(progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		if (handler != null) {
			handler.post(runUpdateTask);
		}
	}
	
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(MaterialsOnJob.this, Job.class);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.materialsonjob, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.activity_jobs: // Jobs
			Intent navigateToJobs = new Intent(MaterialsOnJob.this, Jobs.class);
			startActivity(navigateToJobs);
			return true;
		}
		return true;
	}

	private void updateScreen() {
		final ArrayAdapterMaterialsOnJob adapter = new ArrayAdapterMaterialsOnJob(
				this, WebApi.getWebApi().getJobs_MaterialsOnJob_ID(), WebApi
						.getWebApi().getJobs_MaterialsOnJob_Timestamp(), WebApi
						.getWebApi().getJobs_MaterialsOnJob_InternalItemNo(),
				WebApi.getWebApi().getJobs_MaterialsOnJob_ItemName(), WebApi
						.getWebApi().getJobs_MaterialsOnJob_Psc(), WebApi
						.getWebApi().getJobs_MaterialsOnJob_User());

		// Get and save number of materials
		int tmp = WebApi.getWebApi().getJobs_MaterialsOnJob_MatID().length;

		String sSelectedJobId = "";
		if (!loadSetting("selectedJobId").equals("")) {
			sSelectedJobId = loadSetting("selectedJobId");
		}

		saveSetting("sNumberOfMaterialsOnJob" + sSelectedJobId,
				String.valueOf(tmp));

		runOnUiThread(new Runnable() {

			@Override
			public void run() {

				try {
					setListAdapter(adapter);

				} catch (Exception ex) {
					ex.printStackTrace();
				}

				if (progressDialog != null) {
					progressDialog.hide();
				}
			}
		});}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	@Override
	public void onClick(View arg0) {
	}

	public void showToast(final String toast) {
		runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(MaterialsOnJob.this, toast, Toast.LENGTH_SHORT)
						.show();
			}
		});
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.materialsonjob_optional_menu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.activity_delete_material: // Slet materiale
			showDialog(DIALOG_ALERT);
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_ALERT:
			Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Advarsel!");
			builder.setMessage("Materiale slettes!");
			builder.setIcon(R.drawable.icon_warning_sign_yellow);
			builder.setCancelable(true);

			builder.setPositiveButton("Slet",
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {

							new AsyncLoaderDeleteMaterial().execute();
						}
					});
			builder.setNegativeButton("Nej, annuller",
					new DialogInterface.OnClickListener() {
						ViewGroup viewGroup = (ViewGroup) findViewById(R.id.linearlayout_timernewjob);

						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
		return super.onCreateDialog(id);
	}

	private Boolean loadSettingBoolean(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		Boolean content = preferences.getBoolean(tag, false);
		return content;
	}

	private class AsyncLoaderDeleteMaterial extends
			AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {
			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);
			
			String selectedJobId = "";
			if (!loadSetting("selectedJobId").equals("")) {
				selectedJobId = loadSetting("selectedJobId");
			}
			
			String selectedMaterialOnJobIndexId = "";
			if (!loadSetting("selectedMaterialOnJobIndexId").equals("")) {
				selectedMaterialOnJobIndexId = loadSetting("selectedMaterialOnJobIndexId");
			}
			WebApi.getWebApi().deleteMaterial(preferences, selectedJobId,
					selectedMaterialOnJobIndexId);

			// Response error and success handling -> redirection
			// Error
			if (WebApi.getWebApi().isNewData_Jobs_DeleteMaterial() == false
					|| WebApi.getWebApi().isNewData_Response() == false) {
				if (!WebApi.getWebApi().getResponse_ErrorText().equals("")
						|| !WebApi.getWebApi().getResponse_ErrorCode()
								.equals("")) {
					showToast("Materialet blev ikke slettet! " + "Fejlkode: "
							+ WebApi.getWebApi().getResponse_ErrorCode(),
							WebApi.getWebApi().getResponse_ErrorText()
									+ " Pr�v igen senere!");
				} else {
					showToast("Materialet blev ikke slettet - Pr�v igen senere!");
				}
			}

			// Success
			else if (WebApi.getWebApi().isNewData_Response() == true) {
				String text = WebApi.getWebApi().getResponse_ErrorText();
				String code = WebApi.getWebApi().getResponse_ErrorCode();
				if (text.equals("Deleted") && code.equals("0")) {
					showToast("Materialet er slettet!");

					// Get and save number of materials
					int tmp = WebApi.getWebApi().getJobs_MaterialsOnJob_MatID().length;
					tmp++;

					String sSelectedJobId = "";
					if (!loadSetting("selectedJobId").equals("")) {
						sSelectedJobId = loadSetting("selectedJobId");
					}

					saveSetting("sNumberOfMaterialsOnJob" + sSelectedJobId,
							String.valueOf(tmp));
					if (handler != null) {
						handler.post(runUpdateTask);
					}
				} else {
					showToast("Materialet blev ikke slettet! "
							+ "Fejlkode: "
							+ WebApi.getWebApi().getResponse_ErrorCode(),
							WebApi.getWebApi().getResponse_ErrorText()
									+ " Pr�v igen senere!");
				}
			} else {
				showToast("Materialet blev ikke slettet - Pr�v igen senere!");
			}
			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);
			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

		protected void showToast(final String sHeader, final String sContext) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sContext, Toast.LENGTH_LONG)
							.show();
				}
			});
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}
	}

	private class ShowPauseDialog extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(MaterialsOnJob.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class AsyncGetMaterials extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {
			// Initialize
			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);
			String sSelectedJobId = "";
			if (!loadSetting("selectedJobId").equals("")) {
				sSelectedJobId = loadSetting("selectedJobId");
			}
			
			if (!sSelectedJobId.equals("")) {
				WebApi.getWebApi().getJobs_MaterialsOnJob(preferences,
						sSelectedJobId);

				boolean isNewDataMaterialsOnJob = WebApi.getWebApi()
						.isNewData_Jobs_MaterialsOnJob();
				if (isNewDataMaterialsOnJob == true) {
					
					updateScreen();
					
					if (loadSettingBoolean("AutoUpdate") == true) {
						handler.postDelayed(runUpdateTask, 30000);
					}
				} else {
					handler.postDelayed(runUpdateTask, 3000);
				}
			} else {
				showToast("Der skete en fejl - Pr�v igen.");
			}
			
			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}
		
		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}

		protected void showToast(final String sHeader, final String sContext) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sContext, Toast.LENGTH_LONG)
							.show();
				}
			});
		}
	}
}