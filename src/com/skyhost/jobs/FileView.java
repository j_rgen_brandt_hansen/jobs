package com.skyhost.jobs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.FrameLayout.LayoutParams;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.PointF;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class FileView extends Activity implements OnTouchListener {

	private ImageView imageView;
	private int count = 0;
	private Handler handler;
	private ProgressDialog progressDialog;
	private String selectedFilePath;

	// ImageZoom
	// These matrices will be used to move and zoom image
	Matrix matrix = new Matrix();
	Matrix savedMatrix = new Matrix();

	// We can be in one of these 3 states
	static final int NONE = 0;
	static final int DRAG = 1;
	static final int ZOOM = 2;
	int mode = NONE;

	// Remember some things for zooming
	PointF start = new PointF();
	PointF mid = new PointF();
	float oldDist = 1f;

	private Runnable runUpdateTask = new Runnable() {
		public void run() {
			new LoadImage().execute(selectedFilePath);
			count++;
			if (count == 5) {
				Intent intent = new Intent(FileView.this, Job.class);
				startActivity(intent);
				Toast.makeText(getApplicationContext(),
						"Billedet kan ikke hentes - Pr�v igen senere!",
						Toast.LENGTH_LONG);
				finish();
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fileview);

		// Initialize
		handler = new Handler();

		imageView = (ImageView) findViewById(R.id.fileview_imageview);
		imageView.setScaleType(ImageView.ScaleType.FIT_CENTER); // make the
																// image fit to
																// the center.
		imageView.setOnTouchListener(this);
		// imageView.setLayoutParams(params_Image);
		imageView.setAdjustViewBounds(true);
		imageView.setPadding(0, 5, 0, 5);

		selectedFilePath = "";
		if (!loadSetting("selectedFilePath").equals("")) {
			selectedFilePath = loadSetting("selectedFilePath");
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		if (handler != null) {
			handler.removeCallbacks(runUpdateTask);
		}

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (handler != null) {
			handler.post(runUpdateTask);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.info, menu);
		return true;
	}

	public boolean onTouch(View v, MotionEvent event) {
		ImageView view = (ImageView) v;

		// make the image scalable as a matrix
		view.setScaleType(ImageView.ScaleType.MATRIX);
		float scale;

		// Handle touch events here...
		switch (event.getAction() & MotionEvent.ACTION_MASK) {

		case MotionEvent.ACTION_DOWN: // first finger down only
			// Test
			// Display display = getWindowManager().getDefaultDisplay();
			// int screen_width = display.getWidth(); // deprecated
			// int screen_height = display.getHeight();
			//
			// RectF drawableRect = new RectF(0, 0, matrix.MSCALE_X,
			// matrix.MSCALE_Y);
			// RectF viewRect = new RectF(0, 0, screen_width, screen_height);
			// matrix.setRectToRect(drawableRect, viewRect,
			// Matrix.ScaleToFit.CENTER);

			savedMatrix.set(matrix);
			start.set(event.getX(), event.getY());
			mode = DRAG;
			break;
		case MotionEvent.ACTION_UP: // first finger lifted
		case MotionEvent.ACTION_POINTER_UP: // second finger lifted
			mode = NONE;
			break;
		case MotionEvent.ACTION_POINTER_DOWN: // second finger down
			oldDist = spacing(event); // calculates the distance between two
										// points where user touched.

			// minimal distance between both the fingers
			if (oldDist > 5f) {
				savedMatrix.set(matrix);
				midPoint(mid, event); // sets the mid-point of the straight line
										// between two points where user
										// touched.
				mode = ZOOM;
			}
			break;

		case MotionEvent.ACTION_MOVE:
			if (mode == DRAG) { // movement of first finger
				matrix.set(savedMatrix);
				if (view.getLeft() >= -392) {
					matrix.postTranslate(event.getX() - start.x, event.getY()
							- start.y);
				}
			} else if (mode == ZOOM) { // pinch zooming
				float newDist = spacing(event);

				if (newDist > 5f) {
					matrix.set(savedMatrix);
					scale = newDist / oldDist; // thinking I need to play around
												// with this value to limit it**
					matrix.postScale(scale, scale, mid.x, mid.y);
				}
			}
			break;
		}

		// Perform the transformation
		view.setImageMatrix(matrix);

		return true; // indicate event was handled
	}

	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return FloatMath.sqrt(x * x + y * y);
	}

	private void midPoint(PointF point, MotionEvent event) {
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent intent = null;

		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			intent = new Intent(FileView.this, Jobs.class);
			break;
		}
		startActivity(intent);
		return true;
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private Bitmap decodeImage(Bitmap inputBitmap) {
		try {

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			inputBitmap.compress(CompressFormat.PNG, 0 /* ignored for PNG */,
					bos);
			byte[] bitmapdata = bos.toByteArray();
			ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);

			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(bs, null, o);

			// The new size we want to scale to
			final int REQUIRED_SIZE = 800;

			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			while (true) {
				if (o.outWidth < REQUIRED_SIZE && o.outHeight < REQUIRED_SIZE) {
					break;
				}
				o.outWidth /= 2;
				o.outHeight /= 2;
				scale *= 2;
			}
			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			o2.inJustDecodeBounds = false;
			o2.inPreferredConfig = Config.RGB_565;
			o2.inDither = true;

			Bitmap bitmap = BitmapFactory.decodeStream(bs, null, o2);
			try {
				bs.close();
				bs = null;
			} catch (IOException e) {
				return null;
			}
			return bitmap;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private int dpToPx(int dp) {
		float density = getApplicationContext().getResources()
				.getDisplayMetrics().density;
		return Math.round((float) dp * density);
	}

	public static Bitmap drawableToBitmap(Drawable drawable) {
		if (drawable instanceof BitmapDrawable) {
			return ((BitmapDrawable) drawable).getBitmap();
		}

		int width = drawable.getIntrinsicWidth();
		width = width > 0 ? width : 1;
		int height = drawable.getIntrinsicHeight();
		height = height > 0 ? height : 1;

		Bitmap bitmap = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);
		return bitmap;
	}

	private class LoadImage extends AsyncTask<String, String, Bitmap> {
		private Bitmap bitmap;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			new ShowAsyncPauseDialog().execute();
		}

		protected Bitmap doInBackground(String... args) {
			try {
				if (args[0].endsWith(".bmp")) {

					InputStream inputStream = WebApi.getWebApi().bitmapFromUrl(
							args[0]);
					if (WebApi.getWebApi().isNewData_Bitmap_From_Url() == true) {
						bitmap = BitmapFactory.decodeStream(inputStream);
					} else {
						if (handler != null) {
							handler.postDelayed(runUpdateTask, 3000);
						}
					}
				} else {
					Drawable drawable = WebApi.getWebApi().drawableFromUrl(
							args[0]);
					if (WebApi.getWebApi().isNewData_Drawable_From_Url() == true) {
						bitmap = drawableToBitmap(drawable);
					} else {
						if (handler != null) {
							handler.postDelayed(runUpdateTask, 3000);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			final LinearLayout.LayoutParams params_Image = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

			return bitmap;
		}

		protected void onPostExecute(Bitmap image) {

			decodeImage(bitmap);
			final Drawable drawable = new BitmapDrawable(getResources(), bitmap);

			imageView.setImageDrawable(drawable);

			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void showToast(final String sHeader, final String sContext) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sContext, Toast.LENGTH_LONG)
							.show();
				}
			});
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}
	}

	private class ShowAsyncPauseDialog extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(FileView.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}
}