package com.skyhost.jobs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

import com.skyhost.jobs.ArrayAdapterJobs.ViewHolder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

public class QuickScrollListView extends ListActivity {
	private int numberOfMaterialsForJob = 0;
	private String selectedMaterial;

	private static String JARGON;

	// Called when the activity is first created.
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		LinkedList<String> mLinked = new LinkedList<String>();
		for (int i = 0; i < COUNTRIES.length; i++) {
			mLinked.add(COUNTRIES[i]);
		}

		setListAdapter(new MyListAdaptor(this, mLinked));

		ListView lv = getListView();
		lv.setFastScrollEnabled(true);

		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				selectedMaterial = ((TextView) view).getText().toString();
				saveSetting("selectedMaterial", selectedMaterial);
				Intent intent = new Intent(QuickScrollListView.this,
						AddMaterialToJob.class);
				startActivity(intent);

				// new DialogLoaderNumberOfMAterial().execute();
				// // When clicked, popup and ask for details
				// Toast.makeText(getApplicationContext(),
				// ((TextView) view).getText(), Toast.LENGTH_SHORT).show();
			}
		});

		Bundle appData = getIntent().getBundleExtra(SearchManager.APP_DATA);
		if (appData != null) {
			boolean jargon = appData.getBoolean(QuickScrollListView.JARGON);

			
		}

		// Get the intent, verify the action and get the query
		Intent intent = getIntent();
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);
			// doMySearch(query);
		}

	}

	public static final String[] COUNTRIES = new String[] { "Afghanistan",
			"Albania", "Algeria", "American Samoa", "Andorra", "Angola",
			"Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina",
			"Armenia", "Aruba", "Australia", "Austria", "Azerbaijan",
			"Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium",
			"Belize", "Benin", "Bermuda", "Bhutan", "Bolivia",
			"Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil",
			"British Indian Ocean Territory", "British Virgin Islands",
			"Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cote d'Ivoire",
			"Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands",
			"Central African Republic", "Chad", "Chile", "China",
			"Christmas Island", "Cocos (Keeling) Islands", "Colombia",
			"Comoros", "Congo", "Cook Islands", "Costa Rica", "Croatia",
			"Cuba", "Cyprus", "Czech Republic",
			"Democratic Republic of the Congo", "Denmark", "Djibouti",
			"Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt",
			"El Salvador", "Equatorial Guinea", "Eritrea", "Estonia",
			"Ethiopia", "Faeroe Islands", "Falkland Islands", "Fiji",
			"Finland", "Former Yugoslav Republic of Macedonia", "France",
			"French Guiana", "French Polynesia", "French Southern Territories",
			"Gabon", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece",
			"Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala",
			"Guinea", "Guinea-Bissau", "Guyana", "Haiti",
			"Heard Island and McDonald Islands", "Honduras", "Hong Kong",
			"Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq",
			"Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan",
			"Kazakhstan", "Kenya", "Kiribati", "Kuwait", "Kyrgyzstan", "Laos",
			"Latvia", "Lebanon", "Lesotho", "Liberia", "Libya",
			"Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Madagascar",
			"Malawi", "Malaysia", "Maldives", "Mali", "Malta",
			"Marshall Islands", "Martinique", "Mauritania", "Mauritius",
			"Mayotte", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia",
			"Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia",
			"Nauru", "Nepal", "Netherlands", "Netherlands Antilles",
			"New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria",
			"Niue", "Norfolk Island", "North Korea", "Northern Marianas",
			"Norway", "Oman", "Pakistan", "Palau", "Panama",
			"Papua New Guinea", "Paraguay", "Peru", "Philippines",
			"Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar",
			"Reunion", "Romania", "Russia", "Rwanda", "Sqo Tome and Principe",
			"Saint Helena", "Saint Kitts and Nevis", "Saint Lucia",
			"Saint Pierre and Miquelon", "Saint Vincent and the Grenadines",
			"Samoa", "San Marino", "Saudi Arabia", "Senegal", "Seychelles",
			"Sierra Leone", "Singapore", "Slovakia", "Slovenia",
			"Solomon Islands", "Somalia", "South Africa",
			"South Georgia and the South Sandwich Islands", "South Korea",
			"Spain", "Sri Lanka", "Sudan", "Suriname",
			"Svalbard and Jan Mayen", "Swaziland", "Sweden", "Switzerland",
			"Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand",
			"The Bahamas", "The Gambia", "Togo", "Tokelau", "Tonga",
			"Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan",
			"Turks and Caicos Islands", "Tuvalu", "Virgin Islands", "Uganda",
			"Ukraine", "United Arab Emirates", "United Kingdom",
			"United States", "United States Minor Outlying Islands", "Uruguay",
			"Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam",
			"Wallis and Futuna", "Western Sahara", "Yemen", "Yugoslavia",
			"Zambia", "Zimbabwe" };

	/**
	 * The List row creator
	 */
	class MyListAdaptor extends ArrayAdapter<String> implements SectionIndexer {

		HashMap<String, Integer> alphaIndexer;
		String[] sections;
	
		
		
		public MyListAdaptor(Context context, LinkedList<String> items) {
			super(context, R.layout.list_item, items);

			alphaIndexer = new HashMap<String, Integer>();
			int size = items.size();

			for (int x = 0; x < size; x++) {
				String s = items.get(x);

				// get the first letter of the store
				String ch = s.substring(0, 1);
				// convert to uppercase otherwise lowercase a -z will be sorted
				// after upper A-Z
				ch = ch.toUpperCase();

				// put only if the key does not exist
				if (!alphaIndexer.containsKey(ch))
					alphaIndexer.put(ch, x);
			}

			Set<String> sectionLetters = alphaIndexer.keySet();

			// create a list from the set to sort
			ArrayList<String> sectionList = new ArrayList<String>(
					sectionLetters);

			Collections.sort(sectionList);

			sections = new String[sectionList.size()];

			sectionList.toArray(sections);
		}

		public int getPositionForSection(int section) {
			return alphaIndexer.get(sections[section]);
		}

		public int getSectionForPosition(int position) {
			return 0;
		}

		public Object[] getSections() {
			return sections;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.quickscrolllistview, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_search: // Search
			onSearchRequested();
			// Intent quickScrollListView = new Intent(QuickScrollListView.this,
			// QuickScrollListView.class);
			// startActivityForResult(quickScrollListView,
			// Intent.ACTION_SEARCH);
			return true;
		}
		return true;
	}

	private void startActivityForResult(Intent quickScrollListView,
			String actionSearch) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onSearchRequested() {
		Bundle appData = new Bundle();
		appData.putBoolean(QuickScrollListView.JARGON, true);
		startSearch(null, false, appData, false);
		return super.onSearchRequested();
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences(Jobs.PREFERENCES,
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private class DialogLoaderNumberOfMAterial extends
			AsyncTask<Void, Integer, Void> {
		AlertDialog.Builder alertDialog;

		protected void onPreExecute() {
			super.onPreExecute();

			// Creating alert Dialog with one Button
			alertDialog = new AlertDialog.Builder(QuickScrollListView.this);

			// Setting Dialog Title
			alertDialog.setTitle("Indtast antal");

			// Setting Dialog Message
			alertDialog.setMessage(selectedMaterial);
			final EditText input = new EditText(QuickScrollListView.this);
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.MATCH_PARENT);
			input.setLayoutParams(lp);
			input.setInputType(InputType.TYPE_CLASS_NUMBER);
			alertDialog.setView(input);

			// Setting Icon to Dialog
			alertDialog.setIcon(R.drawable.icon_informationgreen);

			// Setting Positive "Yes" Button
			alertDialog.setPositiveButton("Ja",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Write your code here to execute after dialog

							numberOfMaterialsForJob = Integer.parseInt(input
									.getText().toString());
							if (numberOfMaterialsForJob != 0) {

								Toast.makeText(
										getApplicationContext(),
										"Du har indtastet: "
												+ numberOfMaterialsForJob,
										Toast.LENGTH_SHORT).show();
								Intent myIntent1 = new Intent(
										QuickScrollListView.this,
										QuickScrollListView.class);
								startActivityForResult(myIntent1, 0);
							} else {
								Toast.makeText(getApplicationContext(),
										"Der m� kun indtastes tal!",
										Toast.LENGTH_SHORT).show();
							}
						}
					});
			// Setting Negative "NO" Button
			alertDialog.setNegativeButton("Fortryd",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Write your code here to execute after dialog
							dialog.cancel();
						}
					});

			// closed

			// Showing Alert Message
			alertDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return null;
		}

		// alertDialog = new AlertDialog.Builder(ScannerForTools.this);
		// alertDialog.setTitle(alertDialogMessage);
		// alertDialog.setMessage(messageFromServer_Direction);
		// alertDialog.setIcon(R.drawable.checkmark_large1);
		// alertDialog.setPositiveButton("Ok",
		// new DialogInterface.OnClickListener() {
		// public void onClick(DialogInterface dialog, int which) {
		// Intent scanner = new Intent(ScannerForTools.this,
		// ScannerForTools.class);
		// startActivity(scanner);
		// }
		// });
		// alertDialog.show();

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (alertDialog != null) {
				((DialogInterface) alertDialog).dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

}
