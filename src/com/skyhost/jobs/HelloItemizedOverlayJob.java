package com.skyhost.jobs;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.view.View;
import com.google.android.maps.ItemizedOverlay;

public class HelloItemizedOverlayJob extends ItemizedOverlay {
	View info;
	private ArrayList<JobOverlay> mOverlays = new ArrayList<JobOverlay>();
	private Context mContext;
	public static String selectedJobIndex = "";

	public HelloItemizedOverlayJob(Drawable defaultMarker) {
		super(boundCenterBottom(defaultMarker));
	}

	public HelloItemizedOverlayJob(Drawable defaultMarker, Context context) {
		super(boundCenterBottom(defaultMarker));
		mContext = context;
	}
	
	@Override
	protected JobOverlay createItem(int i) {	
		return (JobOverlay) mOverlays.get(i);
	}

	@Override
	public int size() {
		return mOverlays.size();
	}

	@Override
	protected boolean onTap(final int index) {
		final JobOverlay item = mOverlays.get(index);
		AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
		dialog.setMessage(item.getSnippet())
				.setTitle(item.getTitle())
				.setCancelable(true)
				.setIcon(R.drawable.icon_informationgreen)
				.setPositiveButton("Info", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {

						Integer temp = Integer.parseInt(item.getIndex());
						temp -= 1;
						String jobIndex = temp.toString();
						Intent intent = new Intent(mContext,
								Job.class);
						 saveSettingString("selectedJobIndex", jobIndex);
							saveSettingString("sCommand", "onejob");
						 
						mContext.startActivity(intent);
					}		
				});
		AlertDialog alert = dialog.create();
		alert.show();
		return true;
	}

	public void addOverlay(JobOverlay overlayitem) {
		mOverlays.add(overlayitem);
		populate();
	}
	
	private void saveSettingInt(String tag, int content) {
		SharedPreferences preferences = mContext.getSharedPreferences(
				Job.PREFERENCES, Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(tag, content);
		editor.commit();
	}

	private void saveSettingString(String tag, String content) {
		SharedPreferences preferences = mContext.getSharedPreferences(
				HelloGoogleMaps.PREFERENCES, Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}
	
	private String loadSetting(String tag) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		String content = preferences.getString(tag, "");
		return content;
	}	
}