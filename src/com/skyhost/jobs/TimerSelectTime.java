package com.skyhost.jobs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class TimerSelectTime extends Activity implements OnClickListener {

	private Handler handler = new Handler();
	private ProgressDialog progressDialog;

	private Runnable runUpdateTask = new Runnable() {
		public void run() {

			new AsyncTimerTimeAccounts().execute();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timerselecttime);

		new PauseDialogLoader().execute();

		// Initializing
		// Hours
		String[] hours = new String[25];
		for (int i = 0; i < hours.length; i++) {
			hours[i] = i + "";
		}

		Spinner spinnerHours = (Spinner) findViewById(R.id.spinner_hours);
		ArrayAdapter adapterHours = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, hours);
		adapterHours
				.setDropDownViewResource(R.layout.rowlayout_timerselecttime_spinnerhours);
		spinnerHours.setAdapter(adapterHours);
		spinnerHours.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				String selectedHour = "0";
				selectedHour = String.valueOf(position);
				saveSetting("selectedHour", selectedHour);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		// Minutes
		String selectedMinute = "00";
		String[] minutes = new String[4];
		minutes[0] = "00";
		minutes[1] = "15";
		minutes[2] = "30";
		minutes[3] = "45";

		Spinner spinnerMinutes = (Spinner) findViewById(R.id.spinner_minutes);
		ArrayAdapter adapterMinutes = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, minutes);
		adapterMinutes
				.setDropDownViewResource(R.layout.rowlayout_timerselecttime_spinnerminutes);
		spinnerMinutes.setAdapter(adapterMinutes);
		spinnerMinutes.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				if (position == 0) {
					saveSetting("selectedMinute", "00");
				} else if (position == 1) {
					saveSetting("selectedMinute", "15");
				} else if (position == 2) {
					saveSetting("selectedMinute", "30");
				} else if (position == 3) {
					saveSetting("selectedMinute", "45");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		handler = new Handler();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (handler != null) {
			handler.removeCallbacks(runUpdateTask);
		}

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (handler != null) {
			handler.post(runUpdateTask);
		}

		if (progressDialog != null) {
			progressDialog.hide();
		}
	}

	private void updateScreen() {

		String[] timeAccounts = new String[WebApi.getWebApi()
				.getTimer_TimeAccount_Name().size()];

		for (int i = 0; i < WebApi.getWebApi().getTimer_TimeAccount_Name()
				.size(); i++) {
			timeAccounts[i] = WebApi.getWebApi().getTimer_TimeAccount_Name()
					.get(i);
		}

		Spinner spinnerTimeAccount = (Spinner) findViewById(R.id.spinner_timetypeofhour);
		ArrayAdapter adapterTimeAccount = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, timeAccounts);
		adapterTimeAccount
				.setDropDownViewResource(R.layout.rowlayout_timerselecttime_spinnertimeaccount);
		spinnerTimeAccount.setAdapter(adapterTimeAccount);
		spinnerTimeAccount
				.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> parentView,
							View selectedItemView, int position, long id) {
						String selectedTimeAccount = WebApi.getWebApi()
								.getTimer_TimeAccount_Number().get(position);
						saveSetting("selectedTimeAccount", selectedTimeAccount);
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
					}
				});

		Button saveButton = (Button) findViewById(R.id.button_timerfilltime_save);
		saveButton.setOnClickListener(this);

		if (progressDialog != null) {
			progressDialog.hide();
		}
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.timerfilltime, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			Intent jobs = new Intent(TimerSelectTime.this, Jobs.class);
			startActivity(jobs);
			return true;

		case R.id.activity_timer: // Job
			Intent timer = new Intent(TimerSelectTime.this, Timer.class);
			startActivity(timer);
			return true;
		}
		return true;
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.button_timerfilltime_save) {
			EditText editText_Ordernumber = (EditText) findViewById(R.id.edittext_ordernumber);

			String selectedOrderNumber = editText_Ordernumber.getText()
					.toString();
			saveSetting("selectedOrderNumber", selectedOrderNumber);

			String selectedMinute = "";
			if (!loadSetting("selectedMinute").equals("")) {
				selectedMinute = loadSetting("selectedMinute");
			}

			String selectedHour = "";
			if (!loadSetting("selectedHour").equals("")) {
				selectedHour = loadSetting("selectedHour");
			}

			// If nothing is not selected from hour and minute
			if (selectedMinute.equals("00") && selectedHour.equals("0")) {
				Toast.makeText(getApplicationContext(),
						"Du skal v�lge tid: Timer eller minutter",
						Toast.LENGTH_LONG).show();
			} else {

				new AsyncsaveTimeAndTripsOnJob().execute();
			}
		}
	}

	private class AsyncTimerTimeAccounts extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			WebApi.getWebApi().getTimer_TimeAccounts(preferences);

			if (WebApi.getWebApi().isNewData_Timer_TimeAccounts() == true) {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						updateScreen();
					}
				});
				if (preferences.getBoolean("AutoUpdate", false) == true) {
					handler.postDelayed(runUpdateTask, 30000);
				}

			} else {
				handler.postDelayed(runUpdateTask, 3000);
			}

			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class AsyncsaveTimeAndTripsOnJob extends
			AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			String selectedMinute = "";
			if (!loadSetting("selectedMinute").equals("")) {
				selectedMinute = loadSetting("selectedMinute");
			}

			String selectedHour = "";
			if (!loadSetting("selectedHour").equals("")) {
				selectedHour = loadSetting("selectedHour");
			}

			int hours = Integer.parseInt(selectedHour);
			int minutesFromHours = hours * 60;
			int minutes = Integer.parseInt(selectedMinute);
			int total = minutesFromHours + minutes;
			String totalMinutes = String.valueOf(total);

			String selectedRoute = "";
			if (!(loadSetting("selectedTripsIdsOnDay").equals(""))) {
				selectedRoute = loadSetting("selectedTripsIdsOnDay");
			}
			String selectedJobId = "";
			if (!(loadSetting("selectedJobIdOnDay").equals(""))) {
				selectedJobId = loadSetting("selectedJobIdOnDay");
			}
			String selectedDate = "";
			if (!(loadSetting("selectedDate").equals(""))) {
				selectedDate = loadSetting("selectedDate");
			}

			// If fields are not null or empty
			if (!selectedJobId.equals("")) {

				SharedPreferences preferences = getSharedPreferences(
						"PREFERENCES", Activity.MODE_PRIVATE);

				String selectedTimeAccount = "";
				if (!loadSetting("selectedTimeAccount").equals("")) {
					selectedTimeAccount = loadSetting("selectedTimeAccount");
				}

				String selectedOrderNumber = "";
				if (!loadSetting("selectedOrderNumber").equals("")) {
					selectedOrderNumber = loadSetting("selectedOrderNumber");
				}

				WebApi.getWebApi().saveTimeAndTripsOnJob(preferences,
						selectedOrderNumber, totalMinutes, selectedTimeAccount,
						selectedRoute, selectedJobId, selectedDate);
			}

			if (WebApi.getWebApi().isNewData_SaveTimeAndTripsOnJobUrl_Timer() == false) {
				showToast("Fejlkode: "
						+ WebApi.getWebApi().getResponse_ErrorCode(), WebApi
						.getWebApi().getResponse_ErrorText()
						+ " Pr�v igen senere!");

				Intent status = new Intent(TimerSelectTime.this, Jobs.class);
				startActivity(status);
			} else if (WebApi.getWebApi().isNewData_Response() == false) {
				if (WebApi.getWebApi().getResponse_Status() != null) {
					if (!WebApi.getWebApi().getResponse_ErrorText().equals("")) {
						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");

						Intent intent = new Intent(TimerSelectTime.this,
								TimerNewJob.class);
						startActivity(intent);
					} else {
						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");

						Intent intent = new Intent(TimerSelectTime.this,
								TimerNewJob.class);
						startActivity(intent);
					}
				}
			} else if (WebApi.getWebApi()
					.isNewData_SaveTimeAndTripsOnJobUrl_Timer() == true) {
				String tmp = WebApi.getWebApi().getResponse_Status();
				if (tmp.equals("OK")) {
					showToast("Din registrering er gemt");

					saveSetting("reloadDataForTimer", "true");

					Intent intent = new Intent(TimerSelectTime.this,
							Timer.class);
					startActivity(intent);
				}
			} else {
				Intent intent = new Intent(TimerSelectTime.this,
						TimerNewJob.class);
				startActivity(intent);
			}

			return null;
		}

		protected void showToast(final String sHeader, final String sComment) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sComment, Toast.LENGTH_LONG)
							.show();
				}
			});
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(TimerSelectTime.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();

			// // Set Runnable to remove splash screen just in case
			// final Handler handler = new Handler();
			// handler.postDelayed(new Runnable() {
			// @Override
			// public void run() {
			// if(progressDialog != null) {
			// progressDialog.dismiss();}
			// }
			// }, 5000);
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}
}
