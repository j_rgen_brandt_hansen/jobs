package com.skyhost.jobs;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterTimerSelectJob extends ArrayAdapter<String> {

	private final Activity context;
	private String[] headers;
	private String[] orderNumbers;

	public ArrayAdapterTimerSelectJob(Activity context, String[] headers,
			String[] orderNumbers, String[] jobIds) {
		super(context, R.layout.rowlayout_timerselectjob, headers);

		this.context = context;
		this.headers = headers;
		this.orderNumbers = orderNumbers;
	}

	static class ViewHolder {

		public TextView textView_Header;
		public TextView textView_OrderNumbers;
		public TextView textView_JobId;
	}

	@Override
	public int getCount() {
		return headers.length;
	}

	@Override
	public String getItem(int position) {
		return headers[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;

		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.rowlayout_timerselectjob, null);
			holder = new ViewHolder();
			holder.textView_Header = (TextView) convertView
					.findViewById(R.id.timerselectjob_header);
			holder.textView_OrderNumbers = (TextView) convertView
					.findViewById(R.id.timerselectjob_ordernumber);
			holder.textView_JobId = (TextView) convertView
					.findViewById(R.id.timerselectjob_jobid);
			convertView.setTag(holder);
		} else {
			view = convertView;
			holder = (ViewHolder) view.getTag();
		}
		holder.textView_Header.setText(headers[position]);
		if (orderNumbers[position] != null) {
			holder.textView_OrderNumbers.setText("Ordre nr.: "
					+ orderNumbers[position]);
		} else
			holder.textView_OrderNumbers.setText("");
		return convertView;
	}
}