package com.skyhost.jobs;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterMaterialsOnJob extends ArrayAdapter<String> {

	private Activity context;
	private String[] matId;
	private String[] timestamp;
	private String[] internalItemNo;
	private String[] itemName;
	private String[] psc; // amount
	private String[] user;

	public ArrayAdapterMaterialsOnJob(Activity context, String[] matId,
			String[] timestamp, String[] internalItemNo, String[] itemName,
			String[] psc, String[] user) {
		super(context, R.layout.rowlayout_materialsonjob, itemName);

		this.context = context;
		this.matId = matId;
		this.timestamp = timestamp;
		this.internalItemNo = internalItemNo;
		this.itemName = itemName;
		this.psc = psc;
		this.user = user;
	}

	static class ViewHolder {
		public TextView matId;
		public TextView timestamp;
		public TextView internalItemNo;
		public TextView itemName;
		public TextView amountHeader;
		public TextView amount;
		public TextView user;
	}

	@Override
	public int getCount() {
		return itemName.length;
	}

	@Override
	public String getItem(int position) {
		return itemName[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = null;
		ViewHolder holder = null;

		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.rowlayout_materialsonjob, null);
			holder = new ViewHolder();
			holder.matId = (TextView) convertView
					.findViewById(R.id.materialsonjob_matid);
			holder.timestamp = (TextView) convertView
					.findViewById(R.id.materialsonjob_timestamp);
			holder.internalItemNo = (TextView) convertView
					.findViewById(R.id.materialsonjob_internalitemno);
			holder.itemName = (TextView) convertView
					.findViewById(R.id.materialsonjob_itemname);
			holder.amount = (TextView) convertView
					.findViewById(R.id.materialsonjob_header);
			holder.amount = (TextView) convertView
					.findViewById(R.id.materialsonjob_psc);
			holder.user = (TextView) convertView
					.findViewById(R.id.materialsonjob_user);
			convertView.setTag(holder);
		} else {
			view = convertView;
			holder = (ViewHolder) view.getTag();
		}
		if (matId[position] != null) {

			holder.matId.setText(matId[position]);
			holder.timestamp.setText("Dato: " + timestamp[position]);
			holder.internalItemNo.setText("Varenummer: "
					+ internalItemNo[position]);
			holder.itemName.setText(itemName[position]);
			holder.amount.setText(psc[position]);
			holder.user.setText("Tilf�jet af: " + user[position]);
		}
		return convertView;
	}
}