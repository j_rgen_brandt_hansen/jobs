package com.skyhost.jobs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

public class Settings extends Activity {

	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.settings);

		// Initializing
			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);

		EditText newUsername = (EditText) findViewById(R.id.username);
		EditText newPassword = (EditText) findViewById(R.id.password);

		
		final ToggleButton NewAutoUpdateSetting = (ToggleButton) findViewById(R.id.AutoRefresh);

		newUsername.setText(preferences.getString("Username", ""));
		newPassword.setText(preferences.getString("Password", ""));
		NewAutoUpdateSetting.setChecked(preferences.getBoolean("AutoUpdate",
				false));
		if (SplashScreen.getResponse_ErrorText() != null) {
			if (!SplashScreen.getResponse_ErrorText().equals("")||!SplashScreen.getResponse_ErrorText().equals("Job created")) {
				LinearLayout linearLayoutErrorCode = (LinearLayout) findViewById(R.id.settings_linearlayout_errorcode);
				TextView textView_ErrorCode = new TextView(this);
				String error = SplashScreen.getResponse_ErrorText();
				textView_ErrorCode.setText(error);
				textView_ErrorCode.setTextSize(20);
				textView_ErrorCode.setPadding(20, 0, 20, 0);
				textView_ErrorCode.setTypeface(null, Typeface.BOLD);
				textView_ErrorCode.setTextColor(Color.BLACK);
				linearLayoutErrorCode.addView(textView_ErrorCode);
			}
		}
		Button b = (Button) findViewById(R.id.buttonBack);
		b.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {

				EditText NewUsername = (EditText) findViewById(R.id.username);
				EditText NewPassword = (EditText) findViewById(R.id.password);
				
				SharedPreferences preferences = getSharedPreferences(
						"PREFERENCES", Activity.MODE_PRIVATE);
				SharedPreferences.Editor editor = preferences.edit();
				editor.putString("Username", NewUsername.getText().toString());
				editor.putString("Password", NewPassword.getText().toString());
				if (NewAutoUpdateSetting.isChecked() == true) {
					editor.putBoolean("AutoUpdate", true);
				} else {
					editor.putBoolean("AutoUpdate", false);
				}
				// Commit the edits!
				editor.commit();
				WebApi.getWebApi().setResponse_ErrorText("");
				setResult(RESULT_OK);
				Intent intent = new Intent(Settings.this, SplashScreen.class);
				Settings.this.startActivity(intent);
				finish();
			}
		});
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(Settings.this, SplashScreen.class);
		Settings.this.startActivity(intent);
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}
}
