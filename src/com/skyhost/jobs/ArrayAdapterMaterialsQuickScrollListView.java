package com.skyhost.jobs;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterMaterialsQuickScrollListView extends
		ArrayAdapter<String> {

	private Activity context;
	private String[] jobs_Material_MatID; // ikke null
	private String[] jobs_Material_Name;
//	private String[] jobs_Material_Description;
//	private String[] jobs_Material_Colli;
//	private String[] jobs_Material_GroupNo;
//	private String[] jobs_Material_SupplierItemNo;
//	private String[] jobs_Material_EanNo;
//	private String[] jobs_Material_EanBarcode;
//	private String[] jobs_Material_InternalBarcode;
//	private String[] jobs_Material_InternalItemNo;
//	private String[] jobs_Material_InternalItemShortNo;

	public ArrayAdapterMaterialsQuickScrollListView(Activity context,
			String[] jobs_Material_MatID, String[] jobs_Material_Name){
//			String[] jobs_Material_Description, String[] jobs_Material_Colli,
//			String[] jobs_Material_GroupNo,
//			String[][] jobs_Material_SupplierItemNo,
//			String[] jobs_Material_EanNo, String[] jobs_Material_EanBarcode,
//			String[] jobs_Material_InternalBarcode,
//			String[] jobs_Material_InternalItemNo,
//			String[] jobs_Material_InternalItemShortNo) {
		super(context, R.layout.rowlayout_materialsquickscrolllistview,
				jobs_Material_Name);

		this.context = context;
		this.jobs_Material_MatID = jobs_Material_MatID;
		this.jobs_Material_Name = jobs_Material_Name;
//		this.jobs_Material_Description = jobs_Material_Description;
//		this.jobs_Material_Colli = jobs_Material_Colli;
//		this.jobs_Material_GroupNo = jobs_Material_GroupNo;
//		this.jobs_Material_SupplierItemNo = jobs_Material_SupplierItemNo;
//		this.jobs_Material_EanNo = jobs_Material_EanNo;
//		this.jobs_Material_EanBarcode = jobs_Material_EanBarcode;
//		this.jobs_Material_InternalBarcode = jobs_Material_InternalBarcode;
//		this.jobs_Material_InternalItemNo = jobs_Material_InternalItemNo;
//		this.jobs_Material_InternalItemShortNo =jobs_Material_InternalItemShortNo;
	}

	static class ViewHolder {
		public TextView name;
		public TextView id;
//		public TextView description;
//		public TextView colli;
//		public TextView groupNo;
//		public TextView supplierItemNo;
//		public TextView eanNo;
//		public TextView eanBarcode;
//		public TextView internalBarcode;
//		public TextView internalItemNo;
//		public TextView internalItemShortNo;
	}

	@Override
	public int getCount() {
		return jobs_Material_Name.length;
	}

	@Override
	public String getItem(int position) {
		return jobs_Material_Name[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = null;
		ViewHolder holder = null;

		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.rowlayout_materialsquickscrolllistview, null);
			holder = new ViewHolder();
			holder.id = (TextView) convertView
					.findViewById(R.id.quickscrolllistview_material_id);
			holder.name = (TextView) convertView
					.findViewById(R.id.quickscrolllistview_material_name);
//			holder.description = (TextView) convertView
//					.findViewById(R.id.);
//			holder.colli = (TextView) convertView
//					.findViewById(R.id.);
//			holder.groupNo = (TextView) convertView
//					.findViewById(R.id.);
//			holder.supplierItemNo = (TextView) convertView
//					.findViewById(R.id.);
//			holder.eanNo = (TextView) convertView
//					.findViewById(R.id.);
//			holder.eanBarcode = (TextView) convertView
//					.findViewById(R.id.);
//			holder.internalBarcode = (TextView) convertView.findViewById(R.id.);
//			holder.internalItemNo = (TextView) convertView
//					.findViewById(R.id.);
//			holder.internalItemShortNo = (TextView) convertView.findViewById(R.id.);
			
			convertView.setTag(holder);
		} else {
			view = convertView;
			holder = (ViewHolder) view.getTag();
		}

		//holder.id.setText(jobs_Material_MatID[position]);
		holder.name.setText(jobs_Material_Name[position]);

		return convertView;
	}
}
