package com.skyhost.jobs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;

public class GpsApi extends Activity {
	// GPS
	private boolean isGPSFix = false;
	private Location mLastLocation;
	private long mLastLocationMillis;
	private String latitude;
	private String longitude;
	protected static GpsStatus.Listener myGPSListener;
	protected static LocationManager locationManager;
	protected static LocationListener locationListener;
	private boolean runOnce;

	// Singleton
	private static GpsApi gpsApi;

	public static synchronized GpsApi getGpsApi() {
		if (gpsApi == null) {

			gpsApi = new GpsApi();
		}
		return gpsApi;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.job);
		// adpt = ((globalAdapter)getApplicationContext());
	}

	@Override
	public void onPause() {
		super.onPause();

		// Turn off GPS listener
		turnGpsOff();
	}

	public void turnGpsOff() {
		if (locationManager != null) {
			locationManager.removeGpsStatusListener(myGPSListener);
		}

		if (locationManager != null) {
			locationManager.removeUpdates(locationListener);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		runOnce = true;

		turnGpsOn();
	}

	void turnGpsOn() {
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationListener = new MyLocationListener();
		myGPSListener = new MyGPSListener();

		// Detect GPS settings
		if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			showGpsOptions();
		}

		// Turn on GPS listener AND NETWORK LISTENER
		locationManager.addGpsStatusListener(myGPSListener);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				2000, 0, locationListener);
		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 2000, 0, locationListener);
	}

	public boolean bRegistrationOngoing = false;

	public class MyGPSListener implements GpsStatus.Listener {

		public void onGpsStatusChanged(int event) {
			switch (event) {
			case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
				if (mLastLocation != null)
					isGPSFix = (SystemClock.elapsedRealtime() - mLastLocationMillis) < 10000;

				if (isGPSFix) { // A fix has been acquired.
					runOnUiThread(new Runnable() {

						@Override
						public void run() {

						}
					});
				} else { // The fix has been lost.
					runOnUiThread(new Runnable() {

						@Override
						public void run() {

						}
					});
				}
				break;
			case GpsStatus.GPS_EVENT_FIRST_FIX:
				isGPSFix = true;
				break;
			}
		}
	}

	public class MyLocationListener implements LocationListener {
		@Override
		public void onLocationChanged(Location loc) {
			if (loc != null) {
				String lat = String.valueOf(loc.getLatitude());
				String lon = String.valueOf(loc.getLongitude());

				saveSetting("latitude", lat.toString());
				saveSetting("longitude", lon.toString());

				// GPS lost detection
				mLastLocationMillis = SystemClock.elapsedRealtime();
				mLastLocation = loc;

				latitude = Double.toString(loc.getLatitude());
				longitude = Double.toString(loc.getLongitude());
			}
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	private void showGpsOptions() {
		Intent gpsOptionsIntent = new Intent(
				android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		startActivity(gpsOptionsIntent);
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private void saveSetting(String tag, Boolean content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(tag, content);
		editor.commit();
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private Boolean loadSettingBoolean(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		Boolean content = preferences.getBoolean(tag, false);
		return content;
	}

}
