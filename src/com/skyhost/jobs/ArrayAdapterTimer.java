package com.skyhost.jobs;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ArrayAdapterTimer extends ArrayAdapter<String> {

	private static Activity context;
	
	public ArrayAdapterTimer(Activity context, String[] dayNames) {
		super(context, R.layout.rowlayout_timer, 10);
	}

	static class ViewHolder {
		public LinearLayout header;
		public TextView day;
		public ImageView arrow_right;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		View view = null;
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.rowlayout_notes, null, true);
			holder = new ViewHolder();
			holder.day = (TextView) rowView
					.findViewById(R.id.textview_timer_day);
			holder.arrow_right = (ImageView) rowView
					.findViewById(R.id.imageview_arrow_right);
		} else {
			view = convertView;
			holder = (ViewHolder) rowView.getTag();
		}
		holder.day.setText("tekst");
		holder.arrow_right.setImageResource(R.drawable.icon_arrow_right_32);
		return convertView;
	}
}