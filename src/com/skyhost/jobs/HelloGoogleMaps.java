package com.skyhost.jobs;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

@SuppressLint("NewApi")
public class HelloGoogleMaps extends FragmentActivity implements
		OnInfoWindowClickListener, InfoWindowAdapter {

	// private final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;
	// private final int REQUEST_CODE_PICK_ACCOUNT = 1002;
	private ProgressDialog progressDialog;
	private EditText searchBarEditText;
	private Map<Marker, String> jobMap;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		updateScreen();
	}

	private void updateScreen() {

		int height = 400;
		int width = 400;
		try {
			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);

			height = ((int) metrics.heightPixels);
			width = ((int) metrics.widthPixels);
		} catch (Exception ex) {
		}

		final GoogleMap googleMap = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.mapview)).getMap();

		String sCommand = "";

		if (!loadSetting("sCommand").equals("")) {
			sCommand = loadSetting("sCommand");
		}

		jobMap = new HashMap<Marker, String>();

		if (sCommand.equals("allJobs") || sCommand.equals("allJobsNotOneJob")) {
			// int padding = 0;
			LatLngBounds.Builder builder = new LatLngBounds.Builder();
			JSONArray jsonArrayAllJobs = new JSONArray();
			try {

				if (!loadSetting("allJobs").equals("")) {
					jsonArrayAllJobs = new JSONArray(loadSetting("allJobs"));
				}
			} catch (JSONException e) {
				return;
			}

			try {
				jsonArrayAllJobs = new JSONArray(loadSetting("allJobs"));

				for (int i = 0; i < jsonArrayAllJobs.length(); i++) {
					if (WebApi.getWebApi().getJobs_address_street()[i] != null) {
						if (!WebApi.getWebApi().getJobs_address_lat()[i][0]
								.equals("")) {

							double lat = Double.parseDouble(WebApi.getWebApi()
									.getJobs_address_lat()[i][0]);
							double lng = Double.parseDouble(WebApi.getWebApi()
									.getJobs_address_lon()[i][0]);

							if (lat != 0) {
								LatLng point = new LatLng(lat, lng);
								placeMarker(googleMap, builder, i, point);
								googleMap
										.setOnMarkerClickListener(new OnMarkerClickListener() {
											@Override
											public boolean onMarkerClick(
													Marker marker) {
												marker.showInfoWindow();
												return false;
											}
										});
								googleMap
										.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

											@Override
											public void onInfoWindowClick(
													Marker marker) {
												Integer temp = Integer
														.parseInt(jobMap
																.get(marker));
												String poiIndex = temp
														.toString();
												saveSetting("selectedJobIndex",
														poiIndex);
												saveSetting("sCommand",
														"oneJob");
												Intent intent = new Intent(
														HelloGoogleMaps.this,
														Job.class);
												startActivity(intent);
											}
										});
							}

							LatLngBounds bounds = builder.build();
							googleMap.getUiSettings().setCompassEnabled(true);
							googleMap.getUiSettings().setZoomControlsEnabled(
									true);
							googleMap.animateCamera(CameraUpdateFactory
									.zoomTo(10));
							googleMap
									.moveCamera(CameraUpdateFactory
											.newLatLngBounds(bounds, width,
													height, 30));
						}
					}
				}
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}

		// Set Job Address

		if (sCommand.equals("setJobAddress")) {

			final LatLngBounds.Builder builder = new LatLngBounds.Builder();

			int index = 0;
			if (!(loadSetting("selectedJobIndex").equals(""))) {
				index = Integer.parseInt(loadSetting("selectedJobIndex"));
			}
			final int selectedJobIndex = index;
			if (WebApi.getWebApi().getJobs_address_street()[selectedJobIndex] != null) {
				if (!WebApi.getWebApi().getJobs_address_lat()[selectedJobIndex][0]
						.equals("")) {

					final double lat = Double.parseDouble(WebApi.getWebApi()
							.getJobs_address_lat()[selectedJobIndex][0]);
					final double lng = Double.parseDouble(WebApi.getWebApi()
							.getJobs_address_lon()[selectedJobIndex][0]);
					final int widthTemporary = width;
					final int heightTemporary = height;

					FrameLayout searchBarLinearLayout = (FrameLayout) findViewById(R.id.hellogooglemap_searchbar_linearlayout);
					searchBarEditText = (EditText) findViewById(R.id.hellogooglemap_searchbar_edittext);
					searchBarEditText.setSelected(false);
					Button searchBarButton = (Button) findViewById(R.id.hellogooglemap_searchbar_Button);

					Button searchBarRedCross = (Button) findViewById(R.id.hellogooglemap_searchbar_red_cross);
					searchBarRedCross.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							searchBarEditText.setText("");

							InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

							inputManager.toggleSoftInput(
									InputMethodManager.SHOW_IMPLICIT, 0);
						}
					});
					searchBarLinearLayout.setVisibility(View.VISIBLE);
					searchBarEditText.setText(WebApi.getWebApi()
							.getJobs_address_street()[selectedJobIndex][0]);
					searchBarEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
					searchBarEditText
							.setOnEditorActionListener(new EditText.OnEditorActionListener() {
								@Override
								public boolean onEditorAction(TextView v,
										int actionId, KeyEvent event) {
									if (actionId == EditorInfo.IME_ACTION_DONE) {
										searchNewAddress(googleMap, builder,
												selectedJobIndex,
												widthTemporary, heightTemporary);
										return true;
									}
									return false;
								}
							});

					searchBarButton.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {

							searchNewAddress(googleMap, builder,
									selectedJobIndex, widthTemporary,
									heightTemporary);
						}
					});

					if (lat != 0) {
						LatLng point = new LatLng(lat, lng);
						placeMarker(googleMap, builder, selectedJobIndex, point);
						googleMap
								.setOnMarkerClickListener(new OnMarkerClickListener() {
									@Override
									public boolean onMarkerClick(Marker marker) {
										marker.showInfoWindow();
										return false;
									}
								});
						googleMap
								.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

									@Override
									public void onInfoWindowClick(Marker marker) {
										Integer temp = Integer.parseInt(jobMap
												.get(marker));
										String poiIndex = temp.toString();
										saveSetting("selectedJobIndex",
												poiIndex);
										saveSetting("sCommand", "oneJob");

										new DialogLoaderChangeAddress()
												.execute();
									}
								});
						googleMap
								.setOnMarkerDragListener(new OnMarkerDragListener() {

									@Override
									public void onMarkerDragStart(Marker arg0) {

									}

									@Override
									public void onMarkerDragEnd(Marker marker) {

										setOnMarkerDragEnd(googleMap, builder,
												selectedJobIndex, marker);
									}

									@Override
									public void onMarkerDrag(Marker arg0) {
									}
								});
					}

					LatLngBounds bounds = builder.build();
					googleMap.getUiSettings().setCompassEnabled(true);
					googleMap.getUiSettings().setZoomControlsEnabled(true);

					googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
							bounds, width, height, 30));
					googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
				}
			}
		}

		// New Job Address

		if (sCommand.equals("newJobAddress")) {
			// turnGPSOn();
			final LatLngBounds.Builder builder = new LatLngBounds.Builder();

			final String suggestedNewJobAddress = "";
			if (!loadSetting("suggestedNewJobAddress").equals("")) {

				final int widthTemporary = width;
				final int heightTemporary = height;

				searchBarEditText = (EditText) findViewById(R.id.hellogooglemap_searchbar_edittext);
				searchBarEditText
						.setText(loadSetting("suggestedNewJobAddress"));
				searchBarEditText.setSelected(true);

				int iSelectedJobIndex = -1;
				if (!(loadSetting("selectedJobIndex").equals(""))) {
					String temp = loadSetting("selectedJobIndex");
					iSelectedJobIndex = Integer.parseInt(temp);
				}
				final int selectedJobIndex = iSelectedJobIndex;

				searchNewAddress(googleMap, builder, selectedJobIndex,
						widthTemporary, heightTemporary);

				FrameLayout searchBarLinearLayout = (FrameLayout) findViewById(R.id.hellogooglemap_searchbar_linearlayout);
				searchBarEditText = (EditText) findViewById(R.id.hellogooglemap_searchbar_edittext);
				searchBarEditText.setSelected(false);
				Button searchBarButton = (Button) findViewById(R.id.hellogooglemap_searchbar_Button);

				Button searchBarRedCross = (Button) findViewById(R.id.hellogooglemap_searchbar_red_cross);
				searchBarRedCross.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						searchBarEditText.setText("");

						InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

						inputManager.toggleSoftInput(
								InputMethodManager.SHOW_IMPLICIT, 0);
					}
				});
				searchBarLinearLayout.setVisibility(View.VISIBLE);
				searchBarEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
				searchBarEditText
						.setOnEditorActionListener(new EditText.OnEditorActionListener() {
							@Override
							public boolean onEditorAction(TextView v,
									int actionId, KeyEvent event) {
								if (actionId == EditorInfo.IME_ACTION_DONE) {
									searchNewAddress(googleMap, builder,
											selectedJobIndex, widthTemporary,
											heightTemporary);
									return true;
								}
								return false;
							}
						});

				searchBarButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						searchNewAddress(googleMap, builder, selectedJobIndex,
								widthTemporary, heightTemporary);
					}
				});
			}
			// NO address - use phone GPS to find location
			else {
				// turnGPSOn();

				// Getting Google Play availability status
				int status = GooglePlayServicesUtil
						.isGooglePlayServicesAvailable(getBaseContext());

				// Showing status
				if (status != ConnectionResult.SUCCESS) { // Google Play
															// Services are not
															// available

					int requestCode = 10;
					Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
							status, this, requestCode);
					dialog.show();

				} else { // Google Play Services are available

					// GoogleMap googleMap1 = ((SupportMapFragment)
					// getSupportFragmentManager()
					// .findFragmentById(R.id.mapview)).getMap();
					//
					// // Enabling MyLocation Layer of Google Map
					// googleMap1.setMyLocationEnabled(true);
					//
					// // Getting LocationManager object from System Service
					// LOCATION_SERVICE
					// LocationManager locationManager = (LocationManager)
					// getSystemService(LOCATION_SERVICE);
					//
					// // Creating a criteria object to retrieve provider
					// Criteria criteria = new Criteria();
					//
					// // Getting the name of the best provider
					// String provider =
					// locationManager.getBestProvider(criteria, true);
					//
					// // Getting Current Location
					// Location location =
					// locationManager.getLastKnownLocation(provider);
					//
					// if(location!=null){
					// onLocationChanged(location);
					// }
					// locationManager.requestLocationUpdates(provider, 20000,
					// 0, this);

					new AsyncLoaderUpdateGoogleMap().execute();
				}
			}
		}

		else if (sCommand.equals("oneJob")) {
			int index = 0;
			String selectedJobIndex = "";
			if (!(loadSetting("selectedJobIndex").equals(""))) {
				selectedJobIndex = loadSetting("selectedJobIndex");
				index = Integer.parseInt(selectedJobIndex);
			}

			if (WebApi.getWebApi().getJobs_address_lat()[index] != null) {
				String[] tmp = WebApi.getWebApi().getJobs_address_lat()[index];
				if (!WebApi.getWebApi().getJobs_address_lat()[index][0]
						.equals("")) {
					double lat = Double.parseDouble(WebApi.getWebApi()
							.getJobs_address_lat()[index][0]);
					double lng = Double.parseDouble(WebApi.getWebApi()
							.getJobs_address_lon()[index][0]);

					if (lat != 0) {
						LatLng point = new LatLng(lat, lng);
						Marker markerId = googleMap
								.addMarker(new MarkerOptions()
										.position(point)

										.title(WebApi.getWebApi()
												.getJobs_title()[index])
										.snippet(
												WebApi.getWebApi()
														.getJobs_address_street()[index][0])
										.icon(BitmapDescriptorFactory
												.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
						jobMap.put(markerId, Integer.toString(index));
						googleMap
								.setOnMarkerClickListener(new OnMarkerClickListener() {

									@Override
									public boolean onMarkerClick(Marker marker) {
										marker.showInfoWindow();
										return false;
									}
								});
						googleMap
								.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

									@Override
									public void onInfoWindowClick(Marker marker) {
										Intent intent = new Intent(
												HelloGoogleMaps.this, Job.class);
										saveSetting("sCommand", "oneJob");
										startActivity(intent);
									}
								});
						googleMap.getUiSettings().setCompassEnabled(true);
						googleMap.getUiSettings().setZoomControlsEnabled(true);
						googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
						googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
								point, 15));
					}
				} else {
					Toast.makeText(getApplicationContext(),
							"Ingen Positioner p� det valgte job!", 5000).show();
					Intent intent = new Intent(HelloGoogleMaps.this, Jobs.class);
					startActivity(intent);
				}
			} else {
				Toast.makeText(getApplicationContext(),
						"Ingen Positioner p� det valgte job!", 5000).show();
				Intent intent = new Intent(HelloGoogleMaps.this, Jobs.class);
				startActivity(intent);
			}
		}
	}

	/**
	 * @param googleMap
	 * @param builder
	 * @param selectedJobIndex
	 * @param widthTemporary
	 * @param heightTemporary
	 */
	private void searchNewAddress(final GoogleMap googleMap,
			final LatLngBounds.Builder builder, final int selectedJobIndex,
			final int widthTemporary, final int heightTemporary) {

		// Remove Softkeyboard from screen
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.toggleSoftInput(InputMethodManager.RESULT_HIDDEN, 0);

		String temporaryAddressJob = searchBarEditText.getText().toString();

		if (temporaryAddressJob.equals("")) {
			Toast.makeText(getApplicationContext(), "Udfyld s�gefelt...",
					Toast.LENGTH_LONG).show();
		} else {

			String[] params = new String[1];
			params[0] = String.valueOf(temporaryAddressJob);

			try {
				new AsyncGetAddressInfo().execute(params).get(20,
						TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			} catch (TimeoutException e) {
				Toast.makeText(getApplicationContext(),
						"Adressen kunne ikke findes - Pr�v igen",
						Toast.LENGTH_SHORT);
			} catch (Exception e) {
				e.printStackTrace();
			}

			// TODO Timeout and NO answer
			double temporaryLatitudeJob = Double
					.parseDouble(loadSetting("temporaryLatitudeJob"));
			double temporaryLongitudeJob = Double
					.parseDouble(loadSetting("temporaryLongitudeJob"));

			if (temporaryLatitudeJob != 0) {
				LatLng temporaryPoint = new LatLng(temporaryLatitudeJob,
						temporaryLongitudeJob);
				googleMap.clear();

				// New Builder for temporary adddresses
				final LatLngBounds.Builder builderTemporary = new LatLngBounds.Builder();
				setTemporaryaddressJob(temporaryPoint);

				if (!(loadSetting("temporaryAddressJobDisplay").equals(""))) {
					temporaryAddressJob = loadSetting("temporaryAddressJobDisplay");
				}
				placeMarkerTemporary(googleMap, builderTemporary,
						selectedJobIndex, temporaryPoint,
						"Klik -  for at acceptere ny adresse!",
						temporaryAddressJob);

				googleMap.setOnMarkerClickListener(new OnMarkerClickListener() {
					@Override
					public boolean onMarkerClick(Marker marker) {
						marker.showInfoWindow();
						return false;
					}
				});

				googleMap
						.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

							@Override
							public void onInfoWindowClick(Marker marker) {

								Integer temp = Integer.parseInt(jobMap
										.get(marker));
								String poiIndex = temp.toString();
								saveSetting("selectedJobIndex", poiIndex);
								saveSetting("sCommand", "oneJob");
								// New Address: No popUp else change address
								// shows popup
								String[][] s = WebApi.getWebApi()
										.getJobs_address_street();
								if (WebApi.getWebApi().getJobs_address_street()[selectedJobIndex] == null) {
									new AsyncSetJobAddress().execute();
								} else {
									new DialogLoaderChangeAddress().execute();
								}
							}
						});
				googleMap.setOnMarkerDragListener(new OnMarkerDragListener() {

					@Override
					public void onMarkerDragStart(Marker arg0) {

					}

					@Override
					public void onMarkerDragEnd(Marker marker) {

						setOnMarkerDragEnd(googleMap, builder,
								selectedJobIndex, marker);
					}

					@Override
					public void onMarkerDrag(Marker arg0) {
					}
				});
				LatLngBounds bounds = builderTemporary.build();
				googleMap.getUiSettings().setCompassEnabled(true);
				googleMap.getUiSettings().setZoomControlsEnabled(true);

				googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
						bounds, widthTemporary, heightTemporary, 30));
				googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
			} else {
				Toast.makeText(getApplicationContext(),
						"Indtast en gyldig adresse!", Toast.LENGTH_LONG);
			}
		}
	}

	/**
	 * @param googleMap
	 * @param builder
	 * @param selectedJobIndex
	 * @param marker
	 */
	private void setOnMarkerDragEnd(final GoogleMap googleMap,
			final LatLngBounds.Builder builder, final int selectedJobIndex,
			Marker marker) {
		new ShowAsyncPauseDialog().execute();
		String temporaryAddressJob = "";
		setTemporaryaddressJob(marker.getPosition());

		if (!(loadSetting("temporaryAddressJobForDisplay").equals(""))) {
			temporaryAddressJob = loadSetting("temporaryAddressJobForDisplay");
		}

		googleMap.clear();
		Marker markerId = googleMap.addMarker(new MarkerOptions()
				.position(marker.getPosition())
				.title("Klik -  for at acceptere ny adresse!")
				.snippet(temporaryAddressJob)
				.icon(BitmapDescriptorFactory
						.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

		markerId.setDraggable(true);
		markerId.showInfoWindow();
		searchBarEditText.setText(temporaryAddressJob);
		jobMap.put(markerId, Integer.toString(selectedJobIndex));
		builder.include(markerId.getPosition());
		LatLngBounds bounds = builder.build();
		googleMap.getUiSettings().setCompassEnabled(true);
		googleMap.getUiSettings().setZoomControlsEnabled(true);
		googleMap.animateCamera(CameraUpdateFactory.zoomTo(10));
		googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 400,
				400, 30));

		if (progressDialog != null) {
			progressDialog.hide();
		}
	}

	private void placeMarker(GoogleMap googleMap, LatLngBounds.Builder builder,
			int selectedJobIndex, LatLng point) {
		Marker markerId = googleMap
				.addMarker(new MarkerOptions()
						.position(point)
						.title(WebApi.getWebApi().getJobs_title()[selectedJobIndex])
						.snippet(
								WebApi.getWebApi().getJobs_address_street()[selectedJobIndex][0])
						.icon(BitmapDescriptorFactory
								.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
		markerId.setDraggable(true);
		jobMap.put(markerId, Integer.toString(selectedJobIndex));
		builder.include(markerId.getPosition());
	}

	private void placeMarkerTemporary(GoogleMap googleMap,
			LatLngBounds.Builder builder, int selectedJobIndex, LatLng point,
			String title, String snippet) {
		Marker markerId = googleMap.addMarker(new MarkerOptions()
				.position(point)
				.title(title)
				.snippet(snippet)
				.icon(BitmapDescriptorFactory
						.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
		markerId.setDraggable(true);
		markerId.showInfoWindow();
		jobMap.put(markerId, Integer.toString(selectedJobIndex));
		builder.include(markerId.getPosition());
	}

	public JSONObject getLocationInfo(String lat, String lng) {

		HttpGet httpGet = new HttpGet(
				"http://maps.google.com/maps/api/geocode/json?latlng=" + lat
						+ "," + lng + "&sensor=true");
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		} catch (ClientProtocolException e) {
		} catch (IOException e) {
		}

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject(stringBuilder.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	// Android pop-up menu
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		String sCommand = "";
		if (!loadSetting("sCommand").equals("")) {
			sCommand = loadSetting("sCommand");
		}

		if (sCommand.equals("allJobs")) {
			getMenuInflater().inflate(R.menu.hellogooglemaps, menu);
		} else if (sCommand.equals("oneJob")) {
			getMenuInflater().inflate(R.menu.hellogooglemapsonejob, menu);
		} else if (sCommand.equals("allJobsNotOneJob")
				|| sCommand.equals("setJobAddress")) {
			getMenuInflater().inflate(R.menu.hellogooglemapsnotonejob, menu);
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		GoogleMap googleMap = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.mapview)).getMap();
		if (item.getItemId() == R.id.activity_jobs) { // Jobs
			Intent jobs = new Intent(HelloGoogleMaps.this, Jobs.class);
			startActivity(jobs);
			return true;
		} else if (item.getItemId() == R.id.activity_job) { // Vis et Job
			Intent job = new Intent(HelloGoogleMaps.this, Job.class);
			startActivity(job);
			return true;
		} else if (item.getItemId() == R.id.activity_show_one) { // Vis et job
			saveSetting("sCommand", "oneJob");
			Intent active = new Intent(HelloGoogleMaps.this,
					HelloGoogleMaps.class);
			startActivity(active);
			return true;
		} else if (item.getItemId() == R.id.activity_show_all) { // Vis alle
																	// jobs
			saveSetting("sCommand", "allJobs");

			Intent all = new Intent(HelloGoogleMaps.this, HelloGoogleMaps.class);
			startActivity(all);
			return true;
		} else if (item.getItemId() == R.id.map_type_normal) {
			if (item.isChecked()) {
				item.setChecked(false);
			} else {
				item.setChecked(true);
			}
			googleMap.setMapType(googleMap.MAP_TYPE_NORMAL);
			return true;
		} else if (item.getItemId() == R.id.map_type_satellit) {
			if (item.isChecked()) {
				item.setChecked(false);
			} else {
				item.setChecked(true);
			}
			googleMap.setMapType(googleMap.MAP_TYPE_SATELLITE);
			return true;
		} else if (item.getItemId() == R.id.map_type_hybrid) {
			if (item.isChecked()) {
				item.setChecked(false);
			} else {
				item.setChecked(true);
			}
			googleMap.setMapType(googleMap.MAP_TYPE_HYBRID);
			return true;
		} else if (item.getItemId() == R.id.map_type_terrain) {
			if (item.isChecked()) {
				item.setChecked(false);
			} else {
				item.setChecked(true);
			}
			googleMap.setMapType(googleMap.MAP_TYPE_TERRAIN);
			return true;
		}
		return true;
	}

	private void setTemporaryaddressJob(LatLng point) {

		double latitude = point.latitude;
		double longitude = point.longitude;

		String[] params = new String[2];
		params[0] = String.valueOf(latitude);
		params[1] = String.valueOf(longitude);

		try {
			new AsyncGetLocationInfo().execute(params)
					.get(20, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			Toast.makeText(getApplicationContext(),
					"Adressen kunne ikke findes - Pr�v igen",
					Toast.LENGTH_SHORT);
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private void saveSetting(String tag, Boolean content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(tag, content);
		editor.commit();
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private Boolean loadSettingBoolean(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		Boolean content = preferences.getBoolean(tag, false);
		return content;
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
		String poiIndex = jobMap.get(marker);
		Intent intent = new Intent(this, Job.class);
		saveSetting("selectedJobIndex", poiIndex);
		saveSetting("sCommand", "oneJob");
		this.startActivity(intent);
	}

	@Override
	public View getInfoContents(Marker marker) {
		marker.getId();
		return null;
	}

	@Override
	public View getInfoWindow(Marker arg0) {
		return null;
	}

	private static JSONObject getLocationInfo(String address) {

		StringBuilder stringBuilder = new StringBuilder();
		try {

			address = address.replaceAll(" ", "%20");

			HttpPost httppost = new HttpPost(
					"http://maps.google.com/maps/api/geocode/json?address="
							+ address + "&sensor=false");
			HttpClient client = new DefaultHttpClient();
			HttpResponse response;
			stringBuilder = new StringBuilder();

			response = client.execute(httppost);
			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		} catch (ClientProtocolException e) {

		} catch (IOException e) {
		}

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject(stringBuilder.toString());
		} catch (JSONException e) {
		}

		return jsonObject;
	}

	private static boolean getLatLong(JSONObject jsonObject) {

		try {

			double longitute = ((JSONArray) jsonObject.get("results"))
					.getJSONObject(0).getJSONObject("geometry")
					.getJSONObject("location").getDouble("lng");
			double latitude = ((JSONArray) jsonObject.get("results"))
					.getJSONObject(0).getJSONObject("geometry")
					.getJSONObject("location").getDouble("lat");
		} catch (JSONException e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	private class DialogLoaderChangeAddress extends
			AsyncTask<Void, Integer, Void> {
		AlertDialog.Builder alertDialog;

		protected void onPreExecute() {
			super.onPreExecute();

			alertDialog = new AlertDialog.Builder(HelloGoogleMaps.this);

			alertDialog.setTitle("Gem �ndret adresse?");

			alertDialog.setMessage(" - �ndrer adresse p� job.");
			alertDialog.setIcon(R.drawable.icon_hybridmap);
			alertDialog.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							new AsyncSetJobAddress().execute();
							Intent intent = new Intent(HelloGoogleMaps.this,
									Job.class);
							startActivity(intent);
						}
					});
			alertDialog.setNegativeButton("Annuller",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

						}
					});
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					alertDialog.show();
				}
			});
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			saveSetting("reloadJob", true);
			if (alertDialog != null) {
				((DialogInterface) alertDialog).dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class AsyncLoaderUpdateGoogleMap extends
			AsyncTask<Void, Void, Void> implements LocationListener {
		private GoogleMap googleMap;

		protected void onPreExecute() {
			super.onPreExecute();

			final LatLngBounds.Builder builder = new LatLngBounds.Builder();
			final int widthTemporary = 400;
			final int heightTemporary = 400;

			searchBarEditText = (EditText) findViewById(R.id.hellogooglemap_searchbar_edittext);
			searchBarEditText.setText(loadSetting("suggestedNewJobAddress"));
			searchBarEditText.setSelected(true);

			int iSelectedJobIndex = -1;
			if (!(loadSetting("selectedJobIndex").equals(""))) {
				String temp = loadSetting("selectedJobIndex");
				iSelectedJobIndex = Integer.parseInt(temp);
			}

			final int selectedJobIndex = iSelectedJobIndex;

			GoogleMap googleMap1 = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.mapview)).getMap();

			searchBarEditText = (EditText) findViewById(R.id.hellogooglemap_searchbar_edittext);
			searchBarEditText.setSelected(false);
			//
			FrameLayout searchBarLinearLayout = (FrameLayout) findViewById(R.id.hellogooglemap_searchbar_linearlayout);

			Button searchBarButton = (Button) findViewById(R.id.hellogooglemap_searchbar_Button);

			Button searchBarRedCross = (Button) findViewById(R.id.hellogooglemap_searchbar_red_cross);
			searchBarRedCross.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					searchBarEditText.setText("");

					InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

					inputManager.toggleSoftInput(
							InputMethodManager.SHOW_IMPLICIT, 0);
				}
			});
			searchBarLinearLayout.setVisibility(View.VISIBLE);
			searchBarEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
			searchBarEditText
					.setOnEditorActionListener(new EditText.OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							if (actionId == EditorInfo.IME_ACTION_DONE) {
								searchNewAddress(googleMap, builder,
										selectedJobIndex, widthTemporary,
										heightTemporary);
								return true;
							}
							return false;
						}
					});

			searchBarButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					searchNewAddress(googleMap, builder, selectedJobIndex,
							widthTemporary, heightTemporary);
				}
			});

		}

		protected Void doInBackground(Void... arg0) {

			return null;

		}

		protected void onPostExecute(Void test) {
			super.onPostExecute(test);

			int index = 0;
			if (!(loadSetting("selectedJobIndex").equals(""))) {
				index = Integer.parseInt(loadSetting("selectedJobIndex"));
			}
			final int selectedJobIndex = index;

			final LatLngBounds.Builder builder = new LatLngBounds.Builder();

			googleMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.mapview)).getMap();

			LinearLayout helloGoogleMaps = (LinearLayout) findViewById(R.id.hellogooglemaps);
			boolean bFirstGoogleMapRun = true;

			searchNewAddress(googleMap, builder, selectedJobIndex, 400, 400);
			// if (googleMap != null) {
			// googleMap.getUiSettings().setCompassEnabled(true);
			// googleMap.getUiSettings().setZoomControlsEnabled(true);
			// if (bFirstGoogleMapRun == true) {
			// googleMap.moveCamera(CameraUpdateFactory.zoomTo(15));
			// bFirstGoogleMapRun = false;
			// }
			// googleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
			// }

			// turnGPSOff();
		}

		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}

		@Override
		public void onLocationChanged(Location location) {

			// if (runOnce == true) {
			// // Getting latitude of the current location
			// double latitude = location.getLatitude();
			//
			// // Getting longitude of the current location
			// double longitude = location.getLongitude();
			//
			// // Creating a LatLng object for the current location
			// LatLng latLng = new LatLng(latitude, longitude);
			//
			// GoogleMap googleMap = ((SupportMapFragment)
			// getSupportFragmentManager()
			// .findFragmentById(R.id.mapview)).getMap();
			// // Showing the current location in Google Map
			// googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
			//
			// // Zoom in the Google Map
			// googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
			// runOnce = false;
			// }
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	private class AsyncSetJobAddress extends AsyncTask<Void, Void, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... params) {
			// Initialize
			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			String sSelectedJobId = "";
			if (!loadSetting("selectedJobId").equals("")) {
				sSelectedJobId = loadSetting("selectedJobId");
			}

			String addressJob = "";
			if (!loadSetting("temporaryAddressJob").equals("")) {
				String newString = loadSetting("temporaryAddressJob");
				// addressJob = newString.replaceAll("[., ]+","_");
				try {
					addressJob = URLEncoder.encode(newString, "iso-8859-1");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}

			String latitudeJob = "";
			if (!loadSetting("temporaryLatitudeJob").equals("")) {
				latitudeJob = loadSetting("temporaryLatitudeJob");
			}

			String longitudeJob = "";
			if (!loadSetting("temporaryLongitudeJob").equals("")) {
				longitudeJob = loadSetting("temporaryLongitudeJob");
			}

			if (!sSelectedJobId.equals("")) {
				WebApi.getWebApi().setJobAddress(preferences, sSelectedJobId,
						addressJob, latitudeJob, longitudeJob);
			} else {
				showToast("Der skete en fejl - Pr�v igen");

			}
			// Error sending
			boolean isNewDataSetJobAddress = WebApi.getWebApi()
					.isNewData_Jobs_SetJobAddress();
			if (isNewDataSetJobAddress == false) {
				showToast("Fejlkode: "
						+ WebApi.getWebApi().getResponse_ErrorCode(), WebApi
						.getWebApi().getResponse_ErrorText()
						+ " Pr�v igen senere!");

				Intent status = new Intent(HelloGoogleMaps.this, Job.class);
				startActivity(status);
			}
			// Success sending
			else if (isNewDataSetJobAddress == true) {
				String errorHeader = WebApi.getWebApi()
						.getResponse_ErrorHeader();

				// Success answer
				if (errorHeader.equals("OK")) {
					showToast("Din registrering er gemt");
					saveSetting("reloadJob", true);
					saveSetting("reloadJobs", true);

					Intent intent = new Intent(HelloGoogleMaps.this, Job.class);
					startActivity(intent);
				}
				// Error in answer
				else if (WebApi.getWebApi().getResponse_Status()
						.equals("Error")) {
					if (!WebApi.getWebApi().getResponse_ErrorText().equals("")) {
						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");

						Intent intent = new Intent(HelloGoogleMaps.this,
								Job.class);
						startActivity(intent);
					} else {
						showToast("Fejlkode: "
								+ WebApi.getWebApi().getResponse_ErrorCode(),
								WebApi.getWebApi().getResponse_ErrorText()
										+ " Pr�v igen senere!");

						Intent intent = new Intent(HelloGoogleMaps.this,
								Job.class);
						startActivity(intent);
					}
				}
			}
			return null;
		}

		protected void onPostExecute(Void number) {
			super.onPostExecute(number);
			// turnGPSOff();
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}

		protected void showToast(final String sHeader, final String sContext) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sContext, Toast.LENGTH_LONG)
							.show();
				}
			});
		}
	}

	private class AsyncGetLocationInfo extends AsyncTask<String, Integer, Void> {

		String temporaryAddressJobForDisplay;

		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(String... arg0) {
			String temporaryAddressJob = "";

			JSONObject ret = getLocationInfo(arg0[0], arg0[1]);

			JSONObject location;
			temporaryAddressJobForDisplay = "";
			String temp_location_string = "";

			try {
				location = ret.getJSONArray("results").getJSONObject(0);
				temp_location_string = location.getString("formatted_address");

				try {
					String url = URLEncoder.encode(temp_location_string,
							"iso-8859-1");
					temporaryAddressJobForDisplay = java.net.URLDecoder.decode(
							url, "UTF-8");
					saveSetting("temporaryAddressJobForDisplay",
							temporaryAddressJobForDisplay);
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}

			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			if (temp_location_string != "") {
				temporaryAddressJob = temp_location_string;
			}

			saveSetting("temporaryLatitudeJob", arg0[0]);
			saveSetting("temporaryLongitudeJob", arg0[1]);
			if (temporaryAddressJob.equals("")) {
				saveSetting("temporaryAddressJob",
						"Der kunne ikke findes en adresse");
			} else {
				saveSetting("temporaryAddressJob", temporaryAddressJob);
			}

			if (progressDialog != null) {
				progressDialog.cancel();
			}

			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			searchBarEditText.setText(temporaryAddressJobForDisplay);

			if (progressDialog != null) {
				progressDialog.cancel();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class AsyncGetAddressInfo extends AsyncTask<String, Integer, Void> {

		String location_string;

		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(String... arg0) {
			String temporaryAddressJob = "";

			StringBuilder stringBuilder = new StringBuilder();
			try {

				String address = arg0[0].replaceAll(" ", "%20");

				HttpPost httppost = new HttpPost(
						"http://maps.google.com/maps/api/geocode/json?address="
								+ address + "&sensor=false");
				HttpClient client = new DefaultHttpClient();
				HttpResponse response;
				stringBuilder = new StringBuilder();

				response = client.execute(httppost);
				HttpEntity entity = response.getEntity();
				InputStream stream = entity.getContent();
				int b;
				while ((b = stream.read()) != -1) {
					stringBuilder.append((char) b);
				}
			} catch (ClientProtocolException e) {

			} catch (IOException e) {
			}

			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject = new JSONObject(stringBuilder.toString());
			} catch (JSONException e) {
			}

			JSONObject location;
			location_string = "";
			double temporaryLatitudeJob = 0;
			double temporaryLongitudeJob = 0;
			try {
				location = jsonObject.getJSONArray("results").getJSONObject(0);
				location_string = location.getString("formatted_address");

				temporaryLatitudeJob = ((JSONArray) jsonObject.get("results"))
						.getJSONObject(0).getJSONObject("geometry")
						.getJSONObject("location").getDouble("lat");
				temporaryLongitudeJob = ((JSONArray) jsonObject.get("results"))
						.getJSONObject(0).getJSONObject("geometry")
						.getJSONObject("location").getDouble("lng");
			} catch (JSONException e1) {
				e1.printStackTrace();

			}

			if (location_string != "") {
				temporaryAddressJob = location_string;
			}

			saveSetting("temporaryLatitudeJob",
					String.valueOf(temporaryLatitudeJob));
			saveSetting("temporaryLongitudeJob",
					String.valueOf(temporaryLongitudeJob));
			if (temporaryAddressJob.equals("")) {
				saveSetting("temporaryAddressJob",
						"Der kunne ikke findes en adresse");
			} else {
				saveSetting("temporaryAddressJob", temporaryAddressJob);
			}

			if (progressDialog != null) {
				progressDialog.cancel();
			}

			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			searchBarEditText.setText(location_string);

			if (progressDialog != null) {
				progressDialog.cancel();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class ShowAsyncPauseDialog extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(HelloGoogleMaps.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}
}