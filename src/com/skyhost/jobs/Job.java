package com.skyhost.jobs;

import java.text.DecimalFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.util.Linkify;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("ResourceAsColor")
public class Job extends Activity {

	private boolean editState;
	private Handler handler;
	private OnClickListener onClickListenerJobHeader;
	private OnClickListener onClickListenerNote;
	private OnClickListener onClickListenerStatus;
	private OnClickListener onClickListenerOverViewMaterial;
	// private OnClickListener onClickListenerFilesHeader;
	private OnClickListener onClickListenerButtonStartStopTimer;
	private ProgressDialog progressDialog;
	private int textSize;

	// GPS
	private boolean isGPSFix = false;
	private Location mLastLocation;
	private long mLastLocationMillis;
	private String latitude;
	private String longitude;
	protected static GpsStatus.Listener myGPSListener;
	protected static LocationManager locationManager;
	protected static LocationListener locationListener;
	private boolean runOnce;

	// Intentional global variables because of Async methods getting number of
	// materials. Allows number to be set after showing GUI
	TextView textView_OverView_Material;
	ImageView imageView_Materials;
	TextView textView_Job_Titles;
	TextView textView_BeginTime;
	TextView textView_OrderNumber;
	TextView textView_ChangeJobName;
	TextView textView_Time_Deployed;
	TextView textView_Priority;

	private Runnable runUpdateTask = new Runnable() {
		public void run() {
			reloadApi();
		}
	};

	private Runnable runUpdateJobsTask = new Runnable() {
		public void run() {
			new AsyncUpdateJobs().execute();
		}
	};

	private Runnable runUpdatePosition = new Runnable() {
		public void run() {
			new AsyncIntentWithPosition().execute();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.job);

		new ShowAsyncPauseDialog().execute();

		// Initialize
		editState = loadSettingBoolean("editState");
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationListener = new MyLocationListener();
		myGPSListener = new MyGPSListener();

		// Global textsize
		textSize = 18;

		handler = new Handler();
	}

	@Override
	public void onPause() {
		super.onPause();

		if (handler != null) {
			handler.removeCallbacks(runUpdateTask);
		}

		if (handler != null) {
			handler.removeCallbacks(runUpdateJobsTask);
		}

		if (progressDialog != null) {
			progressDialog.cancel();
		}

		// Turn off GPS listener
		if (locationManager != null) {
			locationManager.removeGpsStatusListener(myGPSListener);
		}

		if (locationManager != null) {
			locationManager.removeUpdates(locationListener);
		}

		turnGPSOff();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (handler != null) {
			handler.post(runUpdateTask);
		}

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationListener = new MyLocationListener();
		myGPSListener = new MyGPSListener();

		// Detect GPS settings
		if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			showGpsOptions();
		}

		// Turn on GPS listener AND NETWORK LISTENER
		locationManager.addGpsStatusListener(myGPSListener);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				2000, 0, locationListener);
		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 2000, 0, locationListener);
		turnGPSOn();
	}

	@Override
	public void onBackPressed() {

		// Turn off editState
		saveSetting("editState", false);

		Intent intent = new Intent(Job.this, Jobs.class);
		Job.this.startActivity(intent);
	}

	private void reloadApi() {
		boolean reloadJob = false;
		boolean reloadJobs = false;

		// Reloads Api if jobbutton has been pressed
		reloadJob = loadSettingBoolean("reloadJob");
		reloadJobs = loadSettingBoolean("reloadJobs");

		// Reset boolean
		if (reloadJob == true) {
			new AsyncDataForJob().execute();
			saveSetting("reloadJob", false);

			if (reloadJobs == true) {
				handler.post(runUpdateJobsTask);
				saveSetting("reloadJobs", false);
			}

		} else {

			Date now = new Date();
			Date previous = WebApi.getWebApi().getTimestampJobs();
			long lResult = now.getTime() - previous.getTime();

			// If more than 60 seconds old reload again
			if (lResult <= 0 || lResult > 60000) {
				new AsyncDataForJob().execute();
			} else {
				boolean isNewDataJobs = WebApi.getWebApi().isNewData_Jobs();
				if (isNewDataJobs == true) {
					updateScreen();
				} else {
					handler.postDelayed(runUpdateTask, 3000);
				}
			}
		}

		if (reloadJobs == true) {
			handler.post(runUpdateJobsTask);
			saveSetting("reloadJobs", false);
		}
	}

	private void updateScreen() {
		int selectedJobIndex = -1;
		int textSizeHeaders = 20;
		int topPadding = 20;
		if (!(loadSetting("selectedJobIndex").equals(""))) {
			selectedJobIndex = Integer
					.parseInt(loadSetting("selectedJobIndex"));
		}
		final int jobIndex = selectedJobIndex;

		String title = WebApi.getWebApi().getJobs_title()[selectedJobIndex];
		setTitle(title);

		LinearLayout linearLayout = new LinearLayout(this);
		LinearLayout linearLayoutJob = (LinearLayout) findViewById(R.id.job_linearlayout);
		linearLayoutJob.removeAllViews();
		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		linearLayoutJob.setLayoutParams(params);
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		linearLayout.setWeightSum(1);

		// ---------- Button ----------
		final Button button_StartStopTimer = new Button(this);
		button_StartStopTimer.setBackgroundDrawable(getResources().getDrawable(
				R.layout.button_green_skyhost));
		button_StartStopTimer.setText("Start Tidsregistrering");
		button_StartStopTimer.setPadding(5, 0, 5, 0);
		button_StartStopTimer.setHeight(70);
		button_StartStopTimer.setWidth(100);
		button_StartStopTimer.setTextColor(getResources().getColor(
				R.color.black));
		button_StartStopTimer.setTextSize(textSizeHeaders);
		button_StartStopTimer.setTypeface(null, Typeface.BOLD);
		String s = WebApi.getWebApi().getJobs_linked()[selectedJobIndex];
		if (WebApi.getWebApi().getJobs_linked()[selectedJobIndex].equals("1")) {
			button_StartStopTimer.setBackgroundDrawable(getResources()
					.getDrawable(R.layout.button_red));
			button_StartStopTimer.setText("Stop Tidsregistrering");
		}

		onClickListenerButtonStartStopTimer = new OnClickListener() {
			@Override
			public void onClick(View v) {

				button_StartStopTimer.setEnabled(false);

				new ShowAsyncPauseDialog().execute();

				new AsyncChangeLinkedStatus().execute();
			}
		};
		button_StartStopTimer
				.setOnClickListener(onClickListenerButtonStartStopTimer);
		linearLayoutJob.addView(button_StartStopTimer);

		if (WebApi.getWebApi().getJobs_jobId() != null) {
			if (!WebApi.getWebApi().getJobs_jobId().get(selectedJobIndex)
					.equals("0")) {
				// ---------- Job ----------
				// Ikke interne jobs

				insertExtraLine(linearLayoutJob);

				// ---------- Job ----------
				final LinearLayout linearlayout_jobHeader = new LinearLayout(
						this);
				LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.MATCH_PARENT,
						LinearLayout.LayoutParams.MATCH_PARENT);
				linearlayout_jobHeader.setLayoutParams(params3);
				linearlayout_jobHeader.setGravity(Gravity.CENTER_VERTICAL);
				linearlayout_jobHeader.setOrientation(LinearLayout.HORIZONTAL);
				linearlayout_jobHeader.setPadding(5, topPadding, 5, 0);
				linearlayout_jobHeader
						.setBackgroundResource(R.color.gray_light);

				final TextView job_AddressHeader = new TextView(this);
				job_AddressHeader.setText("Job:");
				job_AddressHeader.setTypeface(null, Typeface.BOLD);
				job_AddressHeader.setTextSize(textSizeHeaders);
				job_AddressHeader.setTextColor(getResources().getColor(
						R.color.black));

				final TextView job_ChangeAddress = new TextView(this);

				if (WebApi.getWebApi().getJobs_title() != null) {
					textView_Job_Titles = new TextView(this);

					if (!WebApi.getWebApi().getJobs_title()[selectedJobIndex]
							.equals("")) {
						textView_Job_Titles.setText(WebApi.getWebApi()
								.getJobs_title()[selectedJobIndex]);
					}

					textView_Job_Titles.setTextSize(
							TypedValue.COMPLEX_UNIT_DIP, textSize);
					textView_Job_Titles.setPadding(5, 0, 5, 0);
					textView_Job_Titles.setTypeface(null, Typeface.BOLD);
					textView_Job_Titles.setTextColor(Color.BLACK);

					if (editState == true) {

						textView_Job_Titles
								.setCompoundDrawablesWithIntrinsicBounds(0, 0,
										R.drawable.icon_edit_32, 0);
						textView_Job_Titles
								.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View v) {
										Intent intent = new Intent(Job.this,
												JobSetJobTitle.class);
										startActivity(intent);
									}
								});
					}

					job_ChangeAddress.setText("V�lg Adresse p� Kort");
					job_ChangeAddress.setLayoutParams(params3);
					job_ChangeAddress.setGravity(Gravity.RIGHT);
					job_ChangeAddress.setBackgroundResource(R.color.gray_light);
					job_ChangeAddress.setTextSize(16);
					job_ChangeAddress.setTextColor(getResources().getColor(
							R.color.green_dark_skyhost));

					onClickListenerJobHeader = new OnClickListener() {
						@Override
						public void onClick(View v) {
							if (v == job_AddressHeader
									|| v == job_ChangeAddress
									|| v == linearlayout_jobHeader) {
								Intent intent = new Intent(Job.this,
										HelloGoogleMaps.class);

								String sJobAddress = "";

								if (!(loadSetting("sJobAddress").equals(""))) {
									sJobAddress = loadSetting("sJobAddress");
								}

								if (sJobAddress.equals("")) {
									saveSetting("sCommand", "newJobAddress");
								} else {
									saveSetting("sCommand", "setJobAddress");
								}

								startActivity(intent);
							}
						}
					};
					linearlayout_jobHeader.addView(job_AddressHeader);
					linearlayout_jobHeader.addView(job_ChangeAddress);

					linearLayoutJob.addView(linearlayout_jobHeader);
					insertExtraLine(linearLayoutJob);
					linearLayoutJob.addView(textView_Job_Titles);

					job_AddressHeader
							.setOnClickListener(onClickListenerJobHeader);
					job_ChangeAddress
							.setOnClickListener(onClickListenerJobHeader);
					linearlayout_jobHeader
							.setOnClickListener(onClickListenerJobHeader);

				}

				insertExtraLine(linearLayoutJob);
				saveSetting("sJobAddress", "");

				if (WebApi.getWebApi().getJobs_address_street() != null) {
					final TextView textView_Job_Address = new TextView(this);
					if (WebApi.getWebApi().getJobs_address_street()[selectedJobIndex] != null) {
						if (!WebApi.getWebApi().getJobs_address_street()[selectedJobIndex]
								.equals("")) {
							String sJobAddress = WebApi.getWebApi()
									.getJobs_address_street()[selectedJobIndex][0];
							textView_Job_Address.setText(sJobAddress);
							saveSetting("sJobAddress", sJobAddress);
							textView_Job_Address.setTextSize(
									TypedValue.COMPLEX_UNIT_DIP, textSize);
							textView_Job_Address.setPadding(5, 0, 5, 0);
							textView_Job_Address.setTypeface(null,
									Typeface.BOLD);
							textView_Job_Address.setTextColor(Color.BLACK);
							textView_Job_Address
									.setPaintFlags(textView_Job_Address
											.getPaintFlags()
											| Paint.UNDERLINE_TEXT_FLAG);

							textView_Job_Address
									.setOnClickListener(new OnClickListener() {

										@Override
										public void onClick(View v) {

											// Starting Geopoints
											// Update started in onResume
											String latitude = "";
											if (!loadSetting("latitude")
													.equals("")) {
												latitude = loadSetting("latitude");
											}

											String longitude = "";
											if (!loadSetting("longitude")
													.equals("")) {
												longitude = loadSetting("longitude");
											}

											// Destinations
											// Destination Points
											String destinationLongitude = "";
											String destinationLatitude = "";
											try {
												destinationLongitude = WebApi
														.getWebApi()
														.getJobs_address_lon()[jobIndex][0];
												destinationLatitude = WebApi
														.getWebApi()
														.getJobs_address_lat()[jobIndex][0];

											} catch (Exception ex) {
												ex.printStackTrace();
											}

											// Destinationaddress : daddr
											String[][] test = WebApi
													.getWebApi()
													.getJobs_address_street();

											String address = "";
											try {
												address = WebApi
														.getWebApi()
														.getJobs_address_street()[jobIndex][0];
											} catch (Exception ex) {
												ex.printStackTrace();
											}

											// If starting point exists use
											// start + destination: No user
											// input necessary
											if (!latitude.equals("")
													|| destinationLatitude
															.equals("")) {

												String startingAddress = latitude
														+ "," + longitude;

												String formattedDestinationAddress = address
														.replace(", Danmark",
																"");
												Intent intent = new Intent(
														android.content.Intent.ACTION_VIEW,
														Uri.parse("http://maps.google.com/maps?saddr="
																+ startingAddress
																+ "&daddr="
																+ destinationLatitude
																+ ","
																+ destinationLongitude));
												startActivity(intent);
											}
											// Else if No starting points use
											// destination lon or lat
											else if (!destinationLatitude
													.equals("")) {
												Intent intent = new Intent(
														android.content.Intent.ACTION_VIEW,
														Uri.parse("http://maps.google.com/maps?daddr="
																+ destinationLatitude
																+ ","
																+ destinationLongitude));
												startActivity(intent);
											}
											// Else use destination only: user
											// input possible
											else {
												String formattedDestinationAddress = address
														.replace(", Danmark",
																"");
												Intent intent = new Intent(
														android.content.Intent.ACTION_VIEW,
														Uri.parse("http://maps.google.com/maps?daddr="
																+ formattedDestinationAddress));
												startActivity(intent);
											}
										}
									});
						} else {
							job_AddressHeader.setText("Jobnavn:");
						}

						linearLayoutJob.addView(textView_Job_Address);
						insertExtraLine(linearLayoutJob);
					}
				}

			} else {
				// ---------- Job ----------
				// Internt Job
				TextView job_Header = new TextView(this);
				job_Header.setText("");
				job_Header.setBackgroundResource(R.color.gray_light);
				job_Header.setPadding(5, topPadding, 5, 0);
				job_Header.setTextColor(getResources().getColor(R.color.black));
				job_Header.setTypeface(null, Typeface.BOLD);
				job_Header.setTextSize(textSizeHeaders);
				linearLayoutJob.addView(job_Header);

				TextView textView_Job_Titles = new TextView(this);
				if (WebApi.getWebApi().getJobs_title() != null) {
					if (!WebApi.getWebApi().getJobs_title()[selectedJobIndex]
							.equals("")) {
						textView_Job_Titles.setText(WebApi.getWebApi()
								.getJobs_title()[selectedJobIndex]);
					}
					textView_Job_Titles.setTextSize(
							TypedValue.COMPLEX_UNIT_DIP, textSize);
					textView_Job_Titles.setPadding(5, 0, 5, 0);
					textView_Job_Titles.setTypeface(null, Typeface.BOLD);
					textView_Job_Titles.setTextColor(Color.BLACK);
					linearLayoutJob.addView(textView_Job_Titles);
				}
				if (WebApi.getWebApi().getJobs_address_street() != null) {
					TextView textView_Job_Address = new TextView(this);
					if (WebApi.getWebApi().getJobs_address_street()[selectedJobIndex] != null) {
						String s1 = WebApi.getWebApi().getJobs_address_street()[selectedJobIndex][0]
								.toString();
						if (!WebApi.getWebApi().getJobs_address_street()[selectedJobIndex][0]
								.equals("")) {
							textView_Job_Address.setText(WebApi.getWebApi()
									.getJobs_address_street()[selectedJobIndex]
									.toString());
						} else {
							textView_Job_Address.setText("");
						}
					}
					textView_Job_Address.setTextSize(
							TypedValue.COMPLEX_UNIT_DIP, textSize);
					textView_Job_Address.setPadding(5, 0, 5, 0);
					textView_Job_Address.setTypeface(null, Typeface.BOLD);
					textView_Job_Address.setTextColor(Color.BLACK);
					linearLayoutJob.addView(textView_Job_Address);
				}
				insertExtraLine(linearLayoutJob);

				// ---------- Varighed ----------

				TextView duration_Header = new TextView(this);
				duration_Header.setText("Varighed:");
				duration_Header.setBackgroundResource(R.color.gray_light);
				duration_Header.setPadding(5, topPadding, 5, 0);
				duration_Header.setTypeface(null, Typeface.BOLD);
				duration_Header.setTextSize(textSizeHeaders);
				duration_Header.setTextColor(getResources().getColor(
						R.color.black));
				linearLayoutJob.addView(duration_Header);

				TextView textView_Time_Deployed = new TextView(this);
				String[] temp1 = WebApi.getWebApi().getJobs_duration();
				if (WebApi.getWebApi().getJobs_duration() != null) {
					if (!WebApi.getWebApi().getJobs_duration()[selectedJobIndex]
							.equals("")) {
						DecimalFormat df = new DecimalFormat("#.0");
						textView_Time_Deployed
								.setText(df
										.format(Double
												.parseDouble(WebApi.getWebApi()
														.getJobs_duration()[selectedJobIndex]) / 60)
										.toString()
										+ " t Afsat");
					}
					textView_Time_Deployed.setTextSize(
							TypedValue.COMPLEX_UNIT_DIP, textSize);
					textView_Time_Deployed.setPadding(5, 0, 5, 0);
					textView_Time_Deployed.setTypeface(null, Typeface.BOLD);
					textView_Time_Deployed.setTextColor(Color.BLACK);
					linearLayoutJob.addView(textView_Time_Deployed);
				}
				TextView textView_BeginTime = new TextView(this);
				if (WebApi.getWebApi().getJobs_beginTime() != null) {
					if (!WebApi.getWebApi().getJobs_beginTime()[selectedJobIndex]
							.equals("")) {
						textView_BeginTime
								.setText("Job Start: "
										+ WebApi.getWebApi()
												.getJobs_beginTime()[selectedJobIndex]);
					}
					textView_BeginTime.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
							textSize);
					textView_BeginTime.setPadding(5, 0, 5, 0);
					textView_BeginTime.setTypeface(null, Typeface.BOLD);
					textView_BeginTime.setTextColor(Color.BLACK);
					linearLayoutJob.addView(textView_BeginTime);
				}
				TextView textView_EndTime = new TextView(this);
				if (WebApi.getWebApi().getJobs_endTime() != null) {
					if (WebApi.getWebApi().getJobs_endTime()[selectedJobIndex] != null) {
						if (!WebApi.getWebApi().getJobs_endTime()[selectedJobIndex]
								.equals("")) {
							textView_EndTime
									.setText("Afsluttes F�r: "
											+ WebApi.getWebApi()
													.getJobs_endTime()[selectedJobIndex]);
						}
						textView_EndTime.setTextSize(
								TypedValue.COMPLEX_UNIT_DIP, textSize);
						textView_EndTime.setPadding(5, 0, 5, 0);
						textView_EndTime.setTypeface(null, Typeface.BOLD);
						textView_EndTime.setTextColor(Color.RED);
						linearLayoutJob.addView(textView_EndTime);
					}
				}
				insertExtraLine(linearLayoutJob);
			}
		}

		// Varighed
		TextView job_Header = new TextView(this);
		job_Header.setText("Varighed :");
		job_Header.setBackgroundResource(R.color.gray_light);
		job_Header.setPadding(5, topPadding, 5, 0);
		job_Header.setTextColor(getResources().getColor(R.color.black));
		job_Header.setTextSize(textSizeHeaders);
		job_Header.setTypeface(null, Typeface.BOLD);
		linearLayoutJob.addView(job_Header);

		insertExtraLine(linearLayoutJob);

		// Prioritet
		if (WebApi.getWebApi().getJobs_priority() != null) {

			textView_Priority = new TextView(this);

			textView_Priority.setText("Prioritet :");
			textView_Priority
					.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize);
			textView_Priority.setPadding(5, 0, 5, 0);
			textView_Priority.setTypeface(null, Typeface.BOLD);
			textView_Priority.setTextColor(Color.BLACK);
			textView_Priority.setCompoundDrawablesWithIntrinsicBounds(0, 0,
					R.drawable.icon_edit_32, 0);
			textView_Priority.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(Job.this, JobSetPriority.class);
					startActivity(intent);
				}
			});

			if (WebApi.getWebApi().getJobs_drawables() != null) {

				if (WebApi.getWebApi().getJobs_priority()[selectedJobIndex] != null) {
					String priority = WebApi.getWebApi().getJobs_priority()[selectedJobIndex];
					if (priority.equals("low")) {
						textView_Priority.setText("Prioritet: Lav");
						textView_Priority
								.setCompoundDrawablesWithIntrinsicBounds(0, 0,
										R.drawable.low_32, 0);
						linearLayoutJob.addView(textView_Priority);

						insertExtraLine(linearLayoutJob);
					} else if (priority.equals("high")) {
						textView_Priority.setText("Prioritet: H�j");
						textView_Priority
								.setCompoundDrawablesWithIntrinsicBounds(0, 0,
										R.drawable.high_32, 0);
						linearLayoutJob.addView(textView_Priority);

						insertExtraLine(linearLayoutJob);
					} else if (priority.equals("urgent")) {
						textView_Priority.setText("Prioritet: Haster");
						textView_Priority
								.setCompoundDrawablesWithIntrinsicBounds(0, 0,
										R.drawable.urgent_32, 0);
						linearLayoutJob.addView(textView_Priority);

						insertExtraLine(linearLayoutJob);
					} else if (priority.equals("") && editState == true) {
						textView_Priority.setText("Tilf�j Prioritet:");
						textView_Priority
								.setCompoundDrawablesWithIntrinsicBounds(0, 0,
										R.drawable.icon_arrow_right_32, 0);
						// linearLayoutJob.addView(priority_Header);
						linearLayoutJob.addView(textView_Priority);

						insertExtraLine(linearLayoutJob);

					}
				}
			}
		} else {
			if (editState == true) {
				// No priority
				textView_Priority.setText("Tilf�j Prioritet :");
				textView_Priority.setTextSize(textSizeHeaders);
				textView_Priority.setPadding(5, 0, 5, 0);
				textView_Priority.setTypeface(null, Typeface.BOLD);
				textView_Priority.setTextColor(Color.BLACK);
				textView_Priority.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.icon_arrow_right_32, 0);
				textView_Priority.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(Job.this,
								JobSetPriority.class);
						startActivity(intent);
					}
				});
				// linearLayoutJob.addView(priority_Header);
				linearLayoutJob.addView(textView_Priority);
				insertExtraLine(linearLayoutJob);
			}
		}

		// Ordrenummer
		if (WebApi.getWebApi().getJobs_orderNo() != null) {
			TextView textView_OrderNumber_Header = new TextView(this);
			textView_OrderNumber = new TextView(this);
			if (!WebApi.getWebApi().getJobs_orderNo()[selectedJobIndex]
					.equals("")) {

				textView_OrderNumber
						.setText("Ordrenummer: "
								+ WebApi.getWebApi().getJobs_orderNo()[selectedJobIndex]);

				textView_OrderNumber.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
						textSize);

				textView_OrderNumber.setPadding(5, 0, 5, 0);
				textView_OrderNumber.setTypeface(null, Typeface.BOLD);
				textView_OrderNumber.setTextColor(Color.BLACK);
				textView_OrderNumber.setCompoundDrawablesWithIntrinsicBounds(0,
						0, R.drawable.icon_edit_32, 0);

				textView_OrderNumber.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(Job.this,
								JobSetOrderNumber.class);
						startActivity(intent);
					}
				});

				linearLayoutJob.addView(textView_OrderNumber);
				insertExtraLine(linearLayoutJob);
			} else {
				if (editState == true) {
					textView_OrderNumber.setText("Angiv Ordrenummer: ");
					textView_OrderNumber.setTextSize(
							TypedValue.COMPLEX_UNIT_DIP, textSize);

					textView_OrderNumber.setPadding(5, 0, 5, 0);
					textView_OrderNumber.setTypeface(null, Typeface.BOLD);
					textView_OrderNumber.setTextColor(Color.BLACK);
					textView_OrderNumber
							.setCompoundDrawablesWithIntrinsicBounds(0, 0,
									R.drawable.icon_arrow_right_32, 0);
					textView_OrderNumber
							.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									Intent intent = new Intent(Job.this,
											JobSetOrderNumber.class);
									// saveSetting("editState", false);
									startActivity(intent);
								}
							});

					linearLayoutJob.addView(textView_OrderNumber);

					insertExtraLine(linearLayoutJob);
				}
			}
		}

		// Tid afsat
		if (WebApi.getWebApi().getJobs_duration() != null) {
			textView_Time_Deployed = new TextView(this);
			textView_Time_Deployed.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
					textSize);
			textView_Time_Deployed.setPadding(5, 0, 5, 0);
			textView_Time_Deployed.setTypeface(null, Typeface.BOLD);
			textView_Time_Deployed.setTextColor(Color.BLACK);
			textView_Time_Deployed.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(Job.this, JobSetDuration.class);
					startActivity(intent);
				}
			});

			if (!WebApi.getWebApi().getJobs_duration()[selectedJobIndex]
					.equals("")) {
				DecimalFormat df = new DecimalFormat("#.0");
				textView_Time_Deployed.setText(df.format(
						Double.parseDouble(WebApi.getWebApi()
								.getJobs_duration()[selectedJobIndex]) / 60)
						.toString()
						+ " t Afsat");

				textView_Time_Deployed.setCompoundDrawablesWithIntrinsicBounds(
						0, 0, R.drawable.icon_edit_32, 0);

				linearLayoutJob.addView(textView_Time_Deployed);

				insertExtraLine(linearLayoutJob);
			} else {
				if (editState == true) {
					textView_Time_Deployed.setText("Angiv forventet varighed:");
					textView_Time_Deployed
							.setCompoundDrawablesWithIntrinsicBounds(0, 0,
									R.drawable.icon_arrow_right_32, 0);

					linearLayoutJob.addView(textView_Time_Deployed);
					insertExtraLine(linearLayoutJob);
				}
			}
		}

		// Starttid
		if (WebApi.getWebApi().getJobs_beginTime() != null) {

			textView_BeginTime = new TextView(this);
			if (!WebApi.getWebApi().getJobs_beginTime()[selectedJobIndex]
					.equals("")) {
				textView_BeginTime
						.setText("Job Start: "
								+ WebApi.getWebApi().getJobs_beginTime()[selectedJobIndex]);
			}

			textView_BeginTime.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
					textSize);
			textView_BeginTime.setPadding(5, 0, 5, 0);
			textView_BeginTime.setTypeface(null, Typeface.BOLD);
			textView_BeginTime.setTextColor(Color.BLACK);
			textView_BeginTime.setCompoundDrawablesWithIntrinsicBounds(0, 0,
					R.drawable.icon_edit_32, 0);
			textView_BeginTime.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					saveSetting("begin", true);
					Intent intent = new Intent(Job.this, JobSetTime.class);
					startActivity(intent);
				}
			});
			linearLayoutJob.addView(textView_BeginTime);

			insertExtraLine(linearLayoutJob);
		}

		// Sluttid
		if (WebApi.getWebApi().getJobs_endTime() != null) {
			TextView textView_EndTime = new TextView(this);
			if (!WebApi.getWebApi().getJobs_endTime()[selectedJobIndex]
					.equals("")) {
				textView_EndTime
						.setText("Afsluttes F�r: "
								+ WebApi.getWebApi().getJobs_endTime()[selectedJobIndex]);
			}

			textView_EndTime.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize);
			textView_EndTime.setPadding(5, 0, 5, 0);
			textView_EndTime.setTypeface(null, Typeface.BOLD);
			textView_EndTime.setTextColor(Color.RED);
			textView_EndTime.setCompoundDrawablesWithIntrinsicBounds(0, 0,
					R.drawable.icon_edit_32, 0);
			textView_EndTime.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					saveSetting("begin", false);
					Intent intent = new Intent(Job.this, JobSetTime.class);
					startActivity(intent);
				}
			});
			linearLayoutJob.addView(textView_EndTime);
			insertExtraLine(linearLayoutJob);
		}

		// ---------- Status ----------

		if (!WebApi.getWebApi().getJobs_status()[selectedJobIndex].equals("")) {
			final TextView status_Header = new TextView(this);
			status_Header.setText("Status: ");
			status_Header.setBackgroundResource(R.color.gray_light);
			status_Header.setPadding(5, topPadding, 5, 0);
			status_Header.setTextSize(textSizeHeaders);
			status_Header.setTypeface(null, Typeface.BOLD);
			status_Header.setTextColor(getResources().getColor(R.color.black));
			linearLayoutJob.addView(status_Header);
			if (WebApi.getWebApi().getJobs_status() != null) {
				String[] str = WebApi.getWebApi().getJobs_status();
				final TextView textView_Status = new TextView(this);
				if (!WebApi.getWebApi().getJobs_status()[selectedJobIndex]
						.equals("")) {
					if (WebApi.getWebApi().getJobs_status()[selectedJobIndex]
							.equals("unassigned")) {
						textView_Status.setText("Ikke Tildelt");
					}
					if (WebApi.getWebApi().getJobs_status()[selectedJobIndex]
							.equals("assigned")) {
						textView_Status.setText("Tildelt");
					}
					if (WebApi.getWebApi().getJobs_status()[selectedJobIndex]
							.equals("started")) {
						textView_Status.setText("Startet");
					}
					if (WebApi.getWebApi().getJobs_status()[selectedJobIndex]
							.equals("waiting")) {
						textView_Status.setText("Afventer");
					}
					if (WebApi.getWebApi().getJobs_status()[selectedJobIndex]
							.equals("finished")) {
						textView_Status.setText("Afsluttet");
					}
				}

				textView_Status.setHeight(50);
				textView_Status.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
						textSize);
				textView_Status.setPadding(5, 0, 5, 0);
				textView_Status.setTypeface(null, Typeface.BOLD);
				textView_Status.setTextColor(Color.BLACK);
				textView_Status.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.icon_arrow_right_32, 0);
				onClickListenerStatus = new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (v == textView_Status || v == status_Header) {
							Intent intent = new Intent(Job.this,
									StatusSkyhost.class);
							startActivity(intent);
						}
					}
				};

				registerForContextMenu(textView_Status);
				linearLayoutJob.addView(textView_Status);
				status_Header.setOnClickListener(onClickListenerStatus);
				textView_Status.setOnClickListener(onClickListenerStatus);
			}
			insertExtraLine(linearLayoutJob);
		}

		// ---------- Noter ----------
		final TextView notes_Header = new TextView(this);
		notes_Header.setText("Noter: ");
		notes_Header.setBackgroundResource(R.color.gray_light);
		notes_Header.setPadding(5, topPadding, 5, 0);
		notes_Header.setTextSize(textSizeHeaders);
		notes_Header.setTypeface(null, Typeface.BOLD);

		notes_Header.setTextColor(getResources().getColor(R.color.black));
		linearLayoutJob.addView(notes_Header);

		// Horizontal Linearlayout holds image and number of notes
		LinearLayout linearLayout_Notes = new LinearLayout(this);
		linearLayout_Notes.setOrientation(LinearLayout.HORIZONTAL);

		// ImageView
		final ImageView imageView_Notes = new ImageView(this);
		LinearLayout.LayoutParams params_Image_Notes = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);

		imageView_Notes.setLayoutParams(params_Image_Notes);
		imageView_Notes.setAdjustViewBounds(true);
		imageView_Notes.setPadding(0, 5, 0, 5);

		final TextView textView_Note = new TextView(this);
		if (WebApi.getWebApi().getJobs_notes_names()[selectedJobIndex].length > 0) {
			textView_Note
					.setText("Antal : "
							+ WebApi.getWebApi().getJobs_notes_names()[selectedJobIndex].length);

			// Green image if notes exists
			imageView_Notes.setImageResource(R.drawable.icon_notes_green_64);
		} else {
			// Grey image if no notes
			textView_Note.setText("Antal : 0");
			imageView_Notes.setImageResource(R.drawable.icon_notes_grey_64);
		}

		LinearLayout.LayoutParams params_Notes = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		params_Notes.weight = 1.0f;
		params_Notes.gravity = Gravity.CENTER;

		textView_Note.setLayoutParams(params_Notes);
		textView_Note.setHeight(50);
		textView_Note.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize);
		textView_Note.setPadding(5, 0, 5, 0);
		textView_Note.setTypeface(null, Typeface.BOLD);
		textView_Note.setTextColor(Color.BLACK);

		if (WebApi.getWebApi().getJobs_notes_names()[selectedJobIndex].length > 0) {
			textView_Note.setCompoundDrawablesWithIntrinsicBounds(0, 0,
					R.drawable.icon_arrow_right_32, 0);
			onClickListenerNote = new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (v == textView_Note || v == notes_Header
							|| v == imageView_Notes) {
						Intent intent = new Intent(Job.this, Notes.class);
						startActivity(intent);
					}
				}
			};
		}

		linearLayout_Notes.addView(imageView_Notes);
		linearLayout_Notes.addView(textView_Note);
		linearLayoutJob.addView(linearLayout_Notes);

		notes_Header.setOnClickListener(onClickListenerNote);
		textView_Note.setOnClickListener(onClickListenerNote);
		imageView_Notes.setOnClickListener(onClickListenerNote);

		insertExtraLine(linearLayoutJob);

		// ---------- Materialer p� Job ---------
		// Get number of materials

		new AsyncGetMaterials().execute();

		// Display
		final TextView material_Header = new TextView(this);
		material_Header.setText("Materialer: ");
		material_Header.setPadding(5, topPadding, 5, 0);
		material_Header.setBackgroundResource(R.color.gray_light);
		material_Header.setTextSize(textSizeHeaders);
		material_Header.setTypeface(null, Typeface.BOLD);
		material_Header.setTextColor(getResources().getColor(R.color.black));

		linearLayoutJob.addView(material_Header);

		// Horizontal Linearlayout holds image and number of notes
		LinearLayout linearLayout_Materials = new LinearLayout(this);
		linearLayout_Materials.setOrientation(LinearLayout.HORIZONTAL);

		// ImageView
		imageView_Materials = new ImageView(this);
		LinearLayout.LayoutParams params_Image = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);

		imageView_Materials.setLayoutParams(params_Image);
		imageView_Materials.setAdjustViewBounds(true);
		imageView_Materials.setPadding(0, 5, 0, 5);

		imageView_Materials.setImageResource(R.drawable.icon_material_grey_64);

		textView_OverView_Material = new TextView(this);

		textView_OverView_Material.setText("Antal : 0");

		LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		params4.weight = 1.0f;
		params4.gravity = Gravity.CENTER;

		textView_OverView_Material.setLayoutParams(params4);
		textView_OverView_Material.setHeight(50);
		textView_OverView_Material.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
				textSize);
		textView_OverView_Material.setPadding(5, 0, 5, 0);
		textView_OverView_Material.setTypeface(null, Typeface.BOLD);
		textView_OverView_Material.setTextColor(Color.BLACK);

		textView_OverView_Material.setCompoundDrawablesWithIntrinsicBounds(0,
				0, R.drawable.icon_arrow_right_32, 0);
		onClickListenerOverViewMaterial = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (v == material_Header || v == imageView_Materials
						|| v == textView_OverView_Material) {
					Intent intent = new Intent(Job.this, MaterialsOnJob.class);
					startActivity(intent);
				}
			}
		};

		textView_OverView_Material
				.setOnClickListener(onClickListenerOverViewMaterial);
		material_Header.setOnClickListener(onClickListenerOverViewMaterial);
		imageView_Materials.setOnClickListener(onClickListenerOverViewMaterial);

		linearLayout_Materials.addView(imageView_Materials);
		linearLayout_Materials.addView(textView_OverView_Material);

		registerForContextMenu(textView_OverView_Material);
		linearLayoutJob.addView(linearLayout_Materials);

		insertExtraLine(linearLayoutJob);

		// ---------- Tildelt til ----------
		if (WebApi.getWebApi().getJobs_employees_name()[selectedJobIndex] != null) {
			TextView assignedTo_Header = new TextView(this);
			assignedTo_Header.setText("Tildelt til:");
			assignedTo_Header.setBackgroundResource(R.color.gray_light);
			assignedTo_Header.setPadding(5, topPadding, 5, 0);
			assignedTo_Header.setTextSize(textSizeHeaders);
			assignedTo_Header.setTypeface(null, Typeface.BOLD);
			assignedTo_Header.setTextColor(getResources().getColor(
					R.color.black));
			assignedTo_Header.setCompoundDrawablesWithIntrinsicBounds(0, 0,
					R.drawable.icon_edit_32, 0);
			assignedTo_Header.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					saveSetting("begin", true);
					saveSetting("isNewJob", false);
					Intent intent = new Intent(Job.this, JobSetUsers.class);

					startActivity(intent);
				}
			});
			linearLayoutJob.addView(assignedTo_Header);

			for (int i = 0; i < WebApi.getWebApi().getJobs_employees_name()[selectedJobIndex].length; i++) {

				TextView textView_AssignedTo_Name = new TextView(this);
				if (!WebApi.getWebApi().getJobs_title()[selectedJobIndex]
						.equals("")) {
					textView_AssignedTo_Name.setText(WebApi.getWebApi()
							.getJobs_employees_name()[selectedJobIndex][i]);
				}

				textView_AssignedTo_Name.setTextSize(
						TypedValue.COMPLEX_UNIT_DIP, textSize);
				textView_AssignedTo_Name.setPadding(5, 0, 5, 0);
				textView_AssignedTo_Name.setTypeface(null, Typeface.BOLD);
				textView_AssignedTo_Name.setTextColor(Color.BLACK);
				linearLayoutJob.addView(textView_AssignedTo_Name);

				if (!WebApi.getWebApi().getJobs_employees_phone()[selectedJobIndex][i]
						.equals("")) {
					TextView textView_AssignedTo_Phone = new TextView(this);
					textView_AssignedTo_Phone.setText(WebApi.getWebApi()
							.getJobs_employees_phone()[selectedJobIndex][i]);

					textView_AssignedTo_Phone.setTextSize(
							TypedValue.COMPLEX_UNIT_DIP, textSize);
					textView_AssignedTo_Phone.setPadding(5, 0, 5, 0);
					textView_AssignedTo_Phone.setTypeface(null, Typeface.BOLD);
					textView_AssignedTo_Phone.setTextColor(R.color.darkgreen);
					linearLayoutJob.addView(textView_AssignedTo_Phone);
					Linkify.addLinks(textView_AssignedTo_Phone, Linkify.ALL);
				}

				TextView textView_AssignedTo_Email = new TextView(this);
				if (!textView_AssignedTo_Email.equals("")) {
					textView_AssignedTo_Email.setText(WebApi.getWebApi()
							.getJobs_employees_email()[selectedJobIndex][i]);
				}

				textView_AssignedTo_Email.setTextSize(
						TypedValue.COMPLEX_UNIT_DIP, textSize);
				textView_AssignedTo_Email.setPadding(5, 0, 5, 0);
				textView_AssignedTo_Email.setTypeface(null, Typeface.BOLD);
				textView_AssignedTo_Email.setTextColor(R.color.darkgreen);
				linearLayoutJob.addView(textView_AssignedTo_Email);
				Linkify.addLinks(textView_AssignedTo_Email, Linkify.ALL);

				if (i != WebApi.getWebApi().getJobs_employees_email()[selectedJobIndex].length - 1) {
					TextView textView_EmptySpace = new TextView(this);
					textView_EmptySpace
							.setText("     --------------------------");
					linearLayoutJob.addView(textView_EmptySpace);
				}
			}
		}

		insertExtraLine(linearLayoutJob);

		// ---------- Oprettet ----------
		TextView created_Header = new TextView(this);
		created_Header.setText("Oprettet af:");
		created_Header.setBackgroundResource(R.color.gray_light);
		created_Header.setPadding(5, topPadding, 5, 0);
		created_Header.setTextSize(textSizeHeaders);
		created_Header.setTypeface(null, Typeface.BOLD);
		created_Header.setTextColor(getResources().getColor(R.color.black));
		linearLayoutJob.addView(created_Header);

		TextView textView_CreatedBy = new TextView(this);
		if (!WebApi.getWebApi().getJobs_createdBy()[selectedJobIndex]
				.equals("")) {
			textView_CreatedBy
					.setText(WebApi.getWebApi().getJobs_createdBy()[selectedJobIndex]);
		}

		textView_CreatedBy.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize);
		textView_CreatedBy.setPadding(5, 0, 5, 0);
		textView_CreatedBy.setTypeface(null, Typeface.BOLD);
		textView_CreatedBy.setTextColor(Color.BLACK);
		linearLayoutJob.addView(textView_CreatedBy);

		TextView textView_CreatedAt = new TextView(this);
		if (!WebApi.getWebApi().getJobs_createdBy()[selectedJobIndex]
				.equals("")) {
			textView_CreatedAt
					.setText(WebApi.getWebApi().getJobs_createdAt()[selectedJobIndex]);
		}

		textView_CreatedAt.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize);
		textView_CreatedAt.setPadding(5, 0, 5, 0);
		textView_CreatedAt.setTypeface(null, Typeface.BOLD);
		textView_CreatedAt.setTextColor(Color.BLACK);
		linearLayoutJob.addView(textView_CreatedAt);

		insertExtraLine(linearLayoutJob);

		// ---------- Filer ----------
		/*
		 * Er der filer skal de vises - er der f�rre end eller lig med 3 filer
		 * skal der vises: "vedh�ft filer" er der ingen filer skal der vises:
		 * "Vedh�ft filer" Fil m� ikke overstige 2Mb. (Tilladte filer:
		 * jpg,jpeg,png,bmp,pdf,doc,docx,xls,xlsx)
		 */
		String[][] list = WebApi.getWebApi().getJobs_files_titles();
		if (WebApi.getWebApi().getJobs_files_titles() != null) {
			if (WebApi.getWebApi().getJobs_files_titles()[selectedJobIndex] != null) {

				createFilesection(textSizeHeaders, topPadding, linearLayoutJob);

				insertExtraLine(linearLayoutJob);

				for (int i = 0; i < WebApi.getWebApi().getJobs_files_titles()[selectedJobIndex].length; i++) {
					final int j = i;
					final TextView textView_File_Title = new TextView(this);
					if (!WebApi.getWebApi().getJobs_files_titles()[selectedJobIndex][i]
							.equals("")) {
						textView_File_Title.setText(WebApi.getWebApi()
								.getJobs_files_titles()[selectedJobIndex][i]);
					}

					final int iSelectedJobIndex = selectedJobIndex;

					textView_File_Title.setTextSize(
							TypedValue.COMPLEX_UNIT_DIP, textSize);
					textView_File_Title.setPadding(20, 0, 20, 0);
					textView_File_Title.setTypeface(null, Typeface.BOLD);
					textView_File_Title.setTextColor(Color.BLACK);
					textView_File_Title.setBackgroundResource(R.color.white);
					textView_File_Title
							.setOnClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {
									// Regex works
									// if(WebApi.getWebApi()
									// .getJobs_files_titles()[iSelectedJobIndex][j].matches("([^\\s]+(\\.(?i)(jpg|png|jpeg|bmp))$)")){
									// Secure solution
									if (WebApi.getWebApi()
											.getJobs_files_titles()[iSelectedJobIndex][j]
											.endsWith(".jpg")
											|| WebApi.getWebApi()
													.getJobs_files_titles()[iSelectedJobIndex][j]
													.endsWith(".jpeg")
											|| WebApi.getWebApi()
													.getJobs_files_titles()[iSelectedJobIndex][j]
													.endsWith(".png")
											|| WebApi.getWebApi()
													.getJobs_files_titles()[iSelectedJobIndex][j]
													.endsWith(".bmp")) {
										String temp = WebApi.getWebApi()
												.getJobs_files_paths()[iSelectedJobIndex][j];
										saveSetting("selectedFilePath", temp);
										Intent intent = new Intent(Job.this,
												FileView.class);

										startActivity(intent);
									} else {
										final String googleDocsUrl = "http://docs.google.com/viewer?url=";

										int selectedJobIndex = -1;
										if (!(loadSetting("selectedJobIndex")
												.equals(""))) {
											selectedJobIndex = Integer
													.parseInt(loadSetting("selectedJobIndex"));
										}

										String url = WebApi.getWebApi()
												.getJobs_files_paths()[selectedJobIndex][j];
										Intent intent = new Intent(
												Intent.ACTION_VIEW);
										intent.setDataAndType(
												Uri.parse(googleDocsUrl + url),
												"text/html");
										startActivity(intent);
									}
								}
								// }
							});

					linearLayoutJob.addView(textView_File_Title);

					insertExtraLine(linearLayoutJob);
				}
			}// No Files - append
			else {
				if (editState == true) {
					createFilesection(textSizeHeaders, topPadding,
							linearLayoutJob);
				}
			}
		}

		// No Files - append
		else {
			if (editState == true) {
				createFilesection(textSizeHeaders, topPadding, linearLayoutJob);
			}
		}

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	private void createFilesection(int textSizeHeaders, int topPadding,
			LinearLayout linearLayoutJob) {
		final LinearLayout linearlayout_filesHeader = new LinearLayout(this);
		LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		linearlayout_filesHeader.setLayoutParams(params3);
		linearlayout_filesHeader.setOrientation(LinearLayout.HORIZONTAL);
		linearlayout_filesHeader.setPadding(5, topPadding, 5, 0);
		linearlayout_filesHeader.setBackgroundResource(R.color.gray_light);

		final TextView files_HeaderTextView = new TextView(this);
		files_HeaderTextView.setText("Filer:");
		files_HeaderTextView.setTypeface(null, Typeface.BOLD);
		files_HeaderTextView.setTextSize(textSizeHeaders);
		files_HeaderTextView.setTextColor(getResources()
				.getColor(R.color.black));
		files_HeaderTextView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				goToJobSetFileToJob();
			}
		});

		final TextView files_AddImageTextView = new TextView(this);

		files_AddImageTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
				textSize);
		files_AddImageTextView.setPadding(5, 0, 5, 0);
		files_AddImageTextView.setTypeface(null, Typeface.BOLD);
		files_AddImageTextView.setTextColor(Color.BLACK);

		files_AddImageTextView.setText("Tilf�j Billede");
		files_AddImageTextView.setLayoutParams(params3);
		files_AddImageTextView.setGravity(Gravity.RIGHT);
		files_AddImageTextView.setBackgroundResource(R.color.gray_light);
		files_AddImageTextView.setTextSize(16);
		files_AddImageTextView.setTextColor(getResources().getColor(
				R.color.green_dark_skyhost));
		files_AddImageTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				goToJobSetFileToJob();
			}
		});

		linearlayout_filesHeader.addView(files_HeaderTextView);
		linearlayout_filesHeader.addView(files_AddImageTextView);

		linearLayoutJob.addView(linearlayout_filesHeader);
		// linearLayoutJob.addView(textView_Job_Titles);
		// files_HeaderTextView.setOnClickListener(onClickListenerFilesHeader);
		// files_AddImageTextView.setOnClickListener(onClickListenerFilesHeader);
		// linearlayout_filesHeader.setOnClickListener(onClickListenerFilesHeader);
	}

	private void insertExtraLine(LinearLayout linearLayoutJob) {
		// Extra line spacing
		TextView textView_EmptySpace = new TextView(this);
		linearLayoutJob.addView(textView_EmptySpace);
	}

	private void goToJobSetFileToJob() {
		Intent intent = new Intent(Job.this, JobSetAddFileToJob.class);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		String[][] tmp = WebApi.getWebApi().getJobs_address_lat();

		int selectedJobIndex = -1;
		if (!(loadSetting("selectedJobIndex").equals(""))) {
			selectedJobIndex = Integer
					.parseInt(loadSetting("selectedJobIndex"));
		}

		if (WebApi.getWebApi().getJobs_address_lat()[selectedJobIndex] != null) {
			if (!WebApi.getWebApi().getJobs_address_lat()[selectedJobIndex][0]
					.equals("")) {
				getMenuInflater().inflate(R.menu.jobgooglemaps, menu);
			} else {
				getMenuInflater().inflate(R.menu.job, menu);
			}
		} else {
			getMenuInflater().inflate(R.menu.job, menu);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			intent = new Intent(Job.this, Jobs.class);
			startActivity(intent);
			return true;

		case R.id.activity_edit_job: // Edit Job
			intent = new Intent(Job.this, Job.class);
			saveSetting("editState", true);
			startActivity(intent);
			return true;

		case R.id.activity_newnote: // Add Note
			intent = new Intent(Job.this, NewNote.class);
			startActivity(intent);
			return true;

		case R.id.activity_googlemap: // GoogleMap
			intent = new Intent(Job.this, HelloGoogleMaps.class);
			startActivity(intent);
			return true;

		case R.id.activity_addmaterialtojob: // QuickScrollListView
			intent = new Intent(Job.this, MaterialsQuickScrollListView.class);
			startActivity(intent);
			return true;

		case R.id.activity_materialsonjob: // MaterialsOnJob
			intent = new Intent(Job.this, MaterialsOnJob.class);
			startActivity(intent);
			return true;
		}
		return true;
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private boolean loadSettingBoolean(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		boolean content = preferences.getBoolean(tag, false);
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private void saveSetting(String tag, Boolean content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(tag, content);
		editor.commit();
	}

	private class AsyncDataForJob extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {
			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			Date now = new Date();
			Date previous = WebApi.getWebApi().getTimestampJobs();
			long lResult = now.getTime() - previous.getTime();
			// Hvis l�ngere end 30 sekunder siden vi sidst opdaterede, opdater
			// igen
			if (lResult <= 0 || lResult > 30000) {
				WebApi.getWebApi().Update_Jobs(preferences);
			}

			if (WebApi.getWebApi().isNewData_Jobs() == true) {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						updateScreen();
					}
				});
			} else {
				handler.postDelayed(runUpdateTask, 3000);
			}
			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);
			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class AsyncChangeLinkedStatus extends
			AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			String sSelectedJobId = "";
			if (!(loadSetting("selectedJobId").equals(""))) {
				sSelectedJobId = loadSetting("selectedJobId");
			}

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			WebApi.getWebApi().changeLinkedStatus(preferences, sSelectedJobId);

			saveSetting("reloadJobs", true);

			Intent intent = new Intent(Job.this, Jobs.class);
			startActivity(intent);

			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);
			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class AsyncGetMaterials extends
			AsyncTask<Integer, Integer, Integer> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Integer doInBackground(Integer... params) {
			// Initialize
			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);
			Integer result = 0;
			String sSelectedJobId = "";
			if (!loadSetting("selectedJobId").equals("")) {
				sSelectedJobId = loadSetting("selectedJobId");
			}
			if (!sSelectedJobId.equals("")) {
				WebApi.getWebApi().getJobs_MaterialsOnJob(preferences,
						sSelectedJobId);

				boolean isNewDataMaterialsOnJob = WebApi.getWebApi()
						.isNewData_Jobs_MaterialsOnJob();
				if (isNewDataMaterialsOnJob == true) {

					result = WebApi.getWebApi().getJobs_MaterialsOnJob_ID().length;
				} else {
					handler.postDelayed(runUpdateTask, 3000);
				}
			} else {
				showToast("Der skete en fejl - Pr�v igen.");

			}
			return result;
		}

		protected void onPostExecute(Integer number) {
			super.onPostExecute(number);
			if (number > 0) {
				textView_OverView_Material.setText("Antal : " + number);
				imageView_Materials
						.setImageResource(R.drawable.icon_material_green_64);
			}

			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}

		protected void showToast(final String sHeader, final String sContext) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sContext, Toast.LENGTH_LONG)
							.show();
				}
			});
		}
	}

	private class AsyncUpdateJobs extends AsyncTask<Void, Void, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... params) {
			// Initialize
			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);
			int temp = -1;

			if (!(loadSetting("selectedJobIndex").equals(""))) {
				temp = Integer.parseInt(loadSetting("selectedJobIndex"));
			}

			final int selectedJobIndex = temp;

			// First set boolean false - true will be from most recent update
			WebApi.getWebApi().setNewData_Jobs(false);

			WebApi.getWebApi().Update_Jobs(preferences);

			boolean isNewData_Jobs = WebApi.getWebApi().isNewData_Jobs();

			if (isNewData_Jobs == true) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// JobTitle
						if (!WebApi.getWebApi().getJobs_title()[selectedJobIndex]
								.equals("") && textView_Job_Titles != null) {
							textView_Job_Titles.setText(WebApi.getWebApi()
									.getJobs_title()[selectedJobIndex]);
						}

						TextView t = textView_BeginTime;

						// JobSetStartTime
						if (WebApi.getWebApi().getJobs_beginTime() != null) {
							if (!WebApi.getWebApi().getJobs_beginTime()[selectedJobIndex]
									.equals("") && textView_BeginTime != null) {
								textView_BeginTime
										.setText("Job Start: "
												+ WebApi.getWebApi()
														.getJobs_beginTime()[selectedJobIndex]);
							}
						}

						// JobSetDuration
						if (!WebApi.getWebApi().getJobs_duration()[selectedJobIndex]
								.equals("") && textView_Time_Deployed != null) {
							DecimalFormat df = new DecimalFormat("#.0");
							textView_Time_Deployed
									.setText(df
											.format(Double
													.parseDouble(WebApi
															.getWebApi()
															.getJobs_duration()[selectedJobIndex]) / 60)
											.toString()
											+ " t Afsat");
						}

						// JobSetPriority
						if (WebApi.getWebApi().getJobs_drawables() != null
								&& textView_Priority != null) {

							if (WebApi.getWebApi().getJobs_priority()[selectedJobIndex] != null) {
								String priority = WebApi.getWebApi()
										.getJobs_priority()[selectedJobIndex];
								if (priority.equals("low")) {
									textView_Priority.setText("Prioritet: Lav");
									textView_Priority
											.setCompoundDrawablesWithIntrinsicBounds(
													0, 0, R.drawable.low_32, 0);
								} else if (priority.equals("high")) {
									textView_Priority.setText("Prioritet: H�j");
									textView_Priority
											.setCompoundDrawablesWithIntrinsicBounds(
													0, 0, R.drawable.high_32, 0);
								} else if (priority.equals("urgent")) {
									textView_Priority
											.setText("Prioritet: Haster");
									textView_Priority
											.setCompoundDrawablesWithIntrinsicBounds(
													0, 0, R.drawable.urgent_32,
													0);
								}
							}
						}
					}
				});

			} else {
				handler.postDelayed(runUpdateJobsTask, 5000);
			}
			return null;
		}

		protected void onPostExecute(Void number) {
			super.onPostExecute(number);

			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}
	}

	private class AsyncIntentWithPosition extends
			AsyncTask<Integer, Integer, Integer> {
		protected void onPreExecute() {
			super.onPreExecute();
			GpsApi.getGpsApi().turnGpsOn();
		}

		protected Integer doInBackground(Integer... params) {
			String latitude = "";
			latitude = GpsApi.getGpsApi().getLatitude().toString();

			String longitude = "";
			longitude = GpsApi.getGpsApi().getLongitude().toString();

			if (!latitude.equals("")) {

				latitude = GpsApi.getGpsApi().getLatitude().toString();
				longitude = GpsApi.getGpsApi().getLongitude().toString();
			} else {
				handler.postDelayed(runUpdatePosition, 3000);
			}

			return null;
		}

		protected void onPostExecute(Integer number) {
			super.onPostExecute(number);
			GpsApi.getGpsApi().turnGpsOff();
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}

		protected void showToast(final String sHeader, final String sContext) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sContext, Toast.LENGTH_LONG)
							.show();
				}
			});
		}
	}

	private class AsyncUpdateUsers extends AsyncTask<Integer, Integer, Integer> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Integer doInBackground(Integer... params) {
			// Initialize
			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);
			Integer result = 0;
			String sSelectedJobId = "";
			if (!loadSetting("selectedJobId").equals("")) {
				sSelectedJobId = loadSetting("selectedJobId");
			}
			if (!sSelectedJobId.equals("")) {
				WebApi.getWebApi().getJobs_MaterialsOnJob(preferences,
						sSelectedJobId);

				boolean isNewDataMaterialsOnJob = WebApi.getWebApi()
						.isNewData_Jobs_MaterialsOnJob();
				if (isNewDataMaterialsOnJob == true) {

					result = WebApi.getWebApi().getJobs_MaterialsOnJob_ID().length;
				} else {
					handler.postDelayed(runUpdateTask, 3000);
				}
			} else {
				showToast("Der skete en fejl - Pr�v igen.");

			}
			return result;
		}

		protected void onPostExecute(Integer number) {
			super.onPostExecute(number);

			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}

		protected void showToast(final String sHeader, final String sContext) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sContext, Toast.LENGTH_LONG)
							.show();
				}
			});
		}
	}

	private class ShowAsyncPauseDialog extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(Job.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	public void turnGPSOn() {
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationListener = new MyLocationListener();
		myGPSListener = new MyGPSListener();

		// Detect GPS settings
		if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			showGpsOptions();
		}

		// Turn on GPS listener AND NETWORK LISTENER
		locationManager.addGpsStatusListener(myGPSListener);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				2000, 0, locationListener);
		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 2000, 0, locationListener);
	}

	// automatic turn off the gps
	public void turnGPSOff() {
		if (locationManager != null) {
			locationManager.removeGpsStatusListener(myGPSListener);
		}

		if (locationManager != null) {
			locationManager.removeUpdates(locationListener);
		}
	}

	public boolean bRegistrationOngoing = false;

	private class MyGPSListener implements GpsStatus.Listener {

		public void onGpsStatusChanged(int event) {
			switch (event) {
			case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
				if (mLastLocation != null)
					isGPSFix = (SystemClock.elapsedRealtime() - mLastLocationMillis) < 10000;

				if (isGPSFix) { // A fix has been acquired.
					runOnUiThread(new Runnable() {

						@Override
						public void run() {

						}
					});
				} else { // The fix has been lost.
					runOnUiThread(new Runnable() {

						@Override
						public void run() {

						}
					});
				}
				break;
			case GpsStatus.GPS_EVENT_FIRST_FIX:
				isGPSFix = true;
				break;
			}
		}
	}

	private class MyLocationListener implements LocationListener {
		@Override
		public void onLocationChanged(Location loc) {
			if (loc != null) {
				String lat = String.valueOf(loc.getLatitude());
				String lon = String.valueOf(loc.getLongitude());

				saveSetting("latitude", lat.toString());
				saveSetting("longitude", lon.toString());

				// GPS lost detection
				mLastLocationMillis = SystemClock.elapsedRealtime();
				mLastLocation = loc;

				latitude = Double.toString(loc.getLatitude());
				longitude = Double.toString(loc.getLongitude());

				turnGPSOff();
			}
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	private void showGpsOptions() {
		Intent gpsOptionsIntent = new Intent(
				android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		startActivity(gpsOptionsIntent);
	}
}