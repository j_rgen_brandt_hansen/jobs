package com.skyhost.jobs;

import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("ResourceAsColor")
public class Timer extends ListActivity {

	// Arrays
	private String[] weeks;
	private String[][] days;
	private String[] dates;
	private String[][] regSums;
	private String[][][] jobs;
	private String[][][] recIds;
	private String[][][] jobIDs;
	private String[][][] jobName;
	private String[][][] accountName;
	private String[][][] minutes;
	private String[][][] headers;
	private String[][][] subTitles;

	private Handler handler;
	private ProgressDialog progressDialog;

	private Runnable runUpdateTask = new Runnable() {
		public void run() {

			// Get data
			reloadApi();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timer);

		new PauseDialogLoader().execute();

		// Initialize
		getSystemService(Context.POWER_SERVICE);
		setTitle("Timer");
		handler = new Handler();
	}

	@Override
	protected void onPause() {
		super.onPause();

		if (handler != null) {
			handler.removeCallbacks(runUpdateTask);
		}

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (handler != null) {
			handler.post(runUpdateTask);
		}
	}

	@Override
	public void onBackPressed() {

	}

	private void reloadApi() {

		Date now = new Date();
		Date previous = WebApi.getWebApi().getTimestampJobs();
		long lResult = now.getTime() - previous.getTime();
		// Hvis l�ngere end 30 sekunder siden vi sidst opdaterede, opdater
		// igen
		if (lResult <= 0 || lResult > 30000) {
			new AsyncUpdateTimer().execute();
		} else {
			boolean isNewDataTimer = WebApi.getWebApi().isNewData_Timer();
			if (isNewDataTimer == true) {
				updateScreen();
				if (loadSettingBoolean("AutoUpdate") == true) {
					handler.postDelayed(runUpdateTask, 30000);
				}
			} else {
				handler.postDelayed(runUpdateTask, 3000);
			}
		}
	}

	private void updateScreen() {
		final String[] dayNames = new String[] { "Mandag", "Tirsdag", "Onsdag",
				"Torsdag", "Fredag", "L�rdag", "S�ndag" };

		fillData(dayNames);

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	private void fillData(String[] dayNames) {
		this.weeks = WebApi.getWebApi().getTimer_WeeksNo();
		this.days = WebApi.getWebApi().getTimer_Days();
		this.dates = WebApi.getWebApi().getTimer_Dates();
		this.regSums = WebApi.getWebApi().getTimer_RegSums();
		this.jobs = WebApi.getWebApi().getTimer_Jobs();
		this.recIds = WebApi.getWebApi().getTimer_RecIds();
		this.jobIDs = WebApi.getWebApi().getTimer_JobIDs();
		this.accountName = WebApi.getWebApi().getTimer_AccountName();
		this.jobName = WebApi.getWebApi().getTimer_JobName();
		this.minutes = WebApi.getWebApi().getTimer_Minutes();
		this.headers = WebApi.getWebApi().getTimer_Headers();
		this.subTitles = WebApi.getWebApi().getTimer_SubTitles();

		LinearLayout linearLayout = (LinearLayout) findViewById(R.id.timer_linearlayoutinscrollview);
		linearLayout.removeAllViews();
		linearLayout.setWeightSum(1);

		// ---------- Overskrift ----------
		for (int i = 0; i < weeks.length; i++) {
			TextView timer_Header = new TextView(this);
			timer_Header.setText(weeks[i]);
			timer_Header.setPadding(5, 0, 5, 0);
			timer_Header.setHeight(100);
			timer_Header.setTextColor(getResources().getColor(R.color.black));
			// timer_Header.setBackgroundResource(R.color.gray_light);
			timer_Header.setTextSize(20);
			timer_Header.setTypeface(null, Typeface.BOLD);
			timer_Header.setGravity(Gravity.CENTER);

			linearLayout.addView(timer_Header);

			for (int j = 0; j < dayNames.length; j++) {

				// LinearLayout med text og pil til h�jre
				LinearLayout rowLayout = new LinearLayout(this);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
						LayoutParams.MATCH_PARENT, 100);
				rowLayout.setLayoutParams(params);
				rowLayout.setPadding(5, 0, 0, 0);
				rowLayout
						.setBackgroundResource(R.drawable.timer_listitem_background_selector);
				final int weekNumber = i;
				final int dayNumber = j;
				rowLayout.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						saveSetting("week", String.valueOf(weekNumber));
						saveSetting("day", String.valueOf(dayNumber));

						Intent intent = new Intent(Timer.this,
								TimerNewJob.class);
						startActivity(intent);
					}
				});

				// Linearlayout til tekst til venstre
				LinearLayout linearLayoutForRowLayoutLeft = new LinearLayout(
						this);

				// Day
				TextView textViewDay = new TextView(this);
				textViewDay.setText(dayNames[j]);
				textViewDay.setHeight(80);
				textViewDay.setLayoutParams(new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.MATCH_PARENT, 100));
				textViewDay.setTextSize(16);
				textViewDay.setPadding(10, 0, 5, 0);
				textViewDay.setTypeface(null, Typeface.BOLD);
				textViewDay.setTextColor(Color.BLACK);
				textViewDay.setGravity(Gravity.CENTER_VERTICAL);

				// Tilf�j children til parent
				linearLayoutForRowLayoutLeft.addView(textViewDay);
				rowLayout.addView(linearLayoutForRowLayoutLeft);

				if (regSums[i] != null) {
					if (regSums[i][j] != null) {
						// RegSum
						TextView textViewRegSum = new TextView(this);
						textViewRegSum.setHeight(100);
						textViewRegSum
								.setLayoutParams(new LinearLayout.LayoutParams(
										LinearLayout.LayoutParams.MATCH_PARENT,
										100));
						textViewRegSum.setTextSize(16);
						textViewRegSum.setPadding(0, 0, 5, 0);
						textViewRegSum.setTypeface(null, Typeface.BOLD);
						textViewRegSum.setTextColor(Color.BLACK);
						textViewRegSum.setGravity(Gravity.CENTER_VERTICAL
								| Gravity.RIGHT);
						textViewRegSum.setText(regSums[i][j]);

						// Linearlayout til timer i midten
						LinearLayout layoutForRowLayoutMiddle = new LinearLayout(
								this);
						LinearLayout.LayoutParams paramsMiddle = new LinearLayout.LayoutParams(
								LayoutParams.MATCH_PARENT, 100, 1f);
						layoutForRowLayoutMiddle.setLayoutParams(paramsMiddle);

						// Tilf�j children til parent
						layoutForRowLayoutMiddle.addView(textViewRegSum);
						rowLayout.addView(layoutForRowLayoutMiddle);
					}
				}
				// Linearlayout til billede til h�jre
				LinearLayout linearLayoutForRowLayoutImage = new LinearLayout(
						this);
				LinearLayout.LayoutParams paramsImage = new LinearLayout.LayoutParams(
						LayoutParams.MATCH_PARENT, 60, 2f);
				paramsImage.gravity = Gravity.CENTER_VERTICAL;
				linearLayoutForRowLayoutImage.setLayoutParams(paramsImage);

				// Pil h�jre
				ImageView imageView = new ImageView(this);
				imageView.setImageResource(R.drawable.icon_arrow_right_32);
				imageView.setScaleType(ImageView.ScaleType.FIT_END);

				imageView.setLayoutParams(paramsImage);

				// Divider
				LinearLayout.LayoutParams paramsDivider = new LinearLayout.LayoutParams(
						LayoutParams.FILL_PARENT, 1);
				TextView view = new TextView(this);
				view.setLayoutParams(paramsDivider);
				view.setPadding(25, 0, 25, 0);
				view.setBackgroundColor(Color.DKGRAY);

				// Tilf�j children til parent
				linearLayoutForRowLayoutImage.addView(imageView);
				rowLayout.addView(linearLayoutForRowLayoutImage);
				linearLayout.addView(rowLayout);
				linearLayout.addView(view);
			}
			TextView textView_EmptySpace = new TextView(this);
			textView_EmptySpace.setHeight(100);
			linearLayout.addView(textView_EmptySpace);
		}
		TextView textView_EmptySpace = new TextView(this);
		textView_EmptySpace = new TextView(this);
		linearLayout.addView(textView_EmptySpace);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.timer, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			Intent jobs = new Intent(Timer.this, Jobs.class);
			startActivity(jobs);
			return true;

		case R.id.activity_job: // Job
			Intent job = new Intent(Timer.this, Job.class);
			startActivity(job);
			return true;

		case R.id.activity_hellogooglemaps: // Map
			saveSetting("sCommand", "allJobs");
			Intent map = new Intent(Timer.this, HelloGoogleMaps.class);
			startActivity(map);
			return true;

		case R.id.activity_newnote: // Add Note
			Intent newNote = new Intent(Timer.this, NewNote.class);
			startActivity(newNote);
			return true;

		case R.id.activity_notes: // Notes
			Intent notes = new Intent(Timer.this, Notes.class);
			startActivity(notes);
			return true;

		case R.id.activity_status: // Status
			Intent status = new Intent(Timer.this, StatusSkyhost.class);
			startActivity(status);
			return true;
			//
		case R.id.activity_settings: // Settings
			Intent settings = new Intent(Timer.this, Settings.class);
			startActivity(settings);

		case R.id.activity_show_one: // Job
			saveSetting("sCommand", "oneJob");
			Intent active = new Intent(Timer.this, HelloGoogleMaps.class);
			startActivity(active);
		}
		return true;
	}

	private Boolean loadSettingBoolean(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		Boolean content = preferences.getBoolean(tag, false);
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private class AsyncUpdateTimer extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			WebApi.getWebApi().update_Timer(preferences);

			boolean isNewDataTimer = WebApi.getWebApi().isNewData_Timer();
			if (isNewDataTimer == true) {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						updateScreen();
					}
				});

				if (preferences.getBoolean("AutoUpdate", false) == true) {
					handler.postDelayed(runUpdateTask, 30000);
				}
			} else {
				handler.postDelayed(runUpdateTask, 3000);
			}

			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);

			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(Timer.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);

			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}
}