package com.skyhost.jobs;

import java.util.HashMap;
import com.google.gson.Gson;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class JobSetUsers extends ListActivity implements OnClickListener {

	private ProgressDialog progressDialog;

	public static SparseBooleanArray sparseBooleanArray;
	private Handler mHandler;
	private HashMap<Integer, Boolean> positions;

	private Runnable getUsersOnAccount = new Runnable() {

		@Override
		public void run() {
			new AsyncGetUsersOnAccount().execute();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.jobsetusers);

		new PauseDialogLoader().execute();

		// Initializing
		positions = new HashMap<Integer, Boolean>();
		final ListView listView = getListView();
		listView.setItemsCanFocus(false);
		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		sparseBooleanArray = listView.getCheckedItemPositions();
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// parent.setSelection(position);
				// sparseBooleanArray = ((ListView) parent)
				// .getCheckedItemPositions();
				if (position < positions.size()) {
					if (positions.get(position).equals(false)) {
						positions.put(position, true);
					} else {
						positions.put(position, false);
					}
				}
				((ArrayAdapterJobSetUsers) parent.getAdapter())
						.notifyDataSetChanged();
			}
		});

		Button saveButton = (Button) findViewById(R.id.button_save);
		saveButton.setOnClickListener(this);
		mHandler = new Handler();
	}

	@Override
	public void onPause() {
		super.onPause();

		saveSetting("isNewJob", false);

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		mHandler.post(getUsersOnAccount);

		if (progressDialog != null) {
			progressDialog.hide();
		}
	}

	private void updateScreen() {

		if (loadSettingBoolean("isNewJob") == false) {

			int selectedJobIndex = -1;

			if (!(loadSetting("selectedJobIndex").equals(""))) {
				selectedJobIndex = Integer
						.parseInt(loadSetting("selectedJobIndex"));
			}

			final ArrayAdapterJobSetUsers adapter = new ArrayAdapterJobSetUsers(
					this, WebApi.getWebApi().getUsersOnAccount_Ids(), WebApi
							.getWebApi().getUsersOnAccount_Names(), WebApi
							.getWebApi().getJobs_employees_name(),
					selectedJobIndex);

			runOnUiThread(new Runnable() {

				@Override
				public void run() {

					try {
						setListAdapter(adapter);

					} catch (Exception ex) {
						ex.printStackTrace();
					}

					if (progressDialog != null) {
						progressDialog.hide();
					}
				}
			});
		} else {
			final ArrayAdapterJobSetUsers adapter = new ArrayAdapterJobSetUsers(
					this, WebApi.getWebApi().getUsersOnAccount_Ids(), WebApi
							.getWebApi().getUsersOnAccount_Names(), WebApi
							.getWebApi().getJobs_employees_name(), -1);
			// Duplicate code else performance loss
			runOnUiThread(new Runnable() {

				@Override
				public void run() {

					try {
						setListAdapter(adapter);

					} catch (Exception ex) {
						ex.printStackTrace();
					}

					if (progressDialog != null) {
						progressDialog.hide();
					}
				}
			});
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			Intent jobs = new Intent(JobSetUsers.this, Jobs.class);
			startActivity(jobs);
			return true;

		case R.id.activity_timer: // Job
			Intent timer = new Intent(JobSetUsers.this, Timer.class);
			startActivity(timer);
			return true;
		}
		return true;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private Boolean loadSettingBoolean(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		Boolean content = preferences.getBoolean(tag, false);
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private void saveSetting(String tag, Boolean content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(tag, content);
		editor.commit();
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.button_save) {

			

				String initialJSONArray = "";

				if (!(loadSetting("initialJSONArray").equals(""))) {
					initialJSONArray = loadSetting("initialJSONArray");
				}

				int selectedJobIndex = -1;

				if (!(loadSetting("selectedJobIndex").equals(""))) {
					selectedJobIndex = Integer
							.parseInt(loadSetting("selectedJobIndex"));
				}

				String presentJSONArray = "";
				Gson gson = new Gson();
				presentJSONArray = gson.toJson(positions);

				if (presentJSONArray.equals(initialJSONArray)) {
					Intent intent = new Intent(JobSetUsers.this, Jobs.class);
					startActivity(intent);
				} else {
					StringBuilder stringBuilder = new StringBuilder();
					int size = positions.size();
					for (int i = 0; i < size; i++) {
						if (positions.get(i).equals(true)) {

							if (stringBuilder.length() == 0) {

								String id = WebApi.getWebApi()
										.getUsersOnAccount_Ids()[i];

								stringBuilder.append(id);
							} else {

								String id = WebApi.getWebApi()
										.getUsersOnAccount_Ids()[i];

								stringBuilder.append("," + id);
							}
						}
					}
					
					String result = stringBuilder.toString();

					saveSetting("selectedUsers", result);

					new AsyncSetJobUsers().execute();
				}
		}
	}

	private class AsyncGetUsersOnAccount extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			WebApi.getWebApi().getUsersOnAccount(preferences);

			// Success
			if (WebApi.getWebApi().isNewData_Jobs_GetUsersOnAccount() == true) {
				updateScreen();
			}
			// Failure
			else {
				mHandler.postDelayed(getUsersOnAccount, 3000);
			}

			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);

			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class AsyncSetJobUsers extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			String selectedJobId = "";
			if (!(loadSetting("selectedJobId").equals(""))) {
				selectedJobId = loadSetting("selectedJobId");
			}

			String selectedUsers = "";
			if (!(loadSetting("selectedUsers").equals(""))) {
				selectedUsers = loadSetting("selectedUsers");
			}

			WebApi.getWebApi().setJobUsers(preferences, selectedJobId,
					selectedUsers);

			Intent intent = new Intent(JobSetUsers.this, Jobs.class);

			// Success
			if (WebApi.getWebApi().isNewData_Jobs_SetJobUsers() == true) {
				saveSetting("reloadJob", true);
				startActivity(intent);
			}
			// Failure
			else {
				showToast("Der skete en fejl - Pr�v igen!");
				startActivity(intent);
			}

			return null;
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(JobSetUsers.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	public class ArrayAdapterJobSetUsers extends ArrayAdapter<String> {
		private final Activity context;
		// private Map positions;
		private String[] users;
		private String[][] selectedUsers;
		public ArrayAdapterJobSetUsers(Activity context, String[] ids,
				String[] users, String[][] employees_name, int selectedJobIndex) {
			super(context, R.layout.rowlayout_jobsetusers, users);

			this.context = context;
			this.users = users;
			this.selectedUsers = employees_name;
			int length = users.length;
			for (int i = 0; i < length; i++) {
				positions.put(i, false);
			}

			if (!(selectedJobIndex == -1)) {
				if (selectedUsers[selectedJobIndex] != null) {
					for (int i = 0; i < length; i++) {

						String userName = users[i].toString();
						for (int j = 0; j < selectedUsers[selectedJobIndex].length; j++) {
							if (selectedUsers[selectedJobIndex][j]
									.equals(userName)) {
								positions.put(i, true);
							}
						}
					}
				}
			}

			Gson gson = new Gson();
			String initialJSONArray = gson.toJson(positions);
			saveSetting("initialJSONArray", initialJSONArray);

		}

		class ViewHolder {
			public LinearLayout row;
			public TextView textView_Id;
			public TextView textView_Header;
			public CheckedTextView check;

		}

		@Override
		public int getCount() {
			return users.length;
		}

		@Override
		public String getItem(int position) {
			return users[position];
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = null;

			ViewHolder holder = null;
			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.rowlayout_jobsetusers, null);
				holder = new ViewHolder();

				holder.textView_Id = (TextView) convertView
						.findViewById(R.id.jobsetusers_id);
				holder.textView_Header = (TextView) convertView
						.findViewById(R.id.jobsetusers_user);
				holder.check = (CheckedTextView) convertView
						.findViewById(R.id.jobsetusers_checkedtextview);

				convertView.setTag(holder);
			} else {
				view = convertView;
				holder = (ViewHolder) view.getTag();
			}

			// holder.textView_Id.setText(ids[position].toString());
			String userName = null;
			if (position <= users.length) {
				userName = users[position].toString();
				holder.textView_Header.setText(userName);
			}

			if (positions.get(position).equals(true)) {

				((CheckedTextView) holder.check).setChecked(true);
				// for focus on it
				// int top = (ActiveItem == null) ? 0 : ActiveItem.getTop();
				// ((ListView) parent).setSelectionFromTop(position, top);
			} else {
				((CheckedTextView) holder.check).setChecked(false);
			}

			return convertView;
		}
	}
}