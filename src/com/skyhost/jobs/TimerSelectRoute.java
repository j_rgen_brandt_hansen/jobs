package com.skyhost.jobs;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

public class TimerSelectRoute extends ListActivity implements
		OnCreateContextMenuListener {

	private Handler handler = new Handler();
	private ProgressDialog progressDialog;
	public static SparseBooleanArray sparseBooleanArray;

	private Runnable runUpdateTask = new Runnable() {
		public void run() {

			new AsyncTimerRoutesOnDay().execute();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timerselectroute);

		new PauseDialogLoader().execute();

		// Initializing
		String sSelectedJobOnDay = "";

		if (!(loadSetting("selectedJobOnDay").equals(""))) {
			sSelectedJobOnDay = loadSetting("selectedJobOnDay");
			setTitle(sSelectedJobOnDay + " - V�lg K�rsel");
		}

		sparseBooleanArray = new SparseBooleanArray();
		// Dato p� overskrift
		final ListView listView = getListView();
		listView.setItemsCanFocus(false);

		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				parent.setSelection(position);
				sparseBooleanArray = ((ListView) parent)
						.getCheckedItemPositions();
				((ArrayAdapterTimerSelectRoute) parent.getAdapter())
						.notifyDataSetChanged();
			}
		});

		Button selectTrips = (Button) findViewById(R.id.button_selecttrip);
		selectTrips.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SparseBooleanArray sparseBooleanArray = listView
						.getCheckedItemPositions();
				StringBuffer stringBuffer = new StringBuffer();
				String string = "";
				for (int i = 0; i < sparseBooleanArray.size(); i++) {
					if (sparseBooleanArray.valueAt(i) == true) {
						int position = sparseBooleanArray.keyAt(i);
						String[] tmp = WebApi.getWebApi().getTimer_Route_Id();
						String id = WebApi.getWebApi().getTimer_Route_Id()[position];
						if (stringBuffer.length() > 0) {
							stringBuffer.append("#");
						}
						stringBuffer = stringBuffer.append(id);
					}
				}

				// PT kun muligt at v�lge een k�rsel
				// Flere muligheder tilg�r n�r modulet rettes til
				if (stringBuffer.length() != 0) {
					string = stringBuffer.substring(0, 7);
				}

				saveSetting("selectedTripsIdsOnDay", string.toString());
				Intent goToTimerSelectTime = new Intent(TimerSelectRoute.this,
						TimerSelectTime.class);
				startActivity(goToTimerSelectTime);
			}
		});

		handler = new Handler();
	}

	private void updateScreen() {
		final ArrayAdapterTimerSelectRoute adapter = new ArrayAdapterTimerSelectRoute(
				this, WebApi.getWebApi().getTimer_Route_Id(), WebApi
						.getWebApi().getTimer_Route_Header(), WebApi
						.getWebApi().getTimer_Route_Subtitle());

		runOnUiThread(new Runnable() {

			@Override
			public void run() {

				try {
					setListAdapter(adapter);

					if (progressDialog != null) {
						progressDialog.hide();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});

	}

	@Override
	public void onPause() {
		super.onPause();
		if (handler != null) {
			handler.removeCallbacks(runUpdateTask);
		}

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (handler != null) {
			handler.postDelayed(runUpdateTask, 100);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.timernewjob, menu);
		return true;
	}

	public boolean optionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.timernewjob_optional_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_timer: // Jobs
			Intent timer = new Intent(TimerSelectRoute.this, Timer.class);
			startActivity(timer);
			return true;
		}
		return true;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private class AsyncTimerRoutesOnDay extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {

			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			String selectedDate = "";
			if (!loadSetting("selectedDate").equals("")) {
				selectedDate = loadSetting("selectedDate");
			}

			WebApi.getWebApi().getTimer_RoutesOnDay(preferences, selectedDate);

			boolean routesOnDay = WebApi.getWebApi()
					.isNewData_Timer_RoutesOnDay();
			if (routesOnDay == true) {
				// If no routes, go to timerSelectTime
				if (WebApi.getWebApi().getTimer_JobIdsOnDay().length == 0) {
					Intent goToSelectTrip = new Intent(TimerSelectRoute.this,
							TimerSelectTime.class);
					startActivity(goToSelectTrip);
				} else {
					// If any routes
					updateScreen();
					// No autoupdate, selections will become deselected
					// if (preferences.getBoolean("AutoUpdate", false) == true)
					// {
					// handler.postDelayed(mUpdateTimeTask, 30000);
					// }
				}
			} else {
				handler.postDelayed(runUpdateTask, 10000);
			}

			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);

			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(TimerSelectRoute.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();

//			// Set Runnable to remove splash screen just in case
//			final Handler handler = new Handler();
//			handler.postDelayed(new Runnable() {
//				@Override
//				public void run() {
//					if(progressDialog != null) {
//						progressDialog.dismiss();}
//				}
//			}, 5000);
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);

			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

	}
}