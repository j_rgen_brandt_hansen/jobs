package com.skyhost.jobs;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.client.android.Intents;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

public class MaterialsQuickScrollListView extends ListActivity {

	private Handler handler;
	private ProgressDialog progressDialog;
	private Boolean isSearching = false;
	private String[][] materials = null;

	private Runnable runUpdateTask = new Runnable() {
		public void run() {
			reloadApi();
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.materialsquickscrolllistview);

		new ShowAsyncPauseDialog().execute();

		// Initialize
		final ListView listView = getListView();
		listView.setFastScrollEnabled(true);
		listView.setTextFilterEnabled(true);

		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				TextView matId = (TextView) view
						.findViewById(R.id.quickscrolllistview_material_id);
				String materialId = matId.getText().toString();
				if (!materialId.equals("Ingen match")) {

					// Save materialID
					saveSetting("selectedMaterialId", materialId);

					Intent intent = new Intent(
							MaterialsQuickScrollListView.this,
							AddMaterialToJob.class);
					startActivity(intent);
				} else {
					// Nothing
				}
			}
		});

		final MaterialsQuickScrollListView activity = this;
		final EditText editTextSearch = (EditText) findViewById(R.id.materialsquickscrolllistview_search);
		final ArrayList arrayListName = new ArrayList();
		final ArrayList arrayListId = new ArrayList();
		editTextSearch.addTextChangedListener(new TextWatcher() {

			@SuppressLint("NewApi")
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				int textLength = editTextSearch.getText().length();

				arrayListName.clear();
				arrayListId.clear();

				for (int i = 0; i < materials.length; i++) {
					for (int j = 0; j < materials[i].length; j++) {
						if (!materials[i][j].equals("")) {
							if (textLength <= materials[i][j].length()) {

								if (materials[i][j].toLowerCase().contains(
										s.toString())) {
									String tmp = materials[i][j];
									if (!arrayListName
											.contains(materials[1][j])) {

										arrayListName.add(materials[1][j]);
										arrayListId.add(materials[0][j]);
									}
								}
							}
						}
					}
				}

				// From Lists to Arrays
				String[] names = Arrays.asList(arrayListName.toArray())
						.toArray(new String[arrayListName.toArray().length]);

				String[] ids = Arrays.asList(arrayListId.toArray()).toArray(
						new String[arrayListId.toArray().length]);

				int tmp = names.length;

				if (tmp == 0) {
					// Prepare error message
					names = new String[1];
					names[0] = "Ingen match";
					ids = new String[1];
					ids[0] = "Ingen match";
				}
				MyListAdapter adapter = new MyListAdapter(activity, ids, names);
				setListAdapter(adapter);

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

		editTextSearch
				.setOnFocusChangeListener(new View.OnFocusChangeListener() {
					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						if (hasFocus) {
							getWindow()
									.setSoftInputMode(
											WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
						}
					}
				});

		handler = new Handler();

		// Hide Keyboard - in order to show when searching
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
	}

	@Override
	public void onPause() {
		super.onPause();
		if (handler != null) {
			handler.removeCallbacks(runUpdateTask);
		}

		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		if (handler != null) {
			if (isSearching == false) {
				handler.post(runUpdateTask);
			} else {
				isSearching = false;
			}
		}
	}

	private void reloadApi() {
		// Load Timestamp from last Update Time? no timestamp = no materials
		// Get Timestamp
		String lastUpdateTime = "";
		if (!loadSetting("lastUpdateTime").equals("")) {
			lastUpdateTime = loadSetting("lastUpdateTime");
		}

		// Timestamp exists
		if (!lastUpdateTime.equals("")) {
			// Hvis l�ngere end 1 uge siden vi sidst opdaterede, opdater
			// igen ( 1 uge er millisekunder*sekunder*minutter*timer*dage =
			// 1000*60*60*24*7 = 604800000 )
			Date now = new Date();
			SimpleDateFormat simpleDayFormat = new SimpleDateFormat(
					"EE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
			Date previous = null;
			try {
				previous = simpleDayFormat.parse(lastUpdateTime);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			long lTimeSpan = 0;

			if (previous != null) {
				lTimeSpan = now.getTime() - previous.getTime();
			}
			// TODO Update every time
			else {
				new AsyncUpdateMaterials().execute();
			}

			if (lTimeSpan <= 0 || lTimeSpan > 604800000) {
				new AsyncUpdateMaterials().execute();
			} else {
				// Load saved materials
				try {
					if (!loadSetting("materials").equals("")) {
						Type type = new TypeToken<String[][]>() {
						}.getType();

						String str = loadSetting("materials");
						materials = new Gson().fromJson(str, type);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				updateScreen();
			}
		} else {
			// Timestamp does not exist
			new AsyncUpdateMaterials().execute();
		}

	}

	private void updateScreen() {

		// SearchResults will be overwritten by onResume if no boolean
		if (isSearching == false) {

			if (materials != null) {
				final MyListAdapter adapter = new MyListAdapter(this,
				// 1: name
				// 0: id
						materials[0], materials[1]);

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						try {
							setListAdapter(adapter);
						} catch (Exception ex) {
							ex.printStackTrace();
						}

						if (progressDialog != null) {
							progressDialog.hide();
						}
					}
				});
			}
		}
	}

	private class MyListAdapter extends ArrayAdapter<String> implements
			SectionIndexer, Filterable {

		HashMap<String, Integer> alphaIndexer;
		String[] sections;
		private Activity context;
		private String[] jobs_Material_Name; // ikke null
		private String[] jobs_Material_MatId;

		public MyListAdapter(Activity context, String[] jobs_Material_MatId,
				String[] jobs_Material_Name) {
			super(context, R.layout.list_item, jobs_Material_Name);
			this.context = context;
			this.jobs_Material_Name = jobs_Material_Name;
			this.jobs_Material_MatId = jobs_Material_MatId;

			alphaIndexer = new HashMap<String, Integer>();
			int size = jobs_Material_Name.length;

			for (int x = 0; x < size; x++) {
				String s = jobs_Material_Name[x];
				if (s != null) {
					// get the first letter of the store
					String ch = s.substring(0, 1);
					// convert to uppercase otherwise lowercase a -z will be
					// sorted
					// after upper A-Z
					ch = ch.toUpperCase();

					// put only if the key does not exist
					if (!alphaIndexer.containsKey(ch))
						alphaIndexer.put(ch, x);
				}
			}

			Set<String> sectionLetters = alphaIndexer.keySet();

			// create a list from the set to sort
			ArrayList<String> sectionList = new ArrayList<String>(
					sectionLetters);

			Collections.sort(sectionList);

			sections = new String[sectionList.size()];

			sectionList.toArray(sections);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View view = null;
			ViewHolder holder = null;

			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.rowlayout_materialsquickscrolllistview, null);
				holder = new ViewHolder();
				holder.materialName = (TextView) convertView
						.findViewById(R.id.quickscrolllistview_material_name);
				holder.materialId = (TextView) convertView
						.findViewById(R.id.quickscrolllistview_material_id);
				convertView.setTag(holder);
			} else {
				view = convertView;
				holder = (ViewHolder) view.getTag();
			}
			holder.materialName.setText(jobs_Material_Name[position]);

			// Not needed not saved when sorting
			holder.materialId.setText(jobs_Material_MatId[position]);
			return convertView;
		}

		public int getPositionForSection(int section) {
			return alphaIndexer.get(sections[section]);
		}

		public int getSectionForPosition(int position) {
			return 0;
		}

		public Object[] getSections() {
			return sections;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.materialsquickscrolllistview, menu);
		return true;
	}

	static class ViewHolder {
		public TextView materialName;
		public TextView materialId;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_search: // Search
			LinearLayout linearLayoutSearch = (LinearLayout) findViewById(R.id.materialsquickscrolllistview_linearlayout_search);
			linearLayoutSearch.setVisibility(View.VISIBLE);

			// Open keypad
			EditText editTextsearch = (EditText) findViewById(R.id.materialsquickscrolllistview_search);

			InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			mgr.showSoftInput(editTextsearch, InputMethodManager.SHOW_IMPLICIT);
			editTextsearch.requestFocus();
			return true;

		case R.id.activity_materialsonjob: // MaterialsOnJob
			Intent navigateToMaterialsOnJob = new Intent(
					MaterialsQuickScrollListView.this, MaterialsOnJob.class);
			startActivity(navigateToMaterialsOnJob);
			return true;
		case R.id.activity_scanmaterial: // Scan
			isSearching = true;
			Intent intent = new Intent(MaterialsQuickScrollListView.this,
					com.google.zxing.client.android.CaptureActivity.class);
			intent.setAction(Intents.Scan.ACTION);
			startActivityForResult(intent, 0);
			return true;
		case R.id.activity_updatemateriallist: // Opdater liste

			new AsyncUpdateMaterials().execute();

			return true;
		}
		return true;
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {

				String scanResult = intent.getStringExtra("SCAN_RESULT");

				// Filter non alphabet or non numeric
				String filteredScanResult = scanResult.replaceAll(
						"[^A-Za-z0-9]", "");

				if (!filteredScanResult.equals("")) {
					// saveSetting("scanResult", scanResult);
					EditText editTextSearch = (EditText) findViewById(R.id.materialsquickscrolllistview_search);
					editTextSearch.setText(filteredScanResult);
				}
			}
		}
	}

	private class DialogLoaderNumberOfMaterial extends
			AsyncTask<Void, Integer, Void> {
		AlertDialog.Builder alertDialog;

		protected void onPreExecute() {
			super.onPreExecute();

			// Create alert Dialog with one Button
			alertDialog = new AlertDialog.Builder(
					MaterialsQuickScrollListView.this);

			// Set Dialog Title
			alertDialog.setTitle("Indtast antal");

			// Set Dialog Message
			String selectedMaterial = "";
			if (!loadSetting("selectedMaterial").equals("")) {
				selectedMaterial = loadSetting("selectedMaterial");
			}

			alertDialog.setMessage(selectedMaterial);
			final EditText input = new EditText(
					MaterialsQuickScrollListView.this);
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.MATCH_PARENT);
			input.setLayoutParams(lp);
			input.setInputType(InputType.TYPE_CLASS_NUMBER);
			alertDialog.setView(input);

			// Setting Icon to Dialog
			alertDialog.setIcon(R.drawable.icon_informationgreen);

			// Setting Positive "Yes" Button
			alertDialog.setPositiveButton("Ja",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Write your code here to execute after dialog

							int numberOfMaterialsForJob = Integer
									.parseInt(input.getText().toString());
							if (numberOfMaterialsForJob != 0) {

								Toast.makeText(
										getApplicationContext(),
										"Du har indtastet: "
												+ numberOfMaterialsForJob,
										Toast.LENGTH_SHORT).show();
								Intent myIntent1 = new Intent(
										MaterialsQuickScrollListView.this,
										MaterialsQuickScrollListView.class);
								startActivityForResult(myIntent1, 0);
							} else {
								Toast.makeText(getApplicationContext(),
										"Der m� kun indtastes tal!",
										Toast.LENGTH_SHORT).show();
							}
						}
					});

			// Setting Negative "NO" Button
			alertDialog.setNegativeButton("Fortryd",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Write your code here to execute after dialog
							dialog.cancel();
						}
					});

			// Showing Alert Message
			alertDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (alertDialog != null) {
				((DialogInterface) alertDialog).dismiss();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class AsyncUpdateMaterials extends AsyncTask<Void, Integer, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {
			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);
			WebApi.getWebApi().getJobs_Materials(preferences);

			boolean isNewDataJobs = WebApi.getWebApi()
					.isNewData_Jobs_GetMaterials();
			if (isNewDataJobs == true) {
				Date now = new Date();
				Date previous = WebApi.getWebApi().getTimestampMaterials();
				String sPrevious = previous.toString();
				saveSetting("lastUpdateTime", String.valueOf(sPrevious));
				// Save materials offline
				String[][] materials1 = {
						WebApi.getWebApi().getJobs_Material_MatID(),
						WebApi.getWebApi().getJobs_Material_Name(),
						WebApi.getWebApi().getJobs_Material_Description(),
						WebApi.getWebApi().getJobs_Material_Reference(),
						WebApi.getWebApi().getJobs_Material_Colli(),
						WebApi.getWebApi().getJobs_Material_GroupNo(),
						WebApi.getWebApi().getJobs_Material_GroupName(),
						WebApi.getWebApi().getJobs_Material_SupplierItemNo(),
						WebApi.getWebApi().getJobs_Material_EanNo(),
						WebApi.getWebApi().getJobs_Material_ElNo(),
						WebApi.getWebApi().getJobs_Material_EanBarcode(),
						WebApi.getWebApi().getJobs_Material_InternalBarcode(),
						WebApi.getWebApi().getJobs_Material_InternalItemNo(),
						WebApi.getWebApi()
								.getJobs_Material_InternalItemShortNo(), };
				materials = materials1;
				String json = new Gson().toJson(materials);
				saveSetting("materials", json);

				updateScreen();
			} else {
				handler.postDelayed(runUpdateTask, 3000);
			}

			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);
			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}

	private class ShowAsyncPauseDialog extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(
					MaterialsQuickScrollListView.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}
}