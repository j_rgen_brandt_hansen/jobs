package com.skyhost.jobs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewNote extends Activity implements OnClickListener {

	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newnote);

		// Initialize
		setTitle("Tilf�j note");
		EditText addNote = (EditText) findViewById(R.id.addnote);
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
		Button addNewNoteButton = (Button) findViewById(R.id.add_note_button);
		addNewNoteButton.setOnClickListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		
		if(progressDialog != null) {
			progressDialog.cancel();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.newnote, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.activity_jobs: // Jobs
			Intent jobs = new Intent(NewNote.this, Jobs.class);
			startActivity(jobs);
			return true;

		case R.id.activity_job: // Job
			Intent job = new Intent(NewNote.this, Job.class);
			startActivity(job);
			return true;

		case R.id.activity_newnote: // Add Note
			Intent newNote = new Intent(NewNote.this, NewNote.class);
			startActivity(newNote);
			return true;

		case R.id.activity_notes: // Notes
			Intent notes = new Intent(NewNote.this, Notes.class);
			startActivity(notes);
			return true;

		case R.id.activity_status: // Status
			Intent status = new Intent(NewNote.this, StatusSkyhost.class);
			startActivity(status);
			return true;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.add_note_button) {

			new ShowAsyncPauseDialog().execute();

			new AsyncLoader().execute();
		}
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private class AsyncLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {
			// Initialize
			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);
			Intent navigateToNotes = new Intent(NewNote.this, Notes.class);
			String selectedJobId = null;

			// Save Note
			if (!(loadSetting("selectedJobId").equals(""))) {
				selectedJobId = loadSetting("selectedJobId");

				final EditText addNote = (EditText) findViewById(R.id.addnote);

final String ALLOWED_URI_CHARS = ".,"; 
String newNote = Uri.encode(addNote.getText().toString(), ALLOWED_URI_CHARS);
			

				WebApi.getWebApi().saveNewNote(preferences, selectedJobId,
						newNote);
			}

			// Response error and success handling -> redirection
			if (WebApi.getWebApi().isNewData_Note() == false
					|| WebApi.getWebApi().isNewData_Response() == false) {
				if (!WebApi.getWebApi().getResponse_ErrorText().equals("")
						|| !WebApi.getWebApi().getResponse_ErrorCode()
								.equals("")) {
					showToast("Din note blev ikke gemt! " + "Fejlkode: "
							+ WebApi.getWebApi().getResponse_ErrorCode(),
							WebApi.getWebApi().getResponse_ErrorText()
									+ " Pr�v igen senere!");
				} else {
					showToast("Din note blev ikke gemt - Pr�v igen senere!");

					startActivity(navigateToNotes);
				}
			} else if (WebApi.getWebApi().isNewData_Response() == true) {
				if (WebApi.getWebApi().getResponse_Status().equals("OK")) {
					showToast("Din note er gemt", "");

					startActivity(navigateToNotes);
				}
			} else {
				showToast("Din note blev ikke gemt - Pr�v igen senere!");
				startActivity(navigateToNotes);
			}
			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);

			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

		protected void showToast(final String sHeader, final String sContext) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							sHeader + ": " + sContext, Toast.LENGTH_LONG)
							.show();
				}
			});
		}

		protected void showToast(final String sHeader) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), sHeader,
							Toast.LENGTH_LONG).show();
				}
			});
		}
	}
	
	

	private class ShowAsyncPauseDialog extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(NewNote.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
	}
}
