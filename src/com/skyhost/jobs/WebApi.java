package com.skyhost.jobs;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.google.gson.Gson;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

public class WebApi {

	private String url_Jobs_ChangeStatus = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&job=changestate&state=#status&id=#jobid&lng=da&out=xml";
	private String url_Jobs_GetData = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&job=list&out=xml&ver=2";
	private String url_Jobs_NewNote = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&job=note&id=#jobid&comment=#text&lng=da&out=xml";
	private String url_Jobs_GetMaterials = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&mat=list";
	private String url_Jobs_AddMaterialToJob = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&mat=add&jobid=#jobid&matid=#matid&n=#amount";
	private String url_Jobs_MaterialsOnJob = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&mat=get&jobid=#jobid";
	private String url_Jobs_DeleteMaterial = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&mat=delete&jobid=#jobid&id=#id";
	private String url_Jobs_UsersOnAccount = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&job=ressources";
	// Job
	private String url_Jobs_CreateNewJob = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&job=new&name=#name&orderno=#orderno&comment=#comment&begin#begin&end=#end&duration=#duration&address=#address&lat=#lat&lon=#lon&priority=#priority&users=#users";
	private String url_Jobs_CreateNewJobSimpleMethod = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&job=new&name=#name";
	// TODO find correct command for sending image
	// private String url_Jobs_CreateNewJobImage =
	// "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&job=changeduration&id=#jobid";
	private String url_Jobs_SetJobAddFileToJob = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&job=addfile&id=#jobid";
	private String url_Jobs_SetJobTime = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&job=changetime&id=#jobid&begin=#begin&end=#end";
	private String url_Jobs_SetJobDuration = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&job=changeduration&id=#jobid&duration=#duration";
	private String url_Jobs_SetJobTitle = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&job=changetitle&id=#jobid&name=#name";
	private String url_Jobs_SetJobOrderNumber = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&job=changeorderno&id=#jobid&orderno=#orderno";
	private String url_Jobs_SetJobAddress = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&job=changeaddress&id=#jobid&address=#address&lat=#lat&lon=#lon";
	private String url_Jobs_SetJobUsers = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&job=changeusers&id=#jobid&users=#ressourceid";
	private String url_Jobs_SetJobPriority = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&lng=da&out=xml&job=changepriority&id=#jobid&priority=#priority";

	private String url_Timer_ChangeLinkedStatus = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&job=linked&out=xml&id=#jobid";
	private String url_Timer_DeleteNote = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&out=xml&lng=da&time=delete&id=#recid";
	private String url_Timer_GetData = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&time=list&out=xml";
	private String url_Timer_GetJobsOnDate = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&job=list&out=xml&lng=da&time=jobs&date=#yyyyMMdd&ver=2";
	private String url_Timer_GetRoutesOnDay = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&time=routes&out=xml&date=#yyyyMMdd";
	private String url_Timer_NewNote = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&time=note&id=#recid&comment=#text&lng=da&out=xml";
	private String url_Timer_SaveTimeAndTripsOnJob = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&out=xml&lng=da&time=create&date=#yyyyMMdd&jobid=#jobid&routeid=#routeid&minutes=#minutes&acc=#account&orderno=#ordernumber";

	private String url_Timer_TimeAccounts = "http://api.#domain/gps/api.aspx?user=#username&pass=#password&out=xml&lng=da&time=accounts";

	private boolean newData_AutoUpdate = false;
	private boolean newDate_DeleteData_TimerNewNote = false;
	private boolean newData_Bitmap_From_Url = false;
	private boolean newData_Drawable_From_Url = false;
	private boolean newData_Jobs = false;
	private boolean newData_Jobs_CreateNewJob = false;
	private boolean newData_Jobs_SetJobTime = false;
	private boolean newData_Jobs_SetJobDuration = false;
	private boolean newData_Jobs_SetJobOrderNumber = false;
	private boolean newData_Jobs_SetJobAddFileToJob = false;
	private boolean newData_Jobs_SetJobAddress = false;
	private boolean newData_Jobs_SetJobTitle = false;
	private boolean newData_Jobs_SetJobUsers = false;
	private boolean newData_Jobs_SetJobPriority = false;
	private boolean newData_Jobs_GetMaterials = false;
	private boolean newData_Jobs_AddMaterialToJob = false;
	

	private boolean newData_Jobs_GetUsersOnAccount = false;

	private boolean newData_Jobs_MaterialsOnJob = false;
	private boolean newData_Jobs_DeleteMaterial = false;
	private boolean newData_LinkedStatus = false;
	private boolean newData_Note = false;
	private boolean newData_Response = false;
	private boolean newData_TimerNewNote = false;
	private boolean newData_SaveTimeAndTripsOnJobUrl_Timer = false;
	private boolean newData_Status = false;
	private boolean newData_Timer = false;
	private boolean newData_Timer_SelectJobsOnDateUrl = false;
	private boolean newData_Timer_TimeAccounts = false;
	private boolean newData_Timer_TripsOnDay = false;

	NodeList addressesList;

	private List jobs_jobId;
	private String[] jobs_title;
	private String[] jobs_orderNo;
	private String[] jobs_priority;
	private String[] jobs_linked;
	private String[] jobs_timeRegMinutes;
	private String[] jobs_location;
	private String[] jobs_createdBy;
	private String[] jobs_createdAt;
	private String[] jobs_status;
	private String[] jobs_icon;
	private String[] jobs_beginTime;
	private String[] jobs_endTime;
	private String[] jobs_duration;

	private Drawable[] jobs_drawables;

	private String[][] jobs_addresses_number;
	private String[][] jobs_addresses_street;
	private String[][] jobs_addresses_zip;
	private String[][] jobs_addresses_city;
	private String[][] jobs_addresses_lat;
	private String[][] jobs_addresses_lon;

	private String[][] jobs_employees_name;
	private String[][] jobs_employees_phone;
	private String[][] jobs_employees_email;

	private String[][] jobs_notes_names;
	private String[][] jobs_notes_texts;
	private String[][] jobs_notes_timestamps;

	private String[][] jobs_files_titles;
	private String[][] jobs_files_paths;

	// Material
	private String[] jobs_Material_MatID;
	private String[] jobs_Material_Name;
	private String[] jobs_Material_Description;
	private String[] jobs_Material_Reference;
	private String[] jobs_Material_Colli;
	private String[] jobs_Material_GroupNo;
	private String[] jobs_Material_GroupName;
	private String[] jobs_Material_SupplierItemNo;
	private String[] jobs_Material_EanNo;
	private String[] jobs_Material_ElNo;
	private String[] jobs_Material_EanBarcode;
	private String[] jobs_Material_InternalBarcode;
	private String[] jobs_Material_InternalItemNo;
	private String[] jobs_Material_InternalItemShortNo;

	// Materials On Job
	private String[] jobs_MaterialsOnJob_ID;
	private String[] jobs_MaterialsOnJob_MatID;
	private String[] jobs_MaterialsOnJob_Timestamp;
	private String[] jobs_MaterialsOnJob_InternalItemNo;
	private String[] jobs_MaterialsOnJob_ItemName;
	private String[] jobs_MaterialsOnJob_Pcs;
	private String[] jobs_MaterialsOnJob_User;

	// Users On Account
	private String[] usersOnAccount_Ids;
	private String[] usersOnAccount_Names;
	private String[] usersOnAccount_Phones;
	private String[] usersOnAccount_Emails;

	private String[] timer_WeeksNo;
	private String[][] timer_Days;
	private String[] timer_Dates;
	private String[][] timer_RegSums;
	private String[][][] timer_Jobs;
	private String[][][] timer_RecIds;
	private String[][][] timer_JobIDs;
	private String[][][] timer_OrderNos;
	private String[][][] timer_JobName;
	private String[][][] timer_AccountName;
	private String[][][] timer_Minutes;

	private String[][][] timer_Headers;
	private String[][][] timer_SubTitles;

	private String[] timer_JobIdsOnDay;
	private String[] timer_JobsOnDay;
	private String[] timer_OrdernumbersOnDay;

	private ArrayList<String> timer_TimeAccount_Number;
	private ArrayList<String> timer_TimeAccount_Name;

	private String[] timer_Route_Id;
	private String[] timer_Route_Header;
	private String[] timer_Route_Subtitle;
	private String[] timer_Route_OrderNo;
	private String[] timer_Route_StartTime;
	private String[] timer_Route_StopTime;
	private String[] timer_Route_DriveDuration;
	private String[] timer_Route_StopDuration;
	private String[] timer_Route_Zipcode;
	private String[] timer_Route_City;
	private String[] timer_Route_Street;
	private String[] timer_Route_Lat;
	private String[] timer_Route_Lon;

	// receiveResponseFromServer
	private String response_ErrorCode = "";
	private String response_ErrorText = "";
	private String response_Status = "";
	private String response_Command = "";

	private Date timestampJobs = new Date();
	private Date timestampMaterials = new Date();

	private Date timestampTimer = new Date();
	private Date timestampTimeAccounts = new Date();
	private long timestampTimerRoutesStart;
	private long timestampTimerRoutesElapsedTime;

	private long timestamp_Update_Jobs_Start_Initializing;
	private long timestamp_Update_Jobs_ElapsedTime_Initializing;
	private String response_ErrorHeader;

	private boolean newData_Response_DeleteTimerNewJob;
	private HttpURLConnection connection;

	// Singleton
	private static WebApi webApi;

	public static synchronized WebApi getWebApi() {
		if (webApi == null) {
			webApi = new WebApi();
		}
		return webApi;
	}

	public boolean update_Timer(SharedPreferences preferences) {
		timestampTimer = new Date();
		newData_Timer = false;

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		try {
			String UrlPath = url_Timer_GetData.replaceAll("#domain", Domain)
					.replaceAll("#username", Username)
					.replaceAll("#password", Password);
			// URL url = new URL(UrlPath);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			// Document doc = db.parse(new InputSource(url.openStream()));
			// doc.getDocumentElement().normalize();

			HttpGet httpGet = new HttpGet(UrlPath);
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httpGet);
			Document doc = db.parse(new InputSource(response.getEntity()
					.getContent()));

			doc.getDocumentElement().normalize();

			NodeList nodeList = doc.getElementsByTagName("ErrorCode");
			NodeList jobsList = null;
			NodeList filesList = null;
			Element nodeElement = (Element) nodeList.item(0);
			if (nodeElement == null) {
				// No Errorcode
				response_ErrorCode = "OK";
			} else {
				nodeList = nodeElement.getChildNodes();
				if (nodeList.getLength() > 0) {

					response_ErrorCode = ((Node) nodeList.item(0))
							.getNodeValue();
					if (response_ErrorCode.equals("3")) { // Forkert brugernavn
															// og
						// kodeord
						response_ErrorText = "Forkert brugernavn og/eller kodeord";
					} else {
						response_ErrorText = "Ukendt fejl";
					}
				}
				return false;
			}

			nodeList = doc.getElementsByTagName("Week");

			timer_WeeksNo = new String[nodeList.getLength()];
			timer_Days = new String[nodeList.getLength()][];
			timer_Dates = new String[nodeList.getLength()];
			timer_RegSums = new String[nodeList.getLength()][];
			timer_Jobs = new String[nodeList.getLength()][][];

			timer_RecIds = new String[nodeList.getLength()][][];
			timer_JobIDs = new String[nodeList.getLength()][][];
			timer_OrderNos = new String[nodeList.getLength()][][];
			timer_JobName = new String[nodeList.getLength()][][];
			timer_AccountName = new String[nodeList.getLength()][][];
			timer_Minutes = new String[nodeList.getLength()][][];
			timer_Headers = new String[nodeList.getLength()][][];
			timer_SubTitles = new String[nodeList.getLength()][][];

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				NodeList nList;
				// Element nodeElement = null;

				Element fstElmnt = (Element) node;

				nList = fstElmnt.getElementsByTagName("No");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					timer_WeeksNo[i] = ((Node) nList.item(0)).getNodeValue();
				} else {
					timer_WeeksNo[i] = "";
				}

				try {
					filesList = (NodeList) fstElmnt.getElementsByTagName("Day");
					String[] timer_Days_Temp = new String[filesList.getLength()];
					String[] timer_RegSums_Temp = new String[filesList
							.getLength()];
					String[] timer_Jobs_Temp = new String[filesList.getLength()];

					String[][] timer_RecId_Temp_j = new String[filesList
							.getLength()][];
					String[][] timer_JobId_Temp_j = new String[filesList
							.getLength()][];
					String[][] timer_OrderNo_Temp_j = new String[filesList
							.getLength()][];
					String[][] timer_JobName_Temp_j = new String[filesList
							.getLength()][];
					String[][] timer_AccountName_Temp_j = new String[filesList
							.getLength()][];
					String[][] timer_Minutes_Temp_j = new String[filesList
							.getLength()][];
					String[][] timer_Header_Temp_j = new String[filesList
							.getLength()][];
					String[][] timer_SubTitle_Temp_j = new String[filesList
							.getLength()][];
					for (int j = 0; j < filesList.getLength(); j++) {
						Node subnode = filesList.item(j);
						NodeList subnList;
						Element subnodeElement;

						Element subfstElmnt = (Element) subnode;

						subnList = subfstElmnt.getElementsByTagName("Date");
						subnodeElement = (Element) subnList.item(0);
						subnList = subnodeElement.getChildNodes();
						if (subnList.getLength() > 0) {
							timer_Days_Temp[j] = ((Node) subnList.item(0))
									.getNodeValue();
						} else {
							timer_Days_Temp[j] = "";
						}

						try {
							Node subSubnode = filesList.item(j);
							NodeList subSubnList;
							Element subSubnodeElement;

							Element subSubfstElmnt = (Element) subSubnode;

							subSubnList = subSubfstElmnt
									.getElementsByTagName("RegSum");
							subSubnodeElement = (Element) subSubnList.item(0);
							subSubnList = subSubnodeElement.getChildNodes();
							if (subSubnList.getLength() > 0) {
								timer_RegSums_Temp[j] = ((Node) subSubnList
										.item(0)).getNodeValue();
							} else {
								timer_RegSums_Temp[j] = "";
							}
						} catch (Exception e) {
						}
						timer_Days[i] = timer_Days_Temp;
						if (timer_Days[i] == null) {
							timer_Days[i][j] = "";
						}
						timer_RegSums[i] = timer_RegSums_Temp;
						if (timer_RegSums == null) {
							timer_RegSums[i][j] = "";
						}
						try {
							Node subSubnode = filesList.item(j);
							// NodeList subSubnList;
							Element subSubnodeElement;

							Element subSubfstElmnt = (Element) subSubnode;

							jobsList = subSubfstElmnt
									.getElementsByTagName("Job");
							int length1 = jobsList.getLength();
							String[] timer_RecId_Temp_k = new String[jobsList
									.getLength()];
							String[] timer_JobId_Temp_k = new String[jobsList
									.getLength()];
							String[] timer_OrderNo_Temp_k = new String[jobsList
									.getLength()];
							String[] timer_JobName_Temp_k = new String[jobsList
									.getLength()];
							String[] timer_AccountName_Temp_k = new String[jobsList
									.getLength()];
							String[] timer_Minutes_Temp_k = new String[jobsList
									.getLength()];
							String[] timer_Header_Temp_k = new String[jobsList
									.getLength()];
							String[] timer_SubTitle_Temp_k = new String[jobsList
									.getLength()];
							int length = jobsList.getLength();
							for (int k = 0; k < jobsList.getLength(); k++) {

								Node subSubSubnode = jobsList.item(k);
								NodeList subSubSubnList;
								Element subSubSubnodeElement;

								Element subSubSubfstElmnt = (Element) subSubSubnode;

								subSubSubnList = subSubSubfstElmnt
										.getElementsByTagName("RecID");
								subSubSubnodeElement = (Element) subSubSubnList
										.item(0);
								subSubSubnList = subSubSubnodeElement
										.getChildNodes();
								if (subSubSubnList.getLength() > 0) {
									timer_RecId_Temp_k[k] = ((Node) subSubSubnList
											.item(0)).getNodeValue();
								} else {
									timer_RecId_Temp_k[k] = "";
								}

								subSubSubnList = subSubSubfstElmnt
										.getElementsByTagName("JobID");
								subSubSubnodeElement = (Element) subSubSubnList
										.item(0);
								subSubSubnList = subSubSubnodeElement
										.getChildNodes();
								if (subSubSubnList.getLength() > 0) {
									timer_JobId_Temp_k[k] = ((Node) subSubSubnList
											.item(0)).getNodeValue();
								} else {
									timer_JobId_Temp_k[k] = "";
								}

								subSubSubnList = subSubSubfstElmnt
										.getElementsByTagName("OrderNo");
								subSubSubnodeElement = (Element) subSubSubnList
										.item(0);
								subSubSubnList = subSubSubnodeElement
										.getChildNodes();
								if (subSubSubnList.getLength() > 0) {
									timer_OrderNo_Temp_k[k] = ((Node) subSubSubnList
											.item(0)).getNodeValue();
								} else {
									timer_OrderNo_Temp_k[k] = "";
								}

								subSubSubnList = subSubSubfstElmnt
										.getElementsByTagName("JobName");
								subSubSubnodeElement = (Element) subSubSubnList
										.item(0);
								subSubSubnList = subSubSubnodeElement
										.getChildNodes();
								if (subSubSubnList.getLength() > 0) {
									timer_JobName_Temp_k[k] = ((Node) subSubSubnList
											.item(0)).getNodeValue();
								} else {
									timer_JobName_Temp_k[k] = "";
								}

								subSubSubnList = subSubSubfstElmnt
										.getElementsByTagName("AccountName");
								subSubSubnodeElement = (Element) subSubSubnList
										.item(0);
								subSubSubnList = subSubSubnodeElement
										.getChildNodes();
								if (subSubSubnList.getLength() > 0) {
									timer_AccountName_Temp_k[k] = ((Node) subSubSubnList
											.item(0)).getNodeValue();
								} else {
									timer_AccountName_Temp_k[k] = "";
								}

								subSubSubnList = subSubSubfstElmnt
										.getElementsByTagName("Minutes");
								subSubSubnodeElement = (Element) subSubSubnList
										.item(0);
								subSubSubnList = subSubSubnodeElement
										.getChildNodes();
								if (subSubSubnList.getLength() > 0) {
									timer_Minutes_Temp_k[k] = ((Node) subSubSubnList
											.item(0)).getNodeValue();
								} else {
									timer_Minutes_Temp_k[k] = "";
								}

								subSubSubnList = subSubSubfstElmnt
										.getElementsByTagName("Header");
								subSubSubnodeElement = (Element) subSubSubnList
										.item(0);
								subSubSubnList = subSubSubnodeElement
										.getChildNodes();
								if (subSubSubnList.getLength() > 0) {
									timer_Header_Temp_k[k] = ((Node) subSubSubnList
											.item(0)).getNodeValue();
								} else {
									timer_Header_Temp_k[k] = "";
								}

								subSubSubnList = subSubSubfstElmnt
										.getElementsByTagName("SubTitle");
								subSubSubnodeElement = (Element) subSubSubnList
										.item(0);
								subSubSubnList = subSubSubnodeElement
										.getChildNodes();
								if (subSubSubnList.getLength() > 0) {
									timer_SubTitle_Temp_k[k] = ((Node) subSubSubnList
											.item(0)).getNodeValue();
								} else {
									timer_SubTitle_Temp_k[k] = "";
								}

							}
							timer_RecId_Temp_j[j] = timer_RecId_Temp_k;
							timer_JobId_Temp_j[j] = timer_JobId_Temp_k;
							timer_OrderNo_Temp_j[j] = timer_OrderNo_Temp_k;
							timer_JobName_Temp_j[j] = timer_JobName_Temp_k;
							timer_AccountName_Temp_j[j] = timer_AccountName_Temp_k;
							timer_Minutes_Temp_j[j] = timer_Minutes_Temp_k;
							timer_Header_Temp_j[j] = timer_Header_Temp_k;
							timer_SubTitle_Temp_j[j] = timer_SubTitle_Temp_k;
							if (timer_RecId_Temp_j == null) {
								timer_RecId_Temp_j[j][0] = "";
								timer_JobId_Temp_j[j][0] = "";
								timer_OrderNo_Temp_j[j][0] = "";
								timer_JobName_Temp_j[j][0] = "";
								timer_AccountName_Temp_j[j][0] = "";
								timer_Minutes_Temp_j[j][0] = "";
								timer_Header_Temp_j[j][0] = "";
								timer_SubTitle_Temp_j[j][0] = "";
							}
						} catch (Exception e) {
						}
						timer_RecIds[i] = timer_RecId_Temp_j;
						timer_JobIDs[i] = timer_JobId_Temp_j;
						timer_OrderNos[i] = timer_OrderNo_Temp_j;
						timer_JobName[i] = timer_JobName_Temp_j;
						timer_AccountName[i] = timer_AccountName_Temp_j;
						timer_Minutes[i] = timer_Minutes_Temp_j;
						timer_Headers[i] = timer_Header_Temp_j;
						timer_SubTitles[i] = timer_SubTitle_Temp_j;

					}
				} catch (Exception e) {
				}

			}

		} catch (Exception e) {
			return false;
		}
		newData_Timer = true;
		return true;
	}

	public void saveNewNote(SharedPreferences preferences, String jobId,
			String text) {
		newData_Note = false;
		String[] params = new String[1];
		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Jobs_NewNote.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobId)
					.replaceAll("#text", URLEncoder.encode(text, "UTF-8"));

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Note = true;
	}

	public void saveTimerNewNote(SharedPreferences preferences, String recId,
			String text) {
		newData_TimerNewNote = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Timer_NewNote
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#recid", recId)
					.replaceAll("#text", URLEncoder.encode(text, "UTF-8"));

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_TimerNewNote = true;
	}

	public boolean update_Timer_JobsOnDay(SharedPreferences preferences,
			String date) {
		// timestampJobs = new Date();
		newData_Timer_SelectJobsOnDateUrl = false;

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		try {
			String UrlPath = url_Timer_GetJobsOnDate
					.replaceAll("#domain", Domain)
					.replaceAll("#username", Username)
					.replaceAll("#password", Password)
					.replaceAll("#yyyyMMdd", date);
			URL url = new URL(UrlPath);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			// Document doc = db.parse(new InputSource(url.openStream()));
			// doc.getDocumentElement().normalize();

			HttpGet httpGet = new HttpGet(UrlPath);
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httpGet);
			Document doc = db.parse(new InputSource(response.getEntity()
					.getContent()));

			doc.getDocumentElement().normalize();

			NodeList nodeList = doc.getElementsByTagName("ErrorCode");
			NodeList jobsList = null;
			NodeList filesList = null;
			Element nodeElement = (Element) nodeList.item(0);
			if (nodeElement == null) {
				// No Errorcode
				response_ErrorCode = "OK";
			} else {
				nodeList = nodeElement.getChildNodes();
				if (nodeList.getLength() > 0) {

					response_ErrorCode = ((Node) nodeList.item(0))
							.getNodeValue();
					if (response_ErrorCode.equals("3")) { // Forkert brugernavn
															// og
						// kodeord
						response_ErrorText = "Forkert brugernavn og/eller kodeord";
					} else {
						response_ErrorText = "Ukendt fejl";
					}
				}
				return false;
			}

			nodeList = doc.getElementsByTagName("Job");

			timer_JobIdsOnDay = new String[nodeList.getLength()];
			timer_JobsOnDay = new String[nodeList.getLength()];
			timer_OrdernumbersOnDay = new String[nodeList.getLength()];

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				NodeList nList;
				// Element nodeElement = null;

				Element fstElmnt = (Element) node;

				nList = fstElmnt.getElementsByTagName("JobID");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					timer_JobIdsOnDay[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					timer_JobIdsOnDay[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("Title");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					timer_JobsOnDay[i] = ((Node) nList.item(0)).getNodeValue();
				} else {
					timer_JobsOnDay[i] = "";
				}
				try {
					nList = fstElmnt.getElementsByTagName("OrderNo");
					nodeElement = (Element) nList.item(0);
					nList = nodeElement.getChildNodes();
					if (nList.getLength() > 0) {
						timer_OrdernumbersOnDay[i] = ((Node) nList.item(0))
								.getNodeValue();
					} else {
						timer_OrdernumbersOnDay[i] = "";
					}
				} catch (Exception ex) {
				}
			}
		} catch (Exception e) {
			return false;
		}
		newData_Timer_SelectJobsOnDateUrl = true;
		return true;
	}

	public boolean getTimer_TimeAccounts(SharedPreferences preferences) {
		timestampTimeAccounts = new Date();
		newData_Timer_TimeAccounts = false;

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		try {
			String UrlPath = url_Timer_TimeAccounts
					.replaceAll("#domain", Domain)
					.replaceAll("#username", Username)
					.replaceAll("#password", Password);
			URL url = new URL(UrlPath);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			// Document doc = db.parse(new InputSource(url.openStream()));
			// doc.getDocumentElement().normalize();

			HttpGet httpGet = new HttpGet(UrlPath);
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httpGet);
			Document doc = db.parse(new InputSource(response.getEntity()
					.getContent()));
			doc.getDocumentElement().normalize();

			NodeList nodeList = doc.getElementsByTagName("ErrorCode");
			NodeList jobsList = null;
			NodeList filesList = null;
			Element nodeElement = (Element) nodeList.item(0);
			if (nodeElement == null) {
				// No Errorcode
				response_ErrorCode = "OK";
			} else {
				nodeList = nodeElement.getChildNodes();
				if (nodeList.getLength() > 0) {

					response_ErrorCode = ((Node) nodeList.item(0))
							.getNodeValue();
					if (response_ErrorCode.equals("3")) { // Forkert brugernavn
															// og
						// kodeord
						response_ErrorText = "Forkert brugernavn og/eller kodeord";
					} else {
						response_ErrorText = "Ukendt fejl";
					}
				}
				return false;
			}

			nodeList = doc.getElementsByTagName("Account");

			timer_TimeAccount_Name = new ArrayList<String>();
			timer_TimeAccount_Number = new ArrayList<String>();

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				NodeList nList;
				// Element nodeElement = null;

				Element fstElmnt = (Element) node;

				nList = fstElmnt.getElementsByTagName("No");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					timer_TimeAccount_Number.add(i,
							((Node) nList.item(0)).getNodeValue());
				} else {
					timer_TimeAccount_Number.add(i, "");
				}

				nList = fstElmnt.getElementsByTagName("Name");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					timer_TimeAccount_Name.add(i,
							((Node) nList.item(0)).getNodeValue());
				} else {
					timer_TimeAccount_Name.add(i, "");
				}
			}
		} catch (Exception e) {
			return false;
		}
		newData_Timer_TimeAccounts = true;
		return true;
	}

	public void saveTimeAndTripsOnJob(SharedPreferences preferences,
			String orderNumber, String minutes, String timeAccount,
			String routeid, String jobId, String date) {
		newData_SaveTimeAndTripsOnJobUrl_Timer = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Timer_SaveTimeAndTripsOnJob
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobId)
					.replaceAll("#yyyyMMdd", date)
					.replaceAll("#ordernumber",
							URLEncoder.encode(orderNumber, "UTF-8"))
					.replaceAll("#minutes", minutes)
					.replaceAll("#account", timeAccount)
					.replaceAll("#routeid", routeid);

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_SaveTimeAndTripsOnJobUrl_Timer = true;
	}

	public boolean getTimer_RoutesOnDay(SharedPreferences preferences,
			String date) {
		newData_Timer_TripsOnDay = false;
		timestampTimerRoutesStart = System.currentTimeMillis();

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		try {
			String UrlPath = url_Timer_GetRoutesOnDay
					.replaceAll("#domain", Domain)
					.replaceAll("#username", Username)
					.replaceAll("#password", Password)
					.replaceAll("#yyyyMMdd", date);
			// URL url = new URL(UrlPath);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			HttpGet httpGet = new HttpGet(UrlPath);
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httpGet);
			Document doc = db.parse(new InputSource(response.getEntity()
					.getContent()));

			doc.getDocumentElement().normalize();

			NodeList nodeList = doc.getElementsByTagName("ErrorCode");
			NodeList jobsList = null;
			NodeList filesList = null;
			Element nodeElement = (Element) nodeList.item(0);
			if (nodeElement == null) {
				// No Errorcode
				response_ErrorCode = "OK";
			} else {
				nodeList = nodeElement.getChildNodes();
				if (nodeList.getLength() > 0) {

					response_ErrorCode = ((Node) nodeList.item(0))
							.getNodeValue();
					if (response_ErrorCode.equals("3")) { // Forkert brugernavn
															// og
						// kodeord
						response_ErrorText = "Forkert brugernavn og/eller kodeord";
					} else {
						response_ErrorText = "Ukendt fejl";
					}
				}
				return false;
			}

			nodeList = doc.getElementsByTagName("Route");

			timer_Route_Id = new String[nodeList.getLength()];
			timer_Route_Header = new String[nodeList.getLength()];
			timer_Route_Subtitle = new String[nodeList.getLength()];

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				NodeList nList;
				// Element nodeElement = null;

				Element fstElmnt = (Element) node;

				nList = fstElmnt.getElementsByTagName("ID");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					timer_Route_Id[i] = ((Node) nList.item(0)).getNodeValue();
				} else {
					timer_Route_Id[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("Header");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					timer_Route_Header[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					timer_Route_Header[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("Subtitle");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					timer_Route_Subtitle[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					timer_Route_Subtitle[i] = "";
				}
			}
		} catch (Exception e) {
			return false;
		}
		newData_Timer_TripsOnDay = true;
		timestampTimerRoutesElapsedTime = System.currentTimeMillis()
				- timestampTimerRoutesStart;
		return true;
	}

	public void deleteTimerNewNote(SharedPreferences preferences, String recId) {
		newDate_DeleteData_TimerNewNote = false;

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Timer_DeleteNote
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#recid", recId);

			receiveResponseFromServerDeleteTimerNewJob(urlPath);
		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newDate_DeleteData_TimerNewNote = true;
	}

	public void changeStatus(SharedPreferences preferences, String jobId,
			String status) {
		newData_Status = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Jobs_ChangeStatus
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobId)
					.replaceAll("#status", URLEncoder.encode(status, "UTF-8"));

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Status = true;
	}

	public void changeLinkedStatus(SharedPreferences preferences, String jobId) {
		newData_LinkedStatus = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");

		connection = null;

		try {
			String urlPath = url_Timer_ChangeLinkedStatus
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobId);

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_LinkedStatus = true;
	}

	public void receiveResponseFromServerDeleteTimerNewJob(String urlPath) {
		newData_Response_DeleteTimerNewJob = false;
		response_Command = "";
		response_Status = "";

		try {
			URL url = new URL(urlPath);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			HttpGet httpGet = new HttpGet(urlPath);
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httpGet);
			Document doc = db.parse(new InputSource(response.getEntity()
					.getContent()));
			doc.getDocumentElement().normalize();

			// Status
			NodeList nodeList = doc.getElementsByTagName("Command");
			Element nodeElement = (Element) nodeList.item(0);

			nodeElement = (Element) nodeList.item(0);
			if (nodeElement != null) {
				// response_Status = "OK";

				nodeList = nodeElement.getChildNodes();
				if (nodeList.getLength() > 0) {
					response_Command = ((Node) nodeList.item(0)).getNodeValue();
				} else {
					response_Command = "";
				}

				nodeList = doc.getElementsByTagName("Status");
				nodeElement = (Element) nodeList.item(0);
				nodeList = nodeElement.getChildNodes();
				if (nodeList.getLength() > 0) {
					response_Status = ((Node) nodeList.item(0)).getNodeValue();
				} else {
					response_Status = "";
				}
			} else {
				// ErrorCode
				nodeList = doc.getElementsByTagName("ErrorCode");
				nodeElement = (Element) nodeList.item(0);

				if (nodeElement == null) {
					// No Errorcode
					response_ErrorCode = "Ingen svar fra server, pr�v igen senere.";
				} else {
					nodeList = nodeElement.getChildNodes();
					if (nodeList.getLength() > 0) {
						response_ErrorCode = ((Node) nodeList.item(0))
								.getNodeValue();
					} else {
						response_ErrorCode = "";
					}
				}
				nodeList = doc.getElementsByTagName("ErrorText");

				nodeList = nodeElement.getChildNodes();
				if (nodeList.getLength() > 0) {
					response_ErrorText = ((Node) nodeList.item(0))
							.getNodeValue();
				} else {
					response_ErrorText = "";
				}
				return;
			}

		} catch (Exception e) {
			return;
		}
		newData_Response_DeleteTimerNewJob = true;
	}

	public boolean Update_Jobs(SharedPreferences preferences) {
		timestampJobs = new Date();
		newData_Jobs = false;
		response_ErrorCode = "";
		response_ErrorHeader = "";
		response_ErrorText = "";
		response_Status = "";

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		try {

			String UrlPath = url_Jobs_GetData.replaceAll("#domain", Domain)
					.replaceAll("#username", Username)
					.replaceAll("#password", Password);
			// URL url = new URL(UrlPath);

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			HttpGet httpGet = new HttpGet(UrlPath);
			HttpClient httpclient = new DefaultHttpClient();

			HttpResponse response = httpclient.execute(httpGet);

			Document doc = db.parse(new InputSource(response.getEntity()
					.getContent()));
			doc.getDocumentElement().normalize();

			NodeList nodeList = doc.getElementsByTagName("ErrorCode");
			Element nodeElement = (Element) nodeList.item(0);
			if (nodeList.getLength() > 0) {
				nodeList = doc.getElementsByTagName("ErrorText");

				if (nodeElement == null) {
					response_ErrorCode = "OK";
				} else {

					nodeList = doc.getElementsByTagName("ErrorHeader");
					nodeElement = (Element) nodeList.item(0);
					nodeList = nodeElement.getChildNodes();
					if (nodeList.getLength() > 0) {
						response_ErrorHeader = ((Node) nodeList.item(0))
								.getNodeValue();
					} else {
						response_ErrorHeader = "Ukendt fejl";
					}

					nodeList = doc.getElementsByTagName("ErrorCode");
					nodeElement = (Element) nodeList.item(0);
					nodeList = nodeElement.getChildNodes();
					if (nodeList.getLength() > 0) {
						response_ErrorCode = ((Node) nodeList.item(0))
								.getNodeValue();
					} else {
						response_ErrorCode = "Ukendt fejl";
					}

					nodeList = doc.getElementsByTagName("ErrorText");
					nodeElement = (Element) nodeList.item(0);
					nodeList = nodeElement.getChildNodes();
					if (nodeList.getLength() > 0) {
						response_ErrorText = ((Node) nodeList.item(0))
								.getNodeValue();
					} else {
						response_ErrorText = "Ukendt fejl";
					}
					return false;
				}
			} else {
				nodeList = doc.getElementsByTagName("Job");
				timestamp_Update_Jobs_Start_Initializing = System
						.currentTimeMillis();

				jobs_jobId = new ArrayList<String>();
				jobs_title = new String[nodeList.getLength()];
				jobs_orderNo = new String[nodeList.getLength()];
				jobs_priority = new String[nodeList.getLength()];
				jobs_linked = new String[nodeList.getLength()];
				jobs_timeRegMinutes = new String[nodeList.getLength()];
				jobs_location = new String[nodeList.getLength()];
				jobs_createdBy = new String[nodeList.getLength()];
				jobs_createdAt = new String[nodeList.getLength()];
				jobs_status = new String[nodeList.getLength()];
				jobs_icon = new String[nodeList.getLength()];
				jobs_beginTime = new String[nodeList.getLength()];
				jobs_endTime = new String[nodeList.getLength()];
				jobs_duration = new String[nodeList.getLength()];

				jobs_addresses_number = new String[nodeList.getLength()][];
				jobs_addresses_street = new String[nodeList.getLength()][];

				jobs_addresses_city = new String[nodeList.getLength()][];
				jobs_addresses_zip = new String[nodeList.getLength()][];
				jobs_addresses_number = new String[nodeList.getLength()][];
				jobs_addresses_lat = new String[nodeList.getLength()][];
				jobs_addresses_lon = new String[nodeList.getLength()][];

				jobs_employees_name = new String[nodeList.getLength()][];
				jobs_employees_phone = new String[nodeList.getLength()][];
				jobs_employees_email = new String[nodeList.getLength()][];

				jobs_notes_names = new String[nodeList.getLength()][];
				jobs_notes_texts = new String[nodeList.getLength()][];
				jobs_notes_timestamps = new String[nodeList.getLength()][];

				jobs_files_titles = new String[nodeList.getLength()][];

				jobs_files_paths = new String[nodeList.getLength()][];
				timestamp_Update_Jobs_Start_Initializing = System
						.currentTimeMillis()
						- timestamp_Update_Jobs_Start_Initializing;
				for (int i = 0; i < nodeList.getLength(); i++) {
					Node node = nodeList.item(i);
					NodeList nList;

					Element fstElmnt = (Element) node;

					nList = fstElmnt.getElementsByTagName("JobID");
					nodeElement = (Element) nList.item(0);
					nList = nodeElement.getChildNodes();
					if (nList.getLength() > 0) {
						jobs_jobId
								.add(i, ((Node) nList.item(0)).getNodeValue());
					} else {
						jobs_jobId.add(i, "");
					}

					nList = fstElmnt.getElementsByTagName("Title");
					nodeElement = (Element) nList.item(0);
					nList = nodeElement.getChildNodes();
					if (nList.getLength() > 0) {
						jobs_title[i] = ((Node) nList.item(0)).getNodeValue();
					} else {
						jobs_title[i] = "";
					}
					try {
						nList = fstElmnt.getElementsByTagName("OrderNo");
						nodeElement = (Element) nList.item(0);
						nList = nodeElement.getChildNodes();
						if (nList.getLength() > 0) {
							jobs_orderNo[i] = ((Node) nList.item(0))
									.getNodeValue();
						} else {
							jobs_orderNo[i] = "";
						}

						nList = fstElmnt.getElementsByTagName("Priority");
						nodeElement = (Element) nList.item(0);
						nList = nodeElement.getChildNodes();
						if (nList.getLength() > 0) {
							jobs_priority[i] = ((Node) nList.item(0))
									.getNodeValue();
						} else {
							jobs_priority[i] = "";
						}
					} catch (Exception e) {
						jobs_priority[i] = "";
					}
					nList = fstElmnt.getElementsByTagName("Linked");
					nodeElement = (Element) nList.item(0);
					nList = nodeElement.getChildNodes();
					if (nList.getLength() > 0) {
						jobs_linked[i] = ((Node) nList.item(0)).getNodeValue();
					} else {
						jobs_linked[i] = "";
					}
					try {
						nList = fstElmnt.getElementsByTagName("TimeRegMinutes");
						nodeElement = (Element) nList.item(0);
						nList = nodeElement.getChildNodes();
						if (nList.getLength() > 0) {
							jobs_timeRegMinutes[i] = ((Node) nList.item(0))
									.getNodeValue();
						} else {
							jobs_timeRegMinutes[i] = "";
						}
					} catch (Exception e) {
					}
					// ---------- Addresses ----------

					try {
						addressesList = (NodeList) fstElmnt
								.getElementsByTagName("Address");

						String[] jobs_Addresses_Numbers_Temp = new String[addressesList
								.getLength()];
						String[] jobs_Addresses_Streets_Temp = new String[addressesList
								.getLength()];
						String[] jobs_Addresses_Zips_Temp = new String[addressesList
								.getLength()];
						String[] jobs_Addresses_Cities_Temp = new String[addressesList
								.getLength()];
						String[] jobs_Addresses_Latitudes_Temp = new String[addressesList
								.getLength()];
						String[] jobs_Addresses_Longitudes_Temp = new String[addressesList
								.getLength()];
						addressesList.getLength();
						for (int j = 0; j < addressesList.getLength(); j++) {
							Node subnode = addressesList.item(j);
							NodeList subnList;
							Element subnodeElement;

							Element subfstElmnt = (Element) subnode;

							subnList = subfstElmnt.getElementsByTagName("No");
							subnodeElement = (Element) subnList.item(0);
							subnList = subnodeElement.getChildNodes();
							if (subnList.getLength() > 0) {
								jobs_Addresses_Numbers_Temp[j] = ((Node) subnList
										.item(0)).getNodeValue();
							} else {
								jobs_Addresses_Numbers_Temp[j] = "";
							}

							subnList = subfstElmnt
									.getElementsByTagName("Street");
							subnodeElement = (Element) subnList.item(0);
							subnList = subnodeElement.getChildNodes();
							if (subnList.getLength() > 0) {
								jobs_Addresses_Streets_Temp[j] = ((Node) subnList
										.item(0)).getNodeValue();
							} else {
								jobs_Addresses_Streets_Temp[j] = "";
							}

							subnList = subfstElmnt.getElementsByTagName("Zip");
							subnodeElement = (Element) subnList.item(0);
							subnList = subnodeElement.getChildNodes();
							if (subnList.getLength() > 0) {
								jobs_Addresses_Zips_Temp[j] = ((Node) subnList
										.item(0)).getNodeValue();
							} else {
								jobs_Addresses_Zips_Temp[j] = "";
							}

							subnList = subfstElmnt.getElementsByTagName("City");
							subnodeElement = (Element) subnList.item(0);
							subnList = subnodeElement.getChildNodes();
							if (subnList.getLength() > 0) {
								jobs_Addresses_Cities_Temp[j] = ((Node) subnList
										.item(0)).getNodeValue();
							} else {
								jobs_Addresses_Cities_Temp[j] = "";
							}

							subnList = subfstElmnt.getElementsByTagName("Lat");
							subnodeElement = (Element) subnList.item(0);
							subnList = subnodeElement.getChildNodes();
							if (subnList.getLength() > 0) {
								jobs_Addresses_Latitudes_Temp[j] = ((Node) subnList
										.item(0)).getNodeValue().replaceAll(
										",", ".");
							} else {
								jobs_Addresses_Latitudes_Temp[j] = "";
							}

							subnList = subfstElmnt.getElementsByTagName("Lon");
							subnodeElement = (Element) subnList.item(0);
							subnList = subnodeElement.getChildNodes();
							if (subnList.getLength() > 0) {
								jobs_Addresses_Longitudes_Temp[j] = ((Node) subnList
										.item(0)).getNodeValue().replaceAll(
										",", ".");
							} else {
								jobs_Addresses_Longitudes_Temp[j] = "";
							}

							jobs_addresses_number[i] = jobs_Addresses_Numbers_Temp;
							jobs_addresses_street[i] = jobs_Addresses_Streets_Temp;
							jobs_addresses_zip[i] = jobs_Addresses_Zips_Temp;
							jobs_addresses_city[i] = jobs_Addresses_Cities_Temp;
							jobs_addresses_lat[i] = jobs_Addresses_Latitudes_Temp;
							jobs_addresses_lon[i] = jobs_Addresses_Longitudes_Temp;
							if (jobs_addresses_number[i] == null) {
								jobs_addresses_number[i][j] = "";
								jobs_addresses_street[i][j] = "";
								jobs_addresses_zip[i][j] = "";
								jobs_addresses_city[i][j] = "";
								jobs_addresses_lat[i][j] = "";
								jobs_addresses_lon[i][j] = "";
							}
						}
					} catch (Exception e) {
					}
					try {
						nList = fstElmnt.getElementsByTagName("CreatedBy");
						nodeElement = (Element) nList.item(0);
						nList = nodeElement.getChildNodes();
						if (nList.getLength() > 0) {
							String city = ((Node) nList.item(0)).getNodeValue();
							if (city.length() > 1) {
								jobs_createdBy[i] = ((Node) nList.item(0))
										.getNodeValue();
							} else {
								jobs_createdBy[i] = "";
							}
						}

						nList = fstElmnt.getElementsByTagName("CreatedAt");
						nodeElement = (Element) nList.item(0);
						nList = nodeElement.getChildNodes();
						if (nList.getLength() > 0)
							jobs_createdAt[i] = ((Node) nList.item(0))
									.getNodeValue();
						else
							jobs_createdAt[i] = "";
						if (jobs_createdAt[i] != "") {
							SimpleDateFormat format = new SimpleDateFormat(
									"dd-MM-yyyy HH:mm:ss");
							try {
								Calendar c = Calendar.getInstance();
								double z = (double) c.getTimeZone().getOffset(
										c.getTimeInMillis()) / 3600000.0;

								Date date = format.parse(jobs_createdAt[i]);
								c.setTime(date);
								c.add(c.HOUR, (int) z);
								date = c.getTime();
								format = new SimpleDateFormat(
										"dd-MM-yyyy HH:mm");
								jobs_createdAt[i] = format.format(date);
							} catch (Exception e) {
							}
						}

						nList = fstElmnt.getElementsByTagName("Status");
						nodeElement = (Element) nList.item(0);
						nList = nodeElement.getChildNodes();
						if (nList.getLength() > 0) {
							jobs_status[i] = ((Node) nList.item(0))
									.getNodeValue();
						} else {
							jobs_status[i] = "";
						}

						nList = fstElmnt.getElementsByTagName("Icon");
						nodeElement = (Element) nList.item(0);
						nList = nodeElement.getChildNodes();
						if (nList.getLength() > 0) {
							jobs_icon[i] = ((Node) nList.item(0))
									.getNodeValue();
						} else {
							jobs_icon[i] = "";
						}

						// ---------- Files ----------

						try {
							NodeList filesList = (NodeList) fstElmnt
									.getElementsByTagName("File");

							String[] jobs_Files_Titles_Temp = new String[filesList
									.getLength()];
							String[] jobs_Files_Paths_Temp = new String[filesList
									.getLength()];

							filesList.getLength();
							for (int j = 0; j < filesList.getLength(); j++) {
								Node subnode = filesList.item(j);
								NodeList subnList;
								Element subnodeElement;

								Element subfstElmnt = (Element) subnode;

								subnList = subfstElmnt
										.getElementsByTagName("Title");
								subnodeElement = (Element) subnList.item(0);
								subnList = subnodeElement.getChildNodes();
								if (subnList.getLength() > 0) {
									jobs_Files_Titles_Temp[j] = ((Node) subnList
											.item(0)).getNodeValue();
								} else {
									jobs_Files_Titles_Temp[j] = "";
								}

								subnList = subfstElmnt
										.getElementsByTagName("Path");
								subnodeElement = (Element) subnList.item(0);
								subnList = subnodeElement.getChildNodes();
								if (subnList.getLength() > 0) {
									jobs_Files_Paths_Temp[j] = ((Node) subnList
											.item(0)).getNodeValue();
								} else {
									jobs_Files_Paths_Temp[j] = "";
								}

								jobs_files_titles[i] = jobs_Files_Titles_Temp;
								jobs_files_paths[i] = jobs_Files_Paths_Temp;
								if (jobs_files_paths[i] == null) {
									jobs_files_paths[i][j] = "";
									jobs_files_titles[i][j] = "";
								}
							}
						} catch (Exception e) {
						}

						// --------- Assigned To ---------

						NodeList employeeList = (NodeList) fstElmnt
								.getElementsByTagName("Employee");

						String[] jobs_employees_name_temp = new String[employeeList
								.getLength()];
						String[] jobs_employees_phone_temp = new String[employeeList
								.getLength()];
						String[] jobs_employees_email_temp = new String[employeeList
								.getLength()];

						employeeList.getLength();
						for (int j = 0; j < employeeList.getLength(); j++) {
							Node subnode = employeeList.item(j);
							NodeList subnList;
							Element subnodeElement;

							Element subfstElmnt = (Element) subnode;

							subnList = subfstElmnt.getElementsByTagName("Name");
							subnodeElement = (Element) subnList.item(0);
							subnList = subnodeElement.getChildNodes();
							if (subnList.getLength() > 0) {
								jobs_employees_name_temp[j] = ((Node) subnList
										.item(0)).getNodeValue();
							} else {
								jobs_employees_name_temp[j] = "";
							}

							subnList = subfstElmnt
									.getElementsByTagName("Phone");
							subnodeElement = (Element) subnList.item(0);
							subnList = subnodeElement.getChildNodes();
							if (subnList.getLength() > 0) {
								jobs_employees_phone_temp[j] = ((Node) subnList
										.item(0)).getNodeValue();
							} else {
								jobs_employees_phone_temp[j] = "";
							}

							subnList = subfstElmnt
									.getElementsByTagName("Email");
							subnodeElement = (Element) subnList.item(0);
							subnList = subnodeElement.getChildNodes();
							if (subnList.getLength() > 0) {
								jobs_employees_email_temp[j] = ((Node) subnList
										.item(0)).getNodeValue();
							} else {
								jobs_employees_email_temp[j] = "";
							}

							jobs_employees_name[i] = jobs_employees_name_temp;
							jobs_employees_phone[i] = jobs_employees_phone_temp;
							jobs_employees_email[i] = jobs_employees_email_temp;
							if (jobs_employees_name[i] == null) {
								jobs_employees_name[i][j] = "";
								jobs_employees_phone[i][j] = "";
								jobs_employees_email[i][j] = "";
							}

						}

						nList = fstElmnt.getElementsByTagName("BeginTime");
						nodeElement = (Element) nList.item(0);
						nList = nodeElement.getChildNodes();
						if (nList.getLength() > 0)
							jobs_beginTime[i] = ((Node) nList.item(0))
									.getNodeValue();
						else
							jobs_beginTime[i] = "";
						if (jobs_beginTime[i] != "") {
							SimpleDateFormat format = new SimpleDateFormat(
									"dd-MM-yyyy HH:mm:ss");
							try {
								Calendar c = Calendar.getInstance();
								double z = (double) c.getTimeZone().getOffset(
										c.getTimeInMillis()) / 3600000.0;

								Date date = format.parse(jobs_beginTime[i]);
								c.setTime(date);
								c.add(c.HOUR, (int) z);
								date = c.getTime();
								format = new SimpleDateFormat(
										"dd-MM-yyyy HH:mm");
								jobs_beginTime[i] = format.format(date);
							} catch (Exception e) {
							}
						}

						nList = fstElmnt.getElementsByTagName("EndTime");
						nodeElement = (Element) nList.item(0);
						nList = nodeElement.getChildNodes();
						if (nList.getLength() > 0)
							jobs_endTime[i] = ((Node) nList.item(0))
									.getNodeValue();
						else
							jobs_endTime[i] = "";
						if (jobs_endTime[i] != "") {
							SimpleDateFormat format = new SimpleDateFormat(
									"dd-MM-yyyy HH:mm:ss");
							try {
								Calendar c = Calendar.getInstance();
								double z = (double) c.getTimeZone().getOffset(
										c.getTimeInMillis()) / 3600000.0;

								Date date = format.parse(jobs_endTime[i]);
								c.setTime(date);
								c.add(c.HOUR, (int) z);
								date = c.getTime();
								format = new SimpleDateFormat(
										"dd-MM-yyyy HH:mm");
								jobs_endTime[i] = format.format(date);
							} catch (Exception e) {
							}
						}

						nList = fstElmnt.getElementsByTagName("Duration");
						nodeElement = (Element) nList.item(0);
						nList = nodeElement.getChildNodes();
						if (nList.getLength() > 0) {
							jobs_duration[i] = ((Node) nList.item(0))
									.getNodeValue();
						} else {
							jobs_duration[i] = "";
						}

						// --------- >Notes ---------

						try {
							NodeList notesList = (NodeList) fstElmnt
									.getElementsByTagName("Note");

							String[] jobs_Notes_Names_Temp = new String[notesList
									.getLength()];
							String[] jobs_Notes_Texts_Temp = new String[notesList
									.getLength()];
							String[] jobs_Notes_Timestamps_Temp = new String[notesList
									.getLength()];

							notesList.getLength();
							for (int j = 0; j < notesList.getLength(); j++) {
								Node subnode = notesList.item(j);
								NodeList subnList;
								Element subnodeElement;

								Element subfstElmnt = (Element) subnode;

								subnList = subfstElmnt
										.getElementsByTagName("Name");
								subnodeElement = (Element) subnList.item(0);
								subnList = subnodeElement.getChildNodes();
								if (subnList.getLength() > 0) {
									jobs_Notes_Names_Temp[j] = ((Node) subnList
											.item(0)).getNodeValue();
								} else {
									jobs_Notes_Names_Temp[j] = "";
								}

								subnList = subfstElmnt
										.getElementsByTagName("Text");
								subnodeElement = (Element) subnList.item(0);
								subnList = subnodeElement.getChildNodes();
								if (subnList.getLength() > 0) {
									jobs_Notes_Texts_Temp[j] = ((Node) subnList
											.item(0)).getNodeValue();
								} else {
									jobs_Notes_Texts_Temp[j] = "";
								}

								subnList = subfstElmnt
										.getElementsByTagName("Timestamp");
								subnodeElement = (Element) subnList.item(0);
								subnList = subnodeElement.getChildNodes();
								if (subnList.getLength() > 0)
									jobs_Notes_Timestamps_Temp[j] = ((Node) subnList
											.item(0)).getNodeValue();
								else
									jobs_Notes_Timestamps_Temp[j] = "";
								if (jobs_Notes_Timestamps_Temp[j] != "") {
									SimpleDateFormat format = new SimpleDateFormat(
											"dd-MM-yyyy HH:mm:ss");
									try {
										Calendar c = Calendar.getInstance();
										double z = (double) c.getTimeZone()
												.getOffset(c.getTimeInMillis()) / 3600000.0;

										Date date = format
												.parse(jobs_Notes_Timestamps_Temp[j]);
										c.setTime(date);
										c.add(c.HOUR, (int) z);
										date = c.getTime();
										format = new SimpleDateFormat(
												"dd-MM-yyyy HH:mm");
										jobs_Notes_Timestamps_Temp[j] = format
												.format(date);
									} catch (Exception e) {
									}
								}
							}
							jobs_notes_names[i] = jobs_Notes_Names_Temp;

							// jobs_notes_names.add(jobs_Notes_Names_Temp);
							jobs_notes_texts[i] = jobs_Notes_Texts_Temp;
							jobs_notes_timestamps[i] = jobs_Notes_Timestamps_Temp;
							// if (jobs_notes_names.get(i) == null) {
							// jobs_notes_names[i][j] = "";
							// jobs_notes_texts[i][j] = "";
							// jobs_notes_timestamps[i][j] = "";
							//
							// }
						} catch (Exception e) {
							int lk = 0;
						}
					} catch (Exception e) {

						jobs_orderNo[i] = "";
						jobs_priority[i] = "";

						jobs_timeRegMinutes[i] = "";
						jobs_location[i] = "";
						jobs_createdBy[i] = "";
						jobs_createdAt[i] = "";
						jobs_status[i] = "";
						jobs_icon[i] = "";
						jobs_beginTime[i] = "";
						jobs_endTime[i] = null;
						jobs_duration[i] = "";

						String[] dummyArray = new String[1];
						dummyArray[0] = "";
						jobs_addresses_number[i] = dummyArray;
						jobs_addresses_street[i] = dummyArray;

						jobs_addresses_city[i] = dummyArray;
						jobs_addresses_zip[i] = dummyArray;
						jobs_addresses_number[i] = dummyArray;
						jobs_addresses_lat[i] = dummyArray;
						jobs_addresses_lon[i] = dummyArray;

						jobs_employees_name[i] = dummyArray;
						jobs_employees_phone[i] = dummyArray;
						jobs_employees_email[i] = dummyArray;

						String[] temp = new String[1];
						jobs_notes_names[i] = temp;
						jobs_notes_texts[i] = temp;
						jobs_notes_timestamps[i] = temp;

						jobs_files_titles[i] = null;
						jobs_files_paths[i] = null;
					}
				}
			}
		} catch (Exception e) {

			return false;
		}
		newData_Jobs = true;
		return true;
	}

	public boolean getJobs_Materials(SharedPreferences preferences) {
		timestampTimeAccounts = new Date();
		newData_Jobs_GetMaterials = false;

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		try {
			String UrlPath = url_Jobs_GetMaterials
					.replaceAll("#domain", Domain)
					.replaceAll("#username", Username)
					.replaceAll("#password", Password);
			URL url = new URL(UrlPath);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			HttpGet httpGet = new HttpGet(UrlPath);
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httpGet);
			Document doc = db.parse(new InputSource(response.getEntity()
					.getContent()));
			doc.getDocumentElement().normalize();

			NodeList nodeList = doc.getElementsByTagName("ErrorCode");

			Element nodeElement = (Element) nodeList.item(0);
			if (nodeElement == null) {
				// No Errorcode
				response_ErrorCode = "OK";
			} else {
				nodeList = nodeElement.getChildNodes();
				if (nodeList.getLength() > 0) {

					response_ErrorCode = ((Node) nodeList.item(0))
							.getNodeValue();
					if (response_ErrorCode.equals("3")) { // Forkert brugernavn
															// og
						// kodeord
						response_ErrorText = "Forkert brugernavn og/eller kodeord";
					} else {
						response_ErrorText = "Ukendt fejl";
					}
				}
				return false;
			}

			nodeList = doc.getElementsByTagName("Material");

			jobs_Material_MatID = new String[nodeList.getLength()];
			jobs_Material_Name = new String[nodeList.getLength()];
			jobs_Material_Description = new String[nodeList.getLength()];
			jobs_Material_Reference = new String[nodeList.getLength()];
			jobs_Material_Colli = new String[nodeList.getLength()];
			jobs_Material_GroupNo = new String[nodeList.getLength()];
			jobs_Material_GroupName = new String[nodeList.getLength()];
			jobs_Material_SupplierItemNo = new String[nodeList.getLength()];
			jobs_Material_EanNo = new String[nodeList.getLength()];
			jobs_Material_ElNo = new String[nodeList.getLength()];
			jobs_Material_EanBarcode = new String[nodeList.getLength()];
			jobs_Material_InternalBarcode = new String[nodeList.getLength()];
			jobs_Material_InternalItemNo = new String[nodeList.getLength()];
			jobs_Material_InternalItemShortNo = new String[nodeList.getLength()];

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				NodeList nList;

				Element fstElmnt = (Element) node;

				nList = fstElmnt.getElementsByTagName("MatID");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_Material_MatID[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_Material_MatID[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("Name");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_Material_Name[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_Material_Name[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("Description");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_Material_Description[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_Material_Description[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("Reference");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_Material_Reference[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_Material_Reference[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("Colli");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_Material_Colli[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_Material_Colli[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("GroupNo");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_Material_GroupNo[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_Material_GroupNo[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("GroupName");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_Material_GroupName[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_Material_GroupName[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("SupplierItemNo");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_Material_SupplierItemNo[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_Material_SupplierItemNo[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("EanNo");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_Material_EanNo[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_Material_EanNo[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("ElNo");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_Material_ElNo[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_Material_ElNo[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("EanBarcode");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_Material_EanBarcode[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_Material_EanBarcode[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("InternalBarcode");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_Material_InternalBarcode[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_Material_InternalBarcode[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("InternalItemNo");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_Material_InternalItemNo[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_Material_InternalItemNo[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("InternalItemShortNo");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_Material_InternalItemShortNo[i] = ((Node) nList
							.item(0)).getNodeValue();
				} else {
					jobs_Material_InternalItemShortNo[i] = "";
				}
			}
		} catch (Exception e) {
			return false;
		}
		newData_Jobs_GetMaterials = true;
		return true;
	}

	public void addMaterialToJob(SharedPreferences preferences, String jobid,
			String matid, String amount) {
		newData_Jobs_AddMaterialToJob = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Jobs_AddMaterialToJob
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobid).replaceAll("#matid", matid)
					.replaceAll("#amount", amount);

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Jobs_AddMaterialToJob = true;
	}

	public boolean getJobs_MaterialsOnJob(SharedPreferences preferences,
			String jobid) {
		// timestampTimeAccounts = new Date();
		newData_Jobs_MaterialsOnJob = false;

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		try {
			String UrlPath = url_Jobs_MaterialsOnJob
					.replaceAll("#domain", Domain)
					.replaceAll("#username", Username)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobid);
			URL url = new URL(UrlPath);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			HttpGet httpGet = new HttpGet(UrlPath);
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httpGet);
			Document doc = db.parse(new InputSource(response.getEntity()
					.getContent()));
			doc.getDocumentElement().normalize();

			NodeList nodeList = doc.getElementsByTagName("ErrorCode");

			Element nodeElement = (Element) nodeList.item(0);
			if (nodeElement == null) {
				// No Errorcode
				response_ErrorCode = "OK";
			} else {
				nodeList = nodeElement.getChildNodes();
				if (nodeList.getLength() > 0) {

					response_ErrorCode = ((Node) nodeList.item(0))
							.getNodeValue();
					if (response_ErrorCode.equals("3")) { // Forkert brugernavn
															// og
						// kodeord
						response_ErrorText = "Forkert brugernavn og/eller kodeord";
					} else {
						response_ErrorText = "Ukendt fejl";
					}
				}
				return false;
			}

			nodeList = doc.getElementsByTagName("Material");

			jobs_MaterialsOnJob_ID = new String[nodeList.getLength()];
			jobs_MaterialsOnJob_MatID = new String[nodeList.getLength()];
			jobs_MaterialsOnJob_Timestamp = new String[nodeList.getLength()];
			jobs_MaterialsOnJob_InternalItemNo = new String[nodeList
					.getLength()];
			jobs_MaterialsOnJob_ItemName = new String[nodeList.getLength()];
			jobs_MaterialsOnJob_Pcs = new String[nodeList.getLength()];
			jobs_MaterialsOnJob_User = new String[nodeList.getLength()];

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				NodeList nList;

				Element fstElmnt = (Element) node;

				nList = fstElmnt.getElementsByTagName("ID");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_MaterialsOnJob_ID[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_MaterialsOnJob_ID[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("MatID");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_MaterialsOnJob_MatID[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_MaterialsOnJob_MatID[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("Timestamp");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0)
					jobs_MaterialsOnJob_Timestamp[i] = ((Node) nList.item(0))
							.getNodeValue();
				else
					jobs_MaterialsOnJob_Timestamp[i] = "";
				if (jobs_MaterialsOnJob_Timestamp[i] != "") {
					SimpleDateFormat format = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss");
					try {
						Calendar c = Calendar.getInstance();
						double z = (double) c.getTimeZone().getOffset(
								c.getTimeInMillis()) / 3600000.0;

						Date date = format.parse(jobs_beginTime[i]);
						c.setTime(date);
						c.add(c.HOUR, (int) z);
						date = c.getTime();
						format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
						jobs_MaterialsOnJob_Timestamp[i] = format.format(date);
					} catch (Exception e) {
					}
				}

				nList = fstElmnt.getElementsByTagName("InternalItemNo");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_MaterialsOnJob_InternalItemNo[i] = ((Node) nList
							.item(0)).getNodeValue();
				} else {
					jobs_MaterialsOnJob_InternalItemNo[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("ItemName");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_MaterialsOnJob_ItemName[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_MaterialsOnJob_ItemName[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("Pcs");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_MaterialsOnJob_Pcs[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_MaterialsOnJob_Pcs[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("User");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					jobs_MaterialsOnJob_User[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					jobs_MaterialsOnJob_User[i] = "";
				}
			}
		} catch (Exception e) {
			return false;
		}
		newData_Jobs_MaterialsOnJob = true;
		return true;
	}

	public void deleteMaterial(SharedPreferences preferences, String jobId,
			String id) {
		newData_Jobs_DeleteMaterial = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Jobs_DeleteMaterial
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobId).replaceAll("#id", id);

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Jobs_DeleteMaterial = true;
	}

	private class AsyncConnectionService extends AsyncTask<String, Void, Void> {

		protected void onPreExecute() {
			super.onPreExecute();
		}

		String xmlList = "";

		public void receiveResponseFromServer(String urlPath) {
			newData_Response = false;
			response_Status = "";
			response_ErrorCode = "";
			response_ErrorText = "";
			response_ErrorHeader = "";

			try {

				URL url = new URL(urlPath);

				DocumentBuilderFactory dbf = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder db = dbf.newDocumentBuilder();

				HttpGet httpGet = new HttpGet(urlPath);
				HttpClient httpclient = new DefaultHttpClient();
				HttpResponse response = httpclient.execute(httpGet);
				Document doc = db.parse(new InputSource(response.getEntity()
						.getContent()));

				doc.getDocumentElement().normalize();

				// Status
				NodeList nodeList = doc.getElementsByTagName("Status");
				Element nodeElement = (Element) nodeList.item(0);

				nodeElement = (Element) nodeList.item(0);
				if (nodeElement != null) {
					response_Status = "OK";
				} else {

					// ErrorHeader
					nodeList = doc.getElementsByTagName("ErrorHeader");
					nodeElement = (Element) nodeList.item(0);
					nodeList = nodeElement.getChildNodes();
					if (nodeList.getLength() > 0) {
						response_ErrorHeader = ((Node) nodeList.item(0))
								.getNodeValue();
					} else {
						response_ErrorHeader = "";
					}

					// ErrorCode
					// nodeList = nodeElement.getChildNodes();
					nodeList = doc.getElementsByTagName("ErrorCode");
					nodeElement = (Element) nodeList.item(0);

					if (nodeElement == null) {
						// No Errorcode
						response_ErrorCode = "Ingen svar fra server, pr�v igen senere.";
					} else {
						nodeList = nodeElement.getChildNodes();
						if (nodeList.getLength() > 0) {
							response_ErrorCode = ((Node) nodeList.item(0))
									.getNodeValue();
						} else {
							response_ErrorCode = "";
						}
					}
					nodeList = doc.getElementsByTagName("ErrorText");
					nodeElement = (Element) nodeList.item(0);
					nodeList = nodeElement.getChildNodes();
					if (nodeList.getLength() > 0) {
						response_ErrorText = ((Node) nodeList.item(0))
								.getNodeValue();
					} else {
						response_ErrorText = "";
					}
				}

				// No Errorcode - No Connection
				if (nodeElement == null) {
					response_ErrorText = "Ingen internetforbindelse - Pr�v igen senere.";
				}
			} catch (MalformedURLException e) {
				xmlList = "URL: is a malformed URL";
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				xmlList = "URL: UnsupportedEncodingException";
			} catch (ClientProtocolException e) {
				e.printStackTrace();
				xmlList = "URL: ClientProtocolException";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				xmlList = "URL: SocketTimeoutException";
			} catch (ConnectTimeoutException e) {
				e.printStackTrace();
				xmlList = "URL: ConnectTimeoutException";
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				xmlList = "URL: IOException";
				e.printStackTrace();
			} catch (Exception e) {
				return;
			}
			newData_Response = true;
		}

		@Override
		protected Void doInBackground(String... params) {
			receiveResponseFromServer(params[0]);
			return null;
		}

		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

		}

		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}
	}

	private class AsyncConnectionServiceAndImage extends
			AsyncTask<String, Void, Void> {

		protected void onPreExecute() {
			super.onPreExecute();
		}

		String xmlList = "";

		public void receiveResponseFromServer(String urlPath, String imagePath) {
			newData_Response = false;
			response_Status = "";
			response_ErrorCode = "";
			response_ErrorText = "";
			response_ErrorHeader = "";

			try {

				URL url = new URL(urlPath);

				String lineEnd = "\r\n";
				String twoHyphens = "--";
				String boundary = "*****";

				connection = (HttpURLConnection) url.openConnection();

				// Allow Inputs & Outputs
				connection.setDoInput(true);
				connection.setDoOutput(true);
				connection.setUseCaches(false);

				if (imagePath != null) {
					if (imagePath != "") {
						// Enable POST method
						connection.setRequestMethod("POST");

						connection.setRequestProperty("Connection",
								"Keep-Alive");
						connection.setRequestProperty("Content-Type",
								"multipart/form-data;boundary=" + boundary);

						DataOutputStream outputStream = new DataOutputStream(
								connection.getOutputStream());
						outputStream
								.writeBytes(twoHyphens + boundary + lineEnd);
						outputStream
								.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\""
										+ "temp.jpg" + "\"" + lineEnd);
						outputStream.writeBytes(lineEnd);

						ByteArrayOutputStream bos = new ByteArrayOutputStream();

						File file = new File(imagePath);

						byte[] buffer = new byte[(int) file.length()];
						try {
							new FileInputStream(file).read(buffer);
							outputStream.write(buffer, 0, buffer.length);

							outputStream.writeBytes(lineEnd);
							outputStream.writeBytes(twoHyphens + boundary
									+ twoHyphens + lineEnd);
						} catch (FileNotFoundException ex) {
							ex.printStackTrace();
							return;
						} catch (Exception ex) {
							ex.printStackTrace();
							return;
						}
					}
				}
// Response
//				DocumentBuilderFactory dbf = DocumentBuilderFactory
//						.newInstance();
//				DocumentBuilder db = dbf.newDocumentBuilder();
//
//				HttpGet httpGet = new HttpGet(urlPath);
//				HttpClient httpclient = new DefaultHttpClient();
//				HttpResponse response = httpclient.execute(httpGet);
//				Document doc = db.parse(new InputSource(response.getEntity()
//						.getContent()));
				Document doc = parseXML(connection.getInputStream());
				doc.getDocumentElement().normalize();

				// Status
				NodeList nodeList = doc.getElementsByTagName("Status");
				Element nodeElement = (Element) nodeList.item(0);

				nodeElement = (Element) nodeList.item(0);
				if (nodeElement != null) {
					response_Status = "OK";
				} else {

					// ErrorHeader
					nodeList = doc.getElementsByTagName("ErrorHeader");
					nodeElement = (Element) nodeList.item(0);
					nodeList = nodeElement.getChildNodes();
					if (nodeList.getLength() > 0) {
						response_ErrorHeader = ((Node) nodeList.item(0))
								.getNodeValue();
					} else {
						response_ErrorHeader = "";
					}

					// ErrorCode
					// nodeList = nodeElement.getChildNodes();
					nodeList = doc.getElementsByTagName("ErrorCode");
					nodeElement = (Element) nodeList.item(0);

					if (nodeElement == null) {
						// No Errorcode
						response_ErrorCode = "Ingen svar fra server, pr�v igen senere.";
					} else {
						nodeList = nodeElement.getChildNodes();
						if (nodeList.getLength() > 0) {
							response_ErrorCode = ((Node) nodeList.item(0))
									.getNodeValue();
						} else {
							response_ErrorCode = "";
						}
					}
					nodeList = doc.getElementsByTagName("ErrorText");
					nodeElement = (Element) nodeList.item(0);
					nodeList = nodeElement.getChildNodes();
					if (nodeList.getLength() > 0) {
						response_ErrorText = ((Node) nodeList.item(0))
								.getNodeValue();
					} else {
						response_ErrorText = "";
					}
				}

				// No Errorcode - No Connection
				if (nodeElement == null) {
					response_ErrorText = "Ingen internetforbindelse - Pr�v igen senere.";
				}
			} catch (MalformedURLException e) {
				xmlList = "URL: is a malformed URL";
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				xmlList = "URL: UnsupportedEncodingException";
			} catch (ClientProtocolException e) {
				e.printStackTrace();
				xmlList = "URL: ClientProtocolException";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				xmlList = "URL: SocketTimeoutException";
			} catch (ConnectTimeoutException e) {
				e.printStackTrace();
				xmlList = "URL: ConnectTimeoutException";
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				xmlList = "URL: IOException";
				e.printStackTrace();
			} catch (Exception e) {
				return;
			}
			newData_Response = true;
		}

		@Override
		protected Void doInBackground(String... params) {
			receiveResponseFromServer(params[0], params[1]);
			return null;
		}

		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

		}

		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}
	}

	public void createNewJob(SharedPreferences preferences, String newJobName,
			String orderNo, String comment, String begin, String end,
			String duration, String address, String lat, String lon,
			String priority, String users, String image) {
		newData_Jobs_CreateNewJob = false;
		connection = null;
		DataOutputStream outputStream = null;

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		try {
			String urlPath = url_Jobs_CreateNewJob
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#name", newJobName)
					.replaceAll("#orderno", orderNo)
					.replaceAll("#comment", comment)
					.replaceAll("#begin", begin).replaceAll("#end", end)
					.replaceAll("#duration", duration)
					.replaceAll("#address", address).replaceAll("#lat", lat)
					.replaceAll("#lon", lon).replaceAll("#priority", priority)
					.replaceAll("#users", users);
			String lineEnd = "\r\n";
			String twoHyphens = "--";
			String boundary = "*****";

			try {
				URL url = new URL(urlPath);
				connection = (HttpURLConnection) url.openConnection();

				// Allow Inputs & Outputs
				connection.setDoInput(true);
				connection.setDoOutput(true);
				connection.setUseCaches(false);

				if (!image.equals("")) {
					// Enable POST method
					connection.setRequestMethod("POST");

					connection.setRequestProperty("Connection", "Keep-Alive");
					connection.setRequestProperty("Content-Type",
							"multipart/form-data;boundary=" + boundary);

					outputStream = new DataOutputStream(
							connection.getOutputStream());
					outputStream.writeBytes(twoHyphens + boundary + lineEnd);
					outputStream
							.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\""
									+ "temp.jpg" + "\"" + lineEnd);
					outputStream.writeBytes(lineEnd);

					// Picture from Poi
					ByteArrayOutputStream bos = new ByteArrayOutputStream();

					File file = new File(image);

					byte[] buffer = new byte[(int) file.length()];
					try {
						new FileInputStream(file).read(buffer);
						outputStream.write(buffer, 0, buffer.length);

						outputStream.writeBytes(lineEnd);
						outputStream.writeBytes(twoHyphens + boundary
								+ twoHyphens + lineEnd);
					} catch (FileNotFoundException ex) {
						ex.printStackTrace();
						return;
					} catch (Exception ex) {
						ex.printStackTrace();
						return;
					}
				}

				// Response
				String Create_response_ErrorHeader = "";
				String Create_response_ErrorCode = "";
				String Create_response_ErrorText = "";
				try {
					Document doc = parseXML(connection.getInputStream());
					doc.getDocumentElement().normalize();

					NodeList nodeList = doc.getElementsByTagName("ErrorCode");

					Element nodeElement = (Element) nodeList.item(0);
					if (nodeElement == null) {
						// No connection
						Create_response_ErrorCode = "OK";
					} else {
						nodeList = nodeElement.getChildNodes();
						if (nodeList.getLength() > 0) {
							Create_response_ErrorCode = ((Node) nodeList
									.item(0)).getNodeValue();
						} else {
							Create_response_ErrorCode = "Ukendt fejl";
						}

						nodeList = doc.getElementsByTagName("ErrorHeader");
						nodeElement = (Element) nodeList.item(0);
						nodeList = nodeElement.getChildNodes();
						if (nodeList.getLength() > 0) {
							Create_response_ErrorHeader = ((Node) nodeList
									.item(0)).getNodeValue();
						} else {
							Create_response_ErrorHeader = "Ukendt fejl";
						}

						nodeList = doc.getElementsByTagName("ErrorCode");
						nodeElement = (Element) nodeList.item(0);
						nodeList = nodeElement.getChildNodes();
						if (nodeList.getLength() > 0) {
							Create_response_ErrorCode = ((Node) nodeList
									.item(0)).getNodeValue();
						} else {
							Create_response_ErrorCode = "Ukendt fejl";
						}

						nodeList = doc.getElementsByTagName("ErrorText");
						nodeElement = (Element) nodeList.item(0);
						nodeList = nodeElement.getChildNodes();
						if (nodeList.getLength() > 0) {
							Create_response_ErrorText = ((Node) nodeList
									.item(0)).getNodeValue();
						} else {
							Create_response_ErrorText = "Ukendt fejl";
						}

					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}

				if (Create_response_ErrorCode.equals("0")
						&& Create_response_ErrorText.equals("Job created.")) {

					// poiList.remove(storedPoi);
					//
					// String json = new Gson().toJson(poiList);
					// saveSetting("poiList", json);
					// // setRedDot();
					//
					// programConsistency = true;

				} else if (Create_response_ErrorHeader.equals("Error")) {

					// Global_response_ErrorHeader =
					// Create_response_ErrorHeader;
					// Global_response_ErrorText = Create_response_ErrorText;
					//
					// programConsistency = false;
					// Intent intent = new Intent(PoiRegister.this,
					// AppSettings.class);
					// startActivity(intent);
					// return -1;
				}

				if (outputStream != null) {
					outputStream.flush();
					outputStream.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Jobs_CreateNewJob = true;
	}

	public void createNewJobSimpleMethod(SharedPreferences preferences,
			String newJobName) {
		newData_Jobs_CreateNewJob = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Jobs_CreateNewJobSimpleMethod
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#name", newJobName);

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Jobs_CreateNewJob = true;
	}

	public void createNewJobImage(SharedPreferences preferences, String jobId) {
		newData_Jobs_CreateNewJob = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Jobs_CreateNewJobSimpleMethod
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobId);

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Jobs_CreateNewJob = true;
	}

	public void setJobTime(SharedPreferences preferences, String jobId,
			String begin, String end) {
		newData_Jobs_SetJobTime = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Jobs_SetJobTime
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobId).replaceAll("#begin", begin)
					.replaceAll("#end", end);

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Jobs_SetJobTime = true;
	}
	
	public void setJobDuration(SharedPreferences preferences, String jobId,
			String duration) {
		newData_Jobs_SetJobDuration = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Jobs_SetJobDuration
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobId)
					.replaceAll("#duration", duration);

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Jobs_SetJobDuration = true;
	}

//	public void setJobDurationAndImage(SharedPreferences preferences,
//			String jobId, String duration, String imagePath) {
//		newData_Jobs_SetJobDuration = false;
//		String[] params = new String[2];
//
//		// Get preferences
//		String Username = preferences.getString("Username", "");
//		String Password = preferences.getString("Password", "");
//		String Domain = preferences.getString("Domain", "skyhost.dk");
//		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);
//
//		connection = null;
//
//		try {
//			String urlPath = url_Jobs_SetJobDuration
//					.replaceAll("#username", Username)
//					.replaceAll("#domain", Domain)
//					.replaceAll("#password", Password)
//					.replaceAll("#jobid", jobId)
//					.replaceAll("#duration", duration);
//
//			// Save string to Array and execute url
//			params[0] = urlPath;
//			params[1] = imagePath;
//			// Intentional .get() do not remove, else false data will occur
//			new AsyncConnectionServiceAndImage().execute(params).get();
//
//		} catch (Exception e) {
//			return;
//		} finally {
//
//			if (connection != null) {
//				connection.disconnect();
//			}
//		}
//		newData_Jobs_SetJobDuration = true;
//	}

	public void setJobOrderNumber(SharedPreferences preferences, String jobId,
			String orderNo) {
		newData_Jobs_SetJobOrderNumber = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Jobs_SetJobOrderNumber
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobId)
					.replaceAll("#orderno", orderNo);

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Jobs_SetJobOrderNumber = true;
	}

	public void setJobTitle(SharedPreferences preferences, String jobId,
			String jobTitle) {
		newData_Jobs_SetJobTitle = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Jobs_SetJobTitle
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobId).replaceAll("#name", jobTitle);

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Jobs_SetJobTitle = true;
	}
	
	public void setJobAddFileToJob(SharedPreferences preferences, String jobId, String imagePath) {
		newData_Jobs_SetJobAddFileToJob = false;
		String[] params = new String[2];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Jobs_SetJobAddFileToJob
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobId);

			// Save string to Array and execute url
			params[0] = urlPath;
			params[1] = imagePath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionServiceAndImage().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Jobs_SetJobAddFileToJob = true;
	}

//	public void setJobOrderNumberAndImage(SharedPreferences preferences,
//			String jobId, String orderNo, String imagePath) {
//		newData_Jobs_SetJobOrderNumber = false;
//		String[] params = new String[2];
//
//		// Get preferences
//		String Username = preferences.getString("Username", "");
//		String Password = preferences.getString("Password", "");
//		String Domain = preferences.getString("Domain", "skyhost.dk");
//		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);
//
//		connection = null;
//
//		try {
//			String urlPath = url_Jobs_SetJobOrderNumber
//					.replaceAll("#username", Username)
//					.replaceAll("#domain", Domain)
//					.replaceAll("#password", Password)
//					.replaceAll("#jobid", jobId)
//					.replaceAll("#orderno", orderNo);
//
//			// Save string to Array and execute url
//			params[0] = urlPath;
//			params[1] = imagePath;
//			// Intentional .get() do not remove, else false data will occur
//			new AsyncConnectionServiceAndImage().execute(params).get();
//
//		} catch (Exception e) {
//			return;
//		} finally {
//
//			if (connection != null) {
//				connection.disconnect();
//			}
//		}
//		newData_Jobs_SetJobOrderNumber = true;
//	}

	public void setJobAddress(SharedPreferences preferences, String jobId,
			String address, String lat, String lon) {
		newData_Jobs_SetJobAddress = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Jobs_SetJobAddress
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobId)
					.replaceAll("#address", address).replaceAll("#lat", lat)
					.replaceAll("#lon", lon);

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Jobs_SetJobAddress = true;
	}

	public boolean getUsersOnAccount(SharedPreferences preferences) {
		newData_Jobs_GetUsersOnAccount = false;

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");

		try {
			String UrlPath = url_Jobs_UsersOnAccount
					.replaceAll("#domain", Domain)
					.replaceAll("#username", Username)
					.replaceAll("#password", Password);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			HttpGet httpGet = new HttpGet(UrlPath);
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httpGet);
			Document doc = db.parse(new InputSource(response.getEntity()
					.getContent()));

			doc.getDocumentElement().normalize();

			NodeList nodeList = doc.getElementsByTagName("ErrorCode");
			Element nodeElement = (Element) nodeList.item(0);
			if (nodeElement == null) {
				// No Errorcode
				response_ErrorCode = "OK";
			} else {
				nodeList = nodeElement.getChildNodes();
				if (nodeList.getLength() > 0) {

					response_ErrorCode = ((Node) nodeList.item(0))
							.getNodeValue();
					if (response_ErrorCode.equals("3")) { // Forkert brugernavn
															// og
						// kodeord
						response_ErrorText = "Forkert brugernavn og/eller kodeord";
					} else {
						response_ErrorText = "Ukendt fejl";
					}
				}
				return false;
			}

			nodeList = doc.getElementsByTagName("Ressouce");

			usersOnAccount_Ids = new String[nodeList.getLength()];
			usersOnAccount_Names = new String[nodeList.getLength()];

			int o = nodeList.getLength();
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				NodeList nList;
				// Element nodeElement = null;

				Element fstElmnt = (Element) node;

				nList = fstElmnt.getElementsByTagName("ID");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					usersOnAccount_Ids[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					usersOnAccount_Ids[i] = "";
				}

				nList = fstElmnt.getElementsByTagName("Name");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					usersOnAccount_Names[i] = ((Node) nList.item(0))
							.getNodeValue();
				} else {
					usersOnAccount_Names[i] = "";
				}
			}
		} catch (Exception e) {
			return false;
		}
		newData_Jobs_GetUsersOnAccount = true;
		return true;
	}

	public void setJobUsers(SharedPreferences preferences, String jobId,
			String users) {
		newData_Jobs_SetJobUsers = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Jobs_SetJobUsers
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobId)
					.replaceAll("#ressourceid", users);

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Jobs_SetJobUsers = true;
	}

	public void setJobPriority(SharedPreferences preferences, String jobId,
			String priority) {
		newData_Jobs_SetJobPriority = false;
		String[] params = new String[1];

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "skyhost.dk");
		newData_AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		connection = null;

		try {
			String urlPath = url_Jobs_SetJobPriority
					.replaceAll("#username", Username)
					.replaceAll("#domain", Domain)
					.replaceAll("#password", Password)
					.replaceAll("#jobid", jobId)
					.replaceAll("#priority", priority);

			// Save string to Array and execute url
			params[0] = urlPath;
			// Intentional .get() do not remove, else false data will occur
			new AsyncConnectionService().execute(params).get();

		} catch (Exception e) {
			return;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
		newData_Jobs_SetJobPriority = true;
	}

	public void storeImages() {
		jobs_drawables = new Drawable[jobs_icon.length];
		for (int i = 0; i < jobs_icon.length; i++) {
			try {
				Drawable drawable = drawableFromUrl(jobs_icon[i]);
				jobs_drawables[i] = drawable;
			} catch (Exception e) {
			}
		}
	}

	private static Document parseXML(InputStream stream) throws Exception {
		DocumentBuilderFactory objDocumentBuilderFactory = null;
		DocumentBuilder objDocumentBuilder = null;
		Document doc = null;
		try {
			objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
			objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

			doc = objDocumentBuilder.parse(stream);
		} catch (Exception ex) {
			throw ex;
		}
		return doc;
	}

	public Drawable drawableFromUrl(String url) throws IOException {
		newData_Drawable_From_Url = false;
		Bitmap x;
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL(url)
					.openConnection();
			connection.connect();
			InputStream input = connection.getInputStream();

			x = BitmapFactory.decodeStream(input);
		} catch (Exception e) {
			return null;
		}
		newData_Drawable_From_Url = true;
		return new BitmapDrawable(x);
	}
	
	public InputStream bitmapFromUrl(String address)
			throws MalformedURLException, IOException {
		newData_Bitmap_From_Url = false;
		InputStream instream = null;
		try {
			HttpGet httpRequest = new HttpGet(URI.create(address));
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = (HttpResponse) httpclient
					.execute(httpRequest);
			HttpEntity entity = response.getEntity();
			BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(entity);
			instream = bufHttpEntity.getContent();
		} catch (Exception e) {
			return null;
		}
		newData_Bitmap_From_Url = true;
		return instream;
	}

	// ------------------------- set -------------------------------

	public void setNewData_Jobs(boolean newData_Jobs) {
		this.newData_Jobs = newData_Jobs;
	}

	public void setJobs_title(String[] jobs_title) {
		jobs_title = jobs_title;
	}

	// ------------------------- get & set -------------------------------
	public String getResponse_ErrorCode() {
		return response_ErrorCode;
	}

	public String getResponse_ErrorText() {
		return response_ErrorText;
	}

	public String getResponse_Status() {
		return response_Status;
	}

	public boolean isAutoUpdate() {
		return newData_AutoUpdate;
	}

	public String[] getJobs_title() {
		return jobs_title;
	}

	public boolean isNewData_Bitmap_From_Url() {
		return newData_Bitmap_From_Url;
	}

	public boolean isNewData_Drawable_From_Url() {
		return newData_Drawable_From_Url;
	}
	
	public boolean isNewData_Jobs() {
		return newData_Jobs;
	}

	public boolean isNewData_LinkedStatus() {
		return newData_LinkedStatus;
	}

	public boolean isNewData_Status() {
		return newData_Status;
	}

	public boolean isNewData_Response() {
		return newData_Response;
	}

	public boolean isNewData_Note() {
		return newData_Note;
	}

	public boolean isNewData_Timer() {
		return newData_Timer;
	}

	public boolean isNewData_TimerNewNote() {
		return newData_TimerNewNote;
	}

	public boolean isNewData_Timer_SelectJobsOnDateUrl() {
		return newData_Timer_SelectJobsOnDateUrl;
	}

	public boolean isNewData_Timer_TimeAccounts() {
		return newData_Timer_TimeAccounts;
	}

	public boolean isNewDate_DeleteData_TimerNewNote() {
		return newDate_DeleteData_TimerNewNote;
	}

	public boolean isNewData_SaveTimeAndTripsOnJobUrl_Timer() {
		return newData_SaveTimeAndTripsOnJobUrl_Timer;
	}

	public boolean isNewData_Jobs_GetMaterials() {
		return newData_Jobs_GetMaterials;
	}

	public boolean isNewData_Jobs_AddMaterialToJob() {
		return newData_Jobs_AddMaterialToJob;
	}

	public boolean isNewData_Jobs_MaterialsOnJob() {
		return newData_Jobs_MaterialsOnJob;
	}

	public boolean isNewData_Jobs_DeleteMaterial() {
		return newData_Jobs_DeleteMaterial;
	}

	public boolean isNewData_Jobs_CreateNewJob() {
		return newData_Jobs_CreateNewJob;
	}

	public boolean isNewData_Jobs_SetJobTime() {
		return newData_Jobs_SetJobTime;
	}

	public boolean isNewData_Jobs_SetJobDuration() {
		return newData_Jobs_SetJobDuration;
	}

	public boolean isNewData_Jobs_SetJobOrderNumber() {
		return newData_Jobs_SetJobOrderNumber;
	}
	
	public boolean isNewData_Jobs_SetJobTitle() {
		return newData_Jobs_SetJobTitle;
	}
	
	public boolean isNewData_Jobs_SetJobAddFileToJob() {
		return newData_Jobs_SetJobAddFileToJob;
	}

	public boolean isNewData_Jobs_SetJobAddress() {
		return newData_Jobs_SetJobAddress;
	}

	public boolean isNewData_Jobs_SetJobUsers() {
		return newData_Jobs_SetJobUsers;
	}

	public boolean isNewData_Jobs_SetJobPriority() {
		return newData_Jobs_SetJobPriority;
	}

	public boolean isNewData_Jobs_GetUsersOnAccount() {
		return newData_Jobs_GetUsersOnAccount;
	}

	public String getUrl_Jobs_GetMaterials() {
		return url_Jobs_GetMaterials;
	}

	public String getSelectTimeUrl_Timer() {
		return url_Timer_SaveTimeAndTripsOnJob;
	}

	public String[] getTimer_JobsOnDay() {
		return timer_JobsOnDay;
	}

	public String[] getTimer_OrdernumbersOnDay() {
		return timer_OrdernumbersOnDay;
	}

	public String[] getJobs_orderNo() {
		return jobs_orderNo;
	}

	public String[] getJobs_priority() {
		return jobs_priority;
	}

	public String[] getJobs_linked() {
		return jobs_linked;
	}

	public String[] getJobs_location() {
		return jobs_location;
	}

	public String[] getJobs_createdBy() {
		return jobs_createdBy;
	}

	public String[] getJobs_createdAt() {
		return jobs_createdAt;
	}

	public String[] getJobs_status() {
		return jobs_status;
	}

	public String[] getJobs_icon() {
		return jobs_icon;
	}

	public String[] getJobs_beginTime() {
		return jobs_beginTime;
	}

	public String[] getJobs_endTime() {
		return jobs_endTime;
	}

	public String[] getJobs_duration() {
		return jobs_duration;
	}

	public String[][] getJobs_address_number() {
		return jobs_addresses_number;
	}

	public String[][] getJobs_address_street() {
		return jobs_addresses_street;
	}

	public String[][] getJobs_address_zip() {
		return jobs_addresses_zip;
	}

	public String[][] getJobs_address_lat() {
		return jobs_addresses_lat;
	}

	public String[][] getJobs_address_lon() {
		return jobs_addresses_lon;
	}

	public String[][] getJobs_employees_name() {
		return jobs_employees_name;
	}

	public String[][] getJobs_employees_phone() {
		return jobs_employees_phone;
	}

	public String[][] getJobs_employees_email() {
		return jobs_employees_email;
	}

	public String[][] getJobs_notes_names() {
		return jobs_notes_names;
	}

	public String[][] getJobs_notes_texts() {
		return jobs_notes_texts;
	}

	public String[][] getJobs_notes_timestamps() {
		return jobs_notes_timestamps;
	}

	public String[] getJobs_timeRegMinutes() {
		return jobs_timeRegMinutes;
	}

	public Drawable[] getJobs_drawables() {
		return jobs_drawables;
	}

	public Date getTimestampJobs() {
		return timestampJobs;
	}

	public Date getTimestampTimer() {
		return timestampTimer;
	}

	public String[] getTimer_WeeksNo() {
		return timer_WeeksNo;
	}

	public String[][] getTimer_Days() {
		return timer_Days;
	}

	public String[] getTimer_Dates() {
		return timer_Dates;
	}

	public String[][] getTimer_RegSums() {
		return timer_RegSums;
	}

	public String[][][] getTimer_Jobs() {
		return timer_Jobs;
	}

	public String[][][] getTimer_RecIds() {
		return timer_RecIds;
	}

	public String[][][] getTimer_JobIDs() {
		return timer_JobIDs;
	}

	public String[][][] getTimer_OrderNos() {
		return timer_OrderNos;
	}

	public String[][][] getTimer_JobName() {
		return timer_JobName;
	}

	public String[][][] getTimer_AccountName() {
		return timer_AccountName;
	}

	public String[][][] getTimer_Minutes() {
		return timer_Minutes;
	}

	public String[][][] getTimer_Headers() {
		return timer_Headers;
	}

	public String[][][] getTimer_SubTitles() {
		return timer_SubTitles;
	}

	public String getResponse_ErrorHeader() {
		return response_ErrorHeader;
	}

	public String[] getTimer_JobIdsOnDay() {
		return timer_JobIdsOnDay;
	}

	public ArrayList<String> getTimer_TimeAccount_Number() {
		return timer_TimeAccount_Number;
	}

	public ArrayList<String> getTimer_TimeAccount_Name() {
		return timer_TimeAccount_Name;
	}

	public boolean isNewData_Timer_RoutesOnDay() {
		return newData_Timer_TripsOnDay;
	}

	public String[] getTimer_Route_Id() {
		return timer_Route_Id;
	}

	public String[] getTimer_Route_Header() {
		return timer_Route_Header;
	}

	public String[] getTimer_Route_Subtitle() {
		return timer_Route_Subtitle;
	}

	public String[] getTimer_Route_OrderNo() {
		return timer_Route_OrderNo;
	}

	public String[] getTimer_Route_StartTime() {
		return timer_Route_StartTime;
	}

	public String[] getTimer_Route_StopTime() {
		return timer_Route_StopTime;
	}

	public String[] getTimer_Route_DriveDuration() {
		return timer_Route_DriveDuration;
	}

	public String[] getTimer_Route_StopDuration() {
		return timer_Route_StopDuration;
	}

	public String[] getTimer_Route_Zipcode() {
		return timer_Route_Zipcode;
	}

	public String[] getTimer_Route_City() {
		return timer_Route_City;
	}

	public String[] getTimer_Route_Street() {
		return timer_Route_Street;
	}

	public String[] getTimer_Route_Lat() {
		return timer_Route_Lat;
	}

	public String[] getTimer_Route_Lon() {
		return timer_Route_Lon;
	}

	public long gettimestampTimerRoutesElapsedTime() {
		return timestampTimerRoutesElapsedTime;
	}

	public List getJobs_jobId() {
		return jobs_jobId;
	}

	public String getResponse_Command() {
		return response_Command;
	}

	public Date getTimestampMaterials() {
		return timestampMaterials;
	}

	public String[] getJobs_Material_MatID() {
		return jobs_Material_MatID;
	}

	public String[] getJobs_Material_Name() {
		return jobs_Material_Name;
	}

	public String[] getJobs_Material_Description() {
		return jobs_Material_Description;
	}

	public String[] getJobs_Material_Colli() {
		return jobs_Material_Colli;
	}

	public String[] getJobs_Material_GroupNo() {
		return jobs_Material_GroupNo;
	}

	public String[] getJobs_Material_SupplierItemNo() {
		return jobs_Material_SupplierItemNo;
	}

	public String[] getJobs_Material_EanNo() {
		return jobs_Material_EanNo;
	}

	public String[] getJobs_Material_EanBarcode() {
		return jobs_Material_EanBarcode;
	}

	public String[] getJobs_Material_InternalBarcode() {
		return jobs_Material_InternalBarcode;
	}

	public String[] getJobs_Material_InternalItemNo() {
		return jobs_Material_InternalItemNo;
	}

	public String[] getJobs_Material_InternalItemShortNo() {
		return jobs_Material_InternalItemShortNo;
	}

	public String[] getJobs_MaterialsOnJob_MatID() {
		return jobs_MaterialsOnJob_MatID;
	}

	public String[] getJobs_MaterialsOnJob_Timestamp() {
		return jobs_MaterialsOnJob_Timestamp;
	}

	public String[] getJobs_MaterialsOnJob_InternalItemNo() {
		return jobs_MaterialsOnJob_InternalItemNo;
	}

	public String[] getJobs_MaterialsOnJob_ItemName() {
		return jobs_MaterialsOnJob_ItemName;
	}

	public String[] getJobs_MaterialsOnJob_Psc() {
		return jobs_MaterialsOnJob_Pcs;
	}

	public String[] getJobs_Material_Reference() {
		return jobs_Material_Reference;
	}

	public String[] getJobs_Material_GroupName() {
		return jobs_Material_GroupName;
	}

	public String[] getJobs_Material_ElNo() {
		return jobs_Material_ElNo;
	}

	public String[] getJobs_MaterialsOnJob_User() {
		return jobs_MaterialsOnJob_User;
	}

	public String[] getJobs_MaterialsOnJob_ID() {
		return jobs_MaterialsOnJob_ID;
	}

	public void setResponse_ErrorText(String response_ErrorText) {
		this.response_ErrorText = response_ErrorText;
	}

	public String[][] getJobs_files_titles() {
		return jobs_files_titles;
	}

	public String[][] getJobs_files_paths() {
		return jobs_files_paths;
	}

	public String[] getUsersOnAccount_Ids() {
		return usersOnAccount_Ids;
	}

	public String[] getUsersOnAccount_Names() {
		return usersOnAccount_Names;
	}

	public String[] getUsersOnAccount_Phones() {
		return usersOnAccount_Phones;
	}

	public String[] getUsersOnAccount_Emails() {
		return usersOnAccount_Emails;
	}

}